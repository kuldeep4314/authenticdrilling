//
//  ProjectCell.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-24.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit

class ProjectCell: UITableViewCell {

    @IBOutlet weak var txtHoleValue: UITextField!
    @IBOutlet weak var txtHoleTitle: UITextField!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var imgRight: UIImageView!
    
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var bgView1: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if bgView != nil{
            
            self.bgView.addshodowToView()
        }
        if bgView1 != nil{
            
            self.bgView1.addshodowToView()
        }
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
