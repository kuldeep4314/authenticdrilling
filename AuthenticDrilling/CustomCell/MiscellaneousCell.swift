//
//  MiscellaneousCell.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-25.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit

class MiscellaneousCell: UITableViewCell {

    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if bgView != nil {
            bgView.addshodowToView()
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
