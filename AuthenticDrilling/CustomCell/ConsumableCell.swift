//
//  ConsumableCell.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-25.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit

class ConsumableCell: UITableViewCell {

    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var txtQty: UITextField!
    @IBOutlet weak var lblUnit: UITextField!
    @IBOutlet weak var txtType: UITextField!
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        if bgView != nil {
            bgView.addshodowToView()
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
