//
//  SupplyCell.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-12-01.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit

class SupplyCell: UITableViewCell {

    
    @IBOutlet weak var txtUnitTitle: UITextField!
    @IBOutlet weak var txtQtyTitle: UITextField!
    @IBOutlet weak var txtQtyHeading: UITextField!
    @IBOutlet weak var miscellTxtView: UITextView!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var txtValue: UITextField!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var txtUnit: UITextField!
    @IBOutlet weak var txtQty: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        if txtUnitTitle != nil{
           
            txtUnitTitle.addshodowToTextField()
            txtUnitTitle.leftview()
        }
        if txtQtyHeading != nil {
            
            txtQtyHeading.addshodowToTextField()
            txtQtyHeading.leftview()
            
        }
        
        if bgView != nil {
            bgView.addshodowToView()
            
        }
        if txtName != nil{
            txtName.leftview()
        }
        if txtValue != nil{
            txtValue.rightVC()
            txtValue.addshodowToTextField()
        }
        
        if txtView != nil {
            
            txtView.addshodowToTextView()
            
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   
}
