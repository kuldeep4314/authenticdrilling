//
//  AddConsumablesTableViewCell.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2018-01-17.
//  Copyright © 2018 Avatar Singh. All rights reserved.
//

import UIKit

class AddConsumablesTableViewCell: UITableViewCell {

    @IBOutlet weak var lblRiser: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var lblTitle2: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var lblTitle1: UILabel!
    @IBOutlet weak var imgView1: UIImageView!
    @IBOutlet weak var lblNotes: UILabel!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var bgView1: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
