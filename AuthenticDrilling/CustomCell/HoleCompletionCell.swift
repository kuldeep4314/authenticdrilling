//
//  HoleCompletionCell.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-12-01.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit

class HoleCompletionCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var headerBgView: UIView!
    @IBOutlet weak var btnHeaderTick: UIButton!

    @IBOutlet weak var txtValue: UITextField!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var lblHeader: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
}
