//
//  ProjectInfoCell.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-30.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit

class ProjectInfoCell: UITableViewCell {

    @IBOutlet weak var lblHours: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if bgView != nil {
            bgView.addshodowToView()
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
