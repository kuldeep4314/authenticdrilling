//
//  DrillCell.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-29.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit

class DrillCell: UITableViewCell {

    @IBOutlet weak var txtOptionValue: UITextField!
    @IBOutlet weak var txtOptionTitle: UITextField!
    @IBOutlet weak var txtValue: UITextField!
    @IBOutlet weak var txtTilte: UITextField!
    @IBOutlet weak var bgView1: UIView!
    @IBOutlet weak var txtNumberValue: UITextField!
    @IBOutlet weak var txtTypeValue: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var txtType: UITextField!
    @IBOutlet weak var txtNumber: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lblHeader: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        if bgView != nil {
            
            bgView.addshodowToView()
        }
        if bgView1 != nil {
            
            bgView1.addshodowToView()
        }
        
        if txtOptionTitle != nil {
            
            txtOptionTitle.addshodowToTextField()
            txtOptionValue.addshodowToTextField()
        }
       
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
