//
//  RentalCell.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-28.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit



class RentalCell: UITableViewCell{

    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var txtName: UITextField!
    
    @IBOutlet weak var save: UIButton!
    
    @IBOutlet weak var txtTitle: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        if bgView != nil {
            bgView.addshodowToView()
        }
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

  
}
