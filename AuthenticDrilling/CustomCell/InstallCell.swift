//
//  InstallCell.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-29.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit

class InstallCell: UITableViewCell {


    @IBOutlet weak var btnAddMinus: UIButton!
    @IBOutlet weak var txtPieces: UITextField!
    @IBOutlet weak var txtTilte: UITextField!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var txtValue: UITextField!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var bgView1: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        if txtTilte != nil {
            txtTilte.addshodowToTextField()
             txtTilte.leftview()
        }
        
        if txtValue != nil{
            txtValue.addshodowToTextField()
            txtValue.leftview()
        }
        if txtName != nil {
         txtName.addshodowToTextField()
            txtName.leftview()
            
        }
       

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
