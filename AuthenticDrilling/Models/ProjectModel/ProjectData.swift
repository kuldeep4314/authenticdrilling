//
//  ProjectData.swift
//
//  Created by goyal on 08/02/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ProjectData: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kProjectDataClientRepKey: String = "client_rep"
  private let kProjectDataClientJobNumberKey: String = "client_job_number"
  private let kProjectDataSupportTruck2Key: String = "support_truck2"
  private let kProjectDataPidKey: String = "pid"
  private let kProjectDataCityNameKey: String = "city_name"
  private let kProjectDataCrewHelpersKey: String = "crew_helpers"
  private let kProjectDataRigKey: String = "rig"
  private let kProjectDataCrewHelpers2Key: String = "crew_helpers2"
  private let kProjectDataADIJobNumberKey: String = "ADI_job_number"
  private let kProjectDataSiteAddressKey: String = "site_address"
  private let kProjectDataTrailer1Key: String = "trailer1"
  private let kProjectDataProjectNameKey: String = "project_name"
  private let kProjectDataSupportTruk1Key: String = "support_truk1"
  private let kProjectDataAddOnKey: String = "add_on"
  private let kProjectDataModifyOnKey: String = "modify_on"
  private let kProjectDataClientNameKey: String = "client_name"
  private let kProjectDataPerdiemsKey: String = "perdiems"
  private let kProjectDataUseridKey: String = "userid"
  private let kProjectDataInjuriesKey: String = "injuries"
  private let kProjectDataCrewDrillerNameKey: String = "crew_driller_name"
  private let kProjectDataStartDateKey: String = "start_date"
  private let kProjectDataTrailer2Key: String = "trailer2"
 private let kProjectDataisOfflineKey: String = "isOffline"
    private let kProjectDataisEditKey: String = "edit"
     private let kProjectDataisLocalAddEditKey: String = "isLocalAddEdit"
      private let kProjectDataisState: String = "state"
  // MARK: Properties
  public var clientRep: String?
  public var clientJobNumber: String?
  public var supportTruck2: String?
  public var pid: String?
  public var cityName: String?
  public var crewHelpers: String?
  public var rig: String?
  public var crewHelpers2: String?
  public var aDIJobNumber: String?
  public var siteAddress: String?
  public var trailer1: String?
  public var projectName: String?
  public var supportTruk1: String?
  public var addOn: String?
  public var modifyOn: String?
  public var clientName: String?
  public var perdiems: String?
  public var userid: String?
  public var injuries: String?
  public var crewDrillerName: String?
  public var startDate: String?
  public var trailer2: String?
    public var state: String?
    public var isOffline: Bool?
  public var edit: Bool?
public var isLocalAddEdit: Bool?
    
  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    clientRep = json[kProjectDataClientRepKey].string
    clientJobNumber = json[kProjectDataClientJobNumberKey].string
    supportTruck2 = json[kProjectDataSupportTruck2Key].string
    pid = json[kProjectDataPidKey].string
    cityName = json[kProjectDataCityNameKey].string
    crewHelpers = json[kProjectDataCrewHelpersKey].string
    rig = json[kProjectDataRigKey].string
    crewHelpers2 = json[kProjectDataCrewHelpers2Key].string
    aDIJobNumber = json[kProjectDataADIJobNumberKey].string
    siteAddress = json[kProjectDataSiteAddressKey].string
    trailer1 = json[kProjectDataTrailer1Key].string
    projectName = json[kProjectDataProjectNameKey].string
    supportTruk1 = json[kProjectDataSupportTruk1Key].string
    addOn = json[kProjectDataAddOnKey].string
    modifyOn = json[kProjectDataModifyOnKey].string
    clientName = json[kProjectDataClientNameKey].string
    perdiems = json[kProjectDataPerdiemsKey].string
    userid = json[kProjectDataUseridKey].string
    injuries = json[kProjectDataInjuriesKey].string
    crewDrillerName = json[kProjectDataCrewDrillerNameKey].string
    startDate = json[kProjectDataStartDateKey].string
    trailer2 = json[kProjectDataTrailer2Key].string
      isOffline = json[kProjectDataisOfflineKey].bool
    edit = json[kProjectDataisEditKey].bool
isLocalAddEdit =  json[kProjectDataisLocalAddEditKey].bool
    state = json[kProjectDataisState].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = clientRep { dictionary[kProjectDataClientRepKey] = value }
    if let value = clientJobNumber { dictionary[kProjectDataClientJobNumberKey] = value }
    if let value = supportTruck2 { dictionary[kProjectDataSupportTruck2Key] = value }
    if let value = pid { dictionary[kProjectDataPidKey] = value }
    if let value = cityName { dictionary[kProjectDataCityNameKey] = value }
    if let value = crewHelpers { dictionary[kProjectDataCrewHelpersKey] = value }
    if let value = rig { dictionary[kProjectDataRigKey] = value }
    if let value = crewHelpers2 { dictionary[kProjectDataCrewHelpers2Key] = value }
    if let value = aDIJobNumber { dictionary[kProjectDataADIJobNumberKey] = value }
    if let value = siteAddress { dictionary[kProjectDataSiteAddressKey] = value }
    if let value = trailer1 { dictionary[kProjectDataTrailer1Key] = value }
    if let value = projectName { dictionary[kProjectDataProjectNameKey] = value }
    if let value = supportTruk1 { dictionary[kProjectDataSupportTruk1Key] = value }
    if let value = addOn { dictionary[kProjectDataAddOnKey] = value }
    if let value = modifyOn { dictionary[kProjectDataModifyOnKey] = value }
    if let value = clientName { dictionary[kProjectDataClientNameKey] = value }
    if let value = perdiems { dictionary[kProjectDataPerdiemsKey] = value }
    if let value = userid { dictionary[kProjectDataUseridKey] = value }
    if let value = injuries { dictionary[kProjectDataInjuriesKey] = value }
    if let value = crewDrillerName { dictionary[kProjectDataCrewDrillerNameKey] = value }
    if let value = startDate { dictionary[kProjectDataStartDateKey] = value }
    if let value = trailer2 { dictionary[kProjectDataTrailer2Key] = value }
     if let value = isOffline{ dictionary[kProjectDataisOfflineKey] = value }
    if let value = edit{ dictionary[kProjectDataisEditKey] = value }
    if let value = isLocalAddEdit{ dictionary[kProjectDataisLocalAddEditKey] = value }
      if let value = state{ dictionary[kProjectDataisState] = value }
    
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.clientRep = aDecoder.decodeObject(forKey: kProjectDataClientRepKey) as? String
    self.clientJobNumber = aDecoder.decodeObject(forKey: kProjectDataClientJobNumberKey) as? String
    self.supportTruck2 = aDecoder.decodeObject(forKey: kProjectDataSupportTruck2Key) as? String
    self.pid = aDecoder.decodeObject(forKey: kProjectDataPidKey) as? String
    self.cityName = aDecoder.decodeObject(forKey: kProjectDataCityNameKey) as? String
    self.crewHelpers = aDecoder.decodeObject(forKey: kProjectDataCrewHelpersKey) as? String
    self.rig = aDecoder.decodeObject(forKey: kProjectDataRigKey) as? String
    self.crewHelpers2 = aDecoder.decodeObject(forKey: kProjectDataCrewHelpers2Key) as? String
    self.aDIJobNumber = aDecoder.decodeObject(forKey: kProjectDataADIJobNumberKey) as? String
    self.siteAddress = aDecoder.decodeObject(forKey: kProjectDataSiteAddressKey) as? String
    self.trailer1 = aDecoder.decodeObject(forKey: kProjectDataTrailer1Key) as? String
    self.projectName = aDecoder.decodeObject(forKey: kProjectDataProjectNameKey) as? String
    self.supportTruk1 = aDecoder.decodeObject(forKey: kProjectDataSupportTruk1Key) as? String
    self.addOn = aDecoder.decodeObject(forKey: kProjectDataAddOnKey) as? String
    self.modifyOn = aDecoder.decodeObject(forKey: kProjectDataModifyOnKey) as? String
    self.clientName = aDecoder.decodeObject(forKey: kProjectDataClientNameKey) as? String
    self.perdiems = aDecoder.decodeObject(forKey: kProjectDataPerdiemsKey) as? String
    self.userid = aDecoder.decodeObject(forKey: kProjectDataUseridKey) as? String
    self.injuries = aDecoder.decodeObject(forKey: kProjectDataInjuriesKey) as? String
    self.crewDrillerName = aDecoder.decodeObject(forKey: kProjectDataCrewDrillerNameKey) as? String
    self.startDate = aDecoder.decodeObject(forKey: kProjectDataStartDateKey) as? String
    self.trailer2 = aDecoder.decodeObject(forKey: kProjectDataTrailer2Key) as? String
    self.isOffline = aDecoder.decodeObject(forKey: kProjectDataisOfflineKey) as? Bool
     self.edit = aDecoder.decodeObject(forKey: kProjectDataisEditKey) as? Bool
    self.isLocalAddEdit = aDecoder.decodeObject(forKey: kProjectDataisLocalAddEditKey) as? Bool
    self.state = aDecoder.decodeObject(forKey: kProjectDataisState) as? String
    
    
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(clientRep, forKey: kProjectDataClientRepKey)
    aCoder.encode(clientJobNumber, forKey: kProjectDataClientJobNumberKey)
    aCoder.encode(supportTruck2, forKey: kProjectDataSupportTruck2Key)
    aCoder.encode(pid, forKey: kProjectDataPidKey)
    aCoder.encode(cityName, forKey: kProjectDataCityNameKey)
    aCoder.encode(crewHelpers, forKey: kProjectDataCrewHelpersKey)
    aCoder.encode(rig, forKey: kProjectDataRigKey)
    aCoder.encode(crewHelpers2, forKey: kProjectDataCrewHelpers2Key)
    aCoder.encode(aDIJobNumber, forKey: kProjectDataADIJobNumberKey)
    aCoder.encode(siteAddress, forKey: kProjectDataSiteAddressKey)
    aCoder.encode(trailer1, forKey: kProjectDataTrailer1Key)
    aCoder.encode(projectName, forKey: kProjectDataProjectNameKey)
    aCoder.encode(supportTruk1, forKey: kProjectDataSupportTruk1Key)
    aCoder.encode(addOn, forKey: kProjectDataAddOnKey)
    aCoder.encode(modifyOn, forKey: kProjectDataModifyOnKey)
    aCoder.encode(clientName, forKey: kProjectDataClientNameKey)
    aCoder.encode(perdiems, forKey: kProjectDataPerdiemsKey)
    aCoder.encode(userid, forKey: kProjectDataUseridKey)
    aCoder.encode(injuries, forKey: kProjectDataInjuriesKey)
    aCoder.encode(crewDrillerName, forKey: kProjectDataCrewDrillerNameKey)
    aCoder.encode(startDate, forKey: kProjectDataStartDateKey)
    aCoder.encode(trailer2, forKey: kProjectDataTrailer2Key)
    aCoder.encode(isOffline, forKey: kProjectDataisOfflineKey)
     aCoder.encode(edit, forKey: kProjectDataisEditKey)
     aCoder.encode(isLocalAddEdit, forKey: kProjectDataisLocalAddEditKey)
       aCoder.encode(state, forKey: kProjectDataisState)
    
  }

}
