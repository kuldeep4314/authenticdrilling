//
//  ProjectResponse.swift
//
//  Created by goyal on 08/02/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ProjectResponse: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kProjectResponseResultKey: String = "result"
  private let kProjectResponseDataKey: String = "data"
  private let kProjectResponseMsgKey: String = "msg"

  // MARK: Properties
  public var result: String?
  public var data: [ProjectData]?
  public var msg: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    result = json[kProjectResponseResultKey].string
    if let items = json[kProjectResponseDataKey].array { data = items.map { ProjectData(json: $0) } }
    msg = json[kProjectResponseMsgKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = result { dictionary[kProjectResponseResultKey] = value }
    if let value = data { dictionary[kProjectResponseDataKey] = value.map { $0.dictionaryRepresentation() } }
    if let value = msg { dictionary[kProjectResponseMsgKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.result = aDecoder.decodeObject(forKey: kProjectResponseResultKey) as? String
    self.data = aDecoder.decodeObject(forKey: kProjectResponseDataKey) as? [ProjectData]
    self.msg = aDecoder.decodeObject(forKey: kProjectResponseMsgKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(result, forKey: kProjectResponseResultKey)
    aCoder.encode(data, forKey: kProjectResponseDataKey)
    aCoder.encode(msg, forKey: kProjectResponseMsgKey)
  }

}
