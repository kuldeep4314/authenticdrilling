//
//  ADRentals.swift
//
//  Created by Avatar Singh on 2017-12-12
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ADRentals: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kADRentalsResponseKey: String = "response"

  // MARK: Properties
  public var response: ADResponse?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    response = ADResponse(json: json[kADRentalsResponseKey])
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = response { dictionary[kADRentalsResponseKey] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.response = aDecoder.decodeObject(forKey: kADRentalsResponseKey) as? ADResponse
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(response, forKey: kADRentalsResponseKey)
  }

}
