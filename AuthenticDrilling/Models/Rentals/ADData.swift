//
//  ADData.swift
//
//  Created by Avatar Singh on 2017-12-12
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ADData: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kADDataPumpNotetypeKey: String = "pump_notetype"
  private let kADDataSteamCleanerKey: String = "steam_cleaner"
  private let kADDataSupportTruck2Key: String = "support_truck2"
  private let kADDataJackhammerKey: String = "jackhammer"
  private let kADDataNotesKey: String = "notes"
  private let kADDataDateKey: String = "date"
  private let kADDataSupportTruck1Key: String = "support_truck1"
  private let kADDataLightPlantKey: String = "light_plant"
  private let kADDataTrackorKey: String = "trackor"
  private let kADDataConcreteSawKey: String = "concrete_saw"
  private let kADDataAirCompressorKey: String = "air_compressor"
  private let kADDataDumpTrailerKey: String = "dump_trailer"
  private let kADDataGeneratorKey: String = "generator"
  private let kADDataRidKey: String = "rid"
  private let kADDataConcreteCoreKey: String = "concrete_core"
  private let kADDataTrashPumpKey: String = "trash_pump"
  private let kADDataDateTimeKey: String = "date_time"
  private let kADDataUseridKey: String = "userid"
  private let kADDataProjectidKey: String = "projectid"
  private let kADDataSkidsteerKey: String = "skidsteer"
  private let kADDataOtherKey: String = "other"

  // MARK: Properties
  public var pumpNotetype: String?
  public var steamCleaner: String?
  public var supportTruck2: String?
  public var jackhammer: String?
  public var notes: String?
  public var date: String?
  public var supportTruck1: String?
  public var lightPlant: String?
  public var trackor: String?
  public var concreteSaw: String?
  public var airCompressor: String?
  public var dumpTrailer: String?
  public var generator: String?
  public var rid: String?
  public var concreteCore: String?
  public var trashPump: String?
  public var dateTime: String?
  public var userid: String?
  public var projectid: String?
  public var skidsteer: String?
  public var other: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    pumpNotetype = json[kADDataPumpNotetypeKey].string
    steamCleaner = json[kADDataSteamCleanerKey].string
    supportTruck2 = json[kADDataSupportTruck2Key].string
    jackhammer = json[kADDataJackhammerKey].string
    notes = json[kADDataNotesKey].string
    date = json[kADDataDateKey].string
    supportTruck1 = json[kADDataSupportTruck1Key].string
    lightPlant = json[kADDataLightPlantKey].string
    trackor = json[kADDataTrackorKey].string
    concreteSaw = json[kADDataConcreteSawKey].string
    airCompressor = json[kADDataAirCompressorKey].string
    dumpTrailer = json[kADDataDumpTrailerKey].string
    generator = json[kADDataGeneratorKey].string
    rid = json[kADDataRidKey].string
    concreteCore = json[kADDataConcreteCoreKey].string
    trashPump = json[kADDataTrashPumpKey].string
    dateTime = json[kADDataDateTimeKey].string
    userid = json[kADDataUseridKey].string
    projectid = json[kADDataProjectidKey].string
    skidsteer = json[kADDataSkidsteerKey].string
    other = json[kADDataOtherKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = pumpNotetype { dictionary[kADDataPumpNotetypeKey] = value }
    if let value = steamCleaner { dictionary[kADDataSteamCleanerKey] = value }
    if let value = supportTruck2 { dictionary[kADDataSupportTruck2Key] = value }
    if let value = jackhammer { dictionary[kADDataJackhammerKey] = value }
    if let value = notes { dictionary[kADDataNotesKey] = value }
    if let value = date { dictionary[kADDataDateKey] = value }
    if let value = supportTruck1 { dictionary[kADDataSupportTruck1Key] = value }
    if let value = lightPlant { dictionary[kADDataLightPlantKey] = value }
    if let value = trackor { dictionary[kADDataTrackorKey] = value }
    if let value = concreteSaw { dictionary[kADDataConcreteSawKey] = value }
    if let value = airCompressor { dictionary[kADDataAirCompressorKey] = value }
    if let value = dumpTrailer { dictionary[kADDataDumpTrailerKey] = value }
    if let value = generator { dictionary[kADDataGeneratorKey] = value }
    if let value = rid { dictionary[kADDataRidKey] = value }
    if let value = concreteCore { dictionary[kADDataConcreteCoreKey] = value }
    if let value = trashPump { dictionary[kADDataTrashPumpKey] = value }
    if let value = dateTime { dictionary[kADDataDateTimeKey] = value }
    if let value = userid { dictionary[kADDataUseridKey] = value }
    if let value = projectid { dictionary[kADDataProjectidKey] = value }
    if let value = skidsteer { dictionary[kADDataSkidsteerKey] = value }
    if let value = other { dictionary[kADDataOtherKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.pumpNotetype = aDecoder.decodeObject(forKey: kADDataPumpNotetypeKey) as? String
    self.steamCleaner = aDecoder.decodeObject(forKey: kADDataSteamCleanerKey) as? String
    self.supportTruck2 = aDecoder.decodeObject(forKey: kADDataSupportTruck2Key) as? String
    self.jackhammer = aDecoder.decodeObject(forKey: kADDataJackhammerKey) as? String
    self.notes = aDecoder.decodeObject(forKey: kADDataNotesKey) as? String
    self.date = aDecoder.decodeObject(forKey: kADDataDateKey) as? String
    self.supportTruck1 = aDecoder.decodeObject(forKey: kADDataSupportTruck1Key) as? String
    self.lightPlant = aDecoder.decodeObject(forKey: kADDataLightPlantKey) as? String
    self.trackor = aDecoder.decodeObject(forKey: kADDataTrackorKey) as? String
    self.concreteSaw = aDecoder.decodeObject(forKey: kADDataConcreteSawKey) as? String
    self.airCompressor = aDecoder.decodeObject(forKey: kADDataAirCompressorKey) as? String
    self.dumpTrailer = aDecoder.decodeObject(forKey: kADDataDumpTrailerKey) as? String
    self.generator = aDecoder.decodeObject(forKey: kADDataGeneratorKey) as? String
    self.rid = aDecoder.decodeObject(forKey: kADDataRidKey) as? String
    self.concreteCore = aDecoder.decodeObject(forKey: kADDataConcreteCoreKey) as? String
    self.trashPump = aDecoder.decodeObject(forKey: kADDataTrashPumpKey) as? String
    self.dateTime = aDecoder.decodeObject(forKey: kADDataDateTimeKey) as? String
    self.userid = aDecoder.decodeObject(forKey: kADDataUseridKey) as? String
    self.projectid = aDecoder.decodeObject(forKey: kADDataProjectidKey) as? String
    self.skidsteer = aDecoder.decodeObject(forKey: kADDataSkidsteerKey) as? String
    self.other = aDecoder.decodeObject(forKey: kADDataOtherKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(pumpNotetype, forKey: kADDataPumpNotetypeKey)
    aCoder.encode(steamCleaner, forKey: kADDataSteamCleanerKey)
    aCoder.encode(supportTruck2, forKey: kADDataSupportTruck2Key)
    aCoder.encode(jackhammer, forKey: kADDataJackhammerKey)
    aCoder.encode(notes, forKey: kADDataNotesKey)
    aCoder.encode(date, forKey: kADDataDateKey)
    aCoder.encode(supportTruck1, forKey: kADDataSupportTruck1Key)
    aCoder.encode(lightPlant, forKey: kADDataLightPlantKey)
    aCoder.encode(trackor, forKey: kADDataTrackorKey)
    aCoder.encode(concreteSaw, forKey: kADDataConcreteSawKey)
    aCoder.encode(airCompressor, forKey: kADDataAirCompressorKey)
    aCoder.encode(dumpTrailer, forKey: kADDataDumpTrailerKey)
    aCoder.encode(generator, forKey: kADDataGeneratorKey)
    aCoder.encode(rid, forKey: kADDataRidKey)
    aCoder.encode(concreteCore, forKey: kADDataConcreteCoreKey)
    aCoder.encode(trashPump, forKey: kADDataTrashPumpKey)
    aCoder.encode(dateTime, forKey: kADDataDateTimeKey)
    aCoder.encode(userid, forKey: kADDataUseridKey)
    aCoder.encode(projectid, forKey: kADDataProjectidKey)
    aCoder.encode(skidsteer, forKey: kADDataSkidsteerKey)
    aCoder.encode(other, forKey: kADDataOtherKey)
  }

}
