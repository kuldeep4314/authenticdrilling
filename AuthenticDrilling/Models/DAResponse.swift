//
//  DAResponse.swift
//
//  Created by goyal on 16/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class DAResponse: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private let kDAResponseResultKey: String = "result"
    private let kDAResponseDataKey: String = "data"
    private let kDAResponseMsgKey: String = "msg"
    
    // MARK: Properties
    public var result: String?
    public var data: DAData?
    public var msg: String?
    
    // MARK: SwiftyJSON Initalizers
    /**
     Initates the instance based on the object
     - parameter object: The object of either Dictionary or Array kind that was passed.
     - returns: An initalized instance of the class.
     */
    convenience public init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /**
     Initates the instance based on the JSON that was passed.
     - parameter json: JSON object from SwiftyJSON.
     - returns: An initalized instance of the class.
     */
    public init(json: JSON) {
        result = json[kDAResponseResultKey].string
        data = DAData(json: json[kDAResponseDataKey])
        msg = json[kDAResponseMsgKey].string
    }
    
    /**
     Generates description of the object in the form of a NSDictionary.
     - returns: A Key value pair containing all valid values in the object.
     */
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = result { dictionary[kDAResponseResultKey] = value }
        if let value = data { dictionary[kDAResponseDataKey] = value.dictionaryRepresentation() }
        if let value = msg { dictionary[kDAResponseMsgKey] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.result = aDecoder.decodeObject(forKey: kDAResponseResultKey) as? String
        self.data = aDecoder.decodeObject(forKey: kDAResponseDataKey) as? DAData
        self.msg = aDecoder.decodeObject(forKey: kDAResponseMsgKey) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(result, forKey: kDAResponseResultKey)
        aCoder.encode(data, forKey: kDAResponseDataKey)
        aCoder.encode(msg, forKey: kDAResponseMsgKey)
    }
    
}
