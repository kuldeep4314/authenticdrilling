//
//  DAData.swift
//
//  Created by goyal on 16/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class DAData: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private let kDADataTrailer2Key: String = "trailer2"
    private let kDADataStatusKey: String = "status"
    private let kDADataSupportTruck2Key: String = "support_truck2"
    private let kDADataEmailKey: String = "email"
    private let kDADataInternalIdentifierKey: String = "id"
    private let kDADataNameKey: String = "name"
    private let kDADataLoginStatusKey: String = "login_status"
    private let kDADataSupportTruck1Key: String = "support_truck1"
    private let kDADataRigKey: String = "rig"
    private let kDADataTrailer1Key: String = "trailer1"
    
    // MARK: Properties
    public var trailer2: String?
    public var status: String?
    public var supportTruck2: String?
    public var email: String?
    public var internalIdentifier: String?
    public var name: String?
    public var loginStatus: String?
    public var supportTruck1: String?
    public var rig: String?
    public var trailer1: String?
    
    // MARK: SwiftyJSON Initalizers
    /**
     Initates the instance based on the object
     - parameter object: The object of either Dictionary or Array kind that was passed.
     - returns: An initalized instance of the class.
     */
    convenience public init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /**
     Initates the instance based on the JSON that was passed.
     - parameter json: JSON object from SwiftyJSON.
     - returns: An initalized instance of the class.
     */
    public init(json: JSON) {
        trailer2 = json[kDADataTrailer2Key].string
        status = json[kDADataStatusKey].string
        supportTruck2 = json[kDADataSupportTruck2Key].string
        email = json[kDADataEmailKey].string
        internalIdentifier = json[kDADataInternalIdentifierKey].string
        name = json[kDADataNameKey].string
        loginStatus = json[kDADataLoginStatusKey].string
        supportTruck1 = json[kDADataSupportTruck1Key].string
        rig = json[kDADataRigKey].string
        trailer1 = json[kDADataTrailer1Key].string
    }
    
    /**
     Generates description of the object in the form of a NSDictionary.
     - returns: A Key value pair containing all valid values in the object.
     */
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = trailer2 { dictionary[kDADataTrailer2Key] = value }
        if let value = status { dictionary[kDADataStatusKey] = value }
        if let value = supportTruck2 { dictionary[kDADataSupportTruck2Key] = value }
        if let value = email { dictionary[kDADataEmailKey] = value }
        if let value = internalIdentifier { dictionary[kDADataInternalIdentifierKey] = value }
        if let value = name { dictionary[kDADataNameKey] = value }
        if let value = loginStatus { dictionary[kDADataLoginStatusKey] = value }
        if let value = supportTruck1 { dictionary[kDADataSupportTruck1Key] = value }
        if let value = rig { dictionary[kDADataRigKey] = value }
        if let value = trailer1 { dictionary[kDADataTrailer1Key] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.trailer2 = aDecoder.decodeObject(forKey: kDADataTrailer2Key) as? String
        self.status = aDecoder.decodeObject(forKey: kDADataStatusKey) as? String
        self.supportTruck2 = aDecoder.decodeObject(forKey: kDADataSupportTruck2Key) as? String
        self.email = aDecoder.decodeObject(forKey: kDADataEmailKey) as? String
        self.internalIdentifier = aDecoder.decodeObject(forKey: kDADataInternalIdentifierKey) as? String
        self.name = aDecoder.decodeObject(forKey: kDADataNameKey) as? String
        self.loginStatus = aDecoder.decodeObject(forKey: kDADataLoginStatusKey) as? String
        self.supportTruck1 = aDecoder.decodeObject(forKey: kDADataSupportTruck1Key) as? String
        self.rig = aDecoder.decodeObject(forKey: kDADataRigKey) as? String
        self.trailer1 = aDecoder.decodeObject(forKey: kDADataTrailer1Key) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(trailer2, forKey: kDADataTrailer2Key)
        aCoder.encode(status, forKey: kDADataStatusKey)
        aCoder.encode(supportTruck2, forKey: kDADataSupportTruck2Key)
        aCoder.encode(email, forKey: kDADataEmailKey)
        aCoder.encode(internalIdentifier, forKey: kDADataInternalIdentifierKey)
        aCoder.encode(name, forKey: kDADataNameKey)
        aCoder.encode(loginStatus, forKey: kDADataLoginStatusKey)
        aCoder.encode(supportTruck1, forKey: kDADataSupportTruck1Key)
        aCoder.encode(rig, forKey: kDADataRigKey)
        aCoder.encode(trailer1, forKey: kDADataTrailer1Key)
    }
    
}
