//
//  AppDelegate.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-23.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit
import UserNotifications
import IQKeyboardManagerSwift
import CoreLocation
import Fabric
import Crashlytics
import EFInternetIndicator
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , CLLocationManagerDelegate{
let reachability = Reachability()!
    var window: UIWindow?
    var catDict = [String : Any]()
var locationManager = CLLocationManager()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
              Fabric.with([Crashlytics.self])
        IQKeyboardManager.sharedManager().enable = true
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
        else  {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        
    UIApplication.shared.statusBarStyle = .lightContent
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: Notification.Name.reachabilityChanged,object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
        
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
       self.fetchCategories()
        self.fetchHelpersApiCall()
        if  standard.value(forKey: "id") as? String != nil {
            self.homeTabBar()
        }
        else{
            self.welcomeViewConroller()
        }
        
        
        self.fetchDropDownsCall(table: "support")
         self.fetchDropDownsCall(table: "rig")
         self.fetchDropDownsCall(table: "client")
         self.fetchDropDownsCall(table: "state")
         self.fetchDropDownsCall(table: "trailer")
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
       
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK:- locationManager Functions
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            print("User allowed us to access location")
            //do whatever init activities here.
        }
    }
    
    
 
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Did location updates is called")
         let userLocation:CLLocation = locations[0] as CLLocation
       
        // self.getAddress(location: userLocation)
        manager.stopUpdatingLocation()
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Did location updates is called but failed getting location \(error)")
    }
    
    
    func isAuthorizedtoGetUserLocation() {
        
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
        }
    }
   
    func getAddress(location:CLLocation){
        
        let postParameters:[String: Any] = [ "latlng": "\(location.coordinate.latitude),\(location.coordinate.longitude)"]
        let url : String = "http://maps.googleapis.com/maps/api/geocode/json"
         print(url)
        Alamofire.request(url, method: .get, parameters: postParameters, encoding: URLEncoding.default, headers: nil).responseJSON {  response in

            if let receivedResults = response.result.value
            {
                let resultParams = JSON(receivedResults)
               print(resultParams)
            }
        }
        
       
    }
    
    
    //MARK:- Push Notification Functions  
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        standard.setValue(deviceTokenString, forKey: "gcm")
        print(standard.value(forKey: "gcm") as! String)
        
    }
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != .none {
            application.registerForRemoteNotifications()
        }
        
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        standard.setValue("11111", forKey: "gcm")
        
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
    }
    
    func homeTabBar()
    {
        let nav =  UIStoryboard.init(name: "Main", bundle: nil).instantiateInitialViewController() as! UINavigationController
        
        let vc =  UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        nav.viewControllers = [vc]
        self.window?.rootViewController = nav
        self.window?.makeKeyAndVisible()
        
    }
    func welcomeViewConroller()
    {
        let nav =  UIStoryboard.init(name: "Main", bundle: nil).instantiateInitialViewController() as! UINavigationController
        
        let vc =  UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        nav.viewControllers = [vc]
        self.window?.rootViewController = nav
        self.window?.makeKeyAndVisible()
        
    }
   
    @objc func reachabilityChanged(note: Notification) {
        
        let reachability = note.object as! Reachability
        
        if reachability.connection != .none{
           
            
            if standard.value(forKey: "id") != nil {
                 print("Network  reachable")
                   SyncData.sharedInstance.syncData()
            }
            
         
        
        } else {
            print("Network not reachable")
        }
    }
    func showAlertWithAction(isProjectsEditAdd: Bool){
       let alertController = UIAlertController(title: "Sync Data", message: "Do you want to sync Data?", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alertController.addAction(cancelAction)
        
        let sync = UIAlertAction(title: "Sync now", style: .default) { (action) in
           
            
            if isProjectsEditAdd == true {
                 SyncData.sharedInstance.dataToSync()
            }
            else{
                 SyncData.sharedInstance.dataToSyncWithoutProjects()
            }
            
        }
        alertController.addAction(sync)
        
        alertController.popoverPresentationController?.sourceView = self.window?.rootViewController?.view
        self.window?.rootViewController?.present(alertController, animated: true) {
            
        }
        
    }
    
    
    func showAlertWithActionWhenLogout(isProjectsEditAdd: Bool){
        let alertController = UIAlertController(title: "Sync Data", message: "Do you want to sync Data?", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alertController.addAction(cancelAction)
        
        let sync = UIAlertAction(title: "Sync now", style: .default) { (action) in
            
            
            if isProjectsEditAdd == true {
                SyncWhenLogOut.sharedInstance.dataToSync()
            }
            else{
                SyncWhenLogOut.sharedInstance.dataToSyncWithoutProjects()
            }
            
        }
        alertController.addAction(sync)
        
        alertController.popoverPresentationController?.sourceView = self.window?.rootViewController?.view
        self.window?.rootViewController?.present(alertController, animated: true) {
            
        }
        
    }

    //MARK:- fetchCategories function
    func  fetchCategories(){
        let parameter  : Parameters  = ["action": "category_all"]
        print(parameter)
        ApiManager.requestPOSTURL(dropDownUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
       
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            self.catDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
            
        standard.set(self.catDict, forKey: "catData")
        
        }) { (error) in
           
            if error.localizedDescription != "The Internet connection appears to be offline."{
           
            }
            print(error.localizedDescription)
        }
    }
    //MARK:- Fetch  Helpers Api Call
    func fetchHelpersApiCall(){
        
        let parameter  : Parameters  = ["action": "get_helpers"]
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(loginURl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            
            guard json["response"]["result"].string == "201" else{
                
                return
            }
           
         let helpers = json["response"]["helper"].arrayValue.map({$0["name"].stringValue})

             standard.set(helpers, forKey: "helpers")
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  error.localizedDescription)
            }
            print(error.localizedDescription)
        }
        
    }
    func fetchDropDownsCall(table : String){
        
        let parameter  : Parameters  = ["action": "common_dropdown", "table": table]
        print(parameter)
        
        ApiManager.requestPOSTURL(dropDownUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            
            
            print(json)
            
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            
            if table == "rig"{
                let array = json["response"]["data"].arrayValue.map({$0["rig_name"].stringValue})
                
                standard.set(array, forKey: table)
            }
            else if table == "trailer"{
                
                  let array = json["response"]["data"].arrayValue.map({$0["trailer_name"].stringValue})
                 standard.set(array, forKey: table)
            }
            else if table == "support"{
                
                let array = json["response"]["data"].arrayValue.map({$0["support_name"].stringValue})
                standard.set(array, forKey: table)
            }
          else  if table == "state"{
                let array = json["response"]["data"].arrayValue.map({$0["state_name"].stringValue})
                 standard.set(array, forKey: table)
            }
            else if table == "client"{
                
                 let array = json["response"]["data"].arrayValue.map({$0["client_name"].stringValue})
                standard.set(array, forKey: table)
            }
            
            
            
        }) { (error) in
            
            print(error.localizedDescription)
        }
        
    }
}

