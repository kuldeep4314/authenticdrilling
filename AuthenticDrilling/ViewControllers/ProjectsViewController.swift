//
//  ProjectsViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-24.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit
import CoreData
class ProjectsViewController: ViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblProjectName: UILabel!
    var projectInfo :ProjectData?
    var projectsDatesArr = [JSON]()
    
    var sessions: [NSManagedObject] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        self.lblProjectName.text = projectInfo?.projectName
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func saveCoredata(strDate : String, isOffline : Bool){
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "WorkingDates",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        session.setValue(strDate, forKey: "date")
        session.setValue(projectId, forKey: "projectid")
        session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
       
        session.setValue(isOffline, forKey: "isOffline")
        CoreDataStack.saveContext()
    }
    
    func deleteAllRecords() {
        let managedContext = CoreDataStack.managedObjectContext
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "WorkingDates")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
            print ("There was an error")
        }
    }
    
    func getCoreData() {
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "WorkingDates")
        
        if Network.isConnectedToNetwork() == false {
            fetchRequest.predicate =  NSPredicate(format: "projectid = %@", projectId)
        }
      
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
             self.projectsDatesArr.removeAll()
            for session in sessions{
                
               let d1 = session.value(forKey: "date")
                self.projectsDatesArr.append(JSON(d1 ?? ""))
            }
     
            // sorted array
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            
            self.projectsDatesArr =   self.projectsDatesArr.sorted() {return formatter.date(from: $0.stringValue ) ?? Date() > formatter.date(from: $1.stringValue ) ?? Date()}
          
            if self.projectsDatesArr.count == 0 {
                appDelegates.window?.rootViewController?.view.makeToast("No Working dates list found", duration: 2, position: .center)
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         if Network.isConnectedToNetwork() == true{
        
        self.loadProductDates()
        }
        else{
            self.getCoreData()
        }
    }
    
    
    @IBAction func projectInfoTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProjectInfoViewController") as! ProjectInfoViewController
        vc.projectInformation = projectInfo
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK:- TableView Data Source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.projectsDatesArr.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProjectCell
        
        cell.lblDate.text =  Comman.changedDateFormatter1(date: self.projectsDatesArr[indexPath.section].stringValue)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "header")
        
        return cell?.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 20
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let story = UIStoryboard.init(name: "Well", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "ProjectDetailsViewController") as! ProjectDetailsViewController
        
        vc.projectInformation = projectInfo
        vc.strProjectDate = self.projectsDatesArr[indexPath.section].stringValue
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK:- Fetch Project Dates function
    func  loadProductDates(){
        let parameter  : Parameters  = ["action": "fetch_date", "userid": standard.value(forKey: "id")!,"projectid": projectId]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
        
                return
            }
            
           let array = json["response"]["data"].arrayValue
            
            self.deleteAllRecords()
            for i in 0..<array.count {
             
                if  ProjectsOffline.sharedInstance.getOfflineTables(tables: "WorkingDates", strDate: array[i]["date"].stringValue, projectID: projectId) == false
                {
                     self.saveCoredata(strDate: array[i]["date"].stringValue, isOffline: false)
               }
                }
            self.projectsDatesArr.removeAll()
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    
    
}
