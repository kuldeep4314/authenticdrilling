//
//  ShowConsumablesInfoViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2018-01-17.
//  Copyright © 2018 Avatar Singh. All rights reserved.
//

import UIKit
import CoreData
class ShowConsumablesInfoViewController: ViewController ,UITableViewDelegate,UITableViewDataSource{
      var subCatgoriesList = [JSON]()
     var subCatgoriesIDs = [String]()
      var sessions: [NSManagedObject] = []
    var  arrRisers = [[String : Any]]()
    var arrScreens = [[String : Any]]()
    
    var surfaceCompletionDict =  [String: Any]()
     var installSupplyDict =  [String: Any]()
     var groutDict =  [String: Any]()
     var sampleSupplyDict =  [String: Any]()
     var miscellanousDict =  [String: Any]()
     var damagedDict =  [String: Any]()
     var dictQtyUnit =  [[String: Any]]()
    let arrSurfaceCompletionKeys  = [["coreboxes_qty","coreboxes_unit"],["linear_qty","linear_unit"]
        ,["pad",""]
        ,["bollars",""]
        ,["dolphin_locks",""]
        
    ]
    
    let arrDamagedKeys  = [["auger_teeth",""],["rock_teeth",""],["split_spoon",""]
        ,["tires_qty","tires_unit"]
        ,["odexcase_qty","odexcase_unit"]
        ,["odexlead_qty","odexlead_unit"]
        
    ]
    
    
    
     let arrSurfaceCompletion = ["Flushmounts","Stickups","Pad","Bollard","Dolphin Locks"]
    let arrDamaged = ["Auger Teeth","Rock Teeth","Split Spoon \n Sample","Tires","Odex Casing" , "Odex Lead"]
    let arrMiscellous = ["Shale Trap","Centralizers"]
    
    let arrSampleSupply = [["coreboxes_qty","coreboxes_unit"],["linears_qty","linears_unit"],["shelby_qty","shelby_unit"],["drum_qty","drum_unit"]
    ,["disposable_qty","disposable_unit"]
    ,["wooden_qty","wooden_unit"]
      ]
    
    let arrMiscellanousKeys = [["shaltrap_qty","shaltrap_size"],["centralizers_qty","centralizers_size"]
    ]
    
    
    let arrNamesImages = [#imageLiteral(resourceName: "imgGroutSupplies"),#imageLiteral(resourceName: "imgSampleSupplies"),#imageLiteral(resourceName: "imgInstallation"),#imageLiteral(resourceName: "imgSurfaceCompletion"),#imageLiteral(resourceName: "imgMiscellaneous"),#imageLiteral(resourceName: "imgDamagedTooling")]
    var selectedIndex = -1
    let arrayHeader = ["Grout Supplies","Sampling Supplies","Installation","Surface Completion","Miscellaneous","Damaged Tooling"]
    
    let arrSampleSupplyNames = ["Core Boxes","Liner","Shelby Tubes","Drums","Disposable Bailers","Wooden Plugs"]
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
      tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let parentVC = self.parent as! ProjectDetailsViewController
        parentVC.delegatesAddConsumable = self
        if parentVC.indexs == 6{
            
            if Network.isConnectedToNetwork() == true {
                self.getOfflineData(tables: "SampleSupply")
                self.getOfflineData(tables: "MiscellaneousSupply")
                self.getOfflineData(tables: "SurfaceCompletion")
                self.getOfflineData(tables: "DamagedTools")
                self.getOfflineData(tables: "InstallSupply")
                self.getOfflineData(tables: "GroutSupply")
            }
            else{
                self.getCoreData(tables: "SampleSupply")
                self.getCoreData(tables: "MiscellaneousSupply")
                self.getCoreData(tables: "SurfaceCompletion")
                self.getCoreData(tables: "DamagedTools")
                self.getCoreData(tables: "InstallSupply")
                self.getCoreData(tables: "GroutSupply")
            }
        }
    }
     @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
        if selectedIndex  == sender.view?.tag {
            selectedIndex  = -1
        }
        else{
            selectedIndex  = (sender.view?.tag)!
        }
        self.tableView.reloadData()
    }
    
    
    //MARK:- TableView Data Source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if selectedIndex == section{
          
            
           
            if selectedIndex == 0 {
                return  self.subCatgoriesList.count + 1
            }
           else  if selectedIndex == 1 {
                return 6
            }
            else  if selectedIndex == 2 {
                return 9 + arrRisers.count + arrScreens.count
            }
            else if selectedIndex == 3{
                return  5
            }
            else if selectedIndex == 4{
              return  3
            }
            else if selectedIndex == 5{
                return  7
            }
            else{
                return 1
            }
            
            
        }
        else{
          return 0
        }
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if selectedIndex == 0 {
            
            
            if indexPath.row == 0 {
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! AddConsumablesTableViewCell
                 cell.lblTitle1.text = JSON(self.groutDict)["category_name"].stringValue
                
                return cell
                
            }
            else{
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddConsumablesTableViewCell
                
                cell.lblTitle.text = self.subCatgoriesList[indexPath.row - 1]["name"].stringValue
                
                let valueQty = dictQtyUnit.filter({$0["id"] as! String == subCatgoriesIDs[indexPath.row - 1]}).map({$0["qty"]})
                let valueUnit = dictQtyUnit.filter({$0["id"] as! String == subCatgoriesIDs[indexPath.row - 1]}).map({$0["unit"]})
                
                if valueQty.count > 0 && valueUnit.count > 0{
                    cell.lblValue.text = "\(valueQty[0]!) \(valueUnit[0]!)"
                }
                else if valueUnit.count > 0 &&  valueQty.count == 0{
                    cell.lblValue.text = " \(valueUnit[0]!)"
                }
                else if valueUnit.count == 0 &&  valueQty.count > 0{
                    cell.lblValue.text = "\(valueQty[0]!)"
                }
                else{
                    cell.lblValue.text = ""
                }
                return cell
            }
           
            
        }
        else if selectedIndex == 1 {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddConsumablesTableViewCell
            
            cell.lblTitle.text = arrSampleSupplyNames[indexPath.row]
            cell.lblValue.text = "\(JSON(self.sampleSupplyDict)[arrSampleSupply[indexPath.row][0]].stringValue) \(JSON(self.sampleSupplyDict)[arrSampleSupply[indexPath.row][1]].stringValue)"
                return cell
        }
            
             else if selectedIndex == 2 {
            
        
            
            if indexPath.row == 0{
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddConsumablesTableViewCell
                cell.lblTitle.text = "Steel"
                  cell.lblValue.text = JSON(self.installSupplyDict)["steel"].stringValue
                
                return cell
            }
            else if indexPath.row == 1{
                 let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddConsumablesTableViewCell
                cell.lblTitle.text = "Size"
                cell.lblValue.text = JSON(self.installSupplyDict)["size"].stringValue
                
                  return cell
            }
            
            else if indexPath.row == 2{
                
                  let cell =  tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as! AddConsumablesTableViewCell
                
                cell.lblTitle2.text = "Single"
                
                 return cell
            }
            else if indexPath.row == 3{
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as! AddConsumablesTableViewCell
                
                cell.lblTitle2.text = "Nested"
                
                return cell
            }
            else if indexPath.row == 4{
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell4", for: indexPath) as! AddConsumablesTableViewCell
                
                cell.lblRiser.text = "Riser"
                
                return cell
            }
                
            else if indexPath.row < 5 + arrRisers.count{
                
                print(indexPath.row)
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddConsumablesTableViewCell
                
               cell.lblTitle.text = "\(JSON(arrRisers[ indexPath.row - 5])["title"].stringValue) inch"
                cell.lblValue.text = "\(JSON(arrRisers[indexPath.row - 5])["value"].stringValue) pieces"
                return cell
            }
            else if indexPath.row == 5 + arrRisers.count{
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell4", for: indexPath) as! AddConsumablesTableViewCell
                
                cell.lblRiser.text = "Screen"
                return cell
            }
            else if indexPath.row == 6 + arrRisers.count{
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddConsumablesTableViewCell
                cell.lblTitle.text = "Slot"
                cell.lblValue.text = JSON(self.installSupplyDict)["slot_screen"].stringValue
                
                return cell
              
            }
            else if indexPath.row < 7 + arrRisers.count + arrScreens.count{
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddConsumablesTableViewCell
                
                cell.lblTitle.text = "\(JSON(arrScreens[ indexPath.row -  (7 + arrRisers.count)])["title"].stringValue) inch"
                cell.lblValue.text = "\(JSON(arrScreens[indexPath.row -  (7 + arrRisers.count)])["value"].stringValue) pieces"
                return cell
            }
            else if indexPath.row == 7 + arrRisers.count + arrScreens.count{
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddConsumablesTableViewCell
                
                cell.lblTitle.text = "Top Cap"
                cell.lblValue.text = JSON(self.installSupplyDict)["topcap"].stringValue
                return cell
            }
            else if indexPath.row == 8 + arrRisers.count + arrScreens.count{
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddConsumablesTableViewCell
                
                cell.lblTitle.text = "Bottom Cap"
                cell.lblValue.text = JSON(self.installSupplyDict)["bottomcap"].stringValue
                return cell
            }
                
            else{
                 let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddConsumablesTableViewCell
                
                return cell
                
            }
            
        }
            
            
            else if selectedIndex == 3 {
             let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddConsumablesTableViewCell
                cell.lblTitle.text = arrSurfaceCompletion[indexPath.row]
            
             cell.lblValue.text = "\(JSON(self.surfaceCompletionDict)[arrSurfaceCompletionKeys[indexPath.row][0]].stringValue) \(JSON(self.surfaceCompletionDict)[arrSurfaceCompletionKeys[indexPath.row][1]].stringValue)"
            return cell
            
        }
            
            
        else if selectedIndex == 4 {
            
            if indexPath.row == 2{
                 let cell =  tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! AddConsumablesTableViewCell
                
                  cell.txtView.text = JSON(self.miscellanousDict)["notes"].stringValue.base64Decoded()
                return cell
            }
            else{
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddConsumablesTableViewCell
                
                cell.lblTitle.text = arrMiscellous[indexPath.row]
                cell.lblValue.text = "\(JSON(self.miscellanousDict)[arrMiscellanousKeys[indexPath.row][0]].stringValue) \(JSON(self.miscellanousDict)[arrMiscellanousKeys[indexPath.row][1]].stringValue)"
                return cell
            }
            
        }
        else if selectedIndex == 5 {
            
            if indexPath.row == 6{
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! AddConsumablesTableViewCell
                
                cell.txtView.text = JSON(self.damagedDict)["notes"].stringValue.base64Decoded()
                return cell
            }
            else{
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddConsumablesTableViewCell
                
                cell.lblTitle.text = arrDamaged[indexPath.row]
                cell.lblValue.text = "\(JSON(self.damagedDict)[arrDamagedKeys[indexPath.row][0]].stringValue) \(JSON(self.damagedDict)[arrDamagedKeys[indexPath.row][1]].stringValue)"
                return cell
            }
            
        }

        else{
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddConsumablesTableViewCell
         
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      
        if selectedIndex == 0 {
            if indexPath.row == 0 {
                
                if JSON(self.groutDict)["category_name"].stringValue == ""
                    {
                return 0
            }
            else{
                return 44
            }
                
            }
            else{
    
                let valueQty = dictQtyUnit.filter({$0["id"] as! String == subCatgoriesIDs[indexPath.row - 1]}).map({$0["qty"]})
                let valueUnit = dictQtyUnit.filter({$0["id"] as! String == subCatgoriesIDs[indexPath.row - 1]}).map({$0["unit"]})
                
                if valueQty.count > 0 && valueUnit.count > 0{
                     return 44
                }
                else {
                     return 0
                    
                }
            }
            
        }
        
        else  if selectedIndex == 1{
            
            if JSON(self.sampleSupplyDict)[arrSampleSupply[indexPath.row][0]].stringValue != "" && JSON(self.sampleSupplyDict)[arrSampleSupply[indexPath.row][1]].stringValue != ""{
                
                return 44
            }
            else{
                return 0
            }
        }
        
        else if selectedIndex == 2 {
            
            
            
            if indexPath.row == 0{
                if JSON(self.installSupplyDict)["steel"].stringValue == ""{
                
                    return 0
                    
                }
                else{
                    return 44
                }
               
            }
            else if indexPath.row == 1{
              
                if JSON(self.installSupplyDict)["size"].stringValue == ""{
                    
                    return 0
                    
                }
                else{
                    return 44
                }
                
            }
                
            else if indexPath.row == 2{
               
                if JSON(self.installSupplyDict)["single"].stringValue == "1" {
                    return 44
                }
                else{
                     return 0
                }
                
                
                
            }
            else if indexPath.row == 3{
                
                if JSON(self.installSupplyDict)["nested"].stringValue == "1" {
                    return 44
                }
                else{
                    return 0
                }
            }
            else if indexPath.row == 4{
    
                if JSON(self.installSupplyDict)["riser"].stringValue == "1" {
                    return 44
                }
                else{
                    return 0
                }
                
                
                
            }
                
            else if indexPath.row < 5 + arrRisers.count{
                
                if JSON(arrRisers[ indexPath.row - 5])["title"].stringValue != "" && JSON(arrRisers[indexPath.row - 5])["value"].stringValue != ""{
                    return 44
                }
                else{
                    return 0
                }
            }
            else if indexPath.row == 5 + arrRisers.count{
                if JSON(self.installSupplyDict)["screen"].stringValue == "1" {
                    return 44
                }
                else{
                    return 0
                }
                
            }
            else if indexPath.row == 6 + arrRisers.count{
                
                if JSON(self.installSupplyDict)["slot_screen"].stringValue == ""{
                    return 0
                    }
                else{
                    return 44
                }
            }
            else if indexPath.row < 7 + arrRisers.count + arrScreens.count{
                
                if JSON(arrScreens[ indexPath.row -  (7 + arrRisers.count)])["title"].stringValue != "" && JSON(arrScreens[indexPath.row -  (7 + arrRisers.count)])["value"].stringValue != ""{
                    return 44
                }
                else{
                    return 0
                }
                

            }
            else if indexPath.row == 7 + arrRisers.count + arrScreens.count{
                
                if JSON(self.installSupplyDict)["topcap"].stringValue == ""{
                    
                    return 0
                    
                }
                else{
                    return 44
                }
                
                
                
            }
            else if indexPath.row == 8 + arrRisers.count + arrScreens.count{
               
                if JSON(self.installSupplyDict)["bottomcap"].stringValue == ""{
                    
                    return 0
                    
                }
                else{
                    return 44
                }
                
            }
                
            else{
                  return 0
                
            }
            
        }
            
            
            
            
            
            
        
       else if selectedIndex == 3{
            if indexPath.row < 2 {
                if JSON(self.surfaceCompletionDict)[arrSurfaceCompletionKeys[indexPath.row][0]].stringValue != "" && JSON(self.surfaceCompletionDict)[arrSurfaceCompletionKeys[indexPath.row][1]].stringValue != ""{
                    
                    return 44
                }
                else{
                    return 0
                }
            }
            else{
                if JSON(self.self.surfaceCompletionDict)[arrSurfaceCompletionKeys[indexPath.row][0]].stringValue != ""{
                    
                    return 44
                }
                else{
                    return 0
                }
                
            }
            
            
        }
        
        
        
      else  if selectedIndex == 4 {
            
            if indexPath.row == 2{
                
                let value  =  JSON(self.miscellanousDict)["notes"].stringValue.base64Decoded()
                
                if value == "Enter your notes" || value == "" {
                    return 0
                }
                else{
                    return 80
                }
            }
            else{
                if JSON(self.miscellanousDict)[arrMiscellanousKeys[indexPath.row][0]].stringValue != "" && JSON(self.miscellanousDict)[arrMiscellanousKeys[indexPath.row][1]].stringValue != ""{
                    
                     return 44
                }
                else{
                      return 0
                }
                
               
            }
        }
  
       else if selectedIndex == 5 {
            
            if indexPath.row == 6{
                
                let value  =  JSON(self.damagedDict)["notes"].stringValue.base64Decoded()
                
                if value == "Enter your notes" || value == "" {
                    return 0
                }
                else{
                    return 80
                }
            }
            else{
        
                if indexPath.row < 3 {
                    if JSON(self.damagedDict)[arrDamagedKeys[indexPath.row][0]].stringValue != "" {
                        
                        return 44
                    }
                    else{
                        return 0
                    }

                }
                
                else{
                    if JSON(self.damagedDict)[arrDamagedKeys[indexPath.row][0]].stringValue != "" && JSON(self.self.damagedDict)[arrDamagedKeys[indexPath.row][1]].stringValue != ""{
                        
                        return 44
                    }
                    else{
                        return 0
                    }

                    
                }
                
                
            }
        }
        
            return 44
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "header") as!  AddConsumablesTableViewCell
        
           cell.lblHeader.text  = arrayHeader[section]
           cell.imgView.image = arrNamesImages[section]
        
        
        if selectedIndex == section {
         cell.imgArrow.image = #imageLiteral(resourceName: "imgDropUP")
            
        }
        else{
              cell.imgArrow.image = #imageLiteral(resourceName: "imgDown")
           
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
        cell.bgView.addGestureRecognizer(tap)
        cell.bgView.tag = section
        cell.bgView.isUserInteractionEnabled = true
        
        cell.bgView.addGestureRecognizer(tap)
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 44
        }
    
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print(indexPath.row)
        }

    
    //MARK:- fetchSamplingDetails function
    func  fetchSamplingDetails(){
        
        let parentVC = self.parent as! ProjectDetailsViewController
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid":projectId,"table":"ac_sampling_supplies","date": parentVC.strProjectDate]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            self.sampleSupplyDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "SampleSupply", strDate: parentVC.strProjectDate, projectID: projectId) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false, tables: "SampleSupply")
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false, tables: "SampleSupply")
                
            }
            self.getCoreData(tables: "SampleSupply")
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    //MARK:- fetchMiscellaneousApi function
    func  fetchMiscellaneousApi(){
      
        let parentVC = self.parent as! ProjectDetailsViewController
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid":projectId,"table":"ac_miscellaneous","date": parentVC.strProjectDate]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            self.miscellanousDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
           
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "MiscellaneousSupply", strDate: parentVC.strProjectDate, projectID: projectId) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false, tables: "MiscellaneousSupply")
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false, tables: "MiscellaneousSupply")
                
            }
            self.getCoreData(tables: "MiscellaneousSupply")
            
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
              self.getCoreData(tables: "MiscellaneousSupply")
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    //MARK:- fetchSurfaceCompletionDetails function
    func  fetchSurfaceCompletionDetails(){
        let parentVC = self.parent as! ProjectDetailsViewController
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid":projectId,"table":"ac_surface_completion_supplies","date": parentVC.strProjectDate]
        print(parameter)
      
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            
            print(json)
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            self.surfaceCompletionDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "SurfaceCompletion", strDate: parentVC.strProjectDate, projectID: projectId) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false, tables: "SurfaceCompletion")
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false, tables: "SurfaceCompletion")
                
            }
            self.getCoreData(tables: "SurfaceCompletion")
          
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            self.getCoreData(tables: "SurfaceCompletion")
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    
    //MARK:- fetchDamagedToolsDetails function
    func  fetchDamagedToolsDetails(){
        
        let parentVC = self.parent as! ProjectDetailsViewController
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid":projectId,"table":"ac_damage_tooling","date": parentVC.strProjectDate]
        print(parameter)
        
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            self.damagedDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "DamagedTools", strDate: parentVC.strProjectDate, projectID: projectId) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false, tables: "DamagedTools")
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false, tables: "DamagedTools")
                
            }
            self.getCoreData(tables: "DamagedTools")
            
            
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    
    //MARK:- fetchGroutDetails function
    func  fetchGroutDetails(){
      
        let parentVC = self.parent as! ProjectDetailsViewController
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid":projectId,"table":"ac_grout_supplies","date": parentVC.strProjectDate]
        print(parameter)
        
        
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            self.groutDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
            self.subCatgoriesList =  json["response"]["data"]["sub_category"].arrayValue
            self.subCatgoriesIDs = json["response"]["data"]["subcat"].stringValue.components(separatedBy: ",")

            self.dictQtyUnit = json["response"]["data"]["subcat_name"].arrayObject as? [[String : Any]]  ?? [[String : Any]]()
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "GroutSupply", strDate: parentVC.strProjectDate, projectID: projectId) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false, tables: "GroutSupply")
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false, tables: "GroutSupply")
                
            }
            self.getCoreData(tables: "GroutSupply")
            
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    //MARK:- //fetchInstallSupply
    func  fetchInstallSupply(){
      
        let parentVC = self.parent as! ProjectDetailsViewController
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid":projectId,"table":"ac_install_supplies","date": parentVC.strProjectDate]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            self.installSupplyDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
            let arrayScreen =  json["response"]["data"]["screen_arr"].arrayValue
            let arrayRiser =  json["response"]["data"]["riser_arr"].arrayValue
            
            self.arrScreens.removeAll()
            self.arrRisers.removeAll()
            for i in 0..<arrayScreen.count{
                let dict =  arrayScreen[i].dictionaryObject
                self.arrScreens.append(dict ?? [String: Any]())
            }
            for i in 0..<arrayRiser.count{
                let dict =  arrayRiser[i].dictionaryObject
                self.arrRisers.append(dict ?? [String: Any]())
            }
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "InstallSupply", strDate: parentVC.strProjectDate, projectID: projectId) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false, tables: "InstallSupply")
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false, tables: "InstallSupply")
                
            }
            self.getCoreData(tables: "InstallSupply")
            
            
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
  
    //MARK:- Update Core Data
    func updateCoredata(isOffline : Bool , edit : Bool , isShowAlert : Bool , tables : String){
        
        let parentVC = self.parent as! ProjectDetailsViewController
        
        let managedContext = CoreDataStack.managedObjectContext
        let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: tables)
        updateFetch.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , parentVC.strProjectDate)
        do {
            let results = try managedContext.fetch(updateFetch) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                
                
                
                
                if tables == "SampleSupply" {
                    // In my case, I only updated the first item in results
                    let session = results![0]
                    
                    let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.sampleSupplyDict)
                    session.setValue(data1, forKey: "dataDetails")
                    
                    session.setValue(parentVC.strProjectDate, forKey: "date")
                    session.setValue(projectId, forKey: "projectid")
                    session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                    session.setValue(isOffline, forKey: "isOffline")
                    session.setValue(edit, forKey: "edit")
                    
                }
                
                if tables == "MiscellaneousSupply" {
                    // In my case, I only updated the first item in results
                    let session = results![0]
                    
                    let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.miscellanousDict)
                    session.setValue(data1, forKey: "dataDetails")
                    
                    session.setValue(parentVC.strProjectDate, forKey: "date")
                    session.setValue(projectId, forKey: "projectid")
                    session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                    session.setValue(isOffline, forKey: "isOffline")
                    session.setValue(edit, forKey: "edit")
                    
                }
                if tables == "SurfaceCompletion" {
                    let session = results![0]
                    
                    let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.surfaceCompletionDict)
                    session.setValue(data1, forKey: "dataDetails")
                    
                    session.setValue(parentVC.strProjectDate, forKey: "date")
                    session.setValue(projectId, forKey: "projectid")
                    session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                    session.setValue(isOffline, forKey: "isOffline")
                    session.setValue(edit, forKey: "edit")
                    
                    
                    
                }
                
                if tables == "DamagedTools" {
                    
                    let session = results![0]
                    
                    let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.damagedDict)
                    session.setValue(data1, forKey: "dataDetails")
                    
                    session.setValue(parentVC.strProjectDate, forKey: "date")
                    session.setValue(projectId, forKey: "projectid")
                    session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                    session.setValue(isOffline, forKey: "isOffline")
                    session.setValue(edit, forKey: "edit")
                    
                    
                }
                  if tables == "InstallSupply" {
                    
                    let session = results![0]
                    
                    let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.installSupplyDict)
                    session.setValue(data1, forKey: "dataDetails")
                    
                    session.setValue(parentVC.strProjectDate, forKey: "date")
                    session.setValue(projectId, forKey: "projectid")
                    session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                    session.setValue(isOffline, forKey: "isOffline")
                    session.setValue(edit, forKey: "edit")
                }
                
                
                if tables == "GroutSupply" {
                    let session = results![0]
                    
                    let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.groutDict)
                    session.setValue(data1, forKey: "dataDetails")
                    
                    session.setValue(parentVC.strProjectDate, forKey: "date")
                    session.setValue(projectId, forKey: "projectid")
                    session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                    session.setValue(isOffline, forKey: "isOffline")
                    session.setValue(edit, forKey: "edit")
                    
                    
                }
                
                
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try managedContext.save()
            
            
            if isShowAlert == true {
                self.navigationController?.popViewController(animated: true)
                appDelegates.window?.rootViewController?.showAlert(messageStr:  "Add consumption Sampling Supplies list updated successfully")
            }
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
        
    }
    
    
    //MARK:- Save Core Data Function
    func saveCoredata(isOffline : Bool,edit: Bool , msg : String , isShow : Bool, tables : String){
        print(isOffline)
        print(edit)
        let parentVC = self.parent as! ProjectDetailsViewController
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: tables,
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        
          if tables == "SampleSupply" {
            print(self.sampleSupplyDict)
            let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.sampleSupplyDict)
            session.setValue(data1, forKey: "dataDetails")
            
            session.setValue(parentVC.strProjectDate, forKey: "date")
            session.setValue(projectId, forKey: "projectid")
            session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
            session.setValue(isOffline, forKey: "isOffline")
            session.setValue(edit, forKey: "edit")
            
            CoreDataStack.saveContext()
            
        }
        
        if tables == "MiscellaneousSupply" {
          
            let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.miscellanousDict)
            session.setValue(data1, forKey: "dataDetails")
            
            session.setValue(parentVC.strProjectDate, forKey: "date")
            session.setValue(projectId, forKey: "projectid")
            session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
            session.setValue(isOffline, forKey: "isOffline")
            session.setValue(edit, forKey: "edit")
            
            CoreDataStack.saveContext()
            
        }
        
        if tables == "SurfaceCompletion" {
            let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.surfaceCompletionDict)
            session.setValue(data1, forKey: "dataDetails")
            
            session.setValue(parentVC.strProjectDate, forKey: "date")
            session.setValue(projectId, forKey: "projectid")
            session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
            session.setValue(isOffline, forKey: "isOffline")
            session.setValue(edit, forKey: "edit")
            
            
            CoreDataStack.saveContext()
            
            
            
        }
        if tables == "DamagedTools" {
            
            let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.damagedDict)
            session.setValue(data1, forKey: "dataDetails")
            
            session.setValue(parentVC.strProjectDate, forKey: "date")
            session.setValue(projectId, forKey: "projectid")
            session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
            session.setValue(isOffline, forKey: "isOffline")
            session.setValue(edit, forKey: "edit")
            CoreDataStack.saveContext()
            }
        
         if tables == "InstallSupply" {
            let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.installSupplyDict)
            session.setValue(data1, forKey: "dataDetails")
            
            session.setValue(parentVC.strProjectDate, forKey: "date")
            session.setValue(projectId, forKey: "projectid")
            session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
            session.setValue(isOffline, forKey: "isOffline")
            session.setValue(edit, forKey: "edit")
            
            
            CoreDataStack.saveContext()
            
        }
        
        
        if tables == "GroutSupply"{
            let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.groutDict )
            
            
            session.setValue(data1, forKey: "dataDetails")
            
            session.setValue(parentVC.strProjectDate, forKey: "date")
            session.setValue(projectId, forKey: "projectid")
            session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
            session.setValue(isOffline, forKey: "isOffline")
            session.setValue(edit, forKey: "edit")
            
            
            CoreDataStack.saveContext()
            
            
        }
        
        
        if isShow == true{
            self.navigationController?.popViewController(animated: true)
            appDelegates.window?.rootViewController?.showAlert(messageStr: msg)
        }
        
    }
    //MARK:- Get Core Data Function
    func getCoreData(tables : String) {
        let parentVC = self.parent as! ProjectDetailsViewController
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: tables)
        
        fetchRequest.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)", projectId , parentVC.strProjectDate)
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            
            if  tables == "SampleSupply"{
                for session in sessions{
                    var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                    
                    dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                    dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                    dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                    dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                    
                    dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                    
                    self.sampleSupplyDict = dictionary
                    
                }
            }
                if tables == "MiscellaneousSupply"{
                    for session in sessions{
                        var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                        
                        dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                        dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                        dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                        dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                        
                        dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                        
                        self.miscellanousDict = dictionary
                        
                    }
                  
            }
            
            if tables == "SurfaceCompletion"{
                
                for session in sessions{
                    var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                    
                    dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                    dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                    dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                    dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                    
                    dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                    
                    self.surfaceCompletionDict = dictionary
                    
                }
                
                
            }
            
              if tables == "DamagedTools"{
                
                for session in sessions{
                    var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                    
                    dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                    dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                    dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                    dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                    
                    dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                    
                    self.damagedDict = dictionary
                }

                
            }
               if tables == "InstallSupply"{
                for session in sessions{
                    var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                    
                    dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                    dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                    dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                    dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                    
                    dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                    
                    
                    let arrayScreen =  JSON(dictionary)["screen_arr"].arrayValue
                    let arrayRiser =   JSON(dictionary)["riser_arr"].arrayValue
                    
                    self.arrScreens.removeAll()
                    self.arrRisers.removeAll()
                    for i in 0..<arrayScreen.count{
                        let dict =  arrayScreen[i].dictionaryObject
                        self.arrScreens.append(dict ?? [String: Any]())
                    }
                    for i in 0..<arrayRiser.count{
                        let dict =  arrayRiser[i].dictionaryObject
                        self.arrRisers.append(dict ?? [String: Any]())
                    }
                    
                    self.installSupplyDict = dictionary
                    
                }
                
              
                
                
            }
            
            
            if tables  == "GroutSupply" {
                
                for session in sessions{
                    var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                    
                    dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                    dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                    dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                    dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                    
                    dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                    
                    self.subCatgoriesList =   JSON(dictionary)["sub_category"].arrayValue
                    
                    self.subCatgoriesIDs =  JSON(dictionary)["subcat"].stringValue.components(separatedBy: ",")
                    
                    self.dictQtyUnit =  JSON(dictionary)["subcat_name"].arrayObject as? [[String : Any]]  ?? [[String : Any]]()
                    
                    self.groutDict = dictionary
                    
                }
                
            }
            
          DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func getOfflineData(tables : String){
        
        let offlineDetails = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: tables)
        print(offlineDetails)
        if offlineDetails.count > 0 {
            
            if tables == "SampleSupply"{
                let parameters : Parameters = ["action": "ac_sampling_supplies" ,"data" :  offlineDetails ]
                
                self.syncOfflineMultipleApi(parameter: parameters, tables: tables)
            }
            
               if tables == "MiscellaneousSupply"{
                
                let parameters : Parameters = ["action": "ac_miscellaneous" ,"data" :  offlineDetails ]
                self.syncOfflineMultipleApi(parameter: parameters, tables: tables)
                
            }
            
            
            if tables == "SurfaceCompletion"{
                let parameters : Parameters = ["action": "ac_surface_completion" ,"data" :  offlineDetails ]
                self.syncOfflineMultipleApi(parameter: parameters, tables: tables)
            }
              if tables == "DamagedTools"{
                let parameters : Parameters = ["action": "ac_damaged_tool" ,"data" :  offlineDetails ]
                
                self.syncOfflineMultipleApi(parameter: parameters, tables: tables)
            }
             if tables == "InstallSupply"{
                
                let parameters : Parameters = ["action": "ac_install_supplies" ,"data" :  offlineDetails ]
                
                self.syncOfflineMultipleApi(parameter: parameters, tables: tables)
                
                
            }
            if tables == "GroutSupply"{
                
                let parameters : Parameters = ["action": "ac_grout_supplies" ,"data" :  offlineDetails ]
                
                self.syncOfflineMultipleApi(parameter: parameters, tables: tables)
            }
            
            
            }
        else{
            if tables == "SampleSupply"{
                
                self.fetchSamplingDetails()
            }
            
            if tables == "MiscellaneousSupply"{
                
                self.fetchMiscellaneousApi()
            }
            if tables == "SurfaceCompletion"{
                
                self.fetchSurfaceCompletionDetails()
            }
            
            if tables == "DamagedTools"{
                
                self.fetchDamagedToolsDetails()
            }
            
            if tables == "InstallSupply"{
                
                self.fetchInstallSupply()
            }
            if tables == "GroutSupply"{
                
                self.fetchGroutDetails()
            }
            
            
       }
        
    }
    
    //MARK: - SyncOfflineMultipleApi
    func syncOfflineMultipleApi(parameter :Parameters ,tables : String){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(consumableUrlOffline, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            ProjectsOffline.sharedInstance.updateCoredataTables(tables: tables)
            self.getCoreData(tables: tables)
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            self.getCoreData(tables: tables)
            print(error.localizedDescription)
        }
    }

    
}
extension  ShowConsumablesInfoViewController : showAddConsumableDelegates{
    
    func  showAddConsumableInfo(){
        
        if Network.isConnectedToNetwork() == true {
            self.getOfflineData(tables: "SampleSupply")
            self.getOfflineData(tables: "MiscellaneousSupply")
            self.getOfflineData(tables: "SurfaceCompletion")
            self.getOfflineData(tables: "DamagedTools")
             self.getOfflineData(tables: "InstallSupply")
            self.getOfflineData(tables: "GroutSupply")
        }
        else{
            self.getCoreData(tables: "SampleSupply")
            self.getCoreData(tables: "MiscellaneousSupply")
            self.getCoreData(tables: "SurfaceCompletion")
            self.getCoreData(tables: "DamagedTools")
            self.getCoreData(tables: "InstallSupply")
              self.getCoreData(tables: "GroutSupply")
        }
   
        
    }
}
