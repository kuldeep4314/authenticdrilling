//
//  ShowSiteInfoViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-12-27.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//


import UIKit
import CoreData
class ShowSiteInfoViewController: ViewController,UITableViewDataSource,UITableViewDelegate{
    
     var sessions: [NSManagedObject] = []
    let arrayNames = ["Select Site Activity","Total Time"]
    var activityList = [JSON]()
    var subCatgoriesList = [[String: Any]]()
    var subCatgoriesIDs = [String]()
    var mainDict = [String: Any]()
    var currentIndex = Int()
    let arrayImagesLeft = [#imageLiteral(resourceName: "imgSelectSite"),#imageLiteral(resourceName: "imgTimer"),#imageLiteral(resourceName: "imgTimer")]
    let arrayImagesRight = [#imageLiteral(resourceName: "imgDropDown"),#imageLiteral(resourceName: "imgWatch"),#imageLiteral(resourceName: "imgWatch")]
    var strDate = String()
    var isShowNotes = Bool()
    var currentTextField = UITextField()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblProjectName: UILabel!
   var projectInfo :ProjectData?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        
        activityList  = JSON(appDelegates.catDict)["Site Activities list"].arrayValue
        print(activityList)
        
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let parentVC = self.parent as! ProjectDetailsViewController
        parentVC.delegatesSiteActivity = self
        if parentVC.indexs == 5{
            if Network.isConnectedToNetwork() == true {
                self.getOfflineData()
            }
            else{
                self.getCoreData()
                
            }
        }
    }
    func getOfflineData(){
        
        let offlineDetails = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "SiteActivity")
        print(offlineDetails)
        if offlineDetails.count > 0 {
            let parameters : Parameters = ["action": "siteactivity_category" ,"data" :  offlineDetails ]
            
            self.syncOfflineMultipleApi(parameter: parameters)
            
        }
        else{
            self.fetchDetails()
        }
        
    }
    
    
    
    //MARK: - SyncOfflineMultipleApi
    func syncOfflineMultipleApi(parameter :Parameters){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(projectUrlOffline, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            ProjectsOffline.sharedInstance.updateCoredataTables(tables: "SiteActivity")
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            self.getCoreData()
            print(error.localizedDescription)
        }
    }
    
    
    //MARK:- Update Core Data
    func updateCoredata(isOffline : Bool , edit : Bool , isShowAlert : Bool){
          let parentVC = self.parent as! ProjectDetailsViewController
        let managedContext = CoreDataStack.managedObjectContext
        let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "SiteActivity")
        updateFetch.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , parentVC.strProjectDate)
        do {
            let results = try managedContext.fetch(updateFetch) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                
                // In my case, I only updated the first item in results
                let session = results![0]
                
                let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
                session.setValue(data1, forKey: "dataDetails")
                
                session.setValue(parentVC.strProjectDate, forKey: "date")
                session.setValue(projectId, forKey: "projectid")
                session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                session.setValue(isOffline, forKey: "isOffline")
                session.setValue(edit, forKey: "edit")
                
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try managedContext.save()
            
            
            if isShowAlert == true {
                self.navigationController?.popViewController(animated: true)
                appDelegates.window?.rootViewController?.showAlert(messageStr:  "site activity list updated successfully")
            }
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
        
    }
    
    
    //MARK:- Save Core Data Function
    func saveCoredata(isOffline : Bool,edit: Bool , msg : String , isShow : Bool){
        print(isOffline)
        print(edit)
          let parentVC = self.parent as! ProjectDetailsViewController
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "SiteActivity",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
        session.setValue(data1, forKey: "dataDetails")
        
        session.setValue(parentVC.strProjectDate, forKey: "date")
        session.setValue(projectId, forKey: "projectid")
        session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
        session.setValue(isOffline, forKey: "isOffline")
        session.setValue(edit, forKey: "edit")
        
        
        CoreDataStack.saveContext()
        
        
        if isShow == true{
            self.navigationController?.popViewController(animated: true)
            appDelegates.window?.rootViewController?.showAlert(messageStr: msg)
        }
        
    }
    //MARK:- Get Core Data Function
    func getCoreData() {
          let parentVC = self.parent as! ProjectDetailsViewController
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "SiteActivity")
        
        fetchRequest.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)", projectId , parentVC.strProjectDate)
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            for session in sessions{
                var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                
                dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                
                dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                
                self.mainDict = dictionary
                
                self.subCatgoriesIDs = JSON(self.mainDict)["subcat"].stringValue.components(separatedBy: ",")
                
                
                let temp = JSON(self.mainDict)["sub_category"].arrayValue
                
                
                self.subCatgoriesList.removeAll()
                for i in 0..<temp.count{
                    
                    self.subCatgoriesList.append(temp[i].dictionaryObject!)
                }
                print(self.subCatgoriesList)
                
                DispatchQueue.main.async {
                    if JSON(self.mainDict)["notes"].stringValue.base64Decoded() != ""{
                        self.isShowNotes = true
                    }
                    else{
                        self.isShowNotes = false
                    }
                    self.tableView.reloadData()
                }
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    //MARK:- TableView Data Source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayNames.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if  self.isShowNotes == true{
                return  1
            }
            else{
                return  subCatgoriesList.count
            }
            
        }
        
        return 0
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if  self.isShowNotes == true {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! RentalCell
            cell.txtView.text = JSON(self.mainDict)["notes"].stringValue.base64Decoded()
    
                cell.txtView.textColor = UIColor.black
            
          
            return cell
        }
        else{
            
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as! RentalCell
            cell.lblName.text = JSON(subCatgoriesList[indexPath.row])["name"].stringValue
          
                cell.btnRight.setImage(#imageLiteral(resourceName: "imgTick"), for: .normal)
            
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if  self.isShowNotes == true {
            return 90
        }
      else  if subCatgoriesIDs.contains(JSON(subCatgoriesList[indexPath.row])["subcategory_id"].stringValue){
             return 60
        }
        else{
          return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      let cell =  tableView.dequeueReusableCell(withIdentifier: "cell") as! RentalCell
        
            
            cell.txtName.placeholder = arrayNames[section]
            if section == 0 {
//                cell.txtTitle.rightview(Img: arrayImagesRight[section])
                cell.txtTitle.rightVC()
                cell.txtName.isHidden = true
                cell.txtTitle.text =  JSON(self.mainDict)["site_activity"].stringValue
            }
            else {
                cell.txtTitle.text = arrayNames[section]
              
                cell.txtName.isHidden = false
                cell.txtTitle.leftview(image: arrayImagesLeft[section])
            
                
                let t1 =   Comman.timeInterval(date: JSON(self.mainDict)["date"].stringValue, startTime: JSON(self.mainDict)["start_time"].stringValue, stopTime: JSON(self.mainDict)["stop_time"].stringValue)
                
                cell.txtName.text = t1
                
                
               
            }
            
            cell.txtTitle.leftview(image: arrayImagesLeft[section])
        
          cell.txtName.rightVC()
            return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            if JSON(self.mainDict)["site_activity"].stringValue == ""{
                return 0
            }
            else{
               return 70
            }
           
        }
        else {
           if JSON(self.mainDict)["start_time"].stringValue == ""  && JSON(self.mainDict)["stop_time"].stringValue == ""{
                return 0
            }
            else{
                return 70
            }
            
        }
        }

    //MARK:- fetchDetails function
    func  fetchDetails(){
         let parentVC = self.parent as! ProjectDetailsViewController
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid": projectId,"table":"site_activity","date": parentVC.strProjectDate]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            self.mainDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
            
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "SiteActivity", strDate: parentVC.strProjectDate, projectID: projectId) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false)
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false)
                
            }
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    
}
extension  ShowSiteInfoViewController : showSiteActivityDelegates{
    
    func  showSiteActivityInfo(){
        if Network.isConnectedToNetwork() == true {
            self.getOfflineData()
        }
        else{
            self.getCoreData()
            
        }
        
    }
}

