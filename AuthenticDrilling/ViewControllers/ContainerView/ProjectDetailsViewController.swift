//
//  ProjectDetailsViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-30.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit

  protocol showTravelInfoDelegates {
 func  showTravelInfo()
   }
protocol showRentalsDelegates {
    func  showRentalsInfo()
}
protocol showMiscellaneousDelegates {
    func showMiscellaneousInfo()
}
protocol showDrillDelegates {
    func showDrillInfo()
}
protocol showSiteActivityDelegates {
    func showSiteActivityInfo()
}
protocol showAddConsumableDelegates {
    func  showAddConsumableInfo()
}
protocol showHoleCompletionDelegates {
    func  showHoleCompletionInfo()
}


class ProjectDetailsViewController: ViewController ,UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
  let arrNames = ["Travel","Hole Completion", "Add Rentals" ,"Miscellaneous" ,"Drill","Site Activities","Add Consumables"]
    var projectInformation :ProjectData?
    var strProjectDate = String()
    var indexs = 0
    var delegatesTravel : showTravelInfoDelegates?
    var delegatesRentals : showRentalsDelegates?
    var delegatesMiscellaneous : showMiscellaneousDelegates?
    var delegatesDrill : showDrillDelegates?
    var delegatesSiteActivity : showSiteActivityDelegates?
    
     var delegatesAddConsumable : showAddConsumableDelegates?
      var delegatesHoleCompletion : showHoleCompletionDelegates?
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var lblProjectName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
       self.lblProjectName.text = projectInformation?.projectName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func btnEditTapped(_ sender: Any) {
        
        if indexs == 0{
            
            let story =  UIStoryboard.init(name: "Main", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "TravelViewController") as! TravelViewController
            vc.projectInfo = projectInformation
            vc.strDate = strProjectDate
            vc.isEdit = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else if  indexs == 1{
            
            let story =  UIStoryboard.init(name: "Main", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "HoleCompletionViewController") as! HoleCompletionViewController
            vc.projectInfo = projectInformation
            vc.strDate = strProjectDate
            vc.isEdit = true
            
            let childView = self.childViewControllers
            for cv in childView {
                
                if let controller = cv as? HoleCompletionContainerViewController {
                    
                    if controller.arrHoleNumber.count > 0 {
                          vc.holeNumber = controller.arrHoleNumber[controller.indexs].stringValue
                    }
                    else{
                        vc.holeNumber = ""
                    }
                    print(vc.holeNumber)
                }
                
                
            }
            
            
            self.navigationController?.pushViewController(vc, animated: true)
            }
            
            
       else if  indexs == 2{
            
            let story =  UIStoryboard.init(name: "Main", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "AddRentalViewController") as! AddRentalViewController
            vc.projectInfo = projectInformation
            vc.strDate = strProjectDate
             vc.isEdit = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else if  indexs == 3{
            
            let story =  UIStoryboard.init(name: "Main", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "MiscellaneousInfoViewController") as! MiscellaneousInfoViewController
            vc.projectInfo = projectInformation
            vc.strDate = strProjectDate
            
            vc.isEdit = true
        self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        else if  indexs == 4{
            
            let story =  UIStoryboard.init(name: "Main", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "DrillViewController") as! DrillViewController
            vc.projectInfo = projectInformation
            vc.strDate = strProjectDate
            
              vc.isEdit = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else if  indexs == 5{
            
            let story =  UIStoryboard.init(name: "Main", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "SiteActivitiesViewController") as! SiteActivitiesViewController
            vc.projectInfo = projectInformation
            vc.strDate = strProjectDate
            vc.isEdit = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else if  indexs == 6{
            
            let story =  UIStoryboard.init(name: "Main", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "AddConsumablesViewController") as! AddConsumablesViewController
            vc.projectInformation = projectInformation
            vc.dateString = strProjectDate
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        
    }
    
    
    func  changeChildViewControllersWithIndex(index : Int){
        if index == 0{
            self.delegatesTravel?.showTravelInfo()
        }
        else if  index == 1{
            self.delegatesHoleCompletion?.showHoleCompletionInfo()
        }
        else if  index == 2{
            self.delegatesRentals?.showRentalsInfo()
        }
        else if  index == 3{
            self.delegatesMiscellaneous?.showMiscellaneousInfo()
        }
        else if  index == 4{
            self.delegatesDrill?.showDrillInfo()
        }
        else if  index == 5{
            self.delegatesSiteActivity?.showSiteActivityInfo()
        }
        else if index == 6{
            self.delegatesAddConsumable?.showAddConsumableInfo()
        }

        
    }
    
    
    
    //MARK:- SrollView Delegates
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == self.scrollView{
            let indexOfPage = scrollView.contentOffset.x / scrollView.frame.size.width
            
            self.collectionView.scrollToItem(at: IndexPath.init(row: Int(indexOfPage), section: 0  ), at: .centeredHorizontally, animated: true)
            indexs = Int(indexOfPage)
            self.collectionView.reloadData()
            
            self.changeChildViewControllersWithIndex(index:   indexs)
        }
        }
      
    //MARK:- CollectionView Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrNames.count
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cvCell", for: indexPath)
        
        let lbl = cell.viewWithTag(1) as! UILabel
        lbl.text = self.arrNames[indexPath.row]
        if indexs == indexPath.row {
            
         lbl.textColor = appColor
        }
       else {
            lbl.textColor = UIColor.black
            
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        

        let multiplier = CGFloat(indexPath.row)
        let cg = self.scrollView.frame.size.width * multiplier
        scrollView.scrollRectToVisible(CGRect.init(x: cg, y: 0, width: self.scrollView.frame.size.width, height: self.scrollView.frame.size.height), animated: true)
     
       
        self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
          indexs = indexPath.row
          self.collectionView.reloadData()
        
         self.changeChildViewControllersWithIndex(index:   indexs)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        
        return CGSize(width: 120, height: 44)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    
}

