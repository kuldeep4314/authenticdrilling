//
//  ShowMiscellaneousInfoViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-12-18.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//


import UIKit
import CoreData
class ShowMiscellaneousInfoViewController: ViewController,UITableViewDataSource,UITableViewDelegate {
    
    let arrayKeysHeaders = ["waterfrom_shop","water_bulk","subcordinates","upgrade_ppe"]
    let arrName = ["Water from shop","Water-Bulk" ,"Subcontractors","Upgrade PPE"]
    @IBOutlet weak var tableView: UITableView!
    var sessions: [NSManagedObject] = []
    var mainDict =  [String: Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        let parentVC = self.parent as! ProjectDetailsViewController
        
        parentVC.delegatesMiscellaneous = self
        if parentVC.indexs == 3{
            if Network.isConnectedToNetwork() == true {
                self.getOfflineData()
            }
            else{
                self.getCoreData()
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    func getOfflineData(){
        
        let offlineDetails = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Miscellaneous")
        print(offlineDetails)
        if offlineDetails.count > 0 {
            let parameters : Parameters = ["action": "miscellaneous_category" ,"data" :  offlineDetails ]
            
            self.syncOfflineMultipleApi(parameter: parameters)
            
        }
        else{
            self.fetchDetails()
        }
        
    }
    
    //MARK: - SyncOfflineMultipleApi
    func syncOfflineMultipleApi(parameter :Parameters){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(projectUrlOffline, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            ProjectsOffline.sharedInstance.updateCoredataTables(tables: "Miscellaneous")
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            self.getCoreData()
            print(error.localizedDescription)
        }
    }
    
    
    //MARK:- TableView Data Source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrName.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MiscellaneousCell
            cell.lblName.text = arrName[indexPath.section]
        
                cell.btnRight.setImage(#imageLiteral(resourceName: "imgTick"), for: .normal)
           
            
            return cell
        
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         if  JSON(self.mainDict)[arrayKeysHeaders[indexPath.section]].stringValue == "1"{
             return 44
        }
         else{
            return 0
        }
       
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "header")
        
        return cell?.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if  JSON(self.mainDict)[arrayKeysHeaders[section]].stringValue == "1"{
            return 20
        }
        else{
            return 0
            
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
       
        
    }
    //MARK:- Update Core Data
    func updateCoredata(isOffline : Bool , edit : Bool , isShowAlert : Bool){
        let parentVC = self.parent as! ProjectDetailsViewController
        let managedContext = CoreDataStack.managedObjectContext
        let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Miscellaneous")
        updateFetch.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)", projectId , parentVC.strProjectDate)
        do {
            let results = try managedContext.fetch(updateFetch) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                
                // In my case, I only updated the first item in results
                let session = results![0]
                
                let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
                session.setValue(data1, forKey: "dataDetails")
                
                session.setValue(parentVC.strProjectDate, forKey: "date")
                session.setValue(projectId, forKey: "projectid")
                session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                session.setValue(isOffline, forKey: "isOffline")
                session.setValue(edit, forKey: "edit")
                
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try managedContext.save()
            
            
            if isShowAlert == true {
                self.navigationController?.popViewController(animated: true)
                appDelegates.window?.rootViewController?.showAlert(messageStr:  "miscellaneous list updated successfully")
            }
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
        
    }
    
    
    //MARK:- Save Core Data Function
    func saveCoredata(isOffline : Bool,edit: Bool , msg : String , isShow : Bool){
         let parentVC = self.parent as! ProjectDetailsViewController
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "Miscellaneous",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
        session.setValue(data1, forKey: "dataDetails")
        
        session.setValue(parentVC.strProjectDate, forKey: "date")
        session.setValue(projectId, forKey: "projectid")
        session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
        session.setValue(isOffline, forKey: "isOffline")
        session.setValue(edit, forKey: "edit")
        
        
        CoreDataStack.saveContext()
        
        
        if isShow == true{
            self.navigationController?.popViewController(animated: true)
            appDelegates.window?.rootViewController?.showAlert(messageStr: msg)
        }
        
    }
    //MARK:- Get Core Data Function
    func getCoreData() {
          let parentVC = self.parent as! ProjectDetailsViewController
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Miscellaneous")
        
        fetchRequest.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)", projectId , parentVC.strProjectDate)
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            for session in sessions{
                var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                
                dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                
                dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                
                self.mainDict = dictionary
                
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    
    //MARK:- fetchDetails function
    func  fetchDetails(){
          let parentVC = self.parent as! ProjectDetailsViewController
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid":projectId,"table":"miscellaneous","date": parentVC.strProjectDate]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            self.mainDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
           
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "Miscellaneous", strDate: parentVC.strProjectDate, projectID:projectId) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false)
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false)
                
            }
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
               self.getCoreData()
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
}
extension  ShowMiscellaneousInfoViewController : showMiscellaneousDelegates{
    
    func  showMiscellaneousInfo(){
        if Network.isConnectedToNetwork() == true {
            self.getOfflineData()
        }
        else{
            self.getCoreData()
        }
        
    }
}
