//
//  ShowDrillInfoViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-12-20.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//


import UIKit
import CoreData
class ShowDrillInfoViewController: ViewController ,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate , UITextViewDelegate{
    
    let arrayImagesLeft = [#imageLiteral(resourceName: "imgTimer"),#imageLiteral(resourceName: "imgBitSize"),#imageLiteral(resourceName: "imgMethods")]
    var arraySampleData = [[String : Any]]()
    var isEdit = Bool()
   
    let arrayName = ["Total Time", "Bit Size" ," Method" ,"New","OFFSET","REAM","CAVE","Hole Number","","Total Footage"]
    
    let arrayKeys = ["","bitsize","method","new" ,"offset","ream","cave","hole_number","","total_footage"]

    var sessions: [NSManagedObject] = []
   // var strDate = String()
    //var projectInfo :ProjectData?
    
    var mainDict =  [String: Any]()
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
       
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
        let parentVC = self.parent as! ProjectDetailsViewController
        
     parentVC.delegatesDrill = self
        if parentVC.indexs == 4{
            if Network.isConnectedToNetwork() == true {
                self.getOfflineData()
            }
            else{
                self.getCoreData()
            }
        }
        
    }
    
    
    func getOfflineData(){
        
        let offlineDetails = ProjectsOffline.sharedInstance.getOfflineDrillTablesData(tables: "Drill")
        print(offlineDetails)
        if offlineDetails.count > 0 {
            let parameters : Parameters = ["action": "drill_category" ,"data" :  offlineDetails ]
            
            self.syncOfflineMultipleApi(parameter: parameters)
            
        }
        else{
            self.fetchDetails()
        }
        
    }
    
    //MARK: - SyncOfflineMultipleApi
    func syncOfflineMultipleApi(parameter :Parameters){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(projectUrlOffline, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            ProjectsOffline.sharedInstance.updateCoredataTables(tables: "Drill")
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            self.getCoreData()
            print(error.localizedDescription)
        }
    }
    
    
    
    
    
    //MARK:- Update Core Data
    func updateCoredata(isOffline : Bool , edit : Bool , isShowAlert : Bool){
        let parentVC = self.parent as! ProjectDetailsViewController
        let managedContext = CoreDataStack.managedObjectContext
        let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Drill")
        updateFetch.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)", projectId , parentVC.strProjectDate)
        do {
            let results = try managedContext.fetch(updateFetch) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                
                // In my case, I only updated the first item in results
                let session = results![0]
                let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
                let dataSample: Data = NSKeyedArchiver.archivedData(withRootObject: self.arraySampleData)
                
                session.setValue(dataSample, forKey: "sample")
                session.setValue(data1, forKey: "dataDetails")
                session.setValue(parentVC.strProjectDate, forKey: "date")
                session.setValue( projectId, forKey: "projectid")
                session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                session.setValue(isOffline, forKey: "isOffline")
                session.setValue(edit, forKey: "edit")
                
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try managedContext.save()
            
            
            if isShowAlert == true {
                self.navigationController?.popViewController(animated: true)
                appDelegates.window?.rootViewController?.showAlert(messageStr:  "drill list updated successfully")
            }
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
        
    }
    
    //MARK:- Save Core Data Function
    func saveCoredata(isOffline : Bool,edit: Bool , msg : String , isShow : Bool){
       
        let parentVC = self.parent as! ProjectDetailsViewController
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "Drill",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
        
        let dataSample: Data = NSKeyedArchiver.archivedData(withRootObject: self.arraySampleData)
        
        session.setValue(dataSample, forKey: "sample")
        
        session.setValue(data1, forKey: "dataDetails")
        
        session.setValue(parentVC.strProjectDate, forKey: "date")
        session.setValue(projectId, forKey: "projectid")
        session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
        session.setValue(isOffline, forKey: "isOffline")
        session.setValue(edit, forKey: "edit")
        
        
        CoreDataStack.saveContext()
        
        
        if isShow == true{
            self.navigationController?.popViewController(animated: true)
            appDelegates.window?.rootViewController?.showAlert(messageStr: msg)
        }
        
    }
    //MARK:- Get Core Data Function
    func getCoreData() {
        let parentVC = self.parent as! ProjectDetailsViewController
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Drill")
        
        fetchRequest.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , parentVC.strProjectDate)
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            for session in sessions{
                var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                
                arraySampleData = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "sample") as! Data) as? [[String : Any]] ?? [[String : Any]]()
                
                dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                
                dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                
                self.mainDict = dictionary
                
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    
    
    
    
    
    
    //MARK:- TableView Data Source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 11
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  section == 8{
            return self.arraySampleData.count
        }
        return 1
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.section == 0 {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell11", for: indexPath) as! DrillCell
            cell.txtTilte.text = arrayName[indexPath.section]
            
            cell.txtTilte.leftview(image: arrayImagesLeft[indexPath.section])
            cell.txtValue.rightVC()
            
            
             let t1 =   Comman.timeInterval(date: JSON(self.mainDict)["date"].stringValue, startTime: JSON(self.mainDict)["start_time"].stringValue, stopTime: JSON(self.mainDict)["stop_time"].stringValue)
            
            cell.txtValue.text = t1
            
            
            return cell
        }
        
        
        
      else  if  indexPath.section > 0 &&  indexPath.section < 3 {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell11", for: indexPath) as! DrillCell
            cell.txtTilte.text = arrayName[indexPath.section]
            
            cell.txtTilte.leftview(image: arrayImagesLeft[indexPath.section])
            cell.txtValue.rightVC()
            
            cell.txtValue.text = JSON(mainDict)[arrayKeys[indexPath.section]].stringValue
        
          
            return cell
        }
            
        else  if indexPath.section > 2 &&  indexPath.section < 7 {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DrillCell
            cell.txtName.text = arrayName[indexPath.section]
            cell.txtName.isUserInteractionEnabled = false
            cell.txtName.leftview()
          
                cell.txtName.rightview(Img: #imageLiteral(resourceName: "imgTick"))
            
            return cell
        }
        else  if  indexPath.section == 7 || indexPath.section == 9  {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell11", for: indexPath) as! DrillCell
            cell.txtTilte.text = arrayName[indexPath.section]
            cell.txtValue.tag = indexPath.section
            cell.txtValue.text = JSON(mainDict)[arrayKeys[indexPath.section]].stringValue
            if indexPath.section == 7 {
                cell.txtTilte.leftview(image: #imageLiteral(resourceName: "imgHole"))
            }
            else  {
                cell.txtTilte.leftview()
                
            }
            
            cell.txtValue.keyboardType = .numberPad
            cell.txtValue.rightVC()
            cell.txtValue.inputView = nil
            cell.txtValue.delegate = self
            return cell
        }
            
        else  if indexPath.section == 8 {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as! DrillCell
            cell.txtNumber.text = "Number"
            cell.txtType.text = "Type"
            cell.txtType.leftview(image: #imageLiteral(resourceName: "imgCalculator"))
            cell.txtNumber.leftview(image: #imageLiteral(resourceName: "imgType"))
            
            cell.txtNumberValue.text = JSON(arraySampleData[indexPath.row])["number"].stringValue
            cell.txtTypeValue.text = JSON(arraySampleData[indexPath.row])["type"].stringValue
            
            
            
            cell.txtNumberValue.rightVC()
            cell.txtTypeValue.rightVC()
            
            
            
            return cell
        }
            
            
        else {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! DrillCell
            cell.txtView.text = JSON(self.mainDict)["notes"].stringValue.base64Decoded()
            
            
            if  cell.txtView.text == "" {
                cell.txtView.text = "Enter your notes"
                cell.txtView.textColor = UIColor.lightGray
            }
            else{
                cell.txtView.textColor = UIColor.black
            }
           
            cell.txtView.delegate = self
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 8{
            return 120
        }
            
      else  if indexPath.section > 2 &&  indexPath.section < 7{
            if JSON(self.mainDict)[arrayKeys[indexPath.section]].stringValue == "1"{
                return 44
            }
            else{
                  return 0
            }
        }
           else if  indexPath.section == 7 || indexPath.section == 9
        {
            if JSON(self.mainDict)[arrayKeys[indexPath.section]].stringValue != ""  {
                return 44
            }
            else{
                return 0
            }
            
        }
            
        else if indexPath.section == 10{
            let value  =  JSON(self.mainDict)["notes"].stringValue.base64Decoded()
            
            if value == "Enter your notes" || value == "" {
                return 0
            }
            else{
                return 100
            }
        }
        else if indexPath.section == 0 {
    
            
            if JSON(self.mainDict)["start_time"].stringValue == ""  && JSON(self.mainDict)["stop_time"].stringValue == ""{
                return 0
            }
            else{
                return 44
            }
            
        }
            
            
       else   if indexPath.section > 0 && indexPath.section < 3 {
          
       let value = JSON(mainDict)[arrayKeys[indexPath.section]].stringValue
            
            if value == "" {
                 return 0
            }
            else{
                 return 44
            }
            
        }
        
        
        
        return 44
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! DrillCell
        if section == 8 {
            cell.lblHeader.isHidden = false
            cell.lblHeader.text =  "SAMPLES DATA"
       
        }
       else if section == 10 {
            cell.lblHeader.isHidden = false
            cell.lblHeader.text =  "Notes"
            
        }
        else{
           
            cell.lblHeader.isHidden = true
        }
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        
            if section == 0 {
                
                if JSON(self.mainDict)["start_time"].stringValue == ""  && JSON(self.mainDict)["stop_time"].stringValue == ""{
                    return 0
                }
                else{
                    return 20
                }
                
            }
        
       else if  section > 0 && section < 3 {
            
            let value = JSON(mainDict)[arrayKeys[section]].stringValue
            
            if value == "" {
                return 0
            }
            else{
                return 20
            }
            
        }
        
       else   if section > 2 &&  section < 7{
            if JSON(self.mainDict)[arrayKeys[section]].stringValue == "1"{
                return 20
            }
            else{
                return 0
            }
        }
         else  if section == 7 || section == 9
         {
            if JSON(self.mainDict)[arrayKeys[section]].stringValue != ""  {
                return 20
            }
            else{
                return 0
            }
            
         }
         
      else if section == 8 {
            if self.arraySampleData.count == 0{
                return 0
            }
            else{
                  return 55
            }
          
        }
            
          else if section == 10{
            let value  =  JSON(self.mainDict)["notes"].stringValue.base64Decoded()
            
            if value == "Enter your notes" || value == "" {
                return 0
            }
            else{
                return 44
            }
          }
            
          else{
             return 20
        }
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
      
    }
    
    
    
 
    //MARK:- fetchDetails function
    func  fetchDetails(){
         let parentVC = self.parent as! ProjectDetailsViewController
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid": projectId,"table":"drill","date": parentVC.strProjectDate]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            self.mainDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
            let array =  json["response"]["data"]["sample_data"].arrayValue
             self.arraySampleData.removeAll()
            for i in 0..<array.count{
                
                let dict =  array[i].dictionaryObject
                self.arraySampleData.append(dict ?? [String: Any]())
            }
            print(self.arraySampleData)
            
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "Drill", strDate: parentVC.strProjectDate, projectID: projectId) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false)
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false)
                
            }
            self.getCoreData()
            
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
}
extension  ShowDrillInfoViewController : showDrillDelegates{
    
    func  showDrillInfo(){
        if Network.isConnectedToNetwork() == true {
            self.getOfflineData()
        }
        else{
            self.getCoreData()
        }
        
    }
}

