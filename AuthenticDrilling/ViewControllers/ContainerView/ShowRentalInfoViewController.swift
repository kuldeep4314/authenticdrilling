//
//  ShowRentalInfoViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-12-18.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//



import UIKit
import CoreData
class ShowRentalInfoViewController: ViewController,UITableViewDataSource,UITableViewDelegate ,UITextViewDelegate{
    
     var sessions: [NSManagedObject] = []
    let arrayImagesNames = [#imageLiteral(resourceName: "imgTruck"),#imageLiteral(resourceName: "imgTruck"),#imageLiteral(resourceName: "imgAir"),#imageLiteral(resourceName: "imgStreamCleaner"),#imageLiteral(resourceName: "imgLightPlant"),#imageLiteral(resourceName: "imgGenerator"),#imageLiteral(resourceName: "imgConcrete"),#imageLiteral(resourceName: "ImgConcreateSaw") ,#imageLiteral(resourceName: "imgJackHammer"),#imageLiteral(resourceName: "imgTrashPump"),#imageLiteral(resourceName: "imgTrashPump") ,#imageLiteral(resourceName: "imgTrashPump"),#imageLiteral(resourceName: "imgSkidSteer"),#imageLiteral(resourceName: "imgTractor"),#imageLiteral(resourceName: "imgTrailer"),#imageLiteral(resourceName: "imgOther"),#imageLiteral(resourceName: "imgUser")]
    let arrayNames = ["Support Truck #1","Support Truck #2","Air Compressor","Steam Cleaner","Light Plant","Generator","Concrete Core Machine","Concrete Saw" ,"Jackhammer","Trash Pump","Pump (Notes for type)" ,"","Skidsteer","Tractor","Dump Trailer","Other"]
    var mainDict =  [String: Any]()
    let arrayKeys = ["support_truck1","support_truck2","air_compressor","steam_cleaner","light_plant","generator","concrete_core","concrete_saw" ,"jackhammer","trash_pump","pump_notetype" ,"notes","skidsteer","trackor","dump_trailer","other"]
    
  
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
  
        let parentVC = self.parent as! ProjectDetailsViewController
        
       parentVC.delegatesRentals = self
        if parentVC.indexs == 2{
            if Network.isConnectedToNetwork() == true {
                self.getOfflineData()
            }
            else{
                self.getCoreData()
            }

        }
        
        
    }
    func getOfflineData(){
        
        let offlineDetails = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Rental")
        print(offlineDetails)
        if offlineDetails.count > 0 {
            let parameters : Parameters = ["action": "rental_category" ,"data" :  offlineDetails ]
            print(parameters)
            self.syncOfflineMultipleApi(parameter: parameters)
            
        }
        else{
            self.fetchDetails()
        }
        
    }
    //MARK: - SyncOfflineMultipleApi
    func syncOfflineMultipleApi(parameter :Parameters){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(projectUrlOffline, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            ProjectsOffline.sharedInstance.updateCoredataTables(tables: "Rental")
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            self.getCoreData()
            print(error.localizedDescription)
        }
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:- Update Core Data
    func updateCoredata(isOffline : Bool , edit : Bool , isShowAlert : Bool){
        
        let parentVC = self.parent as! ProjectDetailsViewController
        
     
    
        let managedContext = CoreDataStack.managedObjectContext
        let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Rental")
        updateFetch.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , parentVC.strProjectDate)
        do {
            let results = try managedContext.fetch(updateFetch) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                
                // In my case, I only updated the first item in results
                let session = results![0]
                
                let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
                session.setValue(data1, forKey: "dataDetails")
                
                session.setValue(parentVC.strProjectDate, forKey: "date")
                session.setValue(projectId, forKey: "projectid")
                session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                session.setValue(isOffline, forKey: "isOffline")
                session.setValue(edit, forKey: "edit")
                
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try managedContext.save()
            
            
            if isShowAlert == true {
                self.navigationController?.popViewController(animated: true)
                appDelegates.window?.rootViewController?.showAlert(messageStr:  "rental list updated successfully")
            }
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
        
    }
    
    
    //MARK:- Save Core Data Function
    func saveCoredata(isOffline : Bool,edit: Bool , msg : String , isShow : Bool){
        
         let parentVC = self.parent as! ProjectDetailsViewController
        
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "Rental",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
        session.setValue(data1, forKey: "dataDetails")
        
        session.setValue(parentVC.strProjectDate, forKey: "date")
        session.setValue(projectId, forKey: "projectid")
        session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
        session.setValue(isOffline, forKey: "isOffline")
        session.setValue(edit, forKey: "edit")
        
        
        CoreDataStack.saveContext()
        
        
        if isShow == true{
            self.navigationController?.popViewController(animated: true)
            appDelegates.window?.rootViewController?.showAlert(messageStr: msg)
        }
        
    }
    //MARK:- Get Core Data Function
    func getCoreData() {
           let parentVC = self.parent as! ProjectDetailsViewController
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Rental")
        
        fetchRequest.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , parentVC.strProjectDate)
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            for session in sessions{
                var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                
                dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                
                self.mainDict = dictionary
                
            }
            
            DispatchQueue.main.async {
                
//                EmptyDataSet.sharedInstance.showEmptyData(title: "No Records Found!", image: #imageLiteral(resourceName: "search"), tableView: self.tableView)
                
                self.tableView.reloadData()
            }
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    //MARK:- TextView Delegates
    func textViewDidChange(_ textView: UITextView) {
        self.mainDict.updateValue(textView.text, forKey: "notes")
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        
        if textView.textColor == UIColor.lightGray
        {
            textView.text = ""
            textView.textColor = UIColor.black
            
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = "Enter your notes"
            textView.textColor = UIColor.lightGray
            mainDict.updateValue("", forKey:"notes")
        }
        else{
            mainDict.updateValue( textView.text ?? "", forKey:"notes")
        }
        
    }
    
    
    
    
    //MARK:- TableView Data Source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        
        
        
        return arrayNames.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 11{
            let cell1 =  tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! RentalCell
            cell1.txtView.text = JSON(self.mainDict)[self.arrayKeys[indexPath.section]].stringValue.base64Decoded()?.base64Decoded()
            
            cell1.txtView.delegate = self
            
            
                cell1.txtView.textColor = UIColor.black
            
            return cell1
        }
       
        else{
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RentalCell
            
            cell.lblName.text = arrayNames[indexPath.section]
            cell.imgView.image = arrayImagesNames[indexPath.section]
            
                cell.btnRight.setImage(#imageLiteral(resourceName: "imgTick"), for: .normal)
           
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if  indexPath.section == 11 {
            
          let value  =  JSON(self.mainDict)[self.arrayKeys[indexPath.section]].stringValue.base64Decoded()
            
            if value == "Enter your notes" || value == "" {
                return 0
            }
            else{
                return 100
            }
            
        }
        else{
            let value  = JSON(self.mainDict)[self.arrayKeys[indexPath.section]].stringValue
            
            if value == "1"
            {
                return 44
            }
            else{
                return 0
            }
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! DrillCell
        if section == 11 {
            cell.lblHeader.isHidden = false
            cell.lblHeader.text =  "Notes"
            
        }
        else{
            
            cell.lblHeader.isHidden = true
        }
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if  section == 11 {
            
            let value  =  JSON(self.mainDict)[self.arrayKeys[section]].stringValue.base64Decoded()
            
            if value == "Enter your notes" || value == ""{
                return 0
            }
            else{
                return 40
            }
            
        }
        else{
            let value  = JSON(self.mainDict)[self.arrayKeys[section]].stringValue
            
            if value == "1"
            {
                return 20
            }
            else{
                return 0
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    
    }
  
    
    //MARK:- fetchDetails function
    func  fetchDetails(){
       let parentVC = self.parent as! ProjectDetailsViewController
        
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid": projectId,"table":"rental","date": parentVC.strProjectDate]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            self.mainDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "Rental", strDate: parentVC.strProjectDate, projectID: projectId) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false)
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false)
                
            }
            self.getCoreData()
            
            
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    
    
    
}
extension  ShowRentalInfoViewController : showRentalsDelegates{
    
    func  showRentalsInfo(){
        if Network.isConnectedToNetwork() == true {
            self.getOfflineData()
        }
        else{
            self.getCoreData()
        }

        
    }
}
