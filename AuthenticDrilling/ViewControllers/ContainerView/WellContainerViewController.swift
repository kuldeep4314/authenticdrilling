//
//  WellContainerViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-28.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit
import CoreData
class WellContainerViewController: ViewController ,geoLogicalDelegates,UITextFieldDelegate,riserDelegates,screenDelegates,filterDelegates,groutingDelegates{
    
    @IBOutlet weak var txtWellTitle: UITextField!
    @IBOutlet weak var txtLocationTitle: UITextField!
    @IBOutlet weak var txtStaticWaterTitle: UITextField!
    @IBOutlet weak var containerHeight: NSLayoutConstraint!
    @IBOutlet weak var txtStaticWater: UITextField!
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var txtWell: UITextField!
    
    @IBOutlet weak var btnGeoLogical: UIButton!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var btnRiser: UIButton!
    
    @IBOutlet weak var btnScreen: UIButton!
    
    @IBOutlet weak var btnGrouting: UIButton!
    @IBOutlet weak var btnFilter: UIButton!
    
    
    var strDate = String()
    var projectInfo :ProjectData?
    
    var mainDict =  [String: Any]()
    
    var textFieldKeys = ["well1","location","static_water_level"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
        print(self.mainDict)
        
        if Network.isConnectedToNetwork() == true {
            self.getOfflineData()
        }
        
        // Do any additional setup after loading the view.
    }
    
    func getOfflineData(){
        
        let offlineDetails = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Installation")
        print(offlineDetails)
        if offlineDetails.count > 0 {
            let parameters : Parameters = ["action": "hc_installation" ,"data" :  offlineDetails ]
            
            self.syncOfflineMultipleApi(parameter: parameters)
            
        }
    
    }
    
    
    //MARK: - SyncOfflineMultipleApi
    func syncOfflineMultipleApi(parameter :Parameters){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(holeCompletionOfflineUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            ProjectsOffline.sharedInstance.updateCoredataTables(tables: "Installation")
           
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
        
            print(error.localizedDescription)
        }
    }
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.btnGeologicalTapped(self.btnGeoLogical)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnSaveTapped(_ sender: UIButton) {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
                if aViewController is InstallationViewController {
                    let vc  = aViewController as! InstallationViewController
                 
                    vc.registerWellValues(dict: self.mainDict)
                    self.navigationController!.popToViewController(aViewController, animated: true)
                
            }
        
    }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
                mainDict.updateValue(textField.text ?? "", forKey: textFieldKeys[textField.tag - 20])
                print(mainDict)
    }
    
    func setUpView(){
        self.txtLocation.rightview(Img: #imageLiteral(resourceName: "imgLocation1"))
        self.txtWell.delegate = self
        self.txtLocation.delegate = self
        txtStaticWater.delegate = self
        self.txtStaticWaterTitle.leftview(image: #imageLiteral(resourceName: "imgWater"))
        self.txtStaticWaterTitle.addshodowToTextField()
        self.txtLocationTitle.addshodowToTextField()
        self.txtWellTitle.addshodowToTextField()
        self.btnGeoLogical.addshodowToButton()
        self.btnRiser.addshodowToButton()
        self.btnScreen.addshodowToButton()
        self.btnFilter.addshodowToButton()
        self.btnGrouting.addshodowToButton()
        self.txtWell.text = JSON(self.mainDict)["well1"].stringValue
          self.txtLocation.text = JSON(self.mainDict)["location"].stringValue
          self.txtStaticWater.text = JSON(self.mainDict)["static_water_level"].stringValue
        txtLocationTitle.leftview()
        
        txtWellTitle.leftview()
        
        txtWell.rightVC()
        
        txtStaticWater.rightVC()
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- IBAction Functions
    
    @IBAction func btnGrouting(_ sender: Any) {
        self.removeViewsAndSetColor(index: btnGrouting.tag)
        self.containerHeight.constant = 350
        
        let vc = Grouting.init(frame: CGRect.init(x: 0, y: 0, width: containerView.frame.size.width, height: containerView.frame.size.height))
        vc.tag = 5
        
        vc.delegates = self
        vc.groutingDict = JSON(self.mainDict)["grouting_record"].dictionaryObject ?? [String: Any]()
        vc.setTextFieldValues()
        containerView.addSubview(vc)
        
        
    }
    @IBAction func btnFilterTapped(_ sender: Any) {
        self.removeViewsAndSetColor(index: btnFilter.tag)
        self.containerHeight.constant = 350
        let vc = Filter.init(frame: CGRect.init(x: 0, y: 0, width: containerView.frame.size.width, height: containerView.frame.size.height))
        vc.tag = 4
        vc.delegates = self
        vc.filterDict = JSON(self.mainDict)["filter_pack"].dictionaryObject ?? [String: Any]()
        vc.setTextFieldValues()
        containerView.addSubview(vc)
    }
    @IBAction func btnScreenTapped(_ sender: Any) {
        self.removeViewsAndSetColor(index: btnScreen.tag)
        self.containerHeight.constant = 350
        let vc = Screen.init(frame: CGRect.init(x: 0, y: 0, width: containerView.frame.size.width, height: containerView.frame.size.height))
        vc.tag = 3
        vc.delegates = self
        vc.screenDict = JSON(self.mainDict)["screen"].dictionaryObject ?? [String: Any]()
        vc.setTextFieldValues()
        containerView.addSubview(vc)
    }
    @IBAction func btnGeologicalTapped(_ sender: Any) {
        self.removeViewsAndSetColor(index: btnGeoLogical.tag)
        self.containerHeight.constant = 350
        let vc = GeoLogicalInfo.init(frame: CGRect.init(x: 0, y: 0, width: containerView.frame.size.width, height: containerView.frame.size.height))
        vc.tag = 1
        vc.delegates = self
        vc.geologicalDict = JSON(self.mainDict)["geographical_info"].dictionaryObject ?? [String: Any]()
        vc.setTextFieldValues()
        containerView.addSubview(vc)
    }
    @IBAction func btnRiserTapped(_ sender: Any) {
        self.removeViewsAndSetColor(index: btnRiser.tag)
        self.containerHeight.constant = 350
        let vc = Riser.init(frame: CGRect.init(x: 0, y: 0, width: containerView.frame.size.width, height: containerView.frame.size.height))
        vc.tag = 2
        
        vc.delegates = self
        vc.riserDict = JSON(self.mainDict)["riser"].dictionaryObject ?? [String: Any]()
        vc.setTextFieldValues()
        containerView.addSubview(vc)
    }
    
    func removeViewsAndSetColor(index : Int){
        for i in 1..<6 {
            let vc = self.view.viewWithTag(i)
            let btn = self.view.viewWithTag(10 + i ) as! UIButton
            if 10 + i == index {
                btn.setTitleColor(appColor, for: .normal)
            }
            else{
                btn.setTitleColor(UIColor.black, for: .normal)
            }
            vc?.removeFromSuperview()
        }
        
    }
    
    //MARK:- Custom Delegates Methods
    
    func setGeoInformation(geoDict: [String: Any]){
     
        self.mainDict.updateValue(geoDict, forKey: "geographical_info" )
        print(self.mainDict)
        
    }
    func setRiserInformation(riserDict: [String: Any]){
        self.mainDict.updateValue(riserDict, forKey: "riser" )
        print(self.mainDict)
        
    }
    func setScreenInformation(screenDict: [String: Any]){
          self.mainDict.updateValue(screenDict, forKey: "screen" )
          print(self.mainDict)
    }
    func filterInformation(filterDict: [String: Any]){
        self.mainDict.updateValue(filterDict, forKey: "filter_pack" )
        print(self.mainDict)
    }
    func groutingInformation(groutingDict: [String: Any]){
        self.mainDict.updateValue(groutingDict, forKey: "grouting_record" )
        print(self.mainDict)
        
    }
    
    
}

