//
//  MiscellaneousSuppliesViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-30.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//


import UIKit
import CoreData
class MiscellaneousSuppliesViewController: ViewController,UITableViewDelegate,UITableViewDataSource ,UITextFieldDelegate,UITextViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource{
    let arrNames = ["Shale Trap","Centralizers"]
    let arrayKeysHeaders = ["shaltrap","centralizers"]
    
    var dictKeyValues = ["1":"shaltrap_qty","2":"shaltrap_size","11":"centralizers_qty","12":"centralizers_size"
    ]
    var sessions: [NSManagedObject] = []
      var dropDownList = [JSON]()
    var strDate = String()
    var projectInfo :ProjectData?
    
    var mainDict =  [String: Any]()
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
        
        if  let decoded  = standard.value(forKey: "catData") as? [String : Any]{
            
            dropDownList  = JSON(decoded)["Miscellaneous"].arrayValue
            print(dropDownList)
            
        }
        
        tableView.tableFooterView = UIView()
        if Network.isConnectedToNetwork() == true {
            self.getOfflineData()
        }
        else{
            self.getCoreData()
        }
    }
    func setUpView(){
        
        
    }
    
    //MARK:- TextView Delegates
    func textViewDidChange(_ textView: UITextView) {
        
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        
        if textView.textColor == UIColor.lightGray
        {
            textView.text = ""
            textView.textColor = UIColor.black
            
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = "Enter your notes"
            textView.textColor = UIColor.lightGray
            mainDict.updateValue(
                "", forKey:"notes")
        }
        else{
            mainDict.updateValue( textView.text ?? "", forKey:"notes")
        }
        //  self.tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    print(dictKeyValues[String(textField.tag)]!)
    mainDict.updateValue(textField.text ?? "", forKey:dictKeyValues[String(textField.tag)]!)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        if JSON(self.mainDict)[arrayKeysHeaders[sender.view!.tag]].stringValue == "1" {
            self.mainDict.updateValue("0", forKey: arrayKeysHeaders[sender.view!.tag])
            
        }
        else{
            self.mainDict.updateValue("1", forKey: arrayKeysHeaders[sender.view!.tag])
        }
        
        self.mainDict.updateValue("", forKey: dictKeyValues[String(sender.view!.tag * 10 + 1 )]!)
        self.mainDict.updateValue("", forKey: dictKeyValues[String(sender.view!.tag * 10 + 2 )]!)
        
        self.tableView.reloadData()
    }
    @objc func btnSaveTapped(sender : UIButton){
        
    
        
        
        
        if Network.isConnectedToNetwork() == true {
            
            self.addSampleSupplyApiCall(isOffline: false, edit: false)
        }
        else  if Network.isConnectedToNetwork() == false {
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "MiscellaneousSupply", strDate: self.strDate, projectID: projectId) == true
            {
                self.updateCoredata(isOffline: true, edit: true, isShowAlert: true)
            }
                
            else{
                ProjectsOffline.sharedInstance.getOfflineDates(isOffline: true, strDate: self.strDate, projectID: projectId)
                self.saveCoredata(isOffline: true, edit: false, msg: "Add consumption Miscellaneous list added successfully", isShow: true)
            }
        }
        
        
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
   
    func getOfflineData(){
        
        let offlineDetails = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "MiscellaneousSupply")
        print(offlineDetails)
        if offlineDetails.count > 0 {
            let parameters : Parameters = ["action": "ac_miscellaneous" ,"data" :  offlineDetails ]
             self.syncOfflineMultipleApi(parameter: parameters)
            
        }
        else{
            self.fetchDetails()
        }
        
    }
    
    
    
    //MARK: - SyncOfflineMultipleApi
    func syncOfflineMultipleApi(parameter :Parameters){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(consumableUrlOffline, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            ProjectsOffline.sharedInstance.updateCoredataTables(tables: "MiscellaneousSupply")
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
              self.getCoreData()
            print(error.localizedDescription)
        }
    }

    
    
    
    
    
    
    
    
    
    //MARK:- Update Core Data
    func updateCoredata(isOffline : Bool , edit : Bool , isShowAlert : Bool){
        
        let managedContext = CoreDataStack.managedObjectContext
        let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "MiscellaneousSupply")
        updateFetch.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , strDate)
        do {
            let results = try managedContext.fetch(updateFetch) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                
                // In my case, I only updated the first item in results
                let session = results![0]
                
                let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
                session.setValue(data1, forKey: "dataDetails")
                
                session.setValue(strDate, forKey: "date")
                session.setValue(projectId, forKey: "projectid")
                session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                session.setValue(isOffline, forKey: "isOffline")
                session.setValue(edit, forKey: "edit")
                
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try managedContext.save()
            
            
            if isShowAlert == true {
                self.navigationController?.popViewController(animated: true)
                appDelegates.window?.rootViewController?.showAlert(messageStr:  "Add consumption Miscellaneous list updated successfully")
            }
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
        
    }
    
    
    //MARK:- Save Core Data Function
    func saveCoredata(isOffline : Bool,edit: Bool , msg : String , isShow : Bool){
        print(isOffline)
        print(edit)
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "MiscellaneousSupply",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
        session.setValue(data1, forKey: "dataDetails")
        
        session.setValue(strDate, forKey: "date")
        session.setValue(projectId, forKey: "projectid")
        session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
        session.setValue(isOffline, forKey: "isOffline")
        session.setValue(edit, forKey: "edit")
        
        
        CoreDataStack.saveContext()
        
        
        if isShow == true{
            self.navigationController?.popViewController(animated: true)
            appDelegates.window?.rootViewController?.showAlert(messageStr: msg)
        }
        
    }
    //MARK:- Get Core Data Function
    func getCoreData() {
        
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "MiscellaneousSupply")
        
        fetchRequest.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , strDate)
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            for session in sessions{
                var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                
                dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                
                dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                
                self.mainDict = dictionary
                
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    
    
    
    //MARK:- TableView Data Source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 3 {
            return 0
        }
        if section == 2 {
            return 0
        }
        else{
            if JSON(self.mainDict)[arrayKeysHeaders[section]].stringValue == "1" {
                return 1
            }
            else{
                return 0
            }
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! SupplyCell
          cell.txtUnit.rightview(Img: #imageLiteral(resourceName: "imgDropDown"))
        cell.txtQty.tag = indexPath.section * 10 + 1
        cell.txtUnit.tag = indexPath.section * 10 + 2
        
        cell.txtQty.text = JSON(self.mainDict)[dictKeyValues["\(cell.txtQty.tag )"]!].stringValue
        cell.txtUnit.text = JSON(self.mainDict)[dictKeyValues["\(cell.txtUnit.tag )"]!].stringValue
        
        cell.txtQty.rightVC()
        cell.txtQtyTitle.leftview()
        
        let picker = UIPickerView()
        picker.dataSource = self
        picker.delegate = self
        picker.tag = indexPath.section
        cell.txtUnit.inputView = picker
        
        
        cell.txtUnit.delegate = self
        cell.txtQty.delegate  = self
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        return 44
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        if section == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! SupplyCell
             cell.btnSave.addTarget(self, action: #selector(self.btnSaveTapped(sender:)), for: .touchUpInside)
            return cell.contentView
        }
            
      else  if section == 2{
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell3") as! SupplyCell
            cell.miscellTxtView.text = JSON(self.mainDict)["notes"].stringValue.base64Decoded()
            
            
            if  cell.miscellTxtView.text == "" {
                cell.miscellTxtView.text = "Enter your notes"
                cell.miscellTxtView.textColor = UIColor.lightGray
            }
            else{
                cell.miscellTxtView.textColor = UIColor.black
            }
            cell.miscellTxtView.delegate = self
            return cell.contentView
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SupplyCell
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
            
            cell.bgView.addGestureRecognizer(tap)
            cell.bgView.tag = section
            cell.bgView.isUserInteractionEnabled = true
            
            cell.bgView.addGestureRecognizer(tap)
            
            if JSON(self.mainDict)[arrayKeysHeaders[section]].stringValue == "1" {
                cell.txtName.rightview(Img: #imageLiteral(resourceName: "imgCheck"))
            }
            else{
                cell.txtName.rightview(Img: #imageLiteral(resourceName: "imgUnselect"))
            }
            
            
            cell.txtName.text = self.arrNames[section]
            
            
            
            
            return cell.contentView
        }
        
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 3{
            return 100
        }
       else if section == 2{
            return 120
        }
        else{
             return 60
        }
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK:- PickerView Data Source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dropDownList[pickerView.tag]["subcategory"].arrayValue.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dropDownList[pickerView.tag]["subcategory"][row]["name"].stringValue
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let cell = self.tableView.cellForRow(at: IndexPath.init(row: 0, section: pickerView.tag)) as! SupplyCell
        cell.txtUnit.text = dropDownList[pickerView.tag]["subcategory"][row]["name"].stringValue
    }
    
    
    
    //MARK:- AddSampleSupplyApiCall
    func addSampleSupplyApiCall(isOffline : Bool , edit : Bool ){
        let parameter  : Parameters  =  ["action": "ac_miscellaneous", "userid": standard.value(forKey: "id")!, "projectid":  projectId,"date":strDate
            , "shaltrap": JSON(self.mainDict)["shaltrap"].stringValue
            , "shaltrap_qty": JSON(self.mainDict)["shaltrap_qty"].stringValue
            , "shaltrap_size": JSON(self.mainDict)["shaltrap_size"].stringValue
            , "centralizers": JSON(self.mainDict)["centralizers"].stringValue
            , "centralizers_qty": JSON(self.mainDict)["centralizers_qty"].stringValue
            , "centralizers_size": JSON(self.mainDict)["centralizers_size"].stringValue
            , "notes": JSON(self.mainDict)["notes"].stringValue.base64Decoded() ?? ""
        ]
        
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(consumableUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  json["response"]["msg"].stringValue )
                return
            }
            
           
            DispatchQueue.main.async {
                
                
                    
                    ProjectsOffline.sharedInstance.getOfflineDates(isOffline: isOffline, strDate: self.strDate, projectID: projectId)
                    
                    
                    if ProjectsOffline.sharedInstance.getOfflineTables(tables: "Miscellaneous", strDate: self.strDate, projectID: projectId) == true {
                        self.updateCoredata(isOffline: false, edit: false, isShowAlert: true)
                    }
                    else{
                        self.saveCoredata(isOffline: isOffline, edit: edit, msg: json["response"]["msg"].stringValue, isShow: true)
                        
                    }
                
            }

            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  error.localizedDescription)
            }
            
            print(error.localizedDescription)
        }
        
    }
    
    //MARK:- fetchDetails function
    func  fetchDetails(){
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid": projectId,"table":"ac_miscellaneous","date": strDate]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            self.mainDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
            
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "MiscellaneousSupply", strDate: self.strDate, projectID: projectId) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false)
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false)
                
            }
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
}

