//
//  HoleCompletionContainerViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-12-01.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit

class HoleCompletionContainerViewController: ViewController ,UITableViewDataSource,UITableViewDelegate ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    var  arrRisers = [[String : Any]]()
    var arrScreens = [[String : Any]]()
      var Count  = 0
     var selectedIndex = -1
    var mainDict =  [String: Any]()
    var apiResponse  = 0
    
     var inclinometerKeys = ["single" ,"nested" ,"size","topcap","bottomcap"]
    
     let inclinometerNames = [ "Single" ,"Nested","Size","Top Cap" , "Bottom Cap"]
    
    var filterGroutingKeys = ["material","bag","lbs","from_ft","to_ft","gravity"]
    
    var filterGroutingNames = ["Material","No. of Bags","No. of LBS","From (ft)","To (ft)","Gravity/Tremie"]
    
    var riserScreenKeys = ["hole_diameter","pvc_screen","well_od","from_ft","to_ft"]
    
    var riserScreenNames = ["Hole Diameter","PVC/Screen","Well OD","From (ft)","To (ft)"]
    var geologicalKeys = ["depth_from","depth_to","type","grain_size","colour"]
    
    let geologicalName = ["Depth From","Depth To","Type","Grain Size","Colour"]
    
    
    var arrayInstallKeys = ["","holenumber","total_footage","well","nested","inclinometer","register_well"]
    
    let arrayInstallNames = ["Total Time","Hole Number","Total Footage","Well","Nested"]
    let arrayGroutNames = ["Total Time","Hole Number","New","Top Up","Total Footage"]
    
    
    let arrayGroutKeys = ["","holenumber","new","topup","total_footage"]
    
    
    var arrayAbandonKeys = ["well","location","static_water","material","bags","lbs","from_ft","to_ft","gravity"]
    
    let registerKey = ["well1","location","static_water_level"]
    let registerNames = ["Well #","Location","Static Water Level"]
    let arrayAbondonNames = ["Well #","Location","Static Water Level","Material","No. of Bags","No. of LBS","From(ft)","To (ft)","Gravity/Tremie"]
    
 @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    var indexs = 0
    
    var groutDict =  [String: Any]()
    var abandonDict =  [String: Any]()
    var installDict =  [String: Any]()
    
     var inclinometerDict =  [String: Any]()
    var registerDict = [String : Any]()
    var arrHoleNumber = [JSON]()
    let headerArr = ["GROUT" ,"INSTALLATION","ABANDON","LEAVE HOLE OPEN","BACKFILL CUTTINGS"]
   
    
    let abondonImages = [nil,nil,#imageLiteral(resourceName: "imgWater"),#imageLiteral(resourceName: "imgHoleDiameters"),#imageLiteral(resourceName: "imgBags"),#imageLiteral(resourceName: "imgLbs"),#imageLiteral(resourceName: "imgDepth"),#imageLiteral(resourceName: "imgDepth"),#imageLiteral(resourceName: "imgGravity")]
    
    let installImages = [#imageLiteral(resourceName: "imgTimer"),#imageLiteral(resourceName: "imgHole")]
    override func viewDidLoad() {
        super.viewDidLoad()
    tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let parentVC = self.parent as! ProjectDetailsViewController
        parentVC.delegatesHoleCompletion = self
        if parentVC.indexs == 1{
            
            
            if Network.isConnectedToNetwork() == true {
                
            self.syncOfflineHoleData()
                
             }
          
            else{
                let array =  HoleCompletionOffline.sharedInstace.getOfflineHoleNumber(prjectId: projectId, date: parentVC.strProjectDate)
                print(array)
                
                if array.count > 0 {
                    
               let dict  =   HoleCompletionOffline.sharedInstace.getOfflineHoleDictionary(arrayHole: array, prjectId: projectId, date: parentVC.strProjectDate)
                    
                    self.updateDataFromCoreData(dict: dict)
                    
                    }
                
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
        if selectedIndex  == sender.view?.tag {
            selectedIndex  = -1
        }
        else{
            selectedIndex  = (sender.view?.tag)!
        }
        self.tableView.reloadData()
    }
    
    //MARK:- TableView Data Source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if  arrHoleNumber.count > 0{
            return headerArr.count
        }
        else{
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedIndex == section {
            if section ==  headerArr.count - 1 ||  section == headerArr.count - 2 {
                
                return 0
            }
            else if  section == 0 {
                return 6
            }
            else if  section == 1 {
                return  6 + 4   + JSON(registerDict)["geographical_info"].count + 1
                
                 + JSON(registerDict)["riser"].count + 1
                
                + JSON(registerDict)["screen"].count + 1
                
               + JSON(registerDict)["filter_pack"].count + 1
                
                + JSON(registerDict)["grouting_record"].count + 1
                
                +  inclinometerKeys.count + 1
                
                  + arrRisers.count + 1
                
                   +  2  + arrScreens.count
                // change
                //  4 for well information
            }
                
        else if  section == 2 {
                return  arrayAbondonNames.count + 1
            }
            return 0
        }
        else{
             return 0
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
        
            
             if indexPath.row == 5{
                
                 let cell =  tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! HoleCompletionCell
                
                  cell.txtView.text = JSON(self.groutDict)["notes"].stringValue.base64Decoded()
                 return cell
                
            }
            
           else if indexPath.row == 2 || indexPath.row == 3{
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! HoleCompletionCell
                    cell.txtName.text = arrayGroutNames[indexPath.row]
                
                cell.txtName.rightview(Img: #imageLiteral(resourceName: "imgTick"))
                cell.txtName.leftview()
                return cell
                
            }
            else{
               let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HoleCompletionCell
                
                cell.txtValue.rightVC()
                   cell.txtTitle.text = arrayGroutNames[indexPath.row]
                cell.txtTitle.leftview()

                
                if indexPath.row == 0 {
                                  cell.txtTitle.leftview(image: #imageLiteral(resourceName: "imgTimer"))
                    let t1 =   Comman.timeInterval(date: JSON(self.groutDict)["date"].stringValue, startTime: JSON(self.groutDict)["start_time"].stringValue, stopTime: JSON(self.groutDict)["stop_time"].stringValue)
                    
                     cell.txtValue.text = t1
                    
                    
                }
               else {
                   
                    if indexPath.row == 1 {
                         cell.txtTitle.leftview(image: #imageLiteral(resourceName: "imgHole"))
                    }
                    else{
                         cell.txtTitle.leftview()
                    }
                    
                    cell.txtValue.text = JSON(groutDict)[arrayGroutKeys[indexPath.row]].stringValue
                }
                
                 return cell
            }
            }
            
        else if indexPath.section == 1 {
            
            if indexPath.row == 5 {
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! HoleCompletionCell
                
                cell.txtView.text = JSON(self.installDict)["notes"].stringValue.base64Decoded()
                return cell
                
            }
            else  if indexPath.row < 3 {
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HoleCompletionCell
                cell.txtTitle.text = arrayInstallNames[indexPath.row]
               
                
                if indexPath.row == 0 {
                    cell.txtTitle.leftview(image: installImages[indexPath.row])
                
                let t1 =   Comman.timeInterval(date: JSON(self.installDict)["date"].stringValue, startTime: JSON(self.installDict)["starttime"].stringValue, stopTime: JSON(self.installDict)["stoptime"].stringValue)
                      cell.txtValue.text = t1
                }
                else if indexPath.row == 1{
                     cell.txtTitle.leftview(image: installImages[indexPath.row])
                    cell.txtValue.text = JSON(installDict)[arrayInstallKeys[indexPath.row]].stringValue
                }
                else{
                      cell.txtTitle.leftview()
                    cell.txtValue.text = JSON(installDict)[arrayInstallKeys[indexPath.row]].stringValue
                }
                
                
                cell.txtValue.rightVC()
                return cell
                }
               
            else  if indexPath.row == 3 || indexPath.row == 4 {
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! HoleCompletionCell
                cell.txtName.text = arrayInstallNames[indexPath.row]
                cell.txtName.rightview(Img: #imageLiteral(resourceName: "imgTick"))
                cell.txtName.leftview()
                
                return cell
            }
           else if indexPath.row == 6 {
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cellTitle", for: indexPath) as! HoleCompletionCell
                 cell.lblTitle.text = "Well"
                return cell
                
            }
             
            else if indexPath.row < 10 {
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HoleCompletionCell
                cell.txtTitle.text =  registerNames[indexPath.row - 7]
                cell.txtValue.text = JSON(registerDict)[registerKey[indexPath.row - 7]].stringValue
                cell.txtValue.rightVC()
                cell.txtTitle.leftview()
                return cell
                
            }
            else if indexPath.row == 10 {
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cellTitle", for: indexPath) as! HoleCompletionCell
                cell.lblTitle.text = "Geogical info"
                return cell
                
            }
            else if indexPath.row > 10  && indexPath.row < 11 +  JSON(registerDict)["geographical_info"].count{
                
                
                if indexPath.row == 12 {
                   
                    let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HoleCompletionCell
                    
                    let value = JSON(registerDict)["geographical_info"]["depth_to"].intValue - JSON(registerDict)["geographical_info"]["depth_from"].intValue
                    
                    cell.txtTitle.text =  "Total Footage"
                    cell.txtValue.text = "\(value)"
                    cell.txtValue.rightVC()
                    cell.txtTitle.leftview()
                    return cell
                    
                }
                else {
                
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HoleCompletionCell
                cell.txtTitle.text =  geologicalName[indexPath.row - 11]
                cell.txtValue.text = JSON(registerDict)["geographical_info"][geologicalKeys[indexPath.row - 11]].stringValue
                cell.txtValue.rightVC()
                cell.txtTitle.leftview()
                return cell
                }
            }
            
            else if indexPath.row == 11 +  JSON(registerDict)["geographical_info"].count {
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cellTitle", for: indexPath) as! HoleCompletionCell
                cell.lblTitle.text = "Riser"
                return cell
                
            }
            else if indexPath.row > 11 +  JSON(registerDict)["geographical_info"].count  && indexPath.row < 12 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count {
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HoleCompletionCell
                cell.txtTitle.text =  riserScreenNames[indexPath.row - 12 - JSON(registerDict)["geographical_info"].count]
                cell.txtValue.text = JSON(registerDict)["riser"][riserScreenKeys[indexPath.row - 12 - JSON(registerDict)["geographical_info"].count]].stringValue
                cell.txtValue.rightVC()
                cell.txtTitle.leftview()
                return cell
                
            }
           
                
           
            else if indexPath.row == 12 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count {
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cellTitle", for: indexPath) as! HoleCompletionCell
                cell.lblTitle.text = "Screen"
                return cell
                
            }
            else if indexPath.row > 12 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  && indexPath.row < 13 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count {
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HoleCompletionCell
                cell.txtTitle.text =  riserScreenNames[indexPath.row - 13 - JSON(registerDict)["geographical_info"].count - JSON(registerDict)["riser"].count]
                cell.txtValue.text = JSON(registerDict)["screen"][riserScreenKeys[indexPath.row - 13 - JSON(registerDict)["geographical_info"].count - JSON(registerDict)["riser"].count]].stringValue
                cell.txtValue.rightVC()
                cell.txtTitle.leftview()
                return cell
                
            }
                
            
            else if  indexPath.row == 13 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count {
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cellTitle", for: indexPath) as! HoleCompletionCell
                cell.lblTitle.text = "Filter"
                return cell
                
            }
            else if indexPath.row > 13 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count  && indexPath.row < 14 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count {
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HoleCompletionCell
                cell.txtTitle.text =  filterGroutingNames[indexPath.row - 14 - JSON(registerDict)["geographical_info"].count - JSON(registerDict)["riser"].count - JSON(registerDict)["screen"].count]
                cell.txtValue.text = JSON(registerDict)["filter_pack"][filterGroutingKeys[indexPath.row - 14 - JSON(registerDict)["geographical_info"].count - JSON(registerDict)["riser"].count - JSON(registerDict)["screen"].count]].stringValue
                cell.txtValue.rightVC()
                cell.txtTitle.leftview()
                return cell
                
            }
                
              
                
            else if  indexPath.row == 14 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count {
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cellTitle", for: indexPath) as! HoleCompletionCell
                cell.lblTitle.text = "Grouting Record"
                return cell
                
            }
            else if indexPath.row >  14 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  && indexPath.row < 15 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count{
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HoleCompletionCell
                cell.txtTitle.text =  filterGroutingNames[indexPath.row - 15 - JSON(registerDict)["geographical_info"].count - JSON(registerDict)["riser"].count - JSON(registerDict)["screen"].count - JSON(registerDict)["grouting_record"].count]
                cell.txtValue.text = JSON(registerDict)["grouting_record"][filterGroutingKeys[indexPath.row - 15 - JSON(registerDict)["geographical_info"].count - JSON(registerDict)["riser"].count - JSON(registerDict)["screen"].count - JSON(registerDict)["grouting_record"].count]].stringValue
                cell.txtValue.rightVC()
                cell.txtTitle.leftview()
                return cell
                
            }
           
                
            else if  indexPath.row == 15 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count{
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cellTitle", for: indexPath) as! HoleCompletionCell
                cell.lblTitle.text = "Inclinometer"
                return cell
                
            }
            else if indexPath.row >  15 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count  && indexPath.row < 16 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count + inclinometerKeys.count{
                
                
                
                if indexPath.row == 16 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count  ||  indexPath.row == 17 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count {
                    
                    
                    let cell =  tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! HoleCompletionCell
                    cell.txtName.text = inclinometerNames[indexPath.row - 16 -  JSON(registerDict)["riser"].count  - JSON(registerDict)["geographical_info"].count  - JSON(registerDict)["screen"].count - JSON(registerDict)["filter_pack"].count  - JSON(registerDict)["grouting_record"].count ]
                    
                    cell.txtName.rightview(Img: #imageLiteral(resourceName: "imgTick"))
                    cell.txtName.leftview()
                    
                    return cell
                    
                }
                
                else{
                    let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HoleCompletionCell
                    cell.txtTitle.text =  inclinometerNames[indexPath.row - 16 -  JSON(registerDict)["riser"].count  - JSON(registerDict)["geographical_info"].count  - JSON(registerDict)["screen"].count - JSON(registerDict)["filter_pack"].count  - JSON(registerDict)["grouting_record"].count ]
                    cell.txtValue.text = JSON(inclinometerDict)[inclinometerKeys[indexPath.row - 16 -  JSON(registerDict)["riser"].count  - JSON(registerDict)["geographical_info"].count  - JSON(registerDict)["screen"].count - JSON(registerDict)["filter_pack"].count  - JSON(registerDict)["grouting_record"].count ]].stringValue
                    cell.txtValue.rightVC()
                    cell.txtTitle.leftview()
                    return cell
                    
                }
               
                
            }
                
            else if  indexPath.row == 16 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count + inclinometerKeys.count{
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cellTitle", for: indexPath) as! HoleCompletionCell
                cell.lblTitle.text = "Riser"
                return cell
                
            }
            else if indexPath.row >  16 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count + inclinometerKeys.count  && indexPath.row < 17 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count + inclinometerKeys.count + arrRisers.count{
                
                let value = indexPath.row -  (17 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count + inclinometerKeys.count)
                
                print(value)
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HoleCompletionCell
                cell.txtTitle.text =  "\(JSON(arrRisers[value])["title"].stringValue) inch"
                cell.txtValue.text = "\(JSON(arrRisers[value])["value"].stringValue) pieces"
                cell.txtValue.rightVC()
                cell.txtTitle.leftview()
              return cell
                
    }
          
                
                
            else if  indexPath.row == 17 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count + inclinometerKeys.count + arrRisers.count{
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cellTitle", for: indexPath) as! HoleCompletionCell
                cell.lblTitle.text = "Screen"
                return cell
                
            }
            else if  indexPath.row == 18 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count + inclinometerKeys.count + arrRisers.count{
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HoleCompletionCell
                cell.txtTitle.text =  "Slot"
                cell.txtValue.text = JSON(inclinometerDict)["slot_screen"].stringValue
                cell.txtValue.rightVC()
                cell.txtTitle.leftview()
                
                return cell
            }
                
            else if  indexPath.row > 18 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count + inclinometerKeys.count + arrRisers.count &&   indexPath.row < 19 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count + inclinometerKeys.count + arrRisers.count + arrScreens.count {
                
                let value = indexPath.row - ( 19 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count + inclinometerKeys.count + arrRisers.count)
                
                print(value)
               
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HoleCompletionCell
                cell.txtTitle.text =  "\(JSON(arrScreens[value])["title"].stringValue) inch"
                cell.txtValue.text = "\(JSON(arrScreens[value])["value"].stringValue) pieces"
                cell.txtValue.rightVC()
                cell.txtTitle.leftview()
                return cell
            }
                
            else{
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HoleCompletionCell
                
                return cell
            }
        }
            
            
       else if indexPath.section == 2 {
            
            if indexPath.row == arrayAbondonNames.count {
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! HoleCompletionCell
                
                cell.txtView.text = JSON(self.abandonDict)["notes"].stringValue.base64Decoded()
                return cell
                
            }
           
            else{
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HoleCompletionCell
                cell.txtValue.text = JSON(abandonDict)[arrayAbandonKeys[indexPath.row]].stringValue
                cell.txtValue.rightVC()
                cell.txtTitle.text = arrayAbondonNames[indexPath.row]

                
                if indexPath.row > 1{
                    cell.txtTitle.leftview(image: abondonImages[indexPath.row]!)
                }
                else{
                    cell.txtTitle.leftview()
                }
                return cell
            }
        }
            
        else{
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! HoleCompletionCell
            
            return cell
            
        }
        }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            
            if indexPath.row == 5 {
              let value  =  JSON(self.groutDict)["notes"].stringValue.base64Decoded()
                
                if value == "Enter your notes" || value == "" {
                    return 0
                }
                else{
                    return 100
                }
                
            }
              else if indexPath.row == 2 || indexPath.row == 3{
                
                if JSON(self.groutDict)[arrayGroutKeys[indexPath.row]].stringValue != "1"  {
                    return 0
                }
                else{
                    return 55
                }
            }
            
            else if indexPath.row == 0 {
                if JSON(self.groutDict)["start_time"].stringValue != "" && JSON(self.groutDict)["stop_time"].stringValue != "" {
                    return 55
                }
                else{
                    return 0
                }
            }
                
                
                else{
                if JSON(self.groutDict)[arrayGroutKeys[indexPath.row]].stringValue != ""  {
                    return 55
                }
                else{
                    return 0
                }
            }
        }
         
        else if indexPath.section == 1 {
            
            if indexPath.row == 5 {
                let value  =  JSON(self.installDict)["notes"].stringValue.base64Decoded()
                
                if value == "Enter your notes" || value == "" {
                    return 0
                }
                else{
                    return 80
                }
                
            }
                else  if indexPath.row == 3 || indexPath.row == 4 {
                
                if JSON(self.installDict)[arrayInstallKeys[indexPath.row]].stringValue != "1"  {
                    return 0
                }
                else{
                    return 55
                }
            }
            else if indexPath.row == 6 {
             let value  =  JSON(self.installDict)["well"].stringValue
                if value == "1"{
                    
                   return 55
                }
                else{
                    return 0
                }
            }
                
            else if indexPath.row < 10  &&  indexPath.row > 6{
                let value  =  JSON(self.installDict)["well"].stringValue
                if value == "1" && JSON(registerDict)[registerKey[indexPath.row - 7]].stringValue != ""{
                    
                    return 55
                }
                else{
                    return 0
                }
                }
              
            else if indexPath.row == 10 {
                
                if JSON(registerDict)["geographical_info"] == JSON.null {
                    
                     return 0
                }
                else{
                    return 55
                }
                
            }
            else if indexPath.row > 10  && indexPath.row < 11 +  JSON(registerDict)["geographical_info"].count{
                
                if indexPath.row == 11 {
                     return 0
                    }
                else if indexPath.row == 12 {
                    if JSON(registerDict)["geographical_info"][geologicalKeys[0]].stringValue == "" && JSON(registerDict)["geographical_info"][geologicalKeys[1]].stringValue == "" {
                        
                         return 0
                    }
                    else{
                          return 55
                    }
                    
                }
                
                else  if JSON(registerDict)["geographical_info"][geologicalKeys[indexPath.row - 11]].stringValue == ""{
                    
                    return 0
                }
                else{
                    return 55
                }
            }
            else if indexPath.row == 11 +  JSON(registerDict)["geographical_info"].count {
                
                if JSON(registerDict)["riser"] == JSON.null {
                    
                    return 0
                }
                else{
                    return 55
                }
                
            }
            else if indexPath.row > 11 +  JSON(registerDict)["geographical_info"].count  && indexPath.row < 12 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count {
                
                if JSON(registerDict)["riser"][riserScreenKeys[indexPath.row - 12 - JSON(registerDict)["geographical_info"].count]].stringValue == ""{
                    
                    return 0
                }
                else{
                    return 55
                }
                
            }
             
            else if indexPath.row == 12 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count {
                
                if JSON(registerDict)["screen"] == JSON.null {
                    
                    return 0
                }
                else{
                    return 55
                }
                
            }
            else if indexPath.row > 12 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  && indexPath.row < 13 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count {
                
                if JSON(registerDict)["screen"][riserScreenKeys[indexPath.row - 13 - JSON(registerDict)["geographical_info"].count - JSON(registerDict)["riser"].count]].stringValue == ""{
                    return 0
                }
                else{
                    return 55
                }
                
            }
             
                
            else if  indexPath.row == 13 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count {
                
                if JSON(registerDict)["filter_pack"] == JSON.null {
                    
                    return 0
                }
                else{
                    return 55
                }
            }
            else if indexPath.row > 13 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count  && indexPath.row < 14 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count {
                
                if JSON(registerDict)["filter_pack"][filterGroutingKeys[indexPath.row - 14 - JSON(registerDict)["geographical_info"].count - JSON(registerDict)["riser"].count - JSON(registerDict)["screen"].count]].stringValue == ""{
                    
                    return 0
                }
                else{
                    return 55
                }
                
            }
            else if  indexPath.row == 14 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count {
                
                if JSON(registerDict)["grouting_record"] == JSON.null {
                    
                    return 0
                }
                else{
                    return 55
                }
                
            }
            else if indexPath.row >  14 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  && indexPath.row < 15 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count{
                
            if JSON(registerDict)["grouting_record"][filterGroutingKeys[indexPath.row - 15 - JSON(registerDict)["geographical_info"].count - JSON(registerDict)["riser"].count - JSON(registerDict)["screen"].count - JSON(registerDict)["grouting_record"].count]].stringValue == "" {
                    
                    
                    return 0
                }
                else{
                    return 55
                }
                
            }
                
             
            else if  indexPath.row == 15 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count{
                
                if  arrRisers.count == 0 && arrScreens.count == 0 {
                    
                    return 0
                }
                else{
                    return 55
                }
                
            }
            else if indexPath.row >  15 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count  && indexPath.row < 16 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count + inclinometerKeys.count{
                
                if JSON(inclinometerDict)[inclinometerKeys[indexPath.row - 16 -  JSON(registerDict)["riser"].count  - JSON(registerDict)["geographical_info"].count  - JSON(registerDict)["screen"].count - JSON(registerDict)["filter_pack"].count  - JSON(registerDict)["grouting_record"].count ]].stringValue == ""{
                    return 0
                }
                else{
                    
                    return 55
                }
                
            }
                
            else if  indexPath.row == 16 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count + inclinometerKeys.count{
                
                if  arrRisers.count == 0 {
                    
                    return 0
                }
                else{
                    return 55
                }
                
            }
            else if indexPath.row >  16 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count + inclinometerKeys.count  && indexPath.row < 17 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count + inclinometerKeys.count + arrRisers.count{
                
                let value = indexPath.row -  (17 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count + inclinometerKeys.count)
                
                  print(value)
                if JSON(arrRisers[value])["title"].stringValue != "" && JSON(arrRisers[value])["value"].stringValue != ""{
                    return 55
                }
                else{
                    return 0
                }
                

            }
            else if  indexPath.row == 17 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count + inclinometerKeys.count + arrRisers.count{
                
                if  arrScreens.count == 0 &&  JSON(inclinometerDict)["slot_screen"].stringValue == "" {
                    
                    return 0
                }
                else{
                    return 55
                }
                
            }
            else if  indexPath.row == 18 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count + inclinometerKeys.count + arrRisers.count{
                
                if  JSON(inclinometerDict)["slot_screen"].stringValue == "" {
                    
                    return 0
                }
                else{
                    return 55
                }
                
            }
             
            else if  indexPath.row > 18 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count + inclinometerKeys.count + arrRisers.count &&   indexPath.row < 19 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count + inclinometerKeys.count + arrRisers.count + arrScreens.count {
                
                let value = indexPath.row - ( 19 +  JSON(registerDict)["riser"].count  + JSON(registerDict)["geographical_info"].count  + JSON(registerDict)["screen"].count + JSON(registerDict)["filter_pack"].count  + JSON(registerDict)["grouting_record"].count + inclinometerKeys.count + arrRisers.count)
                
                print(value)
                if JSON(arrScreens[value])["title"].stringValue != "" && JSON(arrScreens[value])["value"].stringValue != ""{
                    return 55
                }
                else{
                    return 0
                }
                
            }
                
            else if indexPath.row == 0 {
                
                if JSON(self.installDict)["starttime"].stringValue != "" && JSON(self.installDict)["stoptime"].stringValue != "" {
                    return 55
                }
                else{
                    return 0
                }
                
                
            }
                
                else{
               
                if JSON(installDict)[arrayInstallKeys[indexPath.row]].stringValue == "" {
                    
                    return 0
                }
                else{
                    return 55
                }
                
               
            }
        }
            
            
        else if indexPath.section == 2 {
            
            if indexPath.row == arrayAbondonNames.count {
                let value  =  JSON(self.abandonDict)["notes"].stringValue.base64Decoded()
                
                if value == "Enter your notes" || value == "" {
                    return 0
                }
                else{
                    return 100
                }
                
            }
            else{
                if JSON(self.abandonDict)[arrayAbandonKeys[indexPath.row]].stringValue != ""  {
                    return 55
                }
                else{
                    return 0
                }
            }
        }
            
        else{
           return 55
            
        }
     }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! HoleCompletionCell
        
        cell.lblHeader.text = headerArr[section]
        
        
        if section == headerArr.count - 1 {
              cell.btnHeaderTick.setImage(#imageLiteral(resourceName: "imgTick"), for: .normal)
        }
      else  if section == headerArr.count - 2 {
             cell.btnHeaderTick.setImage(#imageLiteral(resourceName: "imgTick"), for: .normal)

        }
        else{
            if selectedIndex == section {
                cell.btnHeaderTick.setImage(#imageLiteral(resourceName: "imgDropUP"), for: .normal)
                
            }
            else{
                cell.btnHeaderTick.setImage(#imageLiteral(resourceName: "imgDown"), for: .normal)
            }
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
            
            cell.headerBgView.addGestureRecognizer(tap)
            cell.headerBgView.tag = section
            cell.headerBgView.isUserInteractionEnabled = true
            
            cell.headerBgView.addGestureRecognizer(tap)
        }
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == headerArr.count - 1 {
            if self.arrHoleNumber.count > 0 {
                if JSON(self.mainDict)["main category"][self.arrHoleNumber[indexs].stringValue]["Leave Hole"].stringValue != "1"{
                    return 0
                }
                else{
                    return 44
                }
            }
            else{
                return 0
            }
           
           
           }
        else  if section == headerArr.count - 2 {
            if self.arrHoleNumber.count > 0 {
                if JSON(self.mainDict)["main category"][self.arrHoleNumber[indexs].stringValue]["Backfill"].stringValue != "1"{
                    return 0
                }
                else{
                    return 44
                }
            }
            else{
                return 0
            }
            
        }
        else{
            
           return 44
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    
    //MARK:- fetchDetails function
    func  fetchDetails(){
        self.indexs = 0
        let parentVC = self.parent as! ProjectDetailsViewController
        let parameter  : Parameters  = ["action": "hole_number_details", "userid": standard.value(forKey: "id")!,"projectid": projectId,"date": parentVC.strProjectDate]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(holeCompletionUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            
            guard json["response"]["message"]["result"].stringValue == "201" else{
                return
            }
            self.mainDict = json["response"].dictionaryObject ?? [String: Any]()
            print(self.mainDict)
            
            self.arrHoleNumber = JSON(self.mainDict)["holenumber"].arrayValue
            
            if self.arrHoleNumber.count > 0 {
                self.groutDict = JSON(self.mainDict)["main category"][self.arrHoleNumber[self.indexs].stringValue]["grout"].dictionaryObject ?? [String: Any]()
                
                self.abandonDict = JSON(self.mainDict)["main category"][self.arrHoleNumber[self.indexs].stringValue]["abandon"].dictionaryObject ?? [String: Any]()
                
                self.installDict = JSON(self.mainDict)["main category"][self.arrHoleNumber[self.indexs].stringValue]["installation well"].dictionaryObject ?? [String: Any]()
                
                self.registerDict = JSON(self.mainDict)["main category"][self.arrHoleNumber[self.indexs].stringValue]["installation well"]["register"].dictionaryObject ?? [String: Any]()
                
                
                self.inclinometerDict = JSON(self.mainDict)["main category"][self.arrHoleNumber[self.indexs].stringValue]["installation well"]["inclinometer"].dictionaryObject ?? [String: Any]()
                
                
                let arrayScreen =  JSON(self.inclinometerDict)["screen_arr"].arrayValue
                let arrayRiser =  JSON(self.inclinometerDict)["riser_arr"].arrayValue
                
                self.arrScreens.removeAll()
                self.arrRisers.removeAll()
                for i in 0..<arrayScreen.count{
                    let dict =  arrayScreen[i].dictionaryObject
                    self.arrScreens.append(dict ?? [String: Any]())
                }
                for i in 0..<arrayRiser.count{
                    let dict =  arrayRiser[i].dictionaryObject
                    self.arrRisers.append(dict ?? [String: Any]())
                }
                
                print(self.inclinometerDict)
                
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.collectionView.reloadData()
            }
            
            
        
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    
    //MARK:- CollectionView Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return  self.arrHoleNumber.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cvCell", for: indexPath)
        
        let lbl = cell.viewWithTag(1) as! UILabel
        let lblHole = cell.viewWithTag(5) as! UILabel
        let lblBootom = cell.viewWithTag(2) as! UILabel
        lbl.text =  self.arrHoleNumber[indexPath.row].stringValue
        if indexPath.row == indexs {
            lblBootom.backgroundColor = appColor
            lbl.textColor = appColor
             lblHole.textColor = appColor
      }
        else{
           lblBootom.backgroundColor = UIColor.clear
              lbl.textColor = UIColor.black
             lblHole.textColor = UIColor.black
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
         indexs =   indexPath.row
        
        selectedIndex = -1
        self.groutDict = JSON(self.mainDict)["main category"][self.arrHoleNumber[indexs].stringValue]["grout"].dictionaryObject ?? [String: Any]()
        
        self.abandonDict = JSON(self.mainDict)["main category"][self.arrHoleNumber[indexs].stringValue]["abandon"].dictionaryObject ?? [String: Any]()
        
        self.installDict = JSON(self.mainDict)["main category"][self.arrHoleNumber[indexs].stringValue]["installation well"].dictionaryObject ?? [String: Any]()
        
        self.registerDict = JSON(self.mainDict)["main category"][self.arrHoleNumber[self.indexs].stringValue]["installation well"]["register"].dictionaryObject ?? [String: Any]()
        
        self.inclinometerDict = JSON(self.mainDict)["main category"][self.arrHoleNumber[self.indexs].stringValue]["installation well"]["inclinometer"].dictionaryObject ?? [String: Any]()
        print(self.inclinometerDict)
         collectionView.reloadData()
        self.tableView.reloadData()
      }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if arrHoleNumber.count < 3 {
            return CGSize(width: CGFloat(Int(collectionView.frame.size.width) / arrHoleNumber.count), height: 50)
        }
        else{
           return CGSize(width: 100, height: 50)
        }
        }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
        }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    
    func syncOfflineHoleData(){
          self.Count = 0
        
        self.apiResponse = 0
        let grout = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Grout")
        let abondon = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Abandon")
        
        let inclinometer = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Inclinometer")
        let holeCompletion = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "HoleCompletion")
        
        let installation = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Installation")
        
        
         let dictArr = [grout , abondon,inclinometer,holeCompletion,installation]
        
        for i in 0..<dictArr.count{
            
            if dictArr[i].count == 0 {
                
                self.Count = self.Count + 1
            }
        }
        
        
        if installation.count > 0 {
            
            let parameters : Parameters = ["action": "hc_installation" ,"data" :  installation ]
            syncOfflineDataApi(parameter: parameters, tables: "Installation", urlStr: holeCompletionOfflineUrl)
        }
        
        
        
        if holeCompletion.count > 0 {
            let parameters : Parameters = ["action": "hole_completion_leave_openOrbackFill" ,"data" :  holeCompletion ]
            syncOfflineDataApi(parameter: parameters, tables: "HoleCompletion", urlStr: holeCompletionOfflineUrl)
            
        }
        
        
        if inclinometer.count > 0 {
            let parameters : Parameters = ["action": "ac_install_inclinometer" ,"data" :  inclinometer ]
            
         syncOfflineDataApi(parameter: parameters, tables: "Inclinometer", urlStr: consumableUrlOffline)
        }
        
        if abondon.count > 0 {
            
            let parameters : Parameters = ["action": "hc_abandon" ,"data" :  abondon ]
            
          syncOfflineDataApi(parameter: parameters, tables: "Abandon", urlStr: holeCompletionOfflineUrl)
            
        }
        
        if grout.count > 0 {
            let parameters : Parameters = ["action": "hc_grout" ,"data" :  grout ]
            
          syncOfflineDataApi(parameter: parameters, tables: "Grout", urlStr: holeCompletionOfflineUrl)
        }
         if dictArr[0].count == 0  && dictArr[1].count == 0 && dictArr[2].count == 0 && dictArr[3].count == 0 && dictArr[4].count == 0{
                
                 self.fetchDetails()
            }
        
        
        
    }
    //MARK: - SyncOfflineMultipleApi
    func syncOfflineDataApi(parameter :Parameters , tables : String ,urlStr  : String ){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(urlStr, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            
            self.apiResponse = self.apiResponse + 1
            guard json["response"]["result"].string == "201" else{
                
                if self.apiResponse == self.Count {
                    self.fetchDetails()
                }
                return
            }
            
            ProjectsOffline.sharedInstance.updateCoredataTables(tables: tables)
            
            if self.apiResponse == self.Count {
                self.fetchDetails()
            }
            
            
        }) { (error) in
             self.apiResponse = self.apiResponse + 1
            if self.apiResponse == self.Count {
                self.fetchDetails()
            }
            
            appDelegates.window?.rootViewController?.view.stopIndicator()
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    
    func updateDataFromCoreData(dict : [String : Any]){
        
        self.mainDict = dict
         self.indexs = 0
        self.arrHoleNumber = JSON(self.mainDict)["holenumber"].arrayValue
        
        if self.arrHoleNumber.count > 0 {
            self.groutDict = JSON(self.mainDict)["main category"][self.arrHoleNumber[self.indexs].stringValue]["grout"].dictionaryObject ?? [String: Any]()
            
            self.abandonDict = JSON(self.mainDict)["main category"][self.arrHoleNumber[self.indexs].stringValue]["abandon"].dictionaryObject ?? [String: Any]()
            
            self.installDict = JSON(self.mainDict)["main category"][self.arrHoleNumber[self.indexs].stringValue]["installation well"].dictionaryObject ?? [String: Any]()
            
            self.registerDict = JSON(self.mainDict)["main category"][self.arrHoleNumber[self.indexs].stringValue]["installation well"]["register"].dictionaryObject ?? [String: Any]()
            
            
            self.inclinometerDict = JSON(self.mainDict)["main category"][self.arrHoleNumber[self.indexs].stringValue]["installation well"]["inclinometer"].dictionaryObject ?? [String: Any]()
            
            
            let arrayScreen =  JSON(self.inclinometerDict)["screen_arr"].arrayValue
            let arrayRiser =  JSON(self.inclinometerDict)["riser_arr"].arrayValue
            
            self.arrScreens.removeAll()
            self.arrRisers.removeAll()
            for i in 0..<arrayScreen.count{
                let dict =  arrayScreen[i].dictionaryObject
                self.arrScreens.append(dict ?? [String: Any]())
            }
            for i in 0..<arrayRiser.count{
                let dict =  arrayRiser[i].dictionaryObject
                self.arrRisers.append(dict ?? [String: Any]())
            }
            
            print(self.inclinometerDict)
            
        }
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.collectionView.reloadData()
        }
        
}
    
    
    
}
extension  HoleCompletionContainerViewController : showHoleCompletionDelegates{
    
    func  showHoleCompletionInfo(){
        if Network.isConnectedToNetwork() == true {
            
            self.syncOfflineHoleData()
            
        }
            
            
        else{
              let parentVC = self.parent as! ProjectDetailsViewController
            let array =  HoleCompletionOffline.sharedInstace.getOfflineHoleNumber(prjectId: projectId, date: parentVC.strProjectDate)
            print(array)
            
            if array.count > 0 {
                
                let dict  =   HoleCompletionOffline.sharedInstace.getOfflineHoleDictionary(arrayHole: array, prjectId: projectId, date: parentVC.strProjectDate)
                
                self.updateDataFromCoreData(dict: dict)
                
            }
            
        }
        
    }
}
