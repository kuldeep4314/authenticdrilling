//
//  TravelContainerViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-30.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit
import CoreData
class TravelContainerViewController: ViewController ,UITableViewDataSource,UITableViewDelegate{
 @IBOutlet weak var tableView: UITableView!
 var mainDict =  [String: Any]()
     let arrNames = ["Mob To Site","Hole/Jobsite Travel", "Intermediate Travel","Demobe" ,"JobSite"]
    
      let startTimeKeys = ["mobsite_starttime","hoteltravel_starttime", "intermediate_starttime","demobe_starttime","jobsite_starttime"]
     let stoptTimeKeys = ["mobsite_stoptime","hoteltravel_stoptime", "intermediate_stoptime","demobe_stoptime" , "jobsite_stoptime"]
    
      var sessions: [NSManagedObject] = []
    var timeArray = ["0","0","0","0","0"]
    override func viewDidLoad() {
        super.viewDidLoad()
       tableView.tableFooterView  = UIView()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let parentVC = self.parent as! ProjectDetailsViewController
        
        parentVC.delegatesTravel = self
        if parentVC.indexs == 0{
            if Network.isConnectedToNetwork() == true {
                self.getOfflineData()
            }
            else{
                self.getCoreData()
            }
            }
    }
    
    
    func getOfflineData(){
        let offlineDetails = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Travel")
        print(offlineDetails)
        if offlineDetails.count > 0 {
            let parameters : Parameters = ["action": "travel_category" ,"data" :  offlineDetails ]
            
            self.syncOfflineMultipleApi(parameter: parameters)
            
        }
        else{
            self.fetchDetails()
        }
        
    }
    
     //MARK: - SyncOfflineMultipleApi
    func syncOfflineMultipleApi(parameter :Parameters){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(projectUrlOffline, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            ProjectsOffline.sharedInstance.updateCoredataTables(tables: "Travel")
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            self.getCoreData()
            print(error.localizedDescription)
        }
    }
    
    
    

    //MARK:- TableView Data Source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrNames.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProjectInfoCell
        
        cell.lblName.text = self.arrNames[indexPath.section]
        cell.lblHours.text = self.timeArray[indexPath.section]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 46
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "header")
        
        return cell?.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 15
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK:- Update Core Data
    func updateCoredata(isOffline : Bool , edit : Bool , isShowAlert : Bool){
        let parentVC = self.parent as! ProjectDetailsViewController
        let managedContext = CoreDataStack.managedObjectContext
        let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Travel")
        updateFetch.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)", projectId , parentVC.strProjectDate)
        do {
            let results = try managedContext.fetch(updateFetch) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                
                // In my case, I only updated the first item in results
                let session = results![0]
                
                let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
                session.setValue(data1, forKey: "dataDetails")
                
                session.setValue( parentVC.strProjectDate, forKey: "date")
                session.setValue( projectId, forKey: "projectid")
                session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                session.setValue(isOffline, forKey: "isOffline")
                session.setValue(edit, forKey: "edit")
                
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try managedContext.save()
            
            
            if isShowAlert == true {
                self.navigationController?.popViewController(animated: true)
                appDelegates.window?.rootViewController?.showAlert(messageStr:  "travel list updated successfully")
            }
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
        
    }
    
    
    //MARK:- Save Core Data Function
    func saveCoredata(isOffline : Bool,edit: Bool , msg : String , isShow : Bool){
        print(isOffline)
        print(edit)
        
         let parentVC = self.parent as! ProjectDetailsViewController
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "Travel",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
        session.setValue(data1, forKey: "dataDetails")
        
        session.setValue(parentVC.strProjectDate, forKey: "date")
        session.setValue(projectId, forKey: "projectid")
        session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
        session.setValue(isOffline, forKey: "isOffline")
        session.setValue(edit, forKey: "edit")
        
        
        CoreDataStack.saveContext()
        
        
        if isShow == true{
            self.navigationController?.popViewController(animated: true)
            appDelegates.window?.rootViewController?.showAlert(messageStr: msg)
        }
        
    }
    //MARK:- Get Core Data Function
    func getCoreData() {
        let parentVC = self.parent as! ProjectDetailsViewController
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Travel")
        
        fetchRequest.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)", projectId , parentVC.strProjectDate)
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            for session in sessions{
                var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                
                dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                
                dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                
                self.mainDict = dictionary
                
            }
            
            for i in 0..<5 {
                let t1 =   Comman.timeInterval(date: JSON( self.mainDict)["date"].stringValue, startTime: JSON( self.mainDict)[self.startTimeKeys[i]].stringValue, stopTime: JSON( self.mainDict)[self.stoptTimeKeys[i]].stringValue)
               self.timeArray[i] = t1
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }

        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
   
    
    //MARK:- fetchDetails function
    func  fetchDetails(){
        
        let parentVC = self.parent as! ProjectDetailsViewController
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid": parentVC.projectInformation?.pid ?? "","table":"travel_category","date": parentVC.strProjectDate]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            
            self.mainDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
            
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "Miscellaneous", strDate: parentVC.strProjectDate, projectID: projectId) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false)
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false)
                
            }
            self.getCoreData()
            
            
            
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
}

extension  TravelContainerViewController : showTravelInfoDelegates{

    func  showTravelInfo(){
        if Network.isConnectedToNetwork() == true {
            self.getOfflineData()
        }
        else{
            self.getCoreData()
        }

       
    }
}
