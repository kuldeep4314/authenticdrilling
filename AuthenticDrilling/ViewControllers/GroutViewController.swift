//
//  GroutViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-28.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit
import CoreData
class GroutViewController: ViewController ,UITextFieldDelegate,UITextViewDelegate{
    var sessions: [NSManagedObject] = []
    @IBOutlet weak var txtStopTime: UITextField!
     @IBOutlet weak var txtDepthFrom: UITextField!
     @IBOutlet weak var txtDepthTo: UITextField!
     @IBOutlet weak var txtHoleNumber: UITextField!
    
    @IBOutlet weak var txtTotalFootageValue: UITextField!
    @IBOutlet weak var txtNew: UITextField!
    @IBOutlet weak var txtTopUP: UITextField!
    
    @IBOutlet weak var txtHoleNumberValue: UITextField!
    @IBOutlet weak var txtDepthTovalue: UITextField!
    @IBOutlet weak var txtDepthFromValue: UITextField!
    @IBOutlet weak var txtTotalFootage: UITextField!
     @IBOutlet weak var txtStartTime: UITextField!
    @IBOutlet weak var txtView: UITextView!
    
    var strDate = String()
    var projectInfo :ProjectData?
    var holeNumber = String()
    var textFieldKeys = ["start_time","stop_time","depth_from","depth_to","holenumber","new","topup","total_footage"]
    var mainDict =  [String: Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
       self.mainDict.updateValue(self.holeNumber, forKey: "holenumber")
        self.txtView.text = "Enter your notes"
        self.txtView.textColor = UIColor.lightGray

        self.setUpView()
        if Network.isConnectedToNetwork() == true {
            self.getOfflineData()
        }
        else{
            self.getCoreData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setUpView(){
       
        for i in 1..<9 {
            let txt = self.view.viewWithTag(i) as! UITextField
        
            txt.delegate = self
            if i  < 3 {
                let datePicker = UIDatePicker()
                
                datePicker.addTarget(self, action: #selector(self.dateChange(sender:)), for: .valueChanged)
                datePicker.datePickerMode = .time
                 datePicker.locale = Locale(identifier: "en_GB")
                datePicker.tag = i
                txt.inputView = datePicker
                txt.rightview(Img: #imageLiteral(resourceName: "imgWatch"))
            }
            
        }
        txtTotalFootageValue.rightVC()
        txtDepthTovalue.rightVC()
        txtDepthFromValue.rightVC()
        txtHoleNumberValue.rightVC()
        txtTopUP.leftview()
        txtNew.leftview()
        txtView.addshodowToTextView()
        txtView.delegate = self
        txtTotalFootage.leftview()
        self.txtStartTime.leftview(image: #imageLiteral(resourceName: "imgTimer"))
        self.txtStopTime.leftview(image: #imageLiteral(resourceName: "imgTimer"))
        self.txtDepthFrom.leftview(image:#imageLiteral(resourceName: "imgDepth"))
        
        self.txtDepthTo.leftview(image:#imageLiteral(resourceName: "imgDepth"))
        
        self.txtHoleNumber.leftview(image:#imageLiteral(resourceName: "imgHole"))
        
        txtNew.rightview(Img:#imageLiteral(resourceName: "imgUnselect"))
        txtTopUP.rightview(Img: #imageLiteral(resourceName: "imgUnselect"))
        
    }
    @objc  func dateChange(sender: UIDatePicker){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        
        let txt = self.view.viewWithTag(sender.tag) as! UITextField
        txt.text = formatter.string(from: sender.date)
    }
    func getOfflineData(){
        
        let offlineDetails = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Grout")
        print(offlineDetails)
        if offlineDetails.count > 0 {
            let parameters : Parameters = ["action": "hc_grout" ,"data" :  offlineDetails ]
            
            self.syncOfflineMultipleApi(parameter: parameters)
            
        }
        else{
            self.fetchDetails()
        }
        
    }
    
    
    
    //MARK: - SyncOfflineMultipleApi
    func syncOfflineMultipleApi(parameter :Parameters){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(holeCompletionOfflineUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            ProjectsOffline.sharedInstance.updateCoredataTables(tables: "Grout")
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            self.getCoreData()
            print(error.localizedDescription)
        }
    }
    
    
    func betweenTimeFunction(textField : UITextField ,start :String ,stop :String){
        
        let arrayStartTime = start.components(separatedBy: ":")
        let arrayStopTime = stop.components(separatedBy: ":")
        self.mainDict.updateValue(""  , forKey: textFieldKeys[textField.tag - 1])
        if textField.tag == 1 {
            
            
            if JSON(arrayStartTime[0]).intValue > JSON(arrayStopTime[0]).intValue {
                if JSON(arrayStartTime[0]).intValue >= 12 && JSON(arrayStopTime[0]).intValue < 12{
                    appDelegates.window?.rootViewController?.view.makeToast("Please select stop time between 12-24.", duration: 2, position: .center)
                }
                else{
                    appDelegates.window?.rootViewController?.view.makeToast("Start time should be less than Stop time.", duration: 2, position: .center)
                }
                
            }
            else{
                appDelegates.window?.rootViewController?.view.makeToast("Start time should be less than Stop time.", duration: 2, position: .center)
            }
            
        }
        else{
            if JSON(arrayStartTime[0]).intValue > JSON(arrayStopTime[0]).intValue {
                if JSON(arrayStartTime[0]).intValue >= 12 && JSON(arrayStopTime[0]).intValue < 12{
                    appDelegates.window?.rootViewController?.view.makeToast("Please select stop time between 12-24.", duration: 2, position: .center)
                }
                else{
                    appDelegates.window?.rootViewController?.view.makeToast("Stop time should be greater than Start time.", duration: 2, position: .center)
                }
            }
            else{
                appDelegates.window?.rootViewController?.view.makeToast("Stop time should be greater than Start time.", duration: 2, position: .center)
            }
            
            
        }
        
    }

    
    
     //MARK:- TextField Delegates
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        mainDict.updateValue(textField.text ?? "", forKey:textFieldKeys[textField.tag - 1])
        print(mainDict)
        
        
    print(textField.tag)
        if textField.tag == 1 || textField.tag == 2 {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            
            let dateformatter = DateFormatter()
            dateformatter.dateFormat = "yyyy-MM-dd HH:mm"
            
            if   JSON(self.mainDict)["start_time"].stringValue != "" &&  JSON(self.mainDict)["stop_time"].stringValue != "" {
                let dateCurrent = formatter.string(from: Date())
                
                let firstDate = dateformatter.date(from: "\(dateCurrent) \(JSON(self.mainDict)["start_time"].stringValue)")
                let secondDate = dateformatter.date(from: "\(dateCurrent) \(JSON(self.mainDict)["stop_time"].stringValue )")
                
                
                let check = firstDate?.timeIntervalSince(secondDate!)
                
                if  Int(check!) < 0 {
                    self.mainDict.updateValue(textField.text ?? ""  , forKey: textFieldKeys[textField.tag - 1])
                    
                }
                else{
                    
                
                    self.betweenTimeFunction(textField: textField, start:  JSON(self.mainDict)["start_time"].stringValue, stop: JSON(self.mainDict)["stop_time"].stringValue)
                    
                }
                
            }
            
            
        }
        
        
        
        
        
        
        ///
        if textField.tag == 3 || textField.tag == 4{
        
        if JSON(self.mainDict)["depth_from"].stringValue != "" &&
            
            JSON(self.mainDict)["depth_to"].stringValue != ""{
            
            let value = JSON(self.mainDict)["depth_to"].intValue - JSON(self.mainDict)["depth_from"].intValue
            
            print(value)
            
            if value > 0 {
                mainDict.updateValue("\(value)", forKey:"total_footage")
            }
            else{
                
                mainDict.updateValue("", forKey:"total_footage")
                
                mainDict.updateValue("", forKey:textFieldKeys[textField.tag - 1])
                if textField.tag == 3 {
                    appDelegates.window?.rootViewController?.view.makeToast("Depth from should be less than depth to.", duration: 2, position: .center)
                }
                else{
                    appDelegates.window?.rootViewController?.view.makeToast("Depth to should be greater than depth from.", duration: 2, position: .center)
                }
            }
        }
        }
        self.setUpData()
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSaveTapped(_ sender: Any) {
      
        
        if Network.isConnectedToNetwork() == true {
            
            self.addGroutApiCall(isOffline: false, edit: false)
        }
        else  if Network.isConnectedToNetwork() == false {
            
             if ProjectsOffline.sharedInstance.getOfflineTablesWithHolenumber(tables: "Grout", strDate: self.strDate, projectID: projectId, holenumber: self.holeNumber) == true
            {
                self.updateCoredata(isOffline: true, edit: true, isShowAlert: true)
            }
                
            else{
                ProjectsOffline.sharedInstance.getOfflineDates(isOffline: true, strDate: self.strDate, projectID: projectId)
                self.saveCoredata(isOffline: true, edit: false, msg: "Hole Completition Grout list added successfully", isShow: true)
            }
        }
        
        
    }
  
    @IBAction func btnNewRightTapped(_ sender: UIButton) {
        
        
        if JSON(self.mainDict)[textFieldKeys[sender.tag - 11]].stringValue == "1" {
            self.mainDict.updateValue("0", forKey: textFieldKeys[sender.tag - 11])
            txtNew.rightview(Img: #imageLiteral(resourceName: "imgUnselect"))
            
        }
        else{
            self.mainDict.updateValue("1", forKey: textFieldKeys[sender.tag - 11])
             txtNew.rightview(Img: #imageLiteral(resourceName: "imgCheck"))
        }
        
       print(mainDict)
        
    }
    @IBAction func btnTopUpRightTapped(_ sender: UIButton) {
        
        if JSON(self.mainDict)[textFieldKeys[sender.tag - 11]].stringValue == "1" {
            self.mainDict.updateValue("0", forKey: textFieldKeys[sender.tag - 11])
            txtTopUP.rightview(Img: #imageLiteral(resourceName: "imgUnselect"))
           
        }
        else{
            self.mainDict.updateValue("1", forKey: textFieldKeys[sender.tag - 11])
             txtTopUP.rightview(Img: #imageLiteral(resourceName: "imgCheck"))
        }
          print(mainDict)
    }
    
    
    
    
    
    @IBAction func btnInstallTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "InstallationViewController") as! InstallationViewController
        
        vc.holeNumber = holeNumber
        vc.strDate = strDate
        vc.projectInfo = projectInfo
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnAbandonTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AbandonViewController") as! AbandonViewController
         vc.holeNumber = holeNumber
        vc.strDate = strDate
        vc.projectInfo =  projectInfo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
     //MARK:- TextView Delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        
        if self.txtView.textColor == UIColor.lightGray
        {
            self.txtView.text = ""
            self.txtView.textColor = UIColor.black
            
        }
    }
    

    func textViewDidEndEditing(_ textView: UITextView) {
        
        
        if self.txtView.text.isEmpty {
            self.txtView.text = "Enter your notes"
            self.txtView.textColor = UIColor.lightGray
              mainDict.updateValue( textView.text ?? "", forKey:"notes")
        }else{
            mainDict.updateValue( textView.text ?? "", forKey:"notes")
            print(mainDict)
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        print(newText.count)
        return newText.count <= 200
    }
    func setUpData(){
        
        DispatchQueue.main.async {
            for i in 1..<9 {
                
                let txt = self.view.viewWithTag(i) as! UITextField
                if i == 6 || i == 7 {
                    let value = JSON(self.mainDict)["\(self.textFieldKeys[i - 1])"].stringValue
                    print(value)
                    if value == "1" {
                        txt.rightview(Img:#imageLiteral(resourceName: "imgCheck"))
                    }
                    else{
                        txt.rightview(Img:#imageLiteral(resourceName: "imgUnselect"))
                        
                    }
                }
                else{
                    txt.text =  JSON(self.mainDict)["\(self.textFieldKeys[i - 1])"].stringValue
                }
                
                
            }
            self.txtView.text = JSON(self.mainDict)["notes"].stringValue.base64Decoded()?.base64Decoded()
            
            if self.txtView.text == ""{
                self.txtView.text = "Enter your notes"
                self.txtView.textColor = UIColor.lightGray
            }
            else{
                self.txtView.textColor = UIColor.black
            }
            
            
        }
        
        
    }
    
    //MARK:- Update Core Data
    func updateCoredata(isOffline : Bool , edit : Bool , isShowAlert : Bool){
        
        let managedContext = CoreDataStack.managedObjectContext
        let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Grout")
        updateFetch.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@) AND (holenumber = %@)",  projectId , strDate,self.holeNumber)
        do {
            let results = try managedContext.fetch(updateFetch) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                
                // In my case, I only updated the first item in results
                let session = results![0]
                
                let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
                session.setValue(data1, forKey: "dataDetails")
                
                session.setValue(strDate, forKey: "date")
                session.setValue(projectId, forKey: "projectid")
                session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                session.setValue(isOffline, forKey: "isOffline")
                session.setValue(edit, forKey: "edit")
                session.setValue(self.holeNumber, forKey: "holenumber")
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try managedContext.save()
            
            
            if isShowAlert == true {
                self.navigationController?.popViewController(animated: true)
                appDelegates.window?.rootViewController?.showAlert(messageStr:  "Hole Completition Grout list updated successfully")
            }
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
        
    }
    
    
    //MARK:- Save Core Data Function
    func saveCoredata(isOffline : Bool,edit: Bool , msg : String , isShow : Bool){
        print(isOffline)
        print(edit)
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "Grout",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
        session.setValue(data1, forKey: "dataDetails")
        
        session.setValue(strDate, forKey: "date")
        session.setValue(projectId, forKey: "projectid")
        session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
        session.setValue(isOffline, forKey: "isOffline")
        session.setValue(edit, forKey: "edit")
        session.setValue(self.holeNumber, forKey: "holenumber")
        
        CoreDataStack.saveContext()
        
        
        if isShow == true{
            self.navigationController?.popViewController(animated: true)
            appDelegates.window?.rootViewController?.showAlert(messageStr: msg)
        }
        
    }
    //MARK:- Get Core Data Function
    func getCoreData() {
        
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Grout")
        
        fetchRequest.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@) AND (holenumber = %@)",  projectId , strDate,self.holeNumber)
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            for session in sessions{
                var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                
                dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                 dictionary.updateValue(session.value(forKey: "holenumber") ?? "", forKey: "holeNumber")
                dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                
                self.mainDict = dictionary
                
            }
            
            DispatchQueue.main.async {
              self.setUpData()
            }
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    
    
    
    
    
    
    
    //MARK:- AddGroutApiCall
    func addGroutApiCall(isOffline : Bool , edit : Bool ){
        
        let parameter  : Parameters  =  ["action": "hc_grout", "userid": standard.value(forKey: "id")!, "projectid":  projectId,"date":strDate
            , "start_time": JSON(self.mainDict)["start_time"].stringValue
            , "stop_time": JSON(self.mainDict)["stop_time"].stringValue
            , "depth_from": JSON(self.mainDict)["depth_from"].stringValue
            , "depth_to": JSON(self.mainDict)["depth_to"].stringValue
            , "holenumber": holeNumber
            , "new": JSON(self.mainDict)["new"].stringValue
            , "topup": JSON(self.mainDict)["topup"].stringValue
            
            , "total_footage": JSON(self.mainDict)["total_footage"].stringValue
            , "notes": JSON(self.mainDict)["notes"].stringValue.base64Decoded() ?? ""
        ]
        
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(holeCompletionUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  json["response"]["msg"].stringValue )
                return
            }
            
            DispatchQueue.main.async {
                
                
                    
                    ProjectsOffline.sharedInstance.getOfflineDates(isOffline: isOffline, strDate: self.strDate, projectID: projectId)
                    
                    
                     if ProjectsOffline.sharedInstance.getOfflineTablesWithHolenumber(tables: "Grout", strDate: self.strDate, projectID: projectId, holenumber: self.holeNumber) == true {
                        self.updateCoredata(isOffline: false, edit: false, isShowAlert: true)
                    }
                    else{
                        self.saveCoredata(isOffline: isOffline, edit: edit, msg: json["response"]["msg"].stringValue, isShow: true)
                    }
                
            }

            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  error.localizedDescription)
            }
            
            print(error.localizedDescription)
        }
        
    }
    
    
    
    
    //MARK:- fetchDetails function
    func  fetchDetails(){
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid": projectId,"table":"hc_grout","date": strDate ,"holenumber" : holeNumber]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                  self.setUpData()
        return
            }
            self.mainDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
            self.mainDict.updateValue(self.holeNumber, forKey: "holenumber")
            
            
            if ProjectsOffline.sharedInstance.getOfflineTablesWithHolenumber(tables: "Grout", strDate: self.strDate, projectID: projectId, holenumber: self.holeNumber) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false)
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false)
                
            }
            self.getCoreData()
          
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    
}
