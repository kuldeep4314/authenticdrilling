//
//  TravelViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-25.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit
import CoreData
class TravelViewController: ViewController,UITextFieldDelegate{

    @IBOutlet weak var txtHotelStartTime: UITextField!
    @IBOutlet weak var txtMobStopTime: UITextField!
    @IBOutlet weak var txtMobStartTime: UITextField!
    @IBOutlet weak var txtHotelStopTime: UITextField!
    
    @IBOutlet weak var txtIntermidateStartTime: UITextField!
    @IBOutlet weak var txtIntermidateStopTime: UITextField!
    
    @IBOutlet weak var txtDemobeStartTime: UITextField!
    @IBOutlet weak var txtDemobeStopTime: UITextField!
    
    @IBOutlet weak var txtJobSiteStartTime: UITextField!
    @IBOutlet weak var txtJobSiteStopTime: UITextField!
    var sessions: [NSManagedObject] = []
     var mainDict =  [String: Any]()
    var strDate = String()
   var projectInfo :ProjectData?
    var isEdit = Bool()
    var arraykeys = ["mobsite_starttime","mobsite_stoptime","hoteltravel_starttime","hoteltravel_stoptime","intermediate_starttime","intermediate_stoptime","demobe_starttime","demobe_stoptime","jobsite_starttime","jobsite_stoptime"]
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        
        if Network.isConnectedToNetwork() == true {
            self.getOfflineData()
        }
        else{
            self.getCoreData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpView(){
        
        for i in 1..<11{
            
            let txt = self.view.viewWithTag(i) as! UITextField
            txt.delegate = self
            let picker = UIDatePicker()
            picker.addTarget(self, action: #selector(self.dateChange(sender:)), for: .valueChanged)
            picker.datePickerMode = .time
            picker.locale = Locale(identifier: "en_GB")
            picker.tag = i + 20
            txt.inputView = picker
            
            txt.rightview(Img: #imageLiteral(resourceName: "imgWatch"))
            txt.leftview()
           
            let vc = self.view.viewWithTag( i + 100)
            vc?.addshodowToView()
            let txt1 = self.view.viewWithTag(i + 200) as? UITextField
            txt1?.leftview(image: #imageLiteral(resourceName: "imgTimer"))
        }
        
        
    }
    
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.mainDict.updateValue(textField.text ?? ""  , forKey: arraykeys[textField.tag - 1])
        print(self.mainDict)
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm"
    
        if textField.tag == 1 || textField.tag == 2 {
            
            if  txtMobStartTime.text != "" &&  txtMobStopTime.text != "" {
                 let dateCurrent = formatter.string(from: Date())
                
                let firstDate = dateformatter.date(from: "\(dateCurrent) \(txtMobStartTime.text ?? "00:00")")
                let secondDate = dateformatter.date(from: "\(dateCurrent) \(txtMobStopTime.text ?? "00:50")")
                
                
                let check = firstDate?.timeIntervalSince(secondDate!)
                
                if  Int(check!) < 0 {
                    self.mainDict.updateValue(textField.text ?? ""  , forKey: arraykeys[textField.tag - 1])
                    
                }
                else{
                    
                     self.mainDict.updateValue(""  , forKey: arraykeys[textField.tag - 1])
                    
                    
                    Comman.betweenTimeFunction(textField: textField, textStart: txtMobStartTime, textStop: txtMobStopTime)
                    
                
                }
               
            }
       }
        
        if textField.tag == 3 || textField.tag == 4 {
            
            if  txtHotelStartTime.text != "" &&  txtHotelStopTime.text != "" {
                let dateCurrent = formatter.string(from: Date())
                
                let firstDate = dateformatter.date(from: "\(dateCurrent) \(txtHotelStartTime.text ?? "00:00")")
                let secondDate = dateformatter.date(from: "\(dateCurrent) \(txtHotelStopTime.text ?? "00:50")")
                
                
                let check = firstDate?.timeIntervalSince(secondDate!)
                
                if  Int(check!) < 0 {
                    self.mainDict.updateValue(textField.text ?? ""  , forKey: arraykeys[textField.tag - 1])
                    
                }
                else{
                    
                    self.mainDict.updateValue(""  , forKey: arraykeys[textField.tag - 1])
       Comman.betweenTimeFunction(textField: textField, textStart: txtHotelStartTime, textStop: txtHotelStopTime)
                }
                
            }
            
            
        }
        
        
        if textField.tag == 5 || textField.tag == 6 {
            
            if  txtIntermidateStartTime.text != "" &&  txtIntermidateStopTime.text != "" {
                let dateCurrent = formatter.string(from: Date())
                
                let firstDate = dateformatter.date(from: "\(dateCurrent) \(txtIntermidateStartTime.text ?? "00:00")")
                let secondDate = dateformatter.date(from: "\(dateCurrent) \(txtIntermidateStopTime.text ?? "00:50")")
                
                
                let check = firstDate?.timeIntervalSince(secondDate!)
                
                if  Int(check!) < 0 {
                    self.mainDict.updateValue(textField.text ?? ""  , forKey: arraykeys[textField.tag - 1])
                    
                }
                else{
                    
                    self.mainDict.updateValue(""  , forKey: arraykeys[textField.tag - 1])
                     Comman.betweenTimeFunction(textField: textField, textStart: txtIntermidateStartTime, textStop: txtIntermidateStopTime)
                    
                }
                
            }
            
        }

        if textField.tag == 7 || textField.tag == 8 {
            
            if  txtDemobeStartTime.text != "" &&  txtDemobeStopTime.text != "" {
                let dateCurrent = formatter.string(from: Date())
                
                let firstDate = dateformatter.date(from: "\(dateCurrent) \(txtDemobeStartTime.text ?? "00:00")")
                let secondDate = dateformatter.date(from: "\(dateCurrent) \(txtDemobeStopTime.text ?? "00:50")")
                
                
                let check = firstDate?.timeIntervalSince(secondDate!)
                
                if  Int(check!) < 0 {
                    self.mainDict.updateValue(textField.text ?? ""  , forKey: arraykeys[textField.tag - 1])
                    
                }
                else{
                    
                    self.mainDict.updateValue(""  , forKey: arraykeys[textField.tag - 1])
                    Comman.betweenTimeFunction(textField: textField, textStart: txtDemobeStartTime, textStop: txtDemobeStopTime)
                    
                }
                
            }
            
        }

        if textField.tag == 9 || textField.tag == 10 {
            
            if  txtJobSiteStartTime.text != "" &&  txtJobSiteStopTime.text != "" {
                let dateCurrent = formatter.string(from: Date())
                
                let firstDate = dateformatter.date(from: "\(dateCurrent) \(txtJobSiteStartTime.text ?? "00:00")")
                let secondDate = dateformatter.date(from: "\(dateCurrent) \(txtJobSiteStopTime.text ?? "00:50")")
                
                
                let check = firstDate?.timeIntervalSince(secondDate!)
                
                if  Int(check!) < 0 {
                    self.mainDict.updateValue(textField.text ?? ""  , forKey: arraykeys[textField.tag - 1])
                    
                }
                else{
                    
                    self.mainDict.updateValue(""  , forKey: arraykeys[textField.tag - 1])
                   Comman.betweenTimeFunction(textField: textField, textStart: txtJobSiteStartTime, textStop: txtJobSiteStopTime)
                    
                }
                
            }
            
        }
        
        
        
        for i in 1..<11{
            let txt = self.view.viewWithTag(i) as! UITextField
            txt.text = JSON(self.mainDict)[self.arraykeys[i-1]].stringValue
        }
   }
    
    
    @objc  func dateChange(sender: UIDatePicker){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        print(sender.tag)
        print(formatter.string(from: sender.date))
           let txt = self.view.viewWithTag(sender.tag - 20) as! UITextField
        txt.text = formatter.string(from: sender.date)
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSaveTapped(_ sender: Any) {
         print(self.mainDict)
        if  (JSON(self.mainDict)[arraykeys[0]].stringValue != "" &&  JSON(self.mainDict)[arraykeys[1]].stringValue != "") ||
            (JSON(self.mainDict)[arraykeys[2]].stringValue != "" &&  JSON(self.mainDict)[arraykeys[3]].stringValue != "") ||
            (JSON(self.mainDict)[arraykeys[4]].stringValue != "" &&  JSON(self.mainDict)[arraykeys[5]].stringValue != "") ||
            (JSON(self.mainDict)[arraykeys[6]].stringValue != "" &&  JSON(self.mainDict)[arraykeys[7]].stringValue != "") ||
            (JSON(self.mainDict)[arraykeys[8]].stringValue != "" &&  JSON(self.mainDict)[arraykeys[9]].stringValue != "")
            
            {
                if Network.isConnectedToNetwork() == true {
                    
                    self.travelApiCall(isOffline: false, edit: false)
                }
                else  if Network.isConnectedToNetwork() == false {
                    
                    if ProjectsOffline.sharedInstance.getOfflineTables(tables: "Travel", strDate: self.strDate, projectID: projectId) == true
                    {
                        self.updateCoredata(isOffline: true, edit: true, isShowAlert: true)
                    }
                        
                    else{
                        ProjectsOffline.sharedInstance.getOfflineDates(isOffline: true, strDate: self.strDate, projectID: projectId)
                        self.saveCoredata(isOffline: true, edit: false, msg: "travel list added successfully", isShow: true)
                    }
                }
            
            
        }
        else{
            
            self.showAlert(messageStr: "Missing required information.")
        }
      
        
    }
    
    func getOfflineData(){
        
        let offlineDetails = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Travel")
        print(offlineDetails)
        if offlineDetails.count > 0 {
       let parameters : Parameters = ["action": "travel_category" ,"data" :  offlineDetails ]
            
             self.syncOfflineMultipleApi(parameter: parameters)
            
        }
        else{
            self.fetchDetails()
        }
        
    }
   
    //MARK:- Update Core Data
    func updateCoredata(isOffline : Bool , edit : Bool , isShowAlert : Bool){
        
        let managedContext = CoreDataStack.managedObjectContext
        let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Travel")
        updateFetch.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , strDate)
        do {
            let results = try managedContext.fetch(updateFetch) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                
                // In my case, I only updated the first item in results
                let session = results![0]
                
                let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
                session.setValue(data1, forKey: "dataDetails")
                
                session.setValue(strDate, forKey: "date")
                session.setValue(projectId, forKey: "projectid")
                session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                session.setValue(isOffline, forKey: "isOffline")
                session.setValue(edit, forKey: "edit")
                
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try managedContext.save()
            
            
            if isShowAlert == true {
                self.navigationController?.popViewController(animated: true)
                appDelegates.window?.rootViewController?.showAlert(messageStr:  "travel list updated successfully")
            }
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
        
    }
    
    
    //MARK:- Save Core Data Function
    func saveCoredata(isOffline : Bool,edit: Bool , msg : String , isShow : Bool){
        print(isOffline)
        print(edit)
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "Travel",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
        session.setValue(data1, forKey: "dataDetails")
        
        session.setValue(strDate, forKey: "date")
        session.setValue(projectId, forKey: "projectid")
        session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
        session.setValue(isOffline, forKey: "isOffline")
        session.setValue(edit, forKey: "edit")
        
        
        CoreDataStack.saveContext()
        
        
        if isShow == true{
            self.navigationController?.popViewController(animated: true)
            appDelegates.window?.rootViewController?.showAlert(messageStr: msg)
        }
        
    }
    //MARK:- Get Core Data Function
    func getCoreData() {
        
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Travel")
        
        fetchRequest.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , strDate)
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            for session in sessions{
                var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                
                dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                
                dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                
                self.mainDict = dictionary
                
            }
            
            DispatchQueue.main.async {
                for i in 1..<11{
                    let txt = self.view.viewWithTag(i) as! UITextField
                    txt.text = JSON(self.mainDict)[self.arraykeys[i-1]].stringValue
                }
            }
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
   
    //MARK:- fetchDetails function
    func  fetchDetails(){
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid": projectId,"table":"travel_category","date": strDate]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
          self.mainDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
        
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "Travel", strDate: self.strDate, projectID: projectId) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false)
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false)
                
            }
            self.getCoreData()
            
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    
    
    
    func updateMainDictFunction ( parameter  : Parameters ){
        self.mainDict = parameter
        
    }
    
    //MARK: - SyncOfflineMultipleApi
    func syncOfflineMultipleApi(parameter :Parameters){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(projectUrlOffline, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            ProjectsOffline.sharedInstance.updateCoredataTables(tables: "Travel")
              self.getCoreData()
            
          
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
              self.getCoreData()
            print(error.localizedDescription)
        }
    }

   
    
    
    //MARK:- Travel API
    func travelApiCall(isOffline : Bool , edit : Bool ){
        
        let parameter  : Parameters  =  ["action": "travel_category","date": strDate ,"userid": standard.value(forKey: "id")!, "projectid": projectId, "mobsite_starttime": txtMobStartTime.text ?? "", "mobsite_stoptime": txtMobStopTime.text ?? "", "hoteltravel_starttime": txtHotelStartTime.text ?? "", "hoteltravel_stoptime": txtHotelStopTime.text ?? "", "intermediate_starttime": txtIntermidateStartTime.text ?? "", "intermediate_stoptime": txtIntermidateStopTime.text ?? "", "demobe_starttime": txtDemobeStartTime.text ?? "", "demobe_stoptime": txtDemobeStopTime.text ?? "" ,"jobsite_starttime": txtJobSiteStartTime.text ?? ""
        ,"jobsite_stoptime": txtJobSiteStopTime.text ?? "" ]
        print(parameter)
        
         self.updateMainDictFunction(parameter: parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
        
        
            guard json["response"]["result"].stringValue == "201" else{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  json["response"]["msg"].stringValue )
                return
            }
            
            DispatchQueue.main.async {
                
               
                    
                    ProjectsOffline.sharedInstance.getOfflineDates(isOffline: isOffline, strDate: self.strDate, projectID: projectId)
                     if ProjectsOffline.sharedInstance.getOfflineTables(tables: "Travel", strDate: self.strDate, projectID: projectId) == true {
                        self.updateCoredata(isOffline: false, edit: false, isShowAlert: true)
                    }
                    else{
                        self.saveCoredata(isOffline: isOffline, edit: edit, msg: json["response"]["msg"].stringValue, isShow: true)
                        
                    }
            }

            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  error.localizedDescription)
            }
            
            print(error.localizedDescription)
        }
        
    }
    

}
