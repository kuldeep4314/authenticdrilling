//
//  BillingAndRatesViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-12-15.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit

class BillingAndRatesViewController: ViewController ,UIWebViewDelegate{
var linkStr = String()
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL (string: linkStr)
        let request = URLRequest(url: url!)
        webView.loadRequest(request)
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- WebView Delegates Methods
    public func webViewDidStartLoad(_ webView: UIWebView){
        self.view.startIndicator()
    }
    
    
    public func webViewDidFinishLoad(_ webView: UIWebView){
        self.view.stopIndicator()
    }
    
    
    public func webView(_ webView: UIWebView, didFailLoadWithError error: Error){
          appDelegates.window?.rootViewController?.showAlert(messageStr:  error.localizedDescription)
        self.view.stopIndicator()
    }


}
