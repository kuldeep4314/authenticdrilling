//
//  AbandonViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-29.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit
import CoreData
class AbandonViewController: ViewController ,UITextViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate{
 let picker = UIPickerView()
    let picker1 = UIPickerView()
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var txtGravity: UITextField!
    @IBOutlet weak var txtTo: UITextField!
    @IBOutlet weak var txtFrom: UITextField!
    @IBOutlet weak var txtNoLbs: UITextField!
    @IBOutlet weak var txtNoBags: UITextField!
    @IBOutlet weak var txtMaterial: UITextField!
    @IBOutlet weak var txtStaticWaterLevel: UITextField!
    var holeNumber = String()
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var txtWell: UITextField!
    var arrayPicker = [String]()
     let arrayPicker1 = ["Gravity","Tremie"]
    let arrayImage = [#imageLiteral(resourceName: "imgWater"),#imageLiteral(resourceName: "imgHoleDiameters"),#imageLiteral(resourceName: "imgBags"),#imageLiteral(resourceName: "imgLbs"),#imageLiteral(resourceName: "imgDepth"),#imageLiteral(resourceName: "imgDepth"),#imageLiteral(resourceName: "imgGravity")]
    
     var sessions: [NSManagedObject] = []
    
    var strDate = String()
    var projectInfo :ProjectData?
    
    var textFieldKeys = ["well","location","static_water","material","bags","lbs","from_ft","to_ft","gravity"]
    var mainDict =  [String: Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtView.text = "Enter your notes"
        self.txtView.textColor = UIColor.lightGray
        picker.dataSource = self
        picker.delegate = self
        picker1.dataSource = self
        picker1.delegate = self
        txtMaterial.inputView = picker
        txtGravity.inputView = picker1
        if  let decoded  = standard.value(forKey: "catData") as? [String : Any]{
            arrayPicker  = JSON(decoded)["Material list"].arrayValue.map({$0["name"].stringValue})
          
            
        }
        self.setUpView()
        
        // Do any additional setup after loading the view.
    }
    func setUpView(){
        
        txtLocation.rightview(Img: #imageLiteral(resourceName: "imgLocation1"))
        txtMaterial.rightview(Img: #imageLiteral(resourceName: "imgDropDown"))
        txtGravity.rightview(Img: #imageLiteral(resourceName: "imgDropDown"))
        txtMaterial.leftview()
          txtView.addshodowToTextView()
        txtView.delegate = self
        for i in 1..<10 {
            
            let txt = self.view.viewWithTag(i + 100) as! UITextField
            txt.addshodowToTextField()
            if i > 2{
                txt.leftview(image:  arrayImage[i - 3])
            }
            else{
                txt.leftview()
            }
            let txtValue = self.view.viewWithTag(i) as! UITextField
            txtValue.delegate = self
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
        if Network.isConnectedToNetwork() == true {
            self.getOfflineData()
        }
        else{
            self.getCoreData()
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
         //MARK:- TextField Delegates
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        mainDict.updateValue(textField.text ?? "", forKey:textFieldKeys[textField.tag - 1])
        print(mainDict)
        
    }
    @IBAction func btnSaveTapped(_ sender: Any) {
      
        
        if Network.isConnectedToNetwork() == true {
            
            self.addAbandonApiCall(isOffline: false, edit: false)
        }
        else  if Network.isConnectedToNetwork() == false {
            
            if ProjectsOffline.sharedInstance.getOfflineTablesWithHolenumber(tables: "Abandon", strDate: self.strDate, projectID: projectId, holenumber: self.holeNumber) == true
            {
                self.updateCoredata(isOffline: true, edit: true, isShowAlert: true)
            }
                
            else{
                ProjectsOffline.sharedInstance.getOfflineDates(isOffline: true, strDate: self.strDate, projectID: projectId)
                self.saveCoredata(isOffline: true, edit: false, msg: "Hole Completition Abandon list added successfully", isShow: true)
            }
        }
        
        
    }
    

    func getOfflineData(){
        
        let offlineDetails = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Abandon")
        print(offlineDetails)
        if offlineDetails.count > 0 {
            let parameters : Parameters = ["action": "hc_abandon" ,"data" :  offlineDetails ]
            
            self.syncOfflineMultipleApi(parameter: parameters)
            
        }
        else{
            self.fetchDetails()
        }
        
    }
    
    
    
    //MARK: - SyncOfflineMultipleApi
    func syncOfflineMultipleApi(parameter :Parameters){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(holeCompletionOfflineUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            ProjectsOffline.sharedInstance.updateCoredataTables(tables: "Abandon")
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            self.getCoreData()
            print(error.localizedDescription)
        }
    }
    
    
    
    
    
    
    //MARK:- TextView Delegates
    func textViewDidChange(_ textView: UITextView) {
        
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        
        if self.txtView.textColor == UIColor.lightGray
        {
            self.txtView.text = ""
            self.txtView.textColor = UIColor.black
            
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
        if self.txtView.text.isEmpty {
            self.txtView.text = "Enter your notes"
            self.txtView.textColor = UIColor.lightGray
            mainDict.updateValue( textView.text ?? "", forKey:"notes")
        }
      
            else{
            self.txtView.textColor = UIColor.black
        mainDict.updateValue( textView.text ?? "", forKey:"notes")
                print(mainDict)
            }
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        print(newText.count)
        return newText.count <= 200
    }

    
    
    func setUpData(){
        for i in 1..<10 {
            
            let txt = self.view.viewWithTag(i) as! UITextField
            txt.text =  JSON(self.mainDict)["\(self.textFieldKeys[i - 1])"].stringValue
        }
        self.txtView.text = JSON(self.mainDict)["notes"].stringValue.base64Decoded()?.base64Decoded()
        
        if self.txtView.text == ""{
            self.txtView.text = "Enter your notes"
            self.txtView.textColor = UIColor.lightGray
        }
        else{
            self.txtView.textColor = UIColor.black
        }
        
    
    }
    
    //MARK:- Update Core Data
    func updateCoredata(isOffline : Bool , edit : Bool , isShowAlert : Bool){
        
        let managedContext = CoreDataStack.managedObjectContext
        let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Abandon")
        updateFetch.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@) AND (holenumber = %@)",  projectId , strDate,self.holeNumber)
        do {
            let results = try managedContext.fetch(updateFetch) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                
                // In my case, I only updated the first item in results
                let session = results![0]
                
                let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
                session.setValue(data1, forKey: "dataDetails")
                
                session.setValue(strDate, forKey: "date")
                session.setValue(projectId, forKey: "projectid")
                session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                session.setValue(isOffline, forKey: "isOffline")
                session.setValue(edit, forKey: "edit")
                session.setValue(self.holeNumber, forKey: "holenumber")
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try managedContext.save()
            
            
            if isShowAlert == true {
                self.navigationController?.popViewController(animated: true)
                appDelegates.window?.rootViewController?.showAlert(messageStr:  "Hole Completition Abandon list updated successfully")
            }
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
        
    }
    
    
    //MARK:- Save Core Data Function
    func saveCoredata(isOffline : Bool,edit: Bool , msg : String , isShow : Bool){
        print(isOffline)
        print(edit)
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "Abandon",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
        session.setValue(data1, forKey: "dataDetails")
        
        session.setValue(strDate, forKey: "date")
        session.setValue(projectId, forKey: "projectid")
        session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
        session.setValue(isOffline, forKey: "isOffline")
        session.setValue(edit, forKey: "edit")
        session.setValue(self.holeNumber, forKey: "holenumber")
        
        CoreDataStack.saveContext()
        
        
        if isShow == true{
            self.navigationController?.popViewController(animated: true)
            appDelegates.window?.rootViewController?.showAlert(messageStr: msg)
        }
        
    }
    //MARK:- Get Core Data Function
    func getCoreData() {
        
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Abandon")
        
        fetchRequest.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@) AND (holenumber = %@)",  projectId , strDate,self.holeNumber)
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            for session in sessions{
                var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                
                dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                dictionary.updateValue(session.value(forKey: "holenumber") ?? "", forKey: "holeNumber")
                dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                
                self.mainDict = dictionary
                
            }
            
            DispatchQueue.main.async {
                self.setUpData()
            }
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    
    
    
    
    
    
    
    
    
    //MARK:- PickerView Data Source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == picker1 {
             return arrayPicker1.count
            
        }
        else{
             return arrayPicker.count
        }
       
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
          if pickerView == picker1 {
             return arrayPicker1[row]
        }
          else{
        return arrayPicker[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
          if pickerView == picker1 {
             self.txtGravity.text = arrayPicker1[row]
        }
          else{
             self.txtMaterial.text = arrayPicker[row]
        }
       
    }
    //MARK:- AddAbandonApiCall
    func addAbandonApiCall(isOffline : Bool , edit : Bool ){
        
        
        let parameter  : Parameters  =  ["action": "hc_abandon", "userid": standard.value(forKey: "id")!, "projectid":  projectId,"date":strDate
            , "well": JSON(self.mainDict)["well"].stringValue
            , "location": JSON(self.mainDict)["location"].stringValue
            , "static_water": JSON(self.mainDict)["static_water"].stringValue
            , "material": JSON(self.mainDict)["material"].stringValue
            , "bags": JSON(self.mainDict)["bags"].stringValue
            , "lbs": JSON(self.mainDict)["lbs"].stringValue
            , "from_ft": JSON(self.mainDict)["from_ft"].stringValue
             , "holenumber": holeNumber
            , "to_ft": JSON(self.mainDict)["to_ft"].stringValue
            , "gravity": JSON(self.mainDict)["gravity"].stringValue
            , "notes": JSON(self.mainDict)["notes"].stringValue.base64Decoded() ?? ""
        ]
        
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(holeCompletionUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  json["response"]["msg"].stringValue )
                return
            }
            
            
            DispatchQueue.main.async {
                
                ProjectsOffline.sharedInstance.getOfflineDates(isOffline: isOffline, strDate: self.strDate, projectID: projectId)
                
                
                if ProjectsOffline.sharedInstance.getOfflineTablesWithHolenumber(tables: "Abandon", strDate: self.strDate, projectID: projectId, holenumber: self.holeNumber) == true {
                    self.updateCoredata(isOffline: false, edit: false, isShowAlert: true)
                }
                else{
                    self.saveCoredata(isOffline: isOffline, edit: edit, msg: json["response"]["msg"].stringValue, isShow: true)
                }
                
            }

            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  error.localizedDescription)
            }
            
            print(error.localizedDescription)
        }
        
    }
    
    //MARK:- fetchDetails function
    func  fetchDetails(){
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid": projectId,"table":"hc_abandon","date": strDate,"holenumber": holeNumber]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            self.mainDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
            
            self.mainDict.updateValue(self.holeNumber, forKey: "holenumber")
            
            
            if ProjectsOffline.sharedInstance.getOfflineTablesWithHolenumber(tables: "Abandon", strDate: self.strDate, projectID: projectId, holenumber: self.holeNumber) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false)
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false)
                
            }
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    
}
