//
//  DamagedToolViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-30.
//  Copyright © 2017 Avatar Singh. All rights reserved.

import UIKit
import CoreData
class DamagedToolViewController: ViewController,UITableViewDelegate,UITableViewDataSource ,UITextFieldDelegate,UITextViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource{
    let arrNames = ["Auger Teeth","Rock Teeth","Split Spoon \n Sample","Tires","Odex Casing" , "Odex Lead"]
    let arrImagesNames = [#imageLiteral(resourceName: "imgAugar"),#imageLiteral(resourceName: "imgRock"),#imageLiteral(resourceName: "imgSplit")]
    let arrPlaceHolder = ["Each","Pieces","Pieces"]
    let arrayKeysHeaders = ["auger_teeth","rock_teeth","split_spoon","tires","odex_casing","odexlead" ,"notes"]
    var sessions: [NSManagedObject] = []
    var dictKeyValues = ["31":"tires_qty","32":"tires_unit","41":"odexcase_qty","42":"odexcase_unit" ,"51":"odexlead_qty" , "52":"odexlead_unit"
    ]
    var strDate = String()
    var projectInfo :ProjectData?
    
    var mainDict =  [String: Any]()
      var dropDownList = [JSON]()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
        if  let decoded  = standard.value(forKey: "catData") as? [String : Any]{
            
            dropDownList  = JSON(decoded)["Damaged Tooling"].arrayValue
            print(dropDownList)
        }
        tableView.tableFooterView = UIView()
        
        if Network.isConnectedToNetwork() == true {
            self.getOfflineData()
        }
        else{
            self.getCoreData()
        }

    }
    func setUpView(){
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.tag < 3{
        
            mainDict.updateValue(textField.text ?? "", forKey:arrayKeysHeaders[textField.tag])
        }
        else{
            print(dictKeyValues[String(textField.tag)]!)
            mainDict.updateValue(textField.text ?? "", forKey:dictKeyValues[String(textField.tag)]!)
        }
        
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        if JSON(self.mainDict)[arrayKeysHeaders[sender.view!.tag]].stringValue == "1" {
            self.mainDict.updateValue("0", forKey: arrayKeysHeaders[sender.view!.tag])
            
        }
        else{
            self.mainDict.updateValue("1", forKey: arrayKeysHeaders[sender.view!.tag])
        }
        
        self.mainDict.updateValue("", forKey: dictKeyValues[String(sender.view!.tag * 10 + 1 )]!)
        self.mainDict.updateValue("", forKey: dictKeyValues[String(sender.view!.tag * 10 + 2 )]!)
        self.tableView.reloadData()
    }
    @objc func btnSaveTapped(sender : UIButton){
        
     
        
        if Network.isConnectedToNetwork() == true {
            
            self.damagedApiCall(isOffline: false, edit: false)
        }
        else  if Network.isConnectedToNetwork() == false {
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "DamagedTools", strDate: self.strDate, projectID: projectId) == true
            {
                self.updateCoredata(isOffline: true, edit: true, isShowAlert: true)
            }
                
            else{
                ProjectsOffline.sharedInstance.getOfflineDates(isOffline: true, strDate: self.strDate, projectID: projectId)
                self.saveCoredata(isOffline: true, edit: false, msg: "Add consumption Damaged Tool list added successfully", isShow: true)
            }
        }
    }
    
    
    func getOfflineData(){
        
        let offlineDetails = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "DamagedTools")
        print(offlineDetails)
        if offlineDetails.count > 0 {
            let parameters : Parameters = ["action": "ac_damaged_tool" ,"data" :  offlineDetails ]
            
            self.syncOfflineMultipleApi(parameter: parameters)
            
        }
        else{
            self.fetchDetails()
        }
        
    }
    

    //MARK: - SyncOfflineMultipleApi
    func syncOfflineMultipleApi(parameter :Parameters){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(consumableUrlOffline, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            ProjectsOffline.sharedInstance.updateCoredataTables(tables: "DamagedTools")
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
              self.getCoreData()
            print(error.localizedDescription)
        }
    }
    
    
    //MARK:- Update Core Data
    func updateCoredata(isOffline : Bool , edit : Bool , isShowAlert : Bool){
        
        let managedContext = CoreDataStack.managedObjectContext
        let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "DamagedTools")
        updateFetch.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , strDate)
        do {
            let results = try managedContext.fetch(updateFetch) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                
                // In my case, I only updated the first item in results
                let session = results![0]
                
                let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
                session.setValue(data1, forKey: "dataDetails")
                
                session.setValue(strDate, forKey: "date")
                session.setValue(projectId, forKey: "projectid")
                session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                session.setValue(isOffline, forKey: "isOffline")
                session.setValue(edit, forKey: "edit")
                
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try managedContext.save()
            
            
            if isShowAlert == true {
                self.navigationController?.popViewController(animated: true)
                appDelegates.window?.rootViewController?.showAlert(messageStr:  "Add consumption Damaged Tool list updated successfully")
            }
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
        
    }
    
    
    //MARK:- Save Core Data Function
    func saveCoredata(isOffline : Bool,edit: Bool , msg : String , isShow : Bool){
        print(isOffline)
        print(edit)
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "DamagedTools",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
        session.setValue(data1, forKey: "dataDetails")
        
        session.setValue(strDate, forKey: "date")
        session.setValue(projectId, forKey: "projectid")
        session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
        session.setValue(isOffline, forKey: "isOffline")
        session.setValue(edit, forKey: "edit")
        
        
        CoreDataStack.saveContext()
        
        
        if isShow == true{
            self.navigationController?.popViewController(animated: true)
            appDelegates.window?.rootViewController?.showAlert(messageStr: msg)
        }
        
    }
    //MARK:- Get Core Data Function
    func getCoreData() {
        
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "DamagedTools")
        
        fetchRequest.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , strDate)
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            for session in sessions{
                var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                
                dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                
                dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                
                self.mainDict = dictionary
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //MARK:- TableView Data Source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrNames.count + 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section < 3 {
             return 1
            
        }
        
        if section > 2  && section < 6{
            if JSON(self.mainDict)[arrayKeysHeaders[section]].stringValue == "1" {
                return 1
            }
            else{
                return 0
            }
        }
        else{
            
            return 0
        }
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section < 3 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell3") as! SupplyCell
            cell.lblName.text = arrNames[indexPath.section]
            cell.txtValue.placeholder = arrPlaceHolder [indexPath.section]
            cell.imgView.image = arrImagesNames[indexPath.section]
            
            cell.txtValue.tag = indexPath.section
            
            cell.txtValue.text = JSON(self.mainDict)[arrayKeysHeaders[indexPath.section]].stringValue
            cell.txtValue.delegate = self
            
            cell.txtValue.rightview(Img: #imageLiteral(resourceName: "imgDropDown"))
            
            let picker = UIPickerView()
            picker.dataSource = self
            picker.delegate = self
            picker.tag = indexPath.section
            cell.txtValue.inputView = picker
            
            
            return cell
            
            
        }
        
        
      else {
        
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! SupplyCell
          cell.txtUnit.rightview(Img: #imageLiteral(resourceName: "imgDropDown"))
        cell.txtQty.tag = indexPath.section * 10 + 1
        cell.txtUnit.tag = indexPath.section * 10 + 2
        
        cell.txtQty.text = JSON(self.mainDict)[dictKeyValues["\(cell.txtQty.tag )"]!].stringValue
        cell.txtUnit.text = JSON(self.mainDict)[dictKeyValues["\(cell.txtUnit.tag )"]!].stringValue
        cell.txtUnit.delegate = self
        cell.txtQty.rightVC()
        cell.txtQtyTitle.leftview()
        
        cell.txtQty.delegate  = self
        
        let picker = UIPickerView()
        picker.dataSource = self
        picker.delegate = self
        picker.tag = indexPath.section
        cell.txtUnit.inputView = picker
        return cell
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        return 60
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if self.arrNames.count == section {
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell4") as! SupplyCell
              cell.txtView.delegate = self
      
             cell.txtView.text = JSON(self.mainDict)[arrayKeysHeaders[section]].stringValue.base64Decoded()
            if  cell.txtView.text == "" {
                cell.txtView.text = "Enter your notes"
                cell.txtView.textColor = UIColor.lightGray
            }
            else{
                cell.txtView.textColor = UIColor.black
            }
                cell.txtView.tag = section
            return cell.contentView
        }
        
        
       else if self.arrNames.count  + 1 == section{
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! SupplyCell
            cell.btnSave.addTarget(self, action: #selector(self.btnSaveTapped(sender:)), for: .touchUpInside)
            return cell.contentView
        }
        else if  section > 2  && section < 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SupplyCell
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
            
            cell.bgView.addGestureRecognizer(tap)
            cell.bgView.tag = section
            cell.bgView.isUserInteractionEnabled = true
            
            cell.bgView.addGestureRecognizer(tap)
            
             if JSON(self.mainDict)[arrayKeysHeaders[section]].stringValue == "1" {
                cell.txtName.rightview(Img: #imageLiteral(resourceName: "imgCheck"))
            }
            else{
                cell.txtName.rightview(Img: #imageLiteral(resourceName: "imgUnselect"))
            }
            cell.txtName.text = self.arrNames[section]
            return cell.contentView
        }
        else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell3") as! SupplyCell
           
            
            return cell.contentView
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section < 3{
            
            return 0
        }
        
       else if self.arrNames.count == section ||  self.arrNames.count == section{
            return 120
        }
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }

    //MARK:- TextView Delegates
    func textViewDidChange(_ textView: UITextView) {
        
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        
        if textView.textColor == UIColor.lightGray
        {
           textView.text = ""
           textView.textColor = UIColor.black
            
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = "Enter your notes"
           textView.textColor = UIColor.lightGray
            mainDict.updateValue(
                "", forKey:"notes")
        }
        else{
            mainDict.updateValue( textView.text ?? "", forKey:"notes")
        }
    }
    //MARK:- PickerView Data Source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dropDownList[pickerView.tag]["subcategory"].arrayValue.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dropDownList[pickerView.tag]["subcategory"][row]["name"].stringValue
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
          print(pickerView.tag)
        let cell = self.tableView.cellForRow(at: IndexPath.init(row: 0, section: pickerView.tag)) as! SupplyCell
        
          if pickerView.tag < 3{
             cell.txtValue.text = dropDownList[pickerView.tag]["subcategory"][row]["name"].stringValue
        }
        else{
             cell.txtUnit.text = dropDownList[pickerView.tag]["subcategory"][row]["name"].stringValue
        }
        
        
       
    }
    
    //MARK:- DamagedApiCall
    func damagedApiCall(isOffline: Bool, edit: Bool){
        let parameter  : Parameters  =  ["action": "ac_damaged_tool", "userid": standard.value(forKey: "id")!, "projectid":  projectId,"date":strDate
            , "auger_teeth": JSON(self.mainDict)["auger_teeth"].stringValue
            , "rock_teeth": JSON(self.mainDict)["rock_teeth"].stringValue
            , "split_spoon": JSON(self.mainDict)["split_spoon"].stringValue
            , "tires": JSON(self.mainDict)["tires"].stringValue
            , "tires_qty": JSON(self.mainDict)["tires_qty"].stringValue
            , "tires_unit": JSON(self.mainDict)["tires_unit"].stringValue
            , "odex_casing": JSON(self.mainDict)["odex_casing"].stringValue
            , "odexcase_qty": JSON(self.mainDict)["odexcase_qty"].stringValue
            
            , "odexcase_unit": JSON(self.mainDict)["odexcase_unit"].stringValue
            , "odexlead": JSON(self.mainDict)["odexlead"].stringValue
            
            , "odexlead_qty": JSON(self.mainDict)["odexlead_qty"].stringValue
            , "odexlead_unit": JSON(self.mainDict)["odexlead_unit"].stringValue
            
            , "notes": JSON(self.mainDict)["notes"].stringValue.base64Decoded() ?? ""
        ]
        
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(consumableUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  json["response"]["msg"].stringValue )
                return
            }
            
            
            DispatchQueue.main.async {
                 ProjectsOffline.sharedInstance.getOfflineDates(isOffline: isOffline, strDate: self.strDate, projectID: projectId)
                    
                    
                    if ProjectsOffline.sharedInstance.getOfflineTables(tables: "DamagedTools", strDate: self.strDate, projectID: projectId) == true {
                        self.updateCoredata(isOffline: false, edit: false, isShowAlert: true)
                    }
                    else{
                        self.saveCoredata(isOffline: isOffline, edit: edit, msg: json["response"]["msg"].stringValue, isShow: true)
                        
                    }
           }
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  error.localizedDescription)
            }
            
            print(error.localizedDescription)
        }
        
    }
    
    //MARK:- fetchDetails function
    func  fetchDetails(){
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid": projectId,"table":"ac_damage_tooling","date": strDate]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            self.mainDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
            
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "DamagedTools", strDate: self.strDate, projectID: projectId) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false)
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false)
                
            }
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    
}

