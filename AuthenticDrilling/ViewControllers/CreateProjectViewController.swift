//
//  CreateProjectViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-24.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit
import CoreData
class CreateProjectViewController: ViewController ,UITextFieldDelegate{
    
    @IBOutlet weak var txtStartDate: UITextField!
    
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtClientName: UITextField!
    
    @IBOutlet weak var txtProjectName: UITextField!
    @IBOutlet weak var txtSiteAddress: UITextField!
    
    @IBOutlet weak var txtCityName: UITextField!
    
    @IBOutlet weak var txtAdiJobNumber: UITextField!
    
     var arrayClientPicker = [String]()
      var arrayStatePicker = [String]()
    
    @IBOutlet weak var txtClientJobNumber: UITextField!
    var mainDict =  [String: Any]()
    @IBOutlet weak var btnCreate: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    let datePicker = UIDatePicker()
    @IBOutlet weak var txtClientRepresentative: UITextField!
    var projectInfo :ProjectData?
    var isEdit = Bool()
    
    @IBOutlet weak var txtCrew: UITextField!
    
    let imgArr = [#imageLiteral(resourceName: "imgStartDate"),#imageLiteral(resourceName: "imgClient"),#imageLiteral(resourceName: "imgProject"),#imageLiteral(resourceName: "imgSiteAddress"),#imageLiteral(resourceName: "imgSiteAddress"),#imageLiteral(resourceName: "imgSiteAddress"),#imageLiteral(resourceName: "imgADI"),#imageLiteral(resourceName: "imgClientJobNumber"),#imageLiteral(resourceName: "imgClientRepresentative"),#imageLiteral(resourceName: "imgCrew")]
    var keys = ["start_date","client_name","project_name","site_address","city_name","state","ADI_job_number","client_job_number","client_rep"]
    var keysCheck = ["crew_driller_name","rig","support_truk1","crew_helpers"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.addTarget(self, action: #selector(self.dateChange(sender:)), for: .valueChanged)
        datePicker.datePickerMode = .date
        
        self.txtStartDate.inputView = datePicker
        if isEdit == false {
            let data = DAData.init(json: JSON(standard.value(forKey: "userData") as? [String : Any] ?? [String : Any]()))
            
            mainDict.updateValue(data.trailer1 ?? "", forKey: "trailer1")
            mainDict.updateValue(data.trailer2 ?? "", forKey: "trailer2")
            mainDict.updateValue(data.rig ?? "", forKey: "rig")
            mainDict.updateValue(data.supportTruck1 ?? "", forKey: "support_truk1")
            mainDict.updateValue(data.supportTruck2 ?? "", forKey: "support_truck2")
            mainDict.updateValue("Colorado", forKey: "state")
            txtState.text = "Colorado"
        }
    
        arrayClientPicker =  standard.value(forKey: "client") as? [String] ?? [String]()
         arrayStatePicker =  standard.value(forKey: "state") as? [String] ?? [String]()
        self.setUpView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func setUpView(){
        for i in 1..<10{
            let txt = self.view.viewWithTag(i ) as! UITextField
            
            if i == 2 || i == 6 {
                let picker = UIPickerView()
                picker.dataSource = self
                picker.delegate = self
                picker.tag = 5000 + i
                txt.inputView = picker
                 txt.rightview(Img: #imageLiteral(resourceName: "imgDropDown"))
            }
            
            txt.delegate = self
            let txtTitle = self.view.viewWithTag(i + 100 ) as! UITextField
            txtTitle.leftview(image: imgArr[i-1])
            txtTitle.addshodowToTextField()
        }
        
        txtProjectName.requiredText()
        txtCityName.requiredText()
        txtAdiJobNumber.requiredText()
      
        txtClientRepresentative.requiredText()
        
        
        
        
        let textCrewTitle = self.view.viewWithTag(109 ) as! UITextField
        textCrewTitle.leftview(image: #imageLiteral(resourceName: "imgCrew"))
        self.txtStartDate.rightview(Img: #imageLiteral(resourceName: "IimgStartDate1"))
        self.txtCrew.rightview(Img: #imageLiteral(resourceName: "imgArrow"))
        
        if isEdit == true {
            self.lblTitle.text = "Edit Project"
            self.btnCreate.setTitle("UPDATE", for: .normal)
            
            if Network.isConnectedToNetwork() == true {
                self.loadProject()
            }
            else{
                self.mainDict = projectInfo?.dictionaryRepresentation() ?? [String : Any]()
                for i in 1..<10{
                    let txt = self.view.viewWithTag(i) as! UITextField
                    txt.text = JSON(self.mainDict)["\(self.keys[i - 1])"].stringValue
                }
                
            }
        }
    }
    func setCrewInformation(dict : [String: Any]){
        
        self.mainDict = dict
        print(self.mainDict)
    }
    
    
    @objc  func dateChange(sender: UIDatePicker){
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        txtStartDate.text = formatter.string(from: datePicker.date)
    }
    
    
    @IBAction func crewTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DrillerInformationViewController") as! DrillerInformationViewController
        vc.mainDict =  mainDict
        vc.isEdit = isEdit
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //MARK:- UITEXTFILED DELEGATES
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.mainDict.updateValue(textField.text ?? "", forKey: keys[textField.tag - 1])
        print(mainDict)
        
    }
    
    
    @IBAction func btnBackTapped(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createBtnTapped(_ sender: Any) {
        
        if txtStartDate.text != "" && txtClientName.text != "" && txtProjectName.text != "" && txtCityName.text != "" && txtAdiJobNumber.text != ""  && txtClientRepresentative.text != "" {
            
            
            
            var count = 0
            for  i in 0..<keysCheck.count {
                
                if  JSON(self.mainDict)[keysCheck[i]].stringValue == "" {
                    count = count + 1
                }
                
            }
            
            
            if count == 0 {
                if isEdit == true {
                    self.createProjectApiCall(performAction: "edit")
                }
                else{
                    self.createProjectApiCall(performAction: "create")
                }
                
            }
            else{
                self.showAlert(messageStr: "Missing Required Fields!")
            }
            
            
        }
        else{
            
            self.showAlert(messageStr: "Missing Required Fields!")
        }
    }
    
    func saveCoredata(saveObject : ProjectData, isOffline : Bool , edit : Bool , isLocalAddEdit : Bool){
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "Projects",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
         session.setValue(saveObject.state, forKey: "state")
        session.setValue(saveObject.trailer2, forKey: "trailer2")
        session.setValue(saveObject.clientJobNumber, forKey: "client_job_number")
        session.setValue(saveObject.supportTruck2, forKey: "support_truck2")
        session.setValue(saveObject.pid ?? "", forKey: "pid")
        session.setValue(saveObject.cityName, forKey: "city_name")
        session.setValue(saveObject.crewHelpers, forKey: "crew_helpers")
        session.setValue(saveObject.rig, forKey: "rig")
        session.setValue(saveObject.crewHelpers2, forKey: "crew_helpers2")
        session.setValue(saveObject.aDIJobNumber, forKey: "ADI_job_number".lowercased())
        session.setValue(saveObject.siteAddress, forKey: "site_address")
        session.setValue(saveObject.trailer1, forKey: "trailer1")
        session.setValue(saveObject.projectName, forKey: "project_name")
        session.setValue(saveObject.supportTruk1, forKey: "support_truk1")
        session.setValue(saveObject.addOn, forKey: "add_on")
        session.setValue(saveObject.modifyOn, forKey: "modify_on")
        session.setValue(saveObject.clientName, forKey: "client_name")
        session.setValue(saveObject.perdiems, forKey: "perdiems")
        session.setValue(saveObject.userid, forKey: "userid")
        session.setValue(saveObject.injuries, forKey: "injuries")
        session.setValue(saveObject.crewDrillerName, forKey: "crew_driller_name")
        session.setValue(saveObject.startDate, forKey: "start_date")
        session.setValue(saveObject.clientRep, forKey: "client_rep")
        session.setValue(isOffline, forKey: "isOffline")
        session.setValue(edit, forKey: "edit")
        session.setValue(isLocalAddEdit, forKey: "isLocalAddEdit")
        
        CoreDataStack.saveContext()
        self.navigationController?.popViewController(animated: true)
        appDelegates.window?.rootViewController?.showAlert(messageStr:  "Project created successfully")
    }
    
    func updateCoredata(saveObject : ProjectData, isOffline : Bool , edit : Bool,isLocalAddEdit : Bool){
        
        let managedContext = CoreDataStack.managedObjectContext
        let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Projects")
        updateFetch.predicate = NSPredicate(format: "pid = %@", saveObject.pid ?? "")
        
        do {
            let results = try managedContext.fetch(updateFetch) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                
                // In my case, I only updated the first item in results
                
                let session = results![0]
                    session.setValue(saveObject.state, forKey: "state")
                session.setValue(saveObject.trailer2, forKey: "trailer2")
                session.setValue(saveObject.clientJobNumber, forKey: "client_job_number")
                session.setValue(saveObject.supportTruck2, forKey: "support_truck2")
                session.setValue(saveObject.cityName, forKey: "city_name")
                session.setValue(saveObject.crewHelpers, forKey: "crew_helpers")
                session.setValue(saveObject.rig, forKey: "rig")
                session.setValue(saveObject.crewHelpers2, forKey: "crew_helpers2")
                session.setValue(saveObject.aDIJobNumber, forKey: "ADI_job_number".lowercased())
                session.setValue(saveObject.siteAddress, forKey: "site_address")
                session.setValue(saveObject.trailer1, forKey: "trailer1")
                session.setValue(saveObject.projectName, forKey: "project_name")
                session.setValue(saveObject.supportTruk1, forKey: "support_truk1")
                session.setValue(saveObject.clientName, forKey: "client_name")
                session.setValue(saveObject.perdiems, forKey: "perdiems")
                session.setValue(saveObject.injuries, forKey: "injuries")
                session.setValue(saveObject.crewDrillerName, forKey: "crew_driller_name")
                session.setValue(saveObject.startDate, forKey: "start_date")
                session.setValue(saveObject.clientRep, forKey: "client_rep")
                session.setValue(isOffline, forKey: "isOffline")
                session.setValue(edit, forKey: "edit")
                session.setValue(isLocalAddEdit, forKey: "isLocalAddEdit")
                
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try managedContext.save()
            self.navigationController?.popViewController(animated: true)
            appDelegates.window?.rootViewController?.showAlert(messageStr:  "Project updated successfully")
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
        
    }
    
    //MARK:- Login Api Call
    func createProjectApiCall(performAction : String){
        
        var parameter  : Parameters  =  ["action": "create_project_new", "perform": performAction, "userid": standard.value(forKey: "id")!,"start_date": JSON(self.mainDict)["start_date"].stringValue, "client_name": JSON(self.mainDict)["client_name"].stringValue, "project_name": JSON(self.mainDict)["project_name"].stringValue, "site_address": JSON(self.mainDict)["site_address"].stringValue, "city_name": JSON(self.mainDict)["city_name"].stringValue, "ADI_job_number": JSON(self.mainDict)["ADI_job_number"].stringValue, "client_job_number": JSON(self.mainDict)["client_job_number"].stringValue, "client_rep": JSON(self.mainDict)["client_rep"].stringValue,
                                         "crew_driller_name": JSON(self.mainDict)["crew_driller_name"].stringValue, "crew_helpers":JSON(self.mainDict)["crew_helpers"].stringValue,
                                         
                                         "rig":JSON(self.mainDict)["rig"].stringValue,
                                         "state":JSON(self.mainDict)["state"].stringValue,
                                         "support_truck2":JSON(self.mainDict)["support_truck2"].stringValue,
                                         "support_truk1":JSON(self.mainDict)["support_truk1"].stringValue,
                                         
                                         "trailer1":JSON(self.mainDict)["trailer1"].stringValue,
                                         "trailer2":JSON(self.mainDict)["trailer2"].stringValue,
                                         "perdiems":JSON(self.mainDict)["perdiems"].stringValue,
                                         
                                         "injuries":JSON(self.mainDict)["injuries"].stringValue,
                                         "crew_helpers2":JSON(self.mainDict)["crew_helpers2"].stringValue
        ]
        
        if isEdit == true {
            parameter.updateValue(projectId, forKey: "projectid")
        }
        print(parameter)
        
        if Network.isConnectedToNetwork() == true {
            self.createProjectApi(parameter: parameter)
        }
        else if isEdit == true  && Network.isConnectedToNetwork() == false{
            
            parameter.updateValue(projectId, forKey: "pid")
            self.updateCoredata(saveObject:ProjectData.init(json: JSON(parameter)), isOffline: true, edit: true, isLocalAddEdit:  projectInfo?.isLocalAddEdit ?? false)
            
        }
        else{
            let random = Int(arc4random_uniform(10000))
            parameter.updateValue("\(random)", forKey: "pid")
            parameter.updateValue(Comman.getCurrentDateWithTime(), forKey: "add_on")
            self.saveCoredata(saveObject: ProjectData.init(json: JSON(parameter)), isOffline: true, edit: false, isLocalAddEdit: true)
            
        }
    }
    
    
    func createProjectApi(parameter :Parameters ){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            
            let user = DAUser.init(json: json)
            guard user.response?.result == "201" else{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  user.response?.msg ?? "" )
                return
            }
            
            
            DispatchQueue.main.async {
                self.navigationController?.popViewController(animated: true)
                appDelegates.window?.rootViewController?.showAlert(messageStr:  user.response?.msg ?? "" )
            }
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  error.localizedDescription)
            }
            
            print(error.localizedDescription)
        }
        
    }
    
    //MARK:- Load Project function
    func  loadProject(){
        
        let parameter  : Parameters  = ["action": "fetch_projects", "userid": standard.value(forKey: "id")!, "projectid" : projectId]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            self.mainDict = json["response"]["data"][0].dictionaryObject ?? [String: Any]()
            DispatchQueue.main.async {
                for i in 1..<10{
                    let txt = self.view.viewWithTag(i) as! UITextField
                    txt.text = JSON(self.mainDict)["\(self.keys[i - 1])"].stringValue
                }
            }
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    
  
    
    
}
//MARK:- PickerView Data Source

extension CreateProjectViewController : UIPickerViewDelegate, UIPickerViewDataSource {

func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
}
func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    
    if pickerView.tag == 5002 {
          return arrayClientPicker.count
    }
    else{
          return arrayStatePicker.count
    }
  
    
}


func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    

    if pickerView.tag == 5002 {
        return arrayClientPicker[row]
    }
    else{
        return arrayStatePicker[row]
    }
    
}

func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
     if pickerView.tag == 5002 {
        
        self.txtClientName.text = arrayClientPicker[row]
    }
     else{
         self.txtState.text = arrayStatePicker[row]
    }
    
}
}
