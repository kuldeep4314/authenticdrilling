//
//  HoleCompletionViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-25.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit
import CoreData
class HoleCompletionViewController: ViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    var strDate = String()
    var holeNumber = String()
    var isEdit = Bool()
    var sessions: [NSManagedObject] = []
    var projectInfo :ProjectData?
    let arrImagesLeft = [#imageLiteral(resourceName: "imgHole"),  #imageLiteral(resourceName: "imgGrout") , #imageLiteral(resourceName: "imgInstallation") ,#imageLiteral(resourceName: "imgAbandon"),#imageLiteral(resourceName: "imgAbandon"),#imageLiteral(resourceName: "imgAbandon")]
    let arrImagesRight = [ nil,  #imageLiteral(resourceName: "imgArrow") , #imageLiteral(resourceName: "imgArrow") ,#imageLiteral(resourceName: "imgArrow"),#imageLiteral(resourceName: "imgCheck"),#imageLiteral(resourceName: "imgCheck")]
    var mainDict =  [String: Any]()
    let arrNames = ["Hole Number","Grout" , "Installation" ,"Abandon","Leave Hole open" ,"Backfill cuttings"]
    
    let keyArr = ["holenumber","","","","leavehole","backfill"]
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblProjectName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        
        if isEdit == true {
            self.mainDict.updateValue(holeNumber, forKey: "holenumber")
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
        
        if Network.isConnectedToNetwork() == true {
            self.getOfflineData()
        }
        else if  Network.isConnectedToNetwork() == false && self.holeNumber != "" {
            self.getCoreData()
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc  func btnSaveTapped(sender: UIButton){
        print(sender.tag)
        
        
        if JSON(self.mainDict)[keyArr[0]].stringValue == ""{
            
            appDelegates.window?.rootViewController?.view.makeToast("Please Enter Hole Number.", duration: 2, position: .center)
        }
        else{
          
            if Network.isConnectedToNetwork() == true {
                
                self.addHoleAndBackFillApiCall(isOffline: false, edit: false)
            }
            else  if Network.isConnectedToNetwork() == false {
                
                if ProjectsOffline.sharedInstance.getOfflineTablesWithHolenumber(tables: "HoleCompletion", strDate: self.strDate, projectID: projectId, holenumber: self.holeNumber) == true
                {
                    self.updateCoredata(isOffline: true, edit: true, isShowAlert: true)
                }
                    
                else{
                    ProjectsOffline.sharedInstance.getOfflineDates(isOffline: true, strDate: self.strDate, projectID: projectId)
                    self.saveCoredata(isOffline: true, edit: false, msg: "Hole Completition leave_openOrbackFill list added successfully", isShow: true)
                }
            }
            
            
            
            
        }
        
        
    }
    
    func getOfflineData(){
        
        let offlineDetails = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "HoleCompletion")
        print(offlineDetails)
        if offlineDetails.count > 0 {
            let parameters : Parameters = ["action": "hole_completion_leave_openOrbackFill" ,"data" :  offlineDetails ]
            
            self.syncOfflineMultipleApi(parameter: parameters)
            
        }
        else if self.isEdit == true {
            self.fetchDetails()
        }
        
    }
    
    
    
    //MARK: - SyncOfflineMultipleApi
    func syncOfflineMultipleApi(parameter :Parameters){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(holeCompletionOfflineUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            ProjectsOffline.sharedInstance.updateCoredataTables(tables: "HoleCompletion")
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            self.getCoreData()
            print(error.localizedDescription)
        }
    }
    
    
    
    
    
    
    
    
    
    
    func updateCoredata(isOffline : Bool , edit : Bool , isShowAlert : Bool){
        
        let managedContext = CoreDataStack.managedObjectContext
        let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "HoleCompletion")
        updateFetch.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@) AND (holenumber = %@)",  projectId , strDate,self.holeNumber)
        do {
            let results = try managedContext.fetch(updateFetch) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                
                // In my case, I only updated the first item in results
                let session = results![0]
                
                let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
                session.setValue(data1, forKey: "dataDetails")
                
                session.setValue(strDate, forKey: "date")
                session.setValue(projectId, forKey: "projectid")
                session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                session.setValue(isOffline, forKey: "isOffline")
                session.setValue(edit, forKey: "edit")
                session.setValue(self.holeNumber, forKey: "holenumber")
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try managedContext.save()
            
            
            if isShowAlert == true {
                self.navigationController?.popViewController(animated: true)
                appDelegates.window?.rootViewController?.showAlert(messageStr:  "Hole Completition leave_openOrbackFill list updated successfully")
            }
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
        
    }
    
    
    //MARK:- Save Core Data Function
    func saveCoredata(isOffline : Bool,edit: Bool , msg : String , isShow : Bool){
        print(isOffline)
        print(edit)
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "HoleCompletion",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
        session.setValue(data1, forKey: "dataDetails")
        
        session.setValue(strDate, forKey: "date")
        session.setValue(projectId, forKey: "projectid")
        session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
        session.setValue(isOffline, forKey: "isOffline")
        session.setValue(edit, forKey: "edit")
        session.setValue(self.holeNumber, forKey: "holenumber")
        
        CoreDataStack.saveContext()
        
        
        if isShow == true{
            self.navigationController?.popViewController(animated: true)
            appDelegates.window?.rootViewController?.showAlert(messageStr: msg)
        }
        
    }
    //MARK:- Get Core Data Function
    func getCoreData() {
        
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "HoleCompletion")
        
        fetchRequest.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@) AND (holenumber = %@)",  projectId , strDate,self.holeNumber)
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            for session in sessions{
                var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                
                dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                dictionary.updateValue(session.value(forKey: "holenumber") ?? "", forKey: "holeNumber")
                dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                
                self.mainDict = dictionary
                
            }
            
            DispatchQueue.main.async {
               self.tableView.reloadData()
            }
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
         self.holeNumber = textField.text ?? ""
        self.mainDict.updateValue(textField.text ?? "", forKey: "holenumber")
        
        if Network.isConnectedToNetwork() == true {
            self.fetchDetails()
        }
        else if  Network.isConnectedToNetwork() == false && self.holeNumber != "" {
            self.getCoreData()
        }
        
        
        
        self.tableView.reloadData()
    }
    //MARK:- TableView Data Source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrNames.count + 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == arrNames.count {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! ProjectCell
            cell.btnSave.addTarget(self, action: #selector(self.btnSaveTapped(sender:)), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }
        else  if indexPath.section == 0{
            
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! ProjectCell
            
            cell.txtHoleTitle.leftview(image: #imageLiteral(resourceName: "imgHole"))
            cell.txtHoleValue.rightVC()
            cell.txtHoleValue.delegate = self
            cell.txtHoleValue.text = JSON(self.mainDict)[keyArr[indexPath.section]].stringValue
            return cell
            
            
        }
        else{
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProjectCell
            
            cell.lblDate.text = arrNames[indexPath.section]
            cell.imgView.image = arrImagesLeft[indexPath.section]
            cell.imgRight.image = arrImagesRight[indexPath.section]
            
            if indexPath.section > 3{
                if JSON(self.mainDict)[keyArr[indexPath.section]].stringValue == "1"
                {
                    cell.imgRight.image = #imageLiteral(resourceName: "imgCheck")
                    
                }
                else{
                    cell.imgRight.image = #imageLiteral(resourceName: "imgUnselect")
                }
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == arrNames.count {
            return 100
        }
        return 44
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "header")
        
        return cell?.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 20
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        if JSON(self.mainDict)[keyArr[0]].stringValue == ""{
            
            appDelegates.window?.rootViewController?.view.makeToast("Please Enter Hole Number.", duration: 2, position: .center)
        }
        else{
            if indexPath.section == 0 {
                
            }
            else if indexPath.section == 1 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "GroutViewController") as! GroutViewController
                vc.strDate = strDate
                vc.projectInfo =  projectInfo
                vc.holeNumber = JSON(self.mainDict)[keyArr[0]].stringValue
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.section == 2 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "InstallationViewController") as! InstallationViewController
                vc.holeNumber = JSON(self.mainDict)[keyArr[0]].stringValue
                vc.strDate = strDate
                vc.projectInfo = projectInfo
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else  if indexPath.section == 3 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AbandonViewController") as! AbandonViewController
                vc.holeNumber = JSON(self.mainDict)[keyArr[0]].stringValue
                vc.strDate = strDate
                vc.projectInfo =  projectInfo
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                if JSON(self.mainDict)[keyArr[indexPath.section]].stringValue == "1" {
                    
                    self.mainDict.updateValue("0", forKey: keyArr[indexPath.section])
                }
                else{
                    self.mainDict.updateValue("1", forKey: keyArr[indexPath.section])
                }
                self.tableView.reloadData()
            }
        }
    }
    
    
    //MARK:- AddHoleAndBackFillApiCall
    func addHoleAndBackFillApiCall(isOffline : Bool , edit : Bool ){
        let parameter  : Parameters  =  ["action": "hole_completion_leave_openOrbackFill", "userid": standard.value(forKey: "id")!, "projectid":  projectId,"date":strDate
            , "leavehole": JSON(self.mainDict)["leavehole"].stringValue
            , "backfill": JSON(self.mainDict)["backfill"].stringValue
            , "holenumber": JSON(self.mainDict)["holenumber"].stringValue
            
            
        ]
        
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(holeCompletionUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  json["response"]["msg"].stringValue )
                return
            }
            
            
            DispatchQueue.main.async {
                 ProjectsOffline.sharedInstance.getOfflineDates(isOffline: isOffline, strDate: self.strDate, projectID: projectId)
                
                
                if ProjectsOffline.sharedInstance.getOfflineTablesWithHolenumber(tables: "HoleCompletion", strDate: self.strDate, projectID: projectId, holenumber: self.holeNumber) == true {
                    self.updateCoredata(isOffline: false, edit: false, isShowAlert: true)
                }
                else{
                    self.saveCoredata(isOffline: isOffline, edit: edit, msg: json["response"]["msg"].stringValue, isShow: true)
                }
                
            }
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  error.localizedDescription)
            }
            
            print(error.localizedDescription)
        }
        
    }
    
    //MARK:- fetchDetails function
    func  fetchDetails(){
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid": projectId ,"table":"hole_completion_leave_openOrbackFill","date": strDate ,"holenumber" : holeNumber]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            self.mainDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
            
            
            self.mainDict.updateValue(self.holeNumber, forKey: "holenumber")
            
            
            if ProjectsOffline.sharedInstance.getOfflineTablesWithHolenumber(tables: "HoleCompletion", strDate: self.strDate, projectID: projectId, holenumber: self.holeNumber) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false)
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false)
                
            }
            self.getCoreData()
            
            }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    
    
    
}
