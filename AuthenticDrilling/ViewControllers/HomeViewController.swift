//
//  HomeViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-24.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit
import CoreData
class HomeViewController: ViewController ,UITableViewDelegate ,UITableViewDataSource{
    var sessions: [NSManagedObject] = []
    @IBOutlet weak var tableView: UITableView!
     let refreshControl = UIRefreshControl()
      var arrProjects = [ProjectData]()
        var arrOfflineProjects = [[String : Any?]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.tintColor = appColor
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   @objc func refreshData(_ sender: Any) {
        
     self.loadProducts(arrayIds: [JSON(JSON.self)], isUpdateIds: false)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Network.isConnectedToNetwork() == true {
          self.getOfflineProjects()
        }
        else{
            self.getCoreData()
        }
    }
    
    //MARK:- IBAction Functions
    @IBAction func addBtnTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateProjectViewController") as! CreateProjectViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func editBtnTapped(sender: UIButton){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateProjectViewController") as! CreateProjectViewController
        vc.isEdit = true
        vc.projectInfo = self.arrProjects[sender.tag]
         projectId = self.arrProjects[sender.tag].pid ?? "0"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnLogoutTapped(_ sender: Any) {
        
        
        if Network.isConnectedToNetwork() == true {
            
            self.showAlertWithActionLogout()
            
        }
        else{
             appDelegates.window?.rootViewController?.showAlert(messageStr:  "Please Connect to the Internet!")
        }
    }
    
    func showAlertWithActionLogout(){
        let alertController = UIAlertController(title: nil, message: "Do you want to logout?", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alertController.addAction(cancelAction)
        
        let sync = UIAlertAction(title: "Logout?", style: .default) { (action) in
            
             SyncWhenLogOut.sharedInstance.syncData()
          
        }
        alertController.addAction(sync)
        
        alertController.popoverPresentationController?.sourceView =  appDelegates.window?.rootViewController?.view
        appDelegates.window?.rootViewController?.present(alertController, animated: true) {
            
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    //MARK:- Save Core Data Function
    func saveCoredata(saveObject : ProjectData, isOffline : Bool,edit: Bool){
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "Projects",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
            session.setValue(saveObject.state, forKey: "state")
            session.setValue(saveObject.trailer2, forKey: "trailer2")
            session.setValue(saveObject.clientJobNumber, forKey: "client_job_number")
            session.setValue(saveObject.supportTruck2, forKey: "support_truck2")
            session.setValue(saveObject.pid, forKey: "pid")
            session.setValue(saveObject.cityName, forKey: "city_name")
            session.setValue(saveObject.crewHelpers, forKey: "crew_helpers")
            session.setValue(saveObject.rig, forKey: "rig")
            session.setValue(saveObject.crewHelpers2, forKey: "crew_helpers2")
            session.setValue(saveObject.aDIJobNumber, forKey: "ADI_job_number".lowercased())
            session.setValue(saveObject.siteAddress, forKey: "site_address")
            session.setValue(saveObject.trailer1, forKey: "trailer1")
            session.setValue(saveObject.projectName, forKey: "project_name")
            session.setValue(saveObject.supportTruk1, forKey: "support_truk1")
            session.setValue(saveObject.addOn, forKey: "add_on")
            session.setValue(saveObject.modifyOn, forKey: "modify_on")
            session.setValue(saveObject.clientName, forKey: "client_name")
            session.setValue(saveObject.perdiems, forKey: "perdiems")
            session.setValue(saveObject.userid, forKey: "userid")
            session.setValue(saveObject.injuries, forKey: "injuries")
            session.setValue(saveObject.crewDrillerName, forKey: "crew_driller_name")
            session.setValue(saveObject.startDate, forKey: "start_date")
            session.setValue(saveObject.clientRep, forKey: "client_rep")
            session.setValue(isOffline, forKey: "isOffline")
        session.setValue(edit, forKey: "edit")
        CoreDataStack.saveContext()
    }
    
      //MARK:-Delete Records Data Function
    func deleteWithIdsRecords(ids : String) {
        let managedContext = CoreDataStack.managedObjectContext
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Projects")
     
        deleteFetch.predicate = NSPredicate(format: "(isOffline == 0) AND (pid = %@)", ids)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
            print ("There was an error")
        }
    }
    
    func deleteAllRecords() {
        let managedContext = CoreDataStack.managedObjectContext
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Projects")
       
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
            print ("There was an error")
        }
    }
    
    
    //MARK:- Get Core Data Function
    func getCoreData() {
        
        self.arrProjects.removeAll()
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Projects")
        
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            for session in sessions{
                
                let dict = ["trailer2" :  session.value(forKey: "trailer2"),
                            "state" :  session.value(forKey: "state"),
                "client_job_number" : session.value(forKey: "client_job_number"),
                "support_truck2" : session.value(forKey: "support_truck2"),
                "pid" : session.value(forKey: "pid"),
                "city_name" : session.value(forKey: "city_name"),
                "crew_helpers" : session.value(forKey: "crew_helpers"),
                "rig" : session.value(forKey: "rig"),
                "crew_helpers2" : session.value(forKey: "crew_helpers2"),
                "ADI_job_number" : session.value(forKey: "ADI_job_number".lowercased()),
                "site_address" : session.value(forKey: "site_address"),
                "trailer1" : session.value(forKey: "trailer1"),
                "project_name" : session.value(forKey: "project_name"),
                "support_truk1" : session.value(forKey: "support_truk1"),
                "add_on" : session.value(forKey: "add_on"),
                "modify_on" : session.value(forKey: "modify_on"),
                "client_name" : session.value(forKey: "client_name"),
                "perdiems" : session.value(forKey: "perdiems"),
                "userid" : session.value(forKey: "userid"),
                "injuries" : session.value(forKey: "injuries"),
                "crew_driller_name" : session.value(forKey: "crew_driller_name"),
                "start_date" : session.value(forKey: "start_date"),
                "client_rep" : session.value(forKey: "client_rep")
                    ,
                "isOffline" : session.value(forKey: "isOffline"),
                 "edit" : session.value(forKey: "edit"),
                 "isLocalAddEdit" : session.value(forKey: "isLocalAddEdit")]
                
               // print(dict)
                
                self.arrProjects.append(ProjectData.init(json: JSON(dict)))
                
                }
             self.arrProjects = Comman.sortDateWise(array:  self.arrProjects)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
   
    
    
    
    
    
    
    //MARK:- TableView Data Source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrProjects.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeCell
        cell.lblName.text = arrProjects[indexPath.section].projectName
        cell.lblLocation.text = arrProjects[indexPath.section].siteAddress
        let date1 = String(describing: arrProjects[indexPath.section].startDate!.prefix(10))
        cell.lblDate.text = Comman.changedDateFormatter(date: date1) 
        cell.btnEdit.tag = indexPath.section
        
        cell.btnEdit.addTarget(self, action: #selector(self.editBtnTapped(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "header")
        
        return cell?.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProjectsViewController") as! ProjectsViewController
        vc.projectInfo = self.arrProjects[indexPath.section]
          projectId = self.arrProjects[indexPath.section].pid ?? "0"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Load Projects function
    func  loadProducts(arrayIds :[JSON],isUpdateIds : Bool){
        
        
        
        
        
        let parameter  : Parameters  = ["action": "fetch_project", "userid": standard.value(forKey: "id")!]
       print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            self.refreshControl.endRefreshing()
            print(json)
            
            self.deleteAllRecords()
            let projects = ProjectDA.init(json: json)
           
            guard projects.response?.result == "201" else{
                return
            }
            
           let array = projects.response?.data ??  [ProjectData]()
            
            
            for i in 0..<array.count {
               
                self.saveCoredata(saveObject: array[i], isOffline: false, edit: false)
            }
            
            if isUpdateIds == true {
                let arrayIdsValue = arrayIds.filter({$0["oldid"].stringValue == projectId}).map({$0["serverid"].stringValue})
                if arrayIdsValue.count > 0 {
                    projectId = arrayIdsValue[0]
                }
                UpdateProjectsIDS.sharedInstance.updateProjectsIds(arrayIds: arrayIds)
                
            }
            
             self.getCoreData()
   
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            self.refreshControl.endRefreshing()
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            self.getCoreData()
            print(error.localizedDescription)
        }
    }
    
    func getOfflineProjects() {
        
        self.arrOfflineProjects.removeAll()
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Projects")
        fetchRequest.predicate = NSPredicate(format: "isOffline = %@", "1")
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            for session in sessions{
                
                var dict = ["trailer2" :  session.value(forKey: "trailer2"),
                            "state" :  session.value(forKey: "state"),
                            "client_job_number" : session.value(forKey: "client_job_number"),
                            "support_truck2" : session.value(forKey: "support_truck2"),
                            "city_name" : session.value(forKey: "city_name"),
                            "crew_helpers" : session.value(forKey: "crew_helpers"),
                            "rig" : session.value(forKey: "rig"),
                            "crew_helpers2" : session.value(forKey: "crew_helpers2"),
                            "ADI_job_number" : session.value(forKey: "ADI_job_number".lowercased()),
                            "site_address" : session.value(forKey: "site_address"),
                            "trailer1" : session.value(forKey: "trailer1"),
                            "project_name" : session.value(forKey: "project_name"),
                            "support_truk1" : session.value(forKey: "support_truk1"),
                            
                            "client_name" : session.value(forKey: "client_name"),
                            "perdiems" : session.value(forKey: "perdiems"),
                            "userid" : session.value(forKey: "userid"),
                            "injuries" : session.value(forKey: "injuries"),
                            "crew_driller_name" : session.value(forKey: "crew_driller_name"),
                            "start_date" : session.value(forKey: "start_date"),
                            "client_rep" : session.value(forKey: "client_rep"),
                            "isLocalAddEdit": session.value(forKey: "isLocalAddEdit")
                            ]
                
                
                if session.value(forKey: "isLocalAddEdit") as? Bool == true {
                    dict.updateValue(session.value(forKey: "pid"), forKey: "localid")
                    dict.updateValue("create", forKey: "perform")
                }
                else{
                    
                    if  session.value(forKey: "edit") as? Bool == true {
                        dict.updateValue("edit", forKey: "perform")
                        dict.updateValue(session.value(forKey: "pid"), forKey: "projectid")
                    }
                    else{
                        dict.updateValue(session.value(forKey: "pid"), forKey: "localid")
                        dict.updateValue("create", forKey: "perform")
                        
                    }
                }
                
                self.arrOfflineProjects.append(dict)
            }
            
            if self.arrOfflineProjects.count > 0 {
                
               self.dataToSync()
            }
            else{

                self.loadProducts(arrayIds: [JSON(JSON.self)], isUpdateIds: false)
            }
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    func dataToSync(){
        
        let param : Parameters = ["action" : "create_project","data" :  self.arrOfflineProjects]
        
        self.createProjectApi(parameter: param)
    }
    
    func createProjectApi(parameter :Parameters){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(projectUrlOffline, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            self.loadProducts(arrayIds: json["response"]["ids"].arrayValue, isUpdateIds: true)
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            print(error.localizedDescription)
        }
   }
   
}
