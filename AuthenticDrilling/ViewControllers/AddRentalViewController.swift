//
//  AddRentalViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-28.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//


import UIKit
import CoreData
class AddRentalViewController: ViewController,UITableViewDataSource,UITableViewDelegate ,UITextViewDelegate{
    let arrayImagesNames = [#imageLiteral(resourceName: "imgTruck"),#imageLiteral(resourceName: "imgTruck"),#imageLiteral(resourceName: "imgAir"),#imageLiteral(resourceName: "imgStreamCleaner"),#imageLiteral(resourceName: "imgLightPlant"),#imageLiteral(resourceName: "imgGenerator"),#imageLiteral(resourceName: "imgConcrete"),#imageLiteral(resourceName: "ImgConcreateSaw") ,#imageLiteral(resourceName: "imgJackHammer"),#imageLiteral(resourceName: "imgTrashPump"),#imageLiteral(resourceName: "imgTrashPump") ,#imageLiteral(resourceName: "imgTrashPump"),#imageLiteral(resourceName: "imgSkidSteer"),#imageLiteral(resourceName: "imgTractor"),#imageLiteral(resourceName: "imgTrailer"),#imageLiteral(resourceName: "imgOther"),#imageLiteral(resourceName: "imgUser")]
    let arrayNames = ["Support Truck #1","Support Truck #2","Air Compressor","Steam Cleaner","Light Plant","Generator","Concrete Core Machine","Concrete Saw" ,"Jackhammer","Trash Pump","Pump (Notes for type)" ,"","Skidsteer","Tractor","Dump Trailer","Other",""]
    var mainDict =  [String: Any]()
    var isEdit = Bool()
     let arrayKeys = ["support_truck1","support_truck2","air_compressor","steam_cleaner","light_plant","generator","concrete_core","concrete_saw" ,"jackhammer","trash_pump","pump_notetype" ,"notes","skidsteer","trackor","dump_trailer","other",""]
     var sessions: [NSManagedObject] = []
    var strDate = String()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblProjectName: UILabel!
     var projectInfo :ProjectData?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        
        if Network.isConnectedToNetwork() == true {
            self.getOfflineData()
        }
        else{
            self.getCoreData()
        }
       }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Update Core Data
    func updateCoredata(isOffline : Bool , edit : Bool , isShowAlert : Bool){
        
        let managedContext = CoreDataStack.managedObjectContext
        let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Rental")
        updateFetch.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , strDate)
        do {
            let results = try managedContext.fetch(updateFetch) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                
                // In my case, I only updated the first item in results
                let session = results![0]
                
                let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
                session.setValue(data1, forKey: "dataDetails")
                
                session.setValue(strDate, forKey: "date")
                session.setValue(projectId, forKey: "projectid")
                session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                session.setValue(isOffline, forKey: "isOffline")
                session.setValue(edit, forKey: "edit")
                
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try managedContext.save()
            
            
            if isShowAlert == true {
                self.navigationController?.popViewController(animated: true)
                appDelegates.window?.rootViewController?.showAlert(messageStr:  "rental list updated successfully")
            }
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
        
    }
    
    
    //MARK:- Save Core Data Function
    func saveCoredata(isOffline : Bool,edit: Bool , msg : String , isShow : Bool){
        print(isOffline)
        print(edit)
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "Rental",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
        session.setValue(data1, forKey: "dataDetails")
        
        session.setValue(strDate, forKey: "date")
        session.setValue(projectId, forKey: "projectid")
        session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
        session.setValue(isOffline, forKey: "isOffline")
        session.setValue(edit, forKey: "edit")
        
        
        CoreDataStack.saveContext()
        
        
        if isShow == true{
            self.navigationController?.popViewController(animated: true)
            appDelegates.window?.rootViewController?.showAlert(messageStr: msg)
        }
        
    }
    //MARK:- Get Core Data Function
    func getCoreData() {
        
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Rental")
        
        fetchRequest.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , strDate)
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            for session in sessions{
                var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                
                dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                
                self.mainDict = dictionary
                
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
  @objc  func btnSaveTapped(sender:UIButton){
   
    
    if Network.isConnectedToNetwork() == true {
        
         self.addrentalApiCall(isOffline: false, edit: false)
    }
    else  if Network.isConnectedToNetwork() == false {
        
        if ProjectsOffline.sharedInstance.getOfflineTables(tables: "Rental", strDate: self.strDate, projectID: projectId) == true
        {
            self.updateCoredata(isOffline: true, edit: true, isShowAlert: true)
        }
            
        else{
            ProjectsOffline.sharedInstance.getOfflineDates(isOffline: true, strDate: self.strDate, projectID: projectId)
            self.saveCoredata(isOffline: true, edit: false, msg: "rental list added successfully", isShow: true)
        }
    }
    
    
    
    
    
    
}
   
    //MARK: - SyncOfflineMultipleApi
    func syncOfflineMultipleApi(parameter :Parameters){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(projectUrlOffline, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            ProjectsOffline.sharedInstance.updateCoredataTables(tables: "Rental")
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
              self.getCoreData()
            print(error.localizedDescription)
        }
    }
    
    
    
    func getOfflineData(){
        
        let offlineDetails = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Rental")
        print(offlineDetails)
        if offlineDetails.count > 0 {
            let parameters : Parameters = ["action": "rental_category" ,"data" :  offlineDetails ]
             print(parameters)
            self.syncOfflineMultipleApi(parameter: parameters)
            
        }
        else{
            self.fetchDetails()
        }
        
    }
    
    
    
    
    
    
    //MARK:- TextView Delegates
    func textViewDidChange(_ textView: UITextView) {
        self.mainDict.updateValue(textView.text, forKey: "notes")
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        
        if textView.textColor == UIColor.lightGray
        {
            textView.text = ""
          textView.textColor = UIColor.black
            
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
        if textView.text.isEmpty {
            textView.text = "Enter your notes"
           textView.textColor = UIColor.lightGray
             mainDict.updateValue("", forKey:"notes")
        }
        else{
            mainDict.updateValue( textView.text ?? "", forKey:"notes")
        }

    }
   
    
    //MARK:- TableView Data Source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayNames.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        if indexPath.section == 11{
             let cell1 =  tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! RentalCell
            cell1.txtView.text = JSON(self.mainDict)[self.arrayKeys[indexPath.section]].stringValue.base64Decoded()
            
               cell1.txtView.addshodowToTextView()
           
            cell1.txtView.delegate = self
            
            if  cell1.txtView.text == "" {
                cell1.txtView.text = "Enter your notes"
                cell1.txtView.textColor = UIColor.lightGray
            }
            else{
                cell1.txtView.textColor = UIColor.black
            }
            return cell1
        }
     else if indexPath.section == arrayNames.count - 1{
            let cell2 =  tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! RentalCell
            cell2.saveBtn.addTarget(self, action: #selector(self.btnSaveTapped(sender:)), for: .touchUpInside)
            return cell2
        }
            
            
        else{
             let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RentalCell
            
            cell.lblName.text = arrayNames[indexPath.section]
            cell.imgView.image = arrayImagesNames[indexPath.section]
            
          let value  = JSON(self.mainDict)[self.arrayKeys[indexPath.section]].stringValue
            
            if value == "1"{
                cell.btnRight.setImage(#imageLiteral(resourceName: "imgCheck"), for: .normal)
            }
            else{
                 cell.btnRight.setImage(#imageLiteral(resourceName: "imgUnselect"), for: .normal)
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      if  indexPath.section == 11 || arrayNames.count - 1 == indexPath.section{
             return 100
        }
        return 44
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "header")
        
        return cell?.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 20
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
          let value  = JSON(self.mainDict)[self.arrayKeys[indexPath.section]].stringValue
          print(value)
        if value == "1"{
           self.mainDict.updateValue("0", forKey: self.arrayKeys[indexPath.section])
        }
        else{
             self.mainDict.updateValue("1", forKey: self.arrayKeys[indexPath.section])
        }
        self.tableView.reloadData()
    }
    //MARK:- ADD Rental Api
    func addrentalApiCall(isOffline : Bool , edit : Bool ){
        let parameter  : Parameters  =  ["action": "rental_category", "userid": standard.value(forKey: "id")!, "projectid":  projectId, "support_truck1": JSON(self.mainDict)["support_truck1"].stringValue, "support_truck2": JSON(self.mainDict)["support_truck2"].stringValue, "air_compressor": JSON(self.mainDict)["air_compressor"].stringValue, "steam_cleaner": JSON(self.mainDict)["steam_cleaner"].stringValue, "light_plant": JSON(self.mainDict)["light_plant"].stringValue, "generator": JSON(self.mainDict)["generator"].stringValue, "concrete_core": JSON(self.mainDict)["concrete_core"].stringValue, "concrete_saw":JSON(self.mainDict)["concrete_saw"].stringValue, "jackhammer": JSON(self.mainDict)["jackhammer"].stringValue, "trash_pump": JSON(self.mainDict)["trash_pump"].stringValue, "pump_notetype": JSON(self.mainDict)["pump_notetype"].stringValue, "skidsteer": JSON(self.mainDict)["skidsteer"].stringValue, "trackor": JSON(self.mainDict)["trackor"].stringValue, "dump_trailer": JSON(self.mainDict)["dump_trailer"].stringValue, "other": JSON(self.mainDict)["other"].stringValue, "notes": JSON(self.mainDict)["notes"].stringValue.base64Decoded() ?? "","date": strDate]
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  json["response"]["msg"].stringValue )
                return
            }
            DispatchQueue.main.async {
               ProjectsOffline.sharedInstance.getOfflineDates(isOffline: isOffline, strDate: self.strDate, projectID: projectId)
                
                
                if ProjectsOffline.sharedInstance.getOfflineTables(tables: "Rental", strDate: self.strDate, projectID: projectId) == true {
                    self.updateCoredata(isOffline: false, edit: false, isShowAlert: true)
                }
                else{
                    self.saveCoredata(isOffline: isOffline, edit: edit, msg: json["response"]["msg"].stringValue, isShow: true)
                }
            }

            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  error.localizedDescription)
            }
            
            print(error.localizedDescription)
        }
        
    }
    
    
    
    
    //MARK:-Delete Records Data Function
    func deleteWithIdsRecords(ids : String) {
        let managedContext = CoreDataStack.managedObjectContext
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Rental")
        
        deleteFetch.predicate = NSPredicate(format: "(isOffline == 0) AND (projectid = %@", ids)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
            print ("There was an error")
        }
    }
    
    
    //MARK:- fetchDetails function
    func  fetchDetails(){
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid": projectId,"table":"rental","date": strDate]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            self.mainDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
            
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "Rental", strDate: self.strDate, projectID: projectId) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false)
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false)
                
            }
            self.getCoreData()
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    

}

