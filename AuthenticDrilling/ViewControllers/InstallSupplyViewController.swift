//
//  InstallSupplyViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-29.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit
import CoreData
class InstallSupplyViewController: ViewController,UITableViewDataSource,UITableViewDelegate ,UIPickerViewDataSource ,UIPickerViewDelegate,UITextFieldDelegate{
    let arrNames = ["Steel" , "Single" ,"Nested","Size","Riser" ,"Screen" ,""," Top Cap" , "Bottom Cap"]
    let pickerArr = ["10" , "5" ,"21/2"]
    var sessions: [NSManagedObject] = []
    let arrImagesNames = [#imageLiteral(resourceName: "imgSteel") , nil ,nil,#imageLiteral(resourceName: "imgGrainSize"),nil ,nil,nil ,#imageLiteral(resourceName: "imgTopCap") , #imageLiteral(resourceName: "imgTopCap")]
    var  arrRisers = [[String : Any]]()
     var arrScreens = [[String : Any]]()
     let arrPlaceholder = ["Pieces","Pieces" , "Pieces" ,"Pieces"]
    var arrayKeys = ["steel" , "single" ,"nested" ,"size", "riser","screen","","topcap","bottomcap"]
    
    var strDate = String()
    var projectInfo :ProjectData?
    
    var mainDict =  [String: Any]()
     var dropDownList = [JSON]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblProjectName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
         if  let decoded  = standard.value(forKey: "catData") as? [String : Any]{
              dropDownList  = JSON(decoded)["Install supply"].arrayValue
              print(dropDownList)
           
        }
       self.mainDict.updateValue(dropDownList[2]["subcategory"][0]["name"].stringValue, forKey: "steel")
        arrRisers.append(["title":dropDownList[0]["subcategory"][0]["name"].stringValue,"value": ""])
        arrScreens.append(["title":dropDownList[1]["subcategory"][0]["name"].stringValue,"value": ""])
      
        
        if Network.isConnectedToNetwork() == true {
            self.getOfflineData()
        }
        else{
            self.getCoreData()
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
        
        if JSON(self.mainDict)[arrayKeys[(sender.view?.tag)!]].stringValue == "1"{
            self.mainDict.updateValue("0", forKey: arrayKeys[(sender.view?.tag)!])
            
        }
        else{
            self.mainDict.updateValue("1", forKey: arrayKeys[(sender.view?.tag)!])
        }
        print(self.mainDict)
        self.tableView.reloadData()
    }
   
    
    //MARK:- TextField Delegates
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 1111{
            self.mainDict.updateValue(textField.text ?? "", forKey: "slot_screen")
            
        }
      else if textField.tag == 4 {
            let hint = textField.accessibilityHint
            
            let arr = hint?.components(separatedBy: ",")
            
            let rowValue = Int(arr![1])
            
            
                var dict  = arrRisers[rowValue ?? 0]
                
                dict["title"] = textField.text
                
                arrRisers[rowValue ?? 0] = dict
              print(arrRisers)
            
                self.mainDict.updateValue(arrRisers, forKey: "riser_arr")
            self.tableView.reloadData()
        }
        else if textField.tag == -4 {
            let hint = textField.accessibilityHint
            
            let arr = hint?.components(separatedBy: ",")
            
            let rowValue = Int(arr![1])
            
            
            var dict  = arrRisers[rowValue ?? 0]
            
            dict["value"] = textField.text
            
            arrRisers[rowValue ?? 0] = dict
              print(arrRisers)
             self.mainDict.updateValue(arrRisers, forKey: "riser_arr")
            self.tableView.reloadData()
        }
       else if textField.tag == 5{
            let hint = textField.accessibilityHint
            
            let arr = hint?.components(separatedBy: ",")
            
            let rowValue = Int(arr![1])
            
            
            var dict  = arrScreens[rowValue! - 1]
            
            dict["title"] = textField.text
            
            arrScreens[rowValue! - 1] = dict
             print(arrScreens)
            
            
            
            self.tableView.reloadData()
        }
        else if textField.tag == -5 {
            let hint = textField.accessibilityHint
            
            let arr = hint?.components(separatedBy: ",")
            
            let rowValue = Int(arr![1])
            
            
            var dict  = arrScreens[ rowValue! - 1]
            
            dict["value"] = textField.text
            
            arrScreens[rowValue! - 1 ] = dict
             print(arrScreens)
              self.mainDict.updateValue(arrScreens, forKey: "screen_arr")
            self.tableView.reloadData()
        }
        
        else {
            self.mainDict.updateValue(textField.text ?? "", forKey: arrayKeys[textField.tag])
            
        }
        
        print(mainDict)
    }
    
    
    //MARK:- BUTTON ACTION FUNCTION
    
    @objc  func btnPlusAction(sender : UIButton){
      
        let arr = sender.accessibilityHint?.components(separatedBy: ",")
        let sectionValue = Int(arr![0])
        
        if sectionValue == 4{
            arrRisers.append(["title":dropDownList[0]["subcategory"][0]["name"].stringValue,"value": ""])
            
        }
        else{
            arrScreens.append(["title":dropDownList[1]["subcategory"][0]["name"].stringValue,"value": ""])
        }
        self.tableView.reloadData()
    }
    
    
    @objc  func btnMinusAction(sender : UIButton){
 
        
        let arr = sender.accessibilityHint?.components(separatedBy: ",")
        let sectionValue = Int(arr![0])
        if sectionValue == 4 {
            if arrRisers.count > 1{
                arrRisers.remove(at: sender.tag)
            }
            
        }
        else{
            if arrScreens.count > 1{
                arrScreens.remove(at: sender.tag - 1)
            }
        }
        self.tableView.reloadData()
        
        
        
    }
    
    
    @objc func btnSaveTapped(sender : UIButton){
        
        if Network.isConnectedToNetwork() == true {
            
            self.addInstallSupplyApiCall(isOffline: false, edit: false)
        }
        else  if Network.isConnectedToNetwork() == false {
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "InstallSupply", strDate: self.strDate, projectID: projectId) == true
            {
                self.updateCoredata(isOffline: true, edit: true, isShowAlert: true)
            }
                
            else{
                ProjectsOffline.sharedInstance.getOfflineDates(isOffline: true, strDate: self.strDate, projectID: projectId)
                self.saveCoredata(isOffline: true, edit: false, msg: "Add consumption Install Supplies list added successfully", isShow: true)
            }
        }
        
        
        
    }
    
    func getOfflineData(){
        
        let offlineDetails = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "InstallSupply")
        print(offlineDetails)
        if offlineDetails.count > 0 {
            let parameters : Parameters = ["action": "ac_install_supplies" ,"data" :  offlineDetails ]
            
            self.syncOfflineMultipleApi(parameter: parameters)
            
        }
        else{
            self.fetchDetails()
        }
        
    }
    
    
    
    //MARK: - SyncOfflineMultipleApi
    func syncOfflineMultipleApi(parameter :Parameters){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(consumableUrlOffline, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            ProjectsOffline.sharedInstance.updateCoredataTables(tables: "InstallSupply")
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            self.getCoreData()
            print(error.localizedDescription)
        }
    }
    
    
    
    
    //MARK:- TableView Data Source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrNames.count + 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrNames.count == section  {
            return 0
        }
        else  if section == 0{
          return 1
        }
            
        else{
            if section == 4 {
                if JSON(self.mainDict)[arrayKeys[section]].stringValue == "1"  {
                    return arrRisers.count
                }
                else{
                    return 0
                }
            }
           else if section == 5 {
                if JSON(self.mainDict)[arrayKeys[section]].stringValue == "1"  {
                    return arrScreens.count + 1
                }
                else{
                    return 0
                }
            }
            else{
                return 0
            }
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! InstallCell
            cell.lblName.text = self.arrNames[indexPath.section]
            cell.txtValue.rightview(Img: #imageLiteral(resourceName: "imgDropDown"))
            cell.txtValue.delegate = self
            cell.txtValue.text = JSON(mainDict)[arrayKeys[indexPath.section]].stringValue
            cell.imgView.image = arrImagesNames[indexPath.section]
            cell.txtValue.tag = indexPath.section
             cell.txtValue.keyboardType = .numberPad
            let picker = UIPickerView()
            picker.tag = 1001
            picker.dataSource = self
            picker.delegate = self
            cell.txtValue.inputView = picker
           
            return cell
        }
        
       else if indexPath.section == 4{
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as! InstallCell
            cell.txtTilte.leftview()
             cell.txtTilte.rightview(Img: #imageLiteral(resourceName: "imgDrop"))
             cell.txtTilte.delegate = self
              cell.txtTilte.text = JSON(arrRisers[indexPath.row])["title"].stringValue
              cell.txtTilte.tag = indexPath.section
              cell.txtTilte.accessibilityHint = "\(indexPath.section),\(indexPath.row)"
            
            
            cell.txtPieces.text = JSON(arrRisers[indexPath.row])["value"].stringValue
            
              cell.txtPieces.accessibilityHint = "\(indexPath.section),\(indexPath.row)"
              cell.txtPieces.tag =  -indexPath.section
               cell.txtPieces.delegate = self
            
            let picker = UIPickerView()
            picker.dataSource = self
            picker.delegate = self
            picker.accessibilityHint = "\(indexPath.section),\(indexPath.row)"
             cell.txtTilte.inputView = picker
            
             cell.btnAddMinus.tag = indexPath.row
            cell.btnAddMinus.accessibilityHint = "\(indexPath.section),\(indexPath.row )"
            cell.btnAddMinus.removeTarget(self, action: nil, for: .allEvents)
            if indexPath.row == 0{
                
                 cell.btnAddMinus.setImage(#imageLiteral(resourceName: "ImgAddNumber"), for: .normal)
                cell.btnAddMinus.addTarget(self, action: #selector(self.btnPlusAction(sender:)), for: .touchUpInside)
            }
            else{
                 cell.btnAddMinus.setImage(#imageLiteral(resourceName: "imgMinus"), for: .normal)
                cell.btnAddMinus.addTarget(self, action: #selector(self.btnMinusAction(sender:)), for: .touchUpInside)
                
            }
            
             return cell
        }
        else {
            
            
             if indexPath.row == 0{
                 let cell =  tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! InstallCell
                cell.lblName.text = "Slot .0.10 or"
                cell.txtValue.text = JSON(mainDict)["slot_screen"].stringValue
                cell.txtValue.inputView = nil
                cell.txtValue.delegate = self
                cell.txtValue.rightVC()
                cell.txtValue.tag = 1111
                cell.imgView.image = #imageLiteral(resourceName: "imgPvc")
                return cell
            }
           else {
                
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as! InstallCell
                
                let picker = UIPickerView()
                picker.dataSource = self
                picker.delegate = self
               
                picker.accessibilityHint = "\(indexPath.section),\(indexPath.row )"
                cell.txtTilte.inputView = picker
                cell.txtTilte.delegate = self
                 cell.txtTilte.accessibilityHint = "\(indexPath.section),\(indexPath.row)"
                 cell.txtTilte.tag = indexPath.section
                cell.txtTilte.rightview(Img: #imageLiteral(resourceName: "imgDrop"))
                cell.btnAddMinus.tag = indexPath.row
                cell.txtTilte.text = JSON(arrScreens[indexPath.row - 1])["title"].stringValue

                
                cell.txtPieces.text = JSON(arrScreens[indexPath.row - 1])["value"].stringValue
                
                cell.txtPieces.accessibilityHint = "\(indexPath.section),\(indexPath.row)"
                cell.txtPieces.tag =  -indexPath.section
                cell.txtPieces.delegate = self
                
                
                cell.btnAddMinus.accessibilityHint = "\(indexPath.section),\(indexPath.row )"
          cell.btnAddMinus.removeTarget(self, action: nil, for: .allEvents)
                if indexPath.row == 1{
                    cell.btnAddMinus.setImage(#imageLiteral(resourceName: "ImgAddNumber"), for: .normal)
                    cell.btnAddMinus.addTarget(self, action: #selector(self.btnPlusAction(sender:)), for: .touchUpInside)
                    
                }
                 else if indexPath.row > 1{
                    cell.btnAddMinus.setImage(#imageLiteral(resourceName: "imgMinus"), for: .normal)
                    cell.btnAddMinus.addTarget(self, action: #selector(self.btnMinusAction(sender:)), for: .touchUpInside)
                }
               
                return cell
                
            }
            
        }
    
       

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        return 60
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.arrNames.count == section{
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! InstallCell
            cell.btnSave.addTarget(self, action: #selector(self.btnSaveTapped(sender:)), for: .touchUpInside)
            return cell.contentView
        }
       else if section == 6{
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "separator")
            
            return cell?.contentView
        }
            
        else if section == 3 || section == 7 || section == 8 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! InstallCell
                   cell.lblName.text = self.arrNames[section]
            
            cell.txtValue.delegate = self
            cell.txtValue.text = JSON(mainDict)[arrayKeys[section]].stringValue
               cell.txtValue.tag = section
            cell.txtValue.inputView = nil
            if  section == 3 {
                  cell.txtValue.keyboardType = .numberPad
            }
            else{
                  cell.txtValue.keyboardType = .default
            }
          
            cell.txtValue.rightVC()
            cell.imgView.image = arrImagesNames[section]
                return cell.contentView
            }
            else{
                
                 let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! InstallCell
            
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
                
                cell.bgView.addGestureRecognizer(tap)
                cell.bgView.tag = section
                cell.bgView.isUserInteractionEnabled = true
                
                cell.bgView.addGestureRecognizer(tap)
                
              if JSON(self.mainDict)[arrayKeys[section]].stringValue == "1" {
                    cell.txtName.rightview(Img: #imageLiteral(resourceName: "imgCheck"))
                }
                else{
                    cell.txtName.rightview(Img: #imageLiteral(resourceName: "imgUnselect"))
                }
            cell.txtName.text = self.arrNames[section]
          
            if section == 0 {
                cell.txtName.leftview(image: #imageLiteral(resourceName: "imgSteel"))
                cell.txtName.rightview(Img: #imageLiteral(resourceName: "imgDropDown"))
            }
            return cell.contentView
            }
        }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if self.arrNames.count == section{
            return 120
        }
        else if section == 0{
            return 0
        }
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
       
    }
    
    //MARK:- PickerView Data Source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1001{
            return dropDownList[2]["subcategory"].arrayValue.count
        }
        else{
        return  dropDownList[pickerView.tag]["subcategory"].arrayValue.count
           }
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == 1001{
            return dropDownList[2]["subcategory"][row]["name"].stringValue
        }
        else{
        
      return dropDownList[pickerView.tag]["subcategory"][row]["name"].stringValue
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
      
        if pickerView.tag == 1001{
            let cell = self.tableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! InstallCell
            
            cell.txtValue.text = dropDownList[2]["subcategory"][row]["name"].stringValue
            
        }
        else{
        
        let arr = pickerView.accessibilityHint?.components(separatedBy: ",")
        
        let rowValue = Int(arr![1])
        let sectionValue = Int(arr![0])
        let cell = self.tableView.cellForRow(at: IndexPath.init(row: rowValue!, section: sectionValue!)) as! InstallCell
        
            cell.txtTilte.text =  dropDownList[pickerView.tag]["subcategory"][row]["name"].stringValue
        
    }
    }
    //MARK:- AddInstallSupplyApiCall
    func addInstallSupplyApiCall(isOffline : Bool , edit : Bool){
        let parameter  : Parameters  =  ["action": "ac_install_supplies", "userid": standard.value(forKey: "id")!, "projectid":  projectId,"date":strDate
            , "steel": JSON(self.mainDict)["steel"].stringValue
            , "single": JSON(self.mainDict)["single"].stringValue
            , "nested": JSON(self.mainDict)["nested"].stringValue
            , "size": JSON(self.mainDict)["size"].stringValue
            , "riser": JSON(self.mainDict)["riser"].stringValue
            , "screen": JSON(self.mainDict)["screen"].stringValue
            , "slot_screen": JSON(self.mainDict)["slot_screen"].stringValue
              , "topcap": JSON(self.mainDict)["topcap"].stringValue
            , "bottomcap": JSON(self.mainDict)["bottomcap"].stringValue
            ,
              "screen_arr" : arrScreens,
              "riser_arr" : arrRisers
            
        ]
        
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(consumableUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  json["response"]["msg"].stringValue )
                return
            }
            
            
            DispatchQueue.main.async {
                 ProjectsOffline.sharedInstance.getOfflineDates(isOffline: isOffline, strDate: self.strDate, projectID: projectId)
                
                
                if ProjectsOffline.sharedInstance.getOfflineTables(tables: "InstallSupply", strDate: self.strDate, projectID: projectId) == true {
                    self.updateCoredata(isOffline: false, edit: false, isShowAlert: true)
                }
                else{
                    self.saveCoredata(isOffline: isOffline, edit: edit, msg: json["response"]["msg"].stringValue, isShow: true)
                }
            }
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  error.localizedDescription)
            }
            
            print(error.localizedDescription)
        }
        
    }
    
    //MARK:- Update Core Data
    func updateCoredata(isOffline : Bool , edit : Bool , isShowAlert : Bool){
        
        let managedContext = CoreDataStack.managedObjectContext
        let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "InstallSupply")
        updateFetch.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , strDate)
        do {
            let results = try managedContext.fetch(updateFetch) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                
                // In my case, I only updated the first item in results
                let session = results![0]
                
                let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
                session.setValue(data1, forKey: "dataDetails")
                
                session.setValue(strDate, forKey: "date")
                session.setValue(projectId, forKey: "projectid")
                session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                session.setValue(isOffline, forKey: "isOffline")
                session.setValue(edit, forKey: "edit")
                
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try managedContext.save()
            
            
            if isShowAlert == true {
                self.navigationController?.popViewController(animated: true)
                appDelegates.window?.rootViewController?.showAlert(messageStr:  "Add consumption Install Supplies list updated successfully")
            }
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
        
    }
    
    
    //MARK:- Save Core Data Function
    func saveCoredata(isOffline : Bool,edit: Bool , msg : String , isShow : Bool){
        print(isOffline)
        print(edit)
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "InstallSupply",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
         print(self.mainDict)
        let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
        session.setValue(data1, forKey: "dataDetails")
        
        session.setValue(strDate, forKey: "date")
        session.setValue(projectId, forKey: "projectid")
        session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
        session.setValue(isOffline, forKey: "isOffline")
        session.setValue(edit, forKey: "edit")
        
        
        CoreDataStack.saveContext()
        
        
        if isShow == true{
            self.navigationController?.popViewController(animated: true)
            appDelegates.window?.rootViewController?.showAlert(messageStr: msg)
        }
        
    }
    //MARK:- Get Core Data Function
    func getCoreData() {
        
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "InstallSupply")
        
        fetchRequest.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , strDate)
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            for session in sessions{
                var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                
                dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                
                dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                
                
                let arrayScreen =  JSON(dictionary)["screen_arr"].arrayValue
                let arrayRiser =   JSON(dictionary)["riser_arr"].arrayValue
                
                self.arrScreens.removeAll()
                self.arrRisers.removeAll()
                for i in 0..<arrayScreen.count{
                    let dict =  arrayScreen[i].dictionaryObject
                    self.arrScreens.append(dict ?? [String: Any]())
                }
                for i in 0..<arrayRiser.count{
                    let dict =  arrayRiser[i].dictionaryObject
                    self.arrRisers.append(dict ?? [String: Any]())
                }
                
                self.mainDict = dictionary
                
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    
    
    
    
    
    
    //MARK:- fetchDetails function
    func  fetchDetails(){
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid": projectId,"table":"ac_install_supplies","date": strDate]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
             print(json)
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            self.mainDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
            let arrayScreen =  json["response"]["data"]["screen_arr"].arrayValue
              let arrayRiser =  json["response"]["data"]["riser_arr"].arrayValue
            
            self.arrScreens.removeAll()
            self.arrRisers.removeAll()
            for i in 0..<arrayScreen.count{
                let dict =  arrayScreen[i].dictionaryObject
                self.arrScreens.append(dict ?? [String: Any]())
            }
            for i in 0..<arrayRiser.count{
                let dict =  arrayRiser[i].dictionaryObject
                self.arrRisers.append(dict ?? [String: Any]())
            }
           
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "InstallSupply", strDate: self.strDate, projectID: projectId) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false)
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false)
                
            }
            self.getCoreData()
            
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    
}
