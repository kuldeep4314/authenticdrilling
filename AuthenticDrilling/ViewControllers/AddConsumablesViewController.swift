//
//  AddConsumablesViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-25.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit

class AddConsumablesViewController: ViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    let arrNames = ["Grout Supplies","Sampling Supplies", "Installation" ,"Surface Completion" ,"Miscellaneous" ,"Damaged Tooling"]
    let arrNamesImages = [#imageLiteral(resourceName: "imgGroutSupplies"),#imageLiteral(resourceName: "imgSampleSupplies"),#imageLiteral(resourceName: "imgInstallation"),#imageLiteral(resourceName: "imgSurfaceCompletion"),#imageLiteral(resourceName: "imgMiscellaneous"),#imageLiteral(resourceName: "imgDamagedTooling")]
    var projectInformation :ProjectData?
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblProjectName: UILabel!
    
     var dateString = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- CollectionView Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrNames.count
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        
        cell.lblName.text = arrNames[indexPath.row]
        cell.imgView.image = arrNamesImages[indexPath.row]
        cell.bgView.addshodowToView()
        return cell
        
    }
    
    //MARK:- CollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if indexPath.row == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "GroutSuppliesViewController") as! GroutSuppliesViewController
            vc.projectInfo = projectInformation
            vc.strDate = dateString
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
      else  if indexPath.row == 1{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SamplingSuppliesViewController") as! SamplingSuppliesViewController
            vc.projectInfo = projectInformation
            vc.strDate = dateString
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
       else if indexPath.row == 2{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "InstallSupplyViewController") as! InstallSupplyViewController
            vc.projectInfo = projectInformation
            vc.strDate = dateString
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
       else if indexPath.row == 3{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SurfaceCompletionViewController") as! SurfaceCompletionViewController
            vc.projectInfo = projectInformation
            vc.strDate = dateString
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
      else  if indexPath.row == 4{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MiscellaneousSuppliesViewController") as! MiscellaneousSuppliesViewController
            vc.projectInfo = projectInformation
            vc.strDate = dateString
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else  if indexPath.row == 5{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DamagedToolViewController") as! DamagedToolViewController
            vc.projectInfo = projectInformation
            vc.strDate = dateString
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        
        return CGSize(width: collectionView.frame.size.width/2 - 0.5, height: collectionView.frame.size.width/2 - 0.5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
}

