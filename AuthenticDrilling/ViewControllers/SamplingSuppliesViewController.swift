//
//  SamplingSuppliesViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-29.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit
import CoreData
class SamplingSuppliesViewController: ViewController,UITableViewDelegate,UITableViewDataSource ,UITextFieldDelegate ,UIPickerViewDelegate,UIPickerViewDataSource{
    
    let arrNames = ["Core Boxes","Liner","Shelby Tubes","Drums","Disposable Bailers","Wooden Plugs"]
    var sessions: [NSManagedObject] = []
    let arrayKeysHeaders = ["coreboxes","linears","shelby_tube","drum" ,"disposable","wooden_plug"]
    
    var dictKeyValues = ["1":"coreboxes_qty","2":"coreboxes_unit","11":"linears_qty","12":"linears_unit"
        ,"21":"shelby_qty","22":"shelby_unit"
    ,"31":"drum_qty","32":"drum_unit","41":"disposable_qty","42":"disposable_unit","51":"wooden_qty","52":"wooden_unit"
    ]
    
    var strDate = String()
    var projectInfo :ProjectData?
    
    var mainDict =  [String: Any]()
      var dropDownList = [JSON]()
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if  let decoded  = standard.value(forKey: "catData") as? [String : Any]{
         
            dropDownList  = JSON(decoded)["Sampling Supply"].arrayValue
               print(dropDownList)
        }
        self.mainDict.updateValue(dropDownList[0]["subcategory"][0]["name"].stringValue, forKey: "coreboxes_unit")
        
        self.mainDict.updateValue(dropDownList[1]["subcategory"][0]["name"].stringValue, forKey: "linears_unit")
        self.mainDict.updateValue(dropDownList[2]["subcategory"][0]["name"].stringValue, forKey: "shelby_unit")
        self.mainDict.updateValue(dropDownList[3]["subcategory"][0]["name"].stringValue, forKey: "drum_unit")
        self.mainDict.updateValue(dropDownList[4]["subcategory"][0]["name"].stringValue, forKey: "disposable_unit")
        self.mainDict.updateValue(dropDownList[5]["subcategory"][0]["name"].stringValue, forKey: "wooden_unit")
       tableView.tableFooterView = UIView()
        
        if Network.isConnectedToNetwork() == true {
            self.getOfflineData()
        }
        else{
            self.getCoreData()
        }
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print(dictKeyValues[String(textField.tag)]!)
        mainDict.updateValue(textField.text ?? "", forKey:dictKeyValues[String(textField.tag)]!)
        print(mainDict)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
        print(sender.view!.tag)
        if JSON(self.mainDict)[arrayKeysHeaders[sender.view!.tag]].stringValue == "1" {
            self.mainDict.updateValue("0", forKey: arrayKeysHeaders[sender.view!.tag])
        
           
            self.tableView.reloadData()

        }
        else{
            self.mainDict.updateValue("1", forKey: arrayKeysHeaders[sender.view!.tag])
            
            
            self.tableView.reloadData()
           
        }
        
        
    }
    
    @objc func btnSaveTapped(sender : UIButton){
        if Network.isConnectedToNetwork() == true {
            
            self.addSampleSupplyApiCall(isOffline: false, edit: false)
        }
        else  if Network.isConnectedToNetwork() == false {
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "SampleSupply", strDate: self.strDate, projectID: projectId) == true
            {
                self.updateCoredata(isOffline: true, edit: true, isShowAlert: true)
            }
                
            else{
                ProjectsOffline.sharedInstance.getOfflineDates(isOffline: true, strDate: self.strDate, projectID: projectId)
                self.saveCoredata(isOffline: true, edit: false, msg: "Add consumption Sampling Supplies list added successfully", isShow: true)
            }
        }
        
        
    }
    
    func getOfflineData(){
        
        let offlineDetails = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "SampleSupply")
        print(offlineDetails)
        if offlineDetails.count > 0 {
            let parameters : Parameters = ["action": "ac_sampling_supplies" ,"data" :  offlineDetails ]
            
            self.syncOfflineMultipleApi(parameter: parameters)
            
        }
        else{
            self.fetchDetails()
        }
        
    }
   
    //MARK: - SyncOfflineMultipleApi
    func syncOfflineMultipleApi(parameter :Parameters){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(projectUrlOffline, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            ProjectsOffline.sharedInstance.updateCoredataTables(tables: "SampleSupply")
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            self.getCoreData()
            print(error.localizedDescription)
        }
    }

    
    
    
    
    
    
    //MARK:- TableView Data Source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrNames.count + 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
         if arrNames.count == section {
             return 0
        }
        else{
            if JSON(self.mainDict)[arrayKeysHeaders[section]].stringValue == "1" {
                return 1
            }
            else{
                return 0
            }
        }
       
       
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
                let cell =  tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! SupplyCell
        cell.txtQty.tag = indexPath.section * 10 + 1
        cell.txtUnit.tag = indexPath.section * 10 + 2
        
        cell.txtQty.text = JSON(self.mainDict)[dictKeyValues["\(cell.txtQty.tag )"]!].stringValue
        cell.txtUnit.text = JSON(self.mainDict)[dictKeyValues["\(cell.txtUnit.tag )"]!].stringValue
         cell.txtUnit.rightview(Img: #imageLiteral(resourceName: "imgDropDown"))
        let picker = UIPickerView()
        picker.dataSource = self
        picker.delegate = self
        picker.tag = indexPath.section
        cell.txtUnit.inputView = picker
        cell.txtUnit.delegate = self
        cell.txtQty.delegate  = self
    
                return cell
      
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        return 44
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        if self.arrNames.count == section{
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! SupplyCell
            cell.btnSave.addTarget(self, action: #selector(self.btnSaveTapped(sender:)), for: .touchUpInside)
            return cell.contentView
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SupplyCell
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
            
            cell.bgView.addGestureRecognizer(tap)
            cell.bgView.tag = section
            cell.bgView.isUserInteractionEnabled = true
            
            cell.bgView.addGestureRecognizer(tap)
            
            if JSON(self.mainDict)[arrayKeysHeaders[section]].stringValue == "1" {
                cell.txtName.rightview(Img: #imageLiteral(resourceName: "imgCheck"))
            }
            else{
                cell.txtName.rightview(Img: #imageLiteral(resourceName: "imgUnselect"))
            }
             cell.txtName.text = self.arrNames[section]
            return cell.contentView
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.arrNames.count == section{
            return 100
        }
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.tableView.deselectRow(at: indexPath, animated: true)
    }
    //MARK:- PickerView Data Source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dropDownList[pickerView.tag]["subcategory"].arrayValue.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dropDownList[pickerView.tag]["subcategory"][row]["name"].stringValue
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
          let cell = self.tableView.cellForRow(at: IndexPath.init(row: 0, section: pickerView.tag)) as! SupplyCell
            cell.txtUnit.text = dropDownList[pickerView.tag]["subcategory"][row]["name"].stringValue
    }

    
    //MARK:- Update Core Data
    func updateCoredata(isOffline : Bool , edit : Bool , isShowAlert : Bool){
        
        let managedContext = CoreDataStack.managedObjectContext
        let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "SampleSupply")
        updateFetch.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , strDate)
        do {
            let results = try managedContext.fetch(updateFetch) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                
                // In my case, I only updated the first item in results
                let session = results![0]
                
                let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
                session.setValue(data1, forKey: "dataDetails")
                
                session.setValue(strDate, forKey: "date")
                session.setValue(projectId, forKey: "projectid")
                session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                session.setValue(isOffline, forKey: "isOffline")
                session.setValue(edit, forKey: "edit")
                
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try managedContext.save()
            
            
            if isShowAlert == true {
                self.navigationController?.popViewController(animated: true)
                appDelegates.window?.rootViewController?.showAlert(messageStr:  "Add consumption Sampling Supplies list updated successfully")
            }
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
        
    }
    
    
    //MARK:- Save Core Data Function
    func saveCoredata(isOffline : Bool,edit: Bool , msg : String , isShow : Bool){
        print(isOffline)
        print(edit)
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "SampleSupply",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        print(self.mainDict)
        let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
        session.setValue(data1, forKey: "dataDetails")
        
        session.setValue(strDate, forKey: "date")
        session.setValue(projectId, forKey: "projectid")
        session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
        session.setValue(isOffline, forKey: "isOffline")
        session.setValue(edit, forKey: "edit")
        
        
        CoreDataStack.saveContext()
        
        
        if isShow == true{
            self.navigationController?.popViewController(animated: true)
            appDelegates.window?.rootViewController?.showAlert(messageStr: msg)
        }
        
    }
    //MARK:- Get Core Data Function
    func getCoreData() {
        
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "SampleSupply")
        
        fetchRequest.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , strDate)
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            for session in sessions{
                var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                
                dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                
                dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                
                self.mainDict = dictionary
                
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    
    
    //MARK:- AddSampleSupplyApiCall
    func addSampleSupplyApiCall(isOffline : Bool , edit : Bool){
        let parameter  : Parameters  =  ["action": "ac_sampling_supplies", "userid": standard.value(forKey: "id")!, "projectid":  projectId, "coreboxes": JSON(self.mainDict)["coreboxes"].stringValue, "coreboxes_qty": JSON(self.mainDict)["coreboxes_qty"].stringValue, "coreboxes_unit": JSON(self.mainDict)["coreboxes_unit"].stringValue, "linears": JSON(self.mainDict)["linears"].stringValue, "linears_qty": JSON(self.mainDict)["linears_qty"].stringValue, "linears_unit": JSON(self.mainDict)["linears_unit"].stringValue, "shelby_tube": JSON(self.mainDict)["shelby_tube"].stringValue, "shelby_qty":JSON(self.mainDict)["shelby_qty"].stringValue, "shelby_unit": JSON(self.mainDict)["shelby_unit"].stringValue, "date":strDate
          , "drum": JSON(self.mainDict)["drum"].stringValue
              , "drum_qty": JSON(self.mainDict)["drum_qty"].stringValue
              , "drum_unit": JSON(self.mainDict)["drum_unit"].stringValue
            , "disposable": JSON(self.mainDict)["disposable"].stringValue
            , "disposable_qty": JSON(self.mainDict)["disposable_qty"].stringValue
            , "disposable_unit": JSON(self.mainDict)["disposable_unit"].stringValue
            , "wooden_plug": JSON(self.mainDict)["wooden_plug"].stringValue
            , "wooden_qty": JSON(self.mainDict)["wooden_qty"].stringValue
            , "wooden_unit": JSON(self.mainDict)["wooden_unit"].stringValue
        ]
        
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(consumableUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  json["response"]["msg"].stringValue )
                return
            }
            
            
            DispatchQueue.main.async {
                
                ProjectsOffline.sharedInstance.getOfflineDates(isOffline: isOffline, strDate: self.strDate, projectID: projectId)
                
                
                if ProjectsOffline.sharedInstance.getOfflineTables(tables: "SampleSupply", strDate: self.strDate, projectID: projectId) == true {
                    self.updateCoredata(isOffline: false, edit: false, isShowAlert: true)
                }
                else{
                    self.saveCoredata(isOffline: isOffline, edit: edit, msg: json["response"]["msg"].stringValue, isShow: true)
                }
            }

            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  error.localizedDescription)
            }
            
            print(error.localizedDescription)
        }
        
    }
    
    //MARK:- fetchDetails function
    func  fetchDetails(){
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid": projectId,"table":"ac_sampling_supplies","date": strDate]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            self.mainDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "SampleSupply", strDate: self.strDate, projectID: projectId) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false)
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false)
                
            }
            self.getCoreData()
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    
    
    
}
