//
//  RigInspectionViewController.swift
//  AuthenticDrilling
//
//  Created by goyal on 05/10/18.
//  Copyright © 2018 Avatar Singh. All rights reserved.
//



import UIKit

class RigInspectionViewController: ViewController {
    
    let headersArray = ["","Fluid Levels, Air Filter, and Belts","Rotary Box","Kelly Bar","Function Test","General Inspection and Lube","Comments","Parts to be ordered",""]
    
    let subArray = ["" : [""],"Fluid Levels, Air Filter, and Belts" : ["Check Hydraulic Oil Level","Check Engine Oil Level","Check Tranny Oil Level","Check Right Angle Drive Level","Check Coolant Level in Radiator","Check Engine Air Filter, Replace if Needed","Check Brake Fluid level in Clutch Master Cylinder","Check Winch Gear Oil Level","Check Serpentine Belt on Engine"],"Rotary Box" : ["Check Rotary Box Chain Adjustment/Alignment","Check all Rotary Box and Spindle Plate Bolts for Tightness","Check Spindle to Kelly Alignment","Lube Spindle / Kelly Bearings, and Swivel"],"Kelly Bar" : ["Lube Kelly Bar and Check for excess play in Kelly Bushing","Check Kelly Bar Retaining Bolt","Check Top Kelly and Angle Bearing Mounting Bolts","Check Kelly Bearings","Lube Bearings and U-Joints","Check U-Joint"],"Function Test":["Check Oil Pressure, Voltage (Charging at 14.3V), and Coolant Temp Guages","Check Hydraulic Pressure 2000 psi","Check Winch Shives at Crown and Lube","Test that Emergency Shut-down Switchs Work"],"General Inspection and Lube" : ["Check and Lube U-Joint and Driveline Between Engine and Hydraulic Pump","Check Hydraulic Hoses/Fittings for Leaks and Wear","Check Moyno Chain Adjust/Lube","Check Auto Hammer Chain Adjust/Lube","Check Track Tension and Drive Sprocket, Inspect and Lube Road Wheels (if equipped)","Check Pivot Pins/Keepers - Mast Tilt, Outrigger Plates, Break-out Wrench","Check Battery","Check Tightness of Bolts","Check Spindle","Check Spindle Plate","Check Winch Mounts","Check Right Angle Drive Mount","Check Rig Frame/Mast for Cracks","Check Tower Shives","Check Wiggle Tail Pivoting Pin Mounting Bolts","Check Pivot Pins/Mast Tilt, Outriggers, Break Out Wrench","Check Engine Mounts","Check Fire Extinguisher and Month Punch"],"Comments": [""],"Parts to be ordered" : [""], "image" : [""]]
    
   
    
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: 5))
        // Do any additional setup after loading the view.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        
    }
    @IBAction func btnPriorityTapped(_ sender: UIButton) {
        let points = sender.convert(CGPoint.zero, to: self.tableView)
        
        guard let indexPath = self.tableView.indexPathForRow(at: points) else{
            
            return
        }
        print(indexPath)
        
        
        self.tableView.reloadData()
    }
    
    @objc  func dateChange(sender: UIDatePicker){
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let cell = self.tableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! OptionTableCell
        cell.txtDate.text =  formatter.string(from: sender.date)
        
    }
}
//MARK:- TableView Cells
extension RigInspectionViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return headersArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return JSON(subArray[headersArray[section]]!).arrayValue.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath) as! OptionTableCell
            let datePicker = UIDatePicker()
            datePicker.addTarget(self, action: #selector(self.dateChange(sender:)), for: .valueChanged)
            datePicker.datePickerMode = .date
            
            cell.txtDate.inputView = datePicker
        
            cell.txtDate.leftview(image: #imageLiteral(resourceName: "imgStartDate"))
            cell.txtInspectedBy.leftview(image: #imageLiteral(resourceName: "imgInspection1"))
            cell.txtTruck.leftview(image: #imageLiteral(resourceName: "imgTruck1"))
            cell.txtOdometer.leftview(image: #imageLiteral(resourceName: "imgTimer"))
            
            return cell
            
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell", for: indexPath) as! OptionTableCell
           
            cell.lblTitle.text =  JSON(subArray[headersArray[indexPath.section]]!)[indexPath.row].stringValue
            return cell
            
        }
        else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell", for: indexPath) as! OptionTableCell
           
            cell.lblTitle.text =  JSON(subArray[headersArray[indexPath.section]]!)[indexPath.row].stringValue
            return cell
            
        }
        else if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell", for: indexPath) as! OptionTableCell
          
            cell.lblTitle.text =  JSON(subArray[headersArray[indexPath.section]]!)[indexPath.row].stringValue
            return cell
            
        }
        else if indexPath.section == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell", for: indexPath) as! OptionTableCell
         
            cell.lblTitle.text =  JSON(subArray[headersArray[indexPath.section]]!)[indexPath.row].stringValue
            return cell
            
        }
        else if indexPath.section == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell", for: indexPath) as! OptionTableCell
         
            cell.lblTitle.text =  JSON(subArray[headersArray[indexPath.section]]!)[indexPath.row].stringValue
            return cell
            
        }
            
            
        else if indexPath.section == 6 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellTextView", for: indexPath) as! OptionTableCell
            cell.txtView.addshodowToTextView()
            cell.txtView.delegate = self
            if cell.txtView.text == "" || cell.txtView.text == "Type here..." {
                cell.txtView.text = "Type here..."
                cell.txtView.textColor = UIColor.lightGray
            }
            else{
                cell.txtView.textColor = UIColor.black
            }
            
            return cell
            
        }
        else if indexPath.section == 7 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellTextView", for: indexPath) as! OptionTableCell
            cell.txtView.addshodowToTextView()
            cell.txtView.delegate = self
            if cell.txtView.text == "" || cell.txtView.text == "Type here..."{
                cell.txtView.text = "Type here..."
                cell.txtView.textColor = UIColor.lightGray
            }
            else{
                cell.txtView.textColor = UIColor.black
            }
            return cell
            
        }
        else if indexPath.section == 8 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "celIImage", for: indexPath) as! OptionTableCell
            
            return cell
            
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath) as! OptionTableCell
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "headerCell") as! OptionTableCell
        cell.lblHeader.text = headersArray[section]
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }
        else{
            return 40
        }
    }
    
}


extension RigInspectionViewController : UITextViewDelegate {
    
    //MARK:- TextView Delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray
        {
            textView.text = ""
            textView.textColor = UIColor.black
            
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Type here..."
            textView.textColor = UIColor.lightGray
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        print(newText.count)
        return newText.count <= 300
    }
}
