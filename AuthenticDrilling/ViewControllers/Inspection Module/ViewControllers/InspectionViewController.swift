//
//  InspectionViewController.swift
//  AuthenticDrilling
//
//  Created by goyal on 04/10/18.
//  Copyright © 2018 Avatar Singh. All rights reserved.
//

import UIKit

class InspectionViewController: ViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    let arrNames = ["Job Site Daily Equipment Inspection","Air Compressor", "Trailer Inspection" ,"Rig Inspection" ,"Truck Inspection" ]
    var projectInformation :ProjectData?
    let arrImagesNames = [#imageLiteral(resourceName: "imgEquipment"),#imageLiteral(resourceName: "imgAircompressor"), #imageLiteral(resourceName: "imgTrailerInspection") ,#imageLiteral(resourceName: "imgRigInspection") ,#imageLiteral(resourceName: "imgTruckInspection") ]
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblProjectName: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- CollectionView Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        
        cell.lblName.text = arrNames[indexPath.row]
        cell.imgView.image = arrImagesNames[indexPath.row]
        cell.bgView.addshodowToView()
        return cell
        
    }
    
    //MARK:- CollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if indexPath.row == 0 {
          
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DailyEquipmentViewController") as! DailyEquipmentViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
       
        else   if indexPath.row == 1{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AirCompressorInspectionViewController") as! AirCompressorInspectionViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
            
        else   if indexPath.row == 2{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TrailerInspectionViewController") as! TrailerInspectionViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else   if indexPath.row == 3{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "RigInspectionViewController") as! RigInspectionViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
            
        else   if indexPath.row == 4{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TruckInspectionViewController") as! TruckInspectionViewController
         
            self.navigationController?.pushViewController(vc, animated: true)
        }
        print(indexPath.row)
      
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        
        return CGSize(width: collectionView.frame.size.width/2 - 0.5, height: collectionView.frame.size.width/2 - 0.5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
}
