//
//  DailyEquipmentViewController.swift
//  AuthenticDrilling
//
//  Created by goyal on 05/10/18.
//  Copyright © 2018 Avatar Singh. All rights reserved.
//

import UIKit

class DailyEquipmentViewController: ViewController {
    
    var selectedIndex =  0
    
    let DrillRigArray = ["Operator Panel / Gauges Functional","Reservoirs Filled (fuel, hydraulic, oil)","All Brakes and Clutches Functional","Hydraulic Hoses","Audible Alarm","Water Hose/Valves","Pulleys","Cables","Guards (Moving parts guarded/No guards removed)","Kill Switch","Controls Functional","Warning Lights Functional","Tracks /  Tires (no damage)","No Fluid Leaks (pneumatic/hydraulic)","Fire Extinguisher/Tag (Current Month)","Blocking","First Aid Kits","Spill Kit","Whip Checks","Registration/Insurance/SMM","Bypasses to Fix","Work Lights","Exhaust"]
    
      let truckArray = ["Registration & Insurance","License Plate expire","DOT Reflectors & Mudflaps","MSDS/SDS Book","Horn & Backup Alarm","Fire Extinguisher/Tag (Current Month)","First Aid Kit","Brakes and E Brake","Lights & Signals","Windshield/wipers/mirrors","Clutch","Transmission","Exhaust System","Fluid Levels - Engine, Power Steeing, Coolant, Tranny, Windshield, Radiator, Brake","Tires - inflation/tread/alignment","Rims, Studs & Lug Nuts","Hitch - Securely mounted, bolts","Wheel Chocks/Buggie Whip","Tool Boxes / Chains","Dashboard Error Codes","Leaks","New Damage"]
    
       let trailerArray = ["Registration & Insurance","License Plate expire","DOT Reflectors & Mudflaps","Brakes","Tires - inflation/tread/alignment","Lights & Signals","Rims, Studs & Lug Nuts","Frame Straight","Deck Condition","Hitch","Break Away","Wiring /  Plug","Load Secure","Jack","Wheel Chocks"]
    
      let airCompressorArray = ["Registration/Insurance/SMM","License Plate expire","DOT Reflectors & Mudflaps","Fire Extinguisher/Tag (Current Month)","Brakes","Tires - inflation/tread/alignment","Lights & Signals","Rims, Studs & Lug Nuts","Hitch","Break Away","Wiring / Plug","Drain Control Line Filter","Check Oil Level / Coolant","Check Filters","Air Supply Valve","Wheel Chocks","Gauges/Indicators Functional","Unusual Noises / Vibration"]
    
    let weeklyArray = ["Drain Water out of Oil","Moisture Separator Draining","Check Safety Valve","Drain Air Receivers"]
    
    let placeHolderArray = ["Drill #","Truck #","Trailer #","Air Compressor"]
        let imagesArray = [#imageLiteral(resourceName: "imgTrailer1"),#imageLiteral(resourceName: "imgIntermidate"),#imageLiteral(resourceName: "imgTrailer1"),#imageLiteral(resourceName: "imgAir")]
    var dataArray = [String]()
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        dataArray = DrillRigArray
        self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: 5))
        // Do any additional setup after loading the view.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        
    }
    @IBAction func btnPriorityTapped(_ sender: UIButton) {
        let points = sender.convert(CGPoint.zero, to: self.tableView)
        
        guard let indexPath = self.tableView.indexPathForRow(at: points) else{
            
            return
        }
        print(indexPath)
        
        
        self.tableView.reloadData()
    }
    
    @objc  func dateChange(sender: UIDatePicker){
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let cell = self.tableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! OptionTableCell
        cell.txtDate.text =  formatter.string(from: sender.date)
        
    }
}
//MARK:- TableView Cells
extension DailyEquipmentViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.selectedIndex == 3 {
              return 9
        }
        else{
              return 7
        }
      
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 4 {
          return   dataArray.count
        }
            
        else {
            if self.selectedIndex == 3 && section == 6 {
                return weeklyArray.count
            }
            else{
                 return 1
            }
            
          
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath) as! OptionTableCell
            let datePicker = UIDatePicker()
            datePicker.addTarget(self, action: #selector(self.dateChange(sender:)), for: .valueChanged)
            datePicker.datePickerMode = .date
            
            cell.txtDate.inputView = datePicker
            
            cell.txtDate.leftview(image: #imageLiteral(resourceName: "imgStartDate"))
            cell.txtInspectedBy.leftview(image: #imageLiteral(resourceName: "imgAcc"))
            cell.txtTruck.leftview(image: #imageLiteral(resourceName: "imgProject"))
         
           cell.txtOdometer.leftview(image: #imageLiteral(resourceName: "imgJob"))
            
            return cell
            
        }
       else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell", for: indexPath) as! OptionTableCell
            
            return cell
            
        }
        else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellCollection", for: indexPath) as! OptionTableCell
            cell.delegates = self

            return cell
            
        }
        else if indexPath.section == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellTextField", for: indexPath) as! OptionTableCell
            
           cell.txtField.placeholder = placeHolderArray[selectedIndex]
            cell.txtField.leftview(image: imagesArray[selectedIndex])
            return cell
            
        }
        else if indexPath.section == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell", for: indexPath) as! OptionTableCell
            
            cell.lblTitle.text =  dataArray[indexPath.row]
            return cell
            
        }
        
        if selectedIndex == 3 {
            if indexPath.section == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "weeklyCell", for: indexPath) as! OptionTableCell
            
            
            return cell
            }
            if indexPath.section == 6 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell", for: indexPath) as! OptionTableCell
                
                cell.lblTitle.text =  weeklyArray[indexPath.row]
                return cell
            }
            else if indexPath.section == 7{
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellTextField", for: indexPath) as! OptionTableCell
                cell.txtField.leftview(image: #imageLiteral(resourceName: "imgInspection1"))
                cell.txtField.placeholder = "Inspected By"
                return cell
                
            }
            if indexPath.section == 8 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellTextView", for: indexPath) as! OptionTableCell
                cell.txtView.addshodowToTextView()
                cell.txtView.delegate = self
                if cell.txtView.text == "" || cell.txtView.text == "Type here..." {
                    cell.txtView.text = "Type here..."
                    cell.txtView.textColor = UIColor.lightGray
                }
                else{
                    cell.txtView.textColor = UIColor.black
                }
                
                return cell
                
            }
        }
        else{
             if indexPath.section == 5{
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellTextField", for: indexPath) as! OptionTableCell
                cell.txtField.placeholder = "Inspected By"
                     cell.txtField.leftview(image: #imageLiteral(resourceName: "imgInspection1"))
                return cell
                
            }
            
            
          else  if indexPath.section == 6 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellTextView", for: indexPath) as! OptionTableCell
                cell.txtView.addshodowToTextView()
                cell.txtView.delegate = self
                if cell.txtView.text == "" || cell.txtView.text == "Type here..." {
                    cell.txtView.text = "Type here..."
                    cell.txtView.textColor = UIColor.lightGray
                }
                else{
                    cell.txtView.textColor = UIColor.black
                }
                
                return cell
                
            }
        }
            
       
      
      
        let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath) as! OptionTableCell
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    
}


extension DailyEquipmentViewController : UITextViewDelegate {
    
    //MARK:- TextView Delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray
        {
            textView.text = ""
            textView.textColor = UIColor.black
            
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Type here..."
            textView.textColor = UIColor.lightGray
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        print(newText.count)
        return newText.count <= 300
    }
}

extension DailyEquipmentViewController : optionSelectedDelegates {
    
    func selecledIndexWithValue(index : Int){
        self.selectedIndex = index
        
        if index == 0 {
            dataArray = self.DrillRigArray
        }
        else if index == 1{
             dataArray = self.truckArray
        }
        else if index == 2 {
           dataArray =  trailerArray
        }
        else {
            dataArray = airCompressorArray
        }
        self.tableView.reloadData()
    }
    
}
