//
//  AirCompressorInspectionViewController.swift
//  AuthenticDrilling
//
//  Created by goyal on 05/10/18.
//  Copyright © 2018 Avatar Singh. All rights reserved.
//

import UIKit

class AirCompressorInspectionViewController: ViewController {
    
    let headersArray = ["","GENERAL INSPECTION","ENGINE COMPARTMENT","Comments","Parts to be ordered",""]
    
    let subArray = ["" : [""],"GENERAL INSPECTION" : ["Hitch Ball/Pintle Condition","Tires / Wheels","Marker and Brake Lights/Reflectors","Safety Chains/ Breakaway line/Electr Plug","License Plate / Insurance / Registration","Exterior and Body Panels","Lights and Reflectors","Mud Flaps","Suspension Components","Axle and Hub Oil Level","Trailer Frame","Exhaust","Brake Components","Spare Tire","Jack, lug wrench","Wheel Chocks","Air Supply Valves"],"ENGINE COMPARTMENT" : ["Engine Coolant","Engine Oil","Air End Compressor Oil","Drive Belts","Electrical Wiring","Alternator","Radiator","Fire Extinguisher"],"Comments": [""],"Parts to be ordered" : [""], "image" : [""]]
    
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: 5))
        // Do any additional setup after loading the view.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        
    }
    @IBAction func btnPriorityTapped(_ sender: UIButton) {
        let points = sender.convert(CGPoint.zero, to: self.tableView)
        
        guard let indexPath = self.tableView.indexPathForRow(at: points) else{
            
            return
        }
        print(indexPath)
        
        
        self.tableView.reloadData()
    }
    
    @objc  func dateChange(sender: UIDatePicker){
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let cell = self.tableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! OptionTableCell
        cell.txtDate.text =  formatter.string(from: sender.date)
        
    }
    
    @IBAction func btnSelectImage(_ sender: UIButton) {
        showActionSheet()
    }
    
    
    func showActionSheet(){
        
        let alertController = UIAlertController(title: "Import pictures from", message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alertController.addAction(cancelAction)
        
        let takePhoto = UIAlertAction(title: "Camera", style: .default) { (action) in
            
      self.selectImageFrom(type: "Camera")
            
        }
        alertController.addAction(takePhoto)
        
        let selectPhoto = UIAlertAction(title: "Photo Gallery", style: .default) { (action) in
            self.selectImageFrom(type: "Gallary")
        }
        alertController.addAction(selectPhoto)
        
        
        alertController.popoverPresentationController?.sourceView = self.view
        self.present(alertController, animated: true) {
            
        }
    }
    
    
    func selectImageFrom(type : String){
        let imagePicker = UIImagePickerController()
        if type == "Gallary"{
            
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                
                imagePicker.delegate = self
                imagePicker.sourceType = .savedPhotosAlbum;
                imagePicker.allowsEditing = false
                
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
            
        else{
            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                imagePicker.delegate = self
                imagePicker.sourceType = .camera;
                self.present(imagePicker, animated: true, completion: nil)
            }
            else
            {
                self.showAlert(messageStr: "You don't have camera")
                
            }
            
        }
     }
    
}
//MARK:- TableView Cells
extension AirCompressorInspectionViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return headersArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return JSON(subArray[headersArray[section]]!).arrayValue.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath) as! OptionTableCell
            let datePicker = UIDatePicker()
            datePicker.addTarget(self, action: #selector(self.dateChange(sender:)), for: .valueChanged)
            datePicker.datePickerMode = .date
            
            cell.txtDate.inputView = datePicker
            
            cell.txtDate.leftview(image: #imageLiteral(resourceName: "imgStartDate"))
            cell.txtInspectedBy.leftview(image: #imageLiteral(resourceName: "imgInspection1"))
            cell.txtTruck.leftview(image: #imageLiteral(resourceName: "imgAir"))
            cell.txtOdometer.leftview(image: #imageLiteral(resourceName: "imgTimer"))
            return cell
            
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell", for: indexPath) as! OptionTableCell
            
            cell.txtType.addshodowToTextField()
            cell.txtSelectOption.addshodowToTextField()
              cell.txtSelectOption.rightview(Img:#imageLiteral(resourceName: "imgOrageDropDown"))
            cell.lblTitle.text =  JSON(subArray[headersArray[indexPath.section]]!)[indexPath.row].stringValue
            return cell
            
        }
        else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell", for: indexPath) as! OptionTableCell
            cell.txtType.addshodowToTextField()
              cell.txtSelectOption.rightview(Img:#imageLiteral(resourceName: "imgOrageDropDown"))
            cell.txtSelectOption.addshodowToTextField()
            cell.lblTitle.text =  JSON(subArray[headersArray[indexPath.section]]!)[indexPath.row].stringValue
            return cell
            
        }
       
            
            
        else if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellTextView", for: indexPath) as! OptionTableCell
            cell.txtView.addshodowToTextView()
            cell.txtView.delegate = self
            if cell.txtView.text == "" || cell.txtView.text == "Type here..." {
                cell.txtView.text = "Type here..."
                cell.txtView.textColor = UIColor.lightGray
            }
            else{
                cell.txtView.textColor = UIColor.black
            }
            
            return cell
            
        }
        else if indexPath.section == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellTextView", for: indexPath) as! OptionTableCell
            cell.txtView.addshodowToTextView()
            cell.txtView.delegate = self
            if cell.txtView.text == "" || cell.txtView.text == "Type here..."{
                cell.txtView.text = "Type here..."
                cell.txtView.textColor = UIColor.lightGray
            }
            else{
                cell.txtView.textColor = UIColor.black
            }
            return cell
            
        }
        else if indexPath.section == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "celIImage", for: indexPath) as! OptionTableCell
            
            return cell
            
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath) as! OptionTableCell
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "headerCell") as! OptionTableCell
        cell.lblHeader.text = headersArray[section]
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }
        else{
            return 40
        }
    }
    
}


extension AirCompressorInspectionViewController : UITextViewDelegate {
    
    //MARK:- TextView Delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray
        {
            textView.text = ""
            textView.textColor = UIColor.black
            
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Type here..."
            textView.textColor = UIColor.lightGray
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        print(newText.count)
        return newText.count <= 300
    }
}

extension AirCompressorInspectionViewController : UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let img = info[UIImagePickerControllerOriginalImage] as! UIImage
        print(img)
        dismiss(animated:true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated:true, completion: nil)
    }
}
