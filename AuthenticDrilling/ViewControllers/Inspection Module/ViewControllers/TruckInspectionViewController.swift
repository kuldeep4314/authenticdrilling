//
//  TruckInspectionViewController.swift
//  AuthenticDrilling
//
//  Created by goyal on 04/10/18.
//  Copyright © 2018 Avatar Singh. All rights reserved.
//

import UIKit

class TruckInspectionViewController: ViewController {

    let headersArray = ["","Driver/Passenger Side","REAR VEHICLE","FRONT OF VEHICLE","UNDER THE HOOD","INSIDE TRUCK","Comments","Parts to be ordered",""]
    
    let subArray = ["" : [""],"Driver/Passenger Side" : ["External Side Mirrors (Right and Left)","Tires","Windows"],"REAR VEHICLE" : ["Bumper","License Plate / Insurance / Registration","Truck Bed or Flat Bed","Brake Lights","Rear Window"],"FRONT OF VEHICLE" : ["Headlights (normal and bright)","Turn Signals","Bumper","Windshield","Windshield Wiper Blades"],"UNDER THE HOOD":["Battery","Air Filter","Leaks","Windshield Fluid","Transmission Fluid","Engine Coolant","Engine Oil"],"INSIDE TRUCK" : ["Interior","Registration & Insurance","Parking Brake","Horn","Seat Belts","Gauges","Error Codes","First Aid Kit","Fire Extinguisher","Rearview Mirror"],"Comments": [""],"Parts to be ordered" : [""], "image" : [""]]
    
    var priorityArray = ["" : [0],"Driver/Passenger Side" : [0,0,0],"REAR VEHICLE" : [0,0,0,0,0],"FRONT OF VEHICLE" : [0,0,0,0,0],"UNDER THE HOOD":[0,0,0,0,0,0,0],"INSIDE TRUCK" : [0,0,0,0,0,0,0,0,0,0],"Comments": [0],"Parts to be ordered" : [0],"image" : [0]]
    
    
   
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: 5))
        // Do any additional setup after loading the view.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        
    }
    @IBAction func btnPriorityTapped(_ sender: UIButton) {
        let points = sender.convert(CGPoint.zero, to: self.tableView)
        
        guard let indexPath = self.tableView.indexPathForRow(at: points) else{
            
            return
        }
        print(indexPath)
        
    
        self.tableView.reloadData()
    }
    
    @objc  func dateChange(sender: UIDatePicker){
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let cell = self.tableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! OptionTableCell
        cell.txtDate.text =  formatter.string(from: sender.date)
        
    }
}
//MARK:- TableView Cells
extension TruckInspectionViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return headersArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return JSON(subArray[headersArray[section]]!).arrayValue.count
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath) as! OptionTableCell
            
            let datePicker = UIDatePicker()
            datePicker.addTarget(self, action: #selector(self.dateChange(sender:)), for: .valueChanged)
            datePicker.datePickerMode = .date
            
            cell.txtDate.inputView = datePicker
            
            cell.txtDate.leftview(image: #imageLiteral(resourceName: "imgStartDate"))
            cell.txtInspectedBy.leftview(image: #imageLiteral(resourceName: "imgInspection1"))
            cell.txtTruck.leftview(image: #imageLiteral(resourceName: "imgIntermidate"))
             cell.txtOdometer.leftview(image: #imageLiteral(resourceName: "imgMeter"))
            return cell
            
        }
       else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell", for: indexPath) as! OptionTableCell
          cell.txtType.layer.borderColor = appColor.cgColor
       
            
            
                cell.txtType.addshodowToTextField()
            
              cell.txtSelectOption.addshodowToTextField()
            cell.txtSelectOption.rightview(Img:#imageLiteral(resourceName: "imgOrageDropDown"))
             cell.lblTitle.text =  JSON(subArray[headersArray[indexPath.section]]!)[indexPath.row].stringValue
            return cell
            
        }
        else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell", for: indexPath) as! OptionTableCell
            cell.txtType.addshodowToTextField()
                cell.txtSelectOption.rightview(Img:#imageLiteral(resourceName: "imgOrageDropDown"))
            cell.txtSelectOption.addshodowToTextField()
            cell.lblTitle.text =  JSON(subArray[headersArray[indexPath.section]]!)[indexPath.row].stringValue
            return cell
            
        }
        else if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell", for: indexPath) as! OptionTableCell
            cell.txtType.addshodowToTextField()
               cell.txtSelectOption.rightview(Img:#imageLiteral(resourceName: "imgOrageDropDown"))
            cell.txtSelectOption.addshodowToTextField()
            cell.lblTitle.text =  JSON(subArray[headersArray[indexPath.section]]!)[indexPath.row].stringValue
            return cell
            
        }
        else if indexPath.section == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell", for: indexPath) as! OptionTableCell
            cell.txtType.addshodowToTextField()
                cell.txtSelectOption.rightview(Img:#imageLiteral(resourceName: "imgOrageDropDown"))
            cell.txtSelectOption.addshodowToTextField()
            cell.lblTitle.text =  JSON(subArray[headersArray[indexPath.section]]!)[indexPath.row].stringValue
            return cell
            
        }
        else if indexPath.section == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell", for: indexPath) as! OptionTableCell
            cell.txtType.addshodowToTextField()
              cell.txtSelectOption.rightview(Img: #imageLiteral(resourceName: "imgDropDown"))
            cell.txtSelectOption.addshodowToTextField()
            cell.lblTitle.text =  JSON(subArray[headersArray[indexPath.section]]!)[indexPath.row].stringValue
            return cell
            
        }
            
        
        else if indexPath.section == 6 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellTextView", for: indexPath) as! OptionTableCell
            cell.txtView.addshodowToTextView()
            cell.txtView.delegate = self
            if cell.txtView.text == "" || cell.txtView.text == "Type here..." {
                cell.txtView.text = "Type here..."
                cell.txtView.textColor = UIColor.lightGray
            }
            else{
                cell.txtView.textColor = UIColor.black
            }
            
            return cell
            
        }
        else if indexPath.section == 7 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellTextView", for: indexPath) as! OptionTableCell
               cell.txtView.addshodowToTextView()
             cell.txtView.delegate = self
            if cell.txtView.text == "" || cell.txtView.text == "Type here..."{
                cell.txtView.text = "Type here..."
                cell.txtView.textColor = UIColor.lightGray
            }
            else{
                cell.txtView.textColor = UIColor.black
            }
            return cell
            
        }
        else if indexPath.section == 8 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "celIImage", for: indexPath) as! OptionTableCell
           
            return cell
            
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath) as! OptionTableCell
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "headerCell") as! OptionTableCell
        cell.lblHeader.text = headersArray[section]
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }
        else{
            return 40
        }
    }
   
}


extension TruckInspectionViewController : UITextViewDelegate {
    
    //MARK:- TextView Delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray
        {
            textView.text = ""
            textView.textColor = UIColor.black
            
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Type here..."
            textView.textColor = UIColor.lightGray
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        print(newText.count)
        return newText.count <= 300
    }
}
