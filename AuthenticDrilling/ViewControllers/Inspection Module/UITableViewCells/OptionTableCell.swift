//
//  OptionTableCell.swift
//  AuthenticDrilling
//
//  Created by goyal on 04/10/18.
//  Copyright © 2018 Avatar Singh. All rights reserved.
//

import UIKit

@objc  protocol optionSelectedDelegates{
    @objc optional func selecledIndexWithValue(index : Int)
    
}
class OptionTableCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var btnOption3: UIButton!
    @IBOutlet weak var btnOption2: UIButton!
    @IBOutlet weak var btnOption1: UIButton!
    @IBOutlet weak var btnPlusMinus: UIButton!
    @IBOutlet weak var txtType: UITextField!
    @IBOutlet weak var txtSelectOption: UITextField!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtOdometer: UITextField!
    @IBOutlet weak var txtTruck: UITextField!
    @IBOutlet weak var txtInspectedBy: UITextField!
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var btnHigh: UIButton!
    @IBOutlet weak var btnMedium: UIButton!
    @IBOutlet weak var btnLow: UIButton!
    @IBOutlet weak var switchStatus: UISwitch!
    @IBOutlet weak var txtField: UITextField!
    var delegates : optionSelectedDelegates?
    var indexs = 0
    let arrNames = ["Drill Rig","Truck", "Trailer" ,"Air Compressor"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if txtOdometer != nil {
            txtOdometer.addshodowToTextField()
            txtDate.addshodowToTextField()
            txtTruck.addshodowToTextField()
            txtInspectedBy.addshodowToTextField()
        }
        if collectionView != nil {
            collectionView.dataSource = self
            collectionView.delegate = self
        }
        if txtField != nil {
            txtField.addshodowToTextField()
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension OptionTableCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cvCell", for: indexPath)
        
        let lbl = cell.viewWithTag(1) as! UILabel
        lbl.text = self.arrNames[indexPath.row]
        if indexs == indexPath.row {
            
            lbl.textColor = appColor
        }
        else {
            lbl.textColor = UIColor.black
            
        }
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        indexs = indexPath.row
       collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        self.collectionView.reloadData()
        delegates?.selecledIndexWithValue!(index: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
   
              return CGSize(width:100, height: 44)
       
        
      
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }

}
