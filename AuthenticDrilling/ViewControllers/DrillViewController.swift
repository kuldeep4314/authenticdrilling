 //
 //  DrillViewController.swift
 //  AuthenticDrilling
 //
 //  Created by Avatar Singh on 2017-11-29.
 //  Copyright © 2017 Avatar Singh. All rights reserved.
 //
 
 import UIKit
 import CoreData
 class DrillViewController: ViewController ,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate , UITextViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource{
    let arrayName = ["Start Time","Stop Time" , "Bit Size" ," Method" ,"New","OFFSET","REAM","CAVE" ,"Depth From","Depth To","Hole Number","","Total Footage"]
    let arrayImagesLeft = [ #imageLiteral(resourceName: "imgTimer"),#imageLiteral(resourceName: "imgTimer"),#imageLiteral(resourceName: "imgBitSize"),#imageLiteral(resourceName: "imgMethods")]
    let arrayImagesRight = [#imageLiteral(resourceName: "imgWatch"),#imageLiteral(resourceName: "imgWatch"),#imageLiteral(resourceName: "imgDropDown"),#imageLiteral(resourceName: "imgDropDown")]
    var arraySampleData = [[String : Any]]()
    var isEdit = Bool()
     var sessions: [NSManagedObject] = []
    let arrayKeys = ["start_time","stop_time","bitsize","method","new" ,"offset","ream","cave","depth_from","depth_to","hole_number","","total_footage"]
    
    var strDate = String()
    var projectInfo :ProjectData?
    
    var mainDict =  [String: Any]()
    var drillDropDownList = [JSON]()
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        if  let decoded  = standard.value(forKey: "catData") as? [String : Any]{
            print(decoded)
            drillDropDownList  = JSON(decoded)["Drill"].arrayValue
            print(drillDropDownList)
        }
        
        
        if Network.isConnectedToNetwork() == true {
            self.getOfflineData()
        }
        else{
            self.getCoreData()
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func minusSampleTapped(sender: UIButton){
        self.arraySampleData.remove(at: sender.tag)
        self.tableView.reloadData()
    }
    
    @objc func plusSampleTapped(sender: UIButton){
        
        let array = ["number" : "","type" : ""]
        self.arraySampleData.append(array)
        self.tableView.reloadData()
    }
    @objc  func dateChange(sender: UIDatePicker){
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        
        let cell = tableView.cellForRow(at: IndexPath.init(row: 0, section: sender.tag)) as! DrillCell
        cell.txtValue.text = formatter.string(from: sender.date)
        
    }
    
    func getOfflineData(){
        
        let offlineDetails = ProjectsOffline.sharedInstance.getOfflineDrillTablesData(tables: "Drill")
        print(offlineDetails)
        if offlineDetails.count > 0 {
            let parameters : Parameters = ["action": "drill_category" ,"data" :  offlineDetails ]
            
            self.syncOfflineMultipleApi(parameter: parameters)
            
        }
        else{
            self.fetchDetails()
        }
        
    }
    
    //MARK: - SyncOfflineMultipleApi
    func syncOfflineMultipleApi(parameter :Parameters){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(projectUrlOffline, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            ProjectsOffline.sharedInstance.updateCoredataTables(tables: "Drill")
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
              self.getCoreData()
            print(error.localizedDescription)
        }
    }
    
    
    
    
    
    
    
    
    
    @objc  func btnSaveTapped(sender:UIButton){
      
        if Network.isConnectedToNetwork() == true {
            
            self.drillApiCall(isOffline: false, edit: false)
        }
        else  if Network.isConnectedToNetwork() == false {
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "Drill", strDate: self.strDate, projectID: projectId) == true
            {
                self.updateCoredata(isOffline: true, edit: true, isShowAlert: true)
            }
                
            else{
                ProjectsOffline.sharedInstance.getOfflineDates(isOffline: true, strDate: self.strDate, projectID: projectId)
                self.saveCoredata(isOffline: true, edit: false, msg: "drill list added successfully", isShow: true)
            }
        }
    }
    func betweenTimeFunction(textField : UITextField ,start :String ,stop :String){
        
        let arrayStartTime = start.components(separatedBy: ":")
        let arrayStopTime = stop.components(separatedBy: ":")
         self.mainDict.updateValue(""  , forKey: arrayKeys[textField.tag])
        if textField.tag == 1 {
            
            
            if JSON(arrayStartTime[0]).intValue > JSON(arrayStopTime[0]).intValue {
                if JSON(arrayStartTime[0]).intValue >= 12 && JSON(arrayStopTime[0]).intValue < 12{
                    appDelegates.window?.rootViewController?.view.makeToast("Please select stop time between 12-24.", duration: 2, position: .center)
                }
                else{
                    appDelegates.window?.rootViewController?.view.makeToast("Start time should be less than Stop time.", duration: 2, position: .center)
                }
                
            }
            else{
                appDelegates.window?.rootViewController?.view.makeToast("Start time should be less than Stop time.", duration: 2, position: .center)
            }
            
        }
        else{
            if JSON(arrayStartTime[0]).intValue > JSON(arrayStopTime[0]).intValue {
                if JSON(arrayStartTime[0]).intValue >= 12 && JSON(arrayStopTime[0]).intValue < 12{
                    appDelegates.window?.rootViewController?.view.makeToast("Please select stop time between 12-24.", duration: 2, position: .center)
                }
                else{
                    appDelegates.window?.rootViewController?.view.makeToast("Stop time should be greater than Start time.", duration: 2, position: .center)
                }
            }
            else{
                appDelegates.window?.rootViewController?.view.makeToast("Stop time should be greater than Start time.", duration: 2, position: .center)
            }
            
            
        }
        
    }
    
    
    
    //MARK:- UITextField Delegates
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.tag > 19 {
            let hint = textField.accessibilityHint
            
            if hint!.count > 6 {
                print("number")
                
                
                let index = Int(hint!.chopPrefix(6))
                var dict  = arraySampleData[index ?? 0]
                
                dict["number"] = textField.text
                
                arraySampleData[index ?? 0] = dict
            }
            else{
                print("type")
                let index = Int(hint!.chopPrefix(4))
                var dict = arraySampleData[index ?? 0]
                dict["type"] = textField.text
                arraySampleData[index ?? 0] = dict
                print(arraySampleData)
            }
            
            print(arraySampleData)
            
            self.tableView.reloadData()
        }
            
            
        else{
            print(textField.tag)
          
            mainDict.updateValue(textField.text ?? "", forKey:arrayKeys[textField.tag])
            
            
            if textField.tag == 0 || textField.tag == 1 {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                
                let dateformatter = DateFormatter()
                dateformatter.dateFormat = "yyyy-MM-dd HH:mm"
                
                if   JSON(self.mainDict)["start_time"].stringValue != "" &&  JSON(self.mainDict)["stop_time"].stringValue != "" {
                    let dateCurrent = formatter.string(from: Date())
                    
                    let firstDate = dateformatter.date(from: "\(dateCurrent) \(JSON(self.mainDict)["start_time"].stringValue)")
                    let secondDate = dateformatter.date(from: "\(dateCurrent) \(JSON(self.mainDict)["stop_time"].stringValue )")
                    
                    
                    let check = firstDate?.timeIntervalSince(secondDate!)
                    
                    if  Int(check!) < 0 {
                        self.mainDict.updateValue(textField.text ?? ""  , forKey: arrayKeys[textField.tag])
                        
                    }
                    else{
                        

                        
                        self.betweenTimeFunction(textField: textField, start:  JSON(self.mainDict)["start_time"].stringValue, stop: JSON(self.mainDict)["stop_time"].stringValue)
                        
                        
                    }
                    
                }
                
               
            }
            
            
            
            if textField.tag == 8 || textField.tag == 9 {
            
          if JSON(self.mainDict)["depth_from"].stringValue != "" &&
            
                JSON(self.mainDict)["depth_to"].stringValue != ""{
                
                let value = JSON(self.mainDict)["depth_to"].intValue - JSON(self.mainDict)["depth_from"].intValue
                
                print(value)
            
            if value > 0 {
                 mainDict.updateValue("\(value)", forKey:"total_footage")
            }
            else{
                
                mainDict.updateValue("", forKey:"total_footage")
                
                mainDict.updateValue("", forKey:arrayKeys[textField.tag])
                
                if textField.tag == 8 {
                    appDelegates.window?.rootViewController?.view.makeToast("Depth from should be less than depth to.", duration: 2, position: .center)
                }
                else{
                    appDelegates.window?.rootViewController?.view.makeToast("Depth to should be greater than depth from.", duration: 2, position: .center)
                }
            }
            }
        
        }
        }
        
        self.tableView.reloadData()
    }

    //MARK:- Update Core Data
    func updateCoredata(isOffline : Bool , edit : Bool , isShowAlert : Bool){
        
        let managedContext = CoreDataStack.managedObjectContext
        let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Drill")
        updateFetch.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , strDate)
        do {
            let results = try managedContext.fetch(updateFetch) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                
                // In my case, I only updated the first item in results
                let session = results![0]
                let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
                let dataSample: Data = NSKeyedArchiver.archivedData(withRootObject: self.arraySampleData)
                
                session.setValue(dataSample, forKey: "sample")
                session.setValue(data1, forKey: "dataDetails")
                session.setValue(strDate, forKey: "date")
                session.setValue(projectId, forKey: "projectid")
                session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                session.setValue(isOffline, forKey: "isOffline")
                session.setValue(edit, forKey: "edit")
                
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try managedContext.save()
            
            
            if isShowAlert == true {
                self.navigationController?.popViewController(animated: true)
                appDelegates.window?.rootViewController?.showAlert(messageStr:  "drill list updated successfully")
            }
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
        
    }
    
    //MARK:- Save Core Data Function
    func saveCoredata(isOffline : Bool,edit: Bool , msg : String , isShow : Bool){
        print(isOffline)
        print(edit)
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "Drill",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
        
        let dataSample: Data = NSKeyedArchiver.archivedData(withRootObject: self.arraySampleData)
        
        session.setValue(dataSample, forKey: "sample")
        
        session.setValue(data1, forKey: "dataDetails")
        
        session.setValue(strDate, forKey: "date")
        session.setValue(projectId, forKey: "projectid")
        session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
        session.setValue(isOffline, forKey: "isOffline")
        session.setValue(edit, forKey: "edit")
        
        
        CoreDataStack.saveContext()
        
        
        if isShow == true{
            self.navigationController?.popViewController(animated: true)
            appDelegates.window?.rootViewController?.showAlert(messageStr: msg)
        }
        
    }
    //MARK:- Get Core Data Function
    func getCoreData() {
        
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Drill")
        
        fetchRequest.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , strDate)
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            for session in sessions{
                var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                
              arraySampleData = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "sample") as! Data) as? [[String : Any]] ?? [[String : Any]]()
                
                dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                
                dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                
                self.mainDict = dictionary
                
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    //MARK:- TextView Delegates
    func textViewDidChange(_ textView: UITextView) {
        
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        
        if textView.textColor == UIColor.lightGray
        {
            textView.text = ""
            textView.textColor = UIColor.black
            
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = "Enter your notes"
            textView.textColor = UIColor.lightGray
            mainDict.updateValue(
                "", forKey:"notes")
        }
        else{
            mainDict.updateValue( textView.text ?? "", forKey:"notes")
        }
        //  self.tableView.reloadData()
    }
    
    
    
    //MARK:- TableView Data Source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 14
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  section == 11{
            return self.arraySampleData.count
        }
        return 1
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.section < 2 {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell11", for: indexPath) as! DrillCell
            cell.txtTilte.text = arrayName[indexPath.section]
            
            let picker = UIDatePicker()
            picker.addTarget(self, action: #selector(self.dateChange(sender:)), for: .valueChanged)
            picker.datePickerMode = .time
            picker.locale = Locale(identifier: "en_GB")
            picker.tag = indexPath.section
            cell.txtValue.inputView = picker
            cell.txtValue.placeholder = "00:00"
            
            
            cell.txtValue.isUserInteractionEnabled = true
            cell.txtTilte.leftview(image: arrayImagesLeft[indexPath.section])
            cell.txtValue.rightview(Img: arrayImagesRight[indexPath.section])
            
            cell.txtValue.text = JSON(mainDict)[arrayKeys[indexPath.section]].stringValue
            cell.txtValue.tag = indexPath.section
            cell.txtValue.delegate = self
            return cell
        }
            
        else if indexPath.section > 1 && indexPath.section < 4{
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell12", for: indexPath) as! DrillCell
            cell.txtOptionTitle.text = arrayName[indexPath.section]
            
            let picker = UIPickerView()
            picker.dataSource = self
            picker.delegate = self
            picker.tag = indexPath.section
            cell.txtOptionValue.inputView = picker
            cell.txtOptionValue.placeholder = "#"
            
            
            
            cell.txtOptionTitle.leftview(image: arrayImagesLeft[indexPath.section])
            cell.txtOptionValue.rightview(Img: arrayImagesRight[indexPath.section])
            
            cell.txtOptionValue.text = JSON(mainDict)[arrayKeys[indexPath.section]].stringValue
            cell.txtOptionValue.tag = indexPath.section
            cell.txtOptionValue.delegate = self
            return cell
        }
            
        else  if indexPath.section > 3 &&  indexPath.section < 8 {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DrillCell
            cell.txtName.text = arrayName[indexPath.section]
            cell.txtName.isUserInteractionEnabled = false
            cell.txtName.leftview()
            if JSON(self.mainDict)[arrayKeys[indexPath.section]].stringValue == "1"{
                cell.txtName.rightview(Img: #imageLiteral(resourceName: "imgCheck"))
            }
            else{
                cell.txtName.rightview(Img:#imageLiteral(resourceName: "imgUnselect"))
            }
            
            
            
            return cell
        }
        else  if  indexPath.section == 8 || indexPath.section == 9  || indexPath.section == 10 || indexPath.section == 12{
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell11", for: indexPath) as! DrillCell
            cell.txtTilte.text = arrayName[indexPath.section]
            cell.txtValue.tag = indexPath.section
            cell.txtValue.text = JSON(mainDict)[arrayKeys[indexPath.section]].stringValue
            
            if indexPath.section == 10 {
                cell.txtTilte.leftview(image: #imageLiteral(resourceName: "imgHole"))
                cell.txtValue.isUserInteractionEnabled = true
            }
            else if indexPath.section == 12 {
                cell.txtTilte.leftview()
                cell.txtValue.isUserInteractionEnabled = false
                
            }
            else {
                cell.txtTilte.leftview(image: #imageLiteral(resourceName: "imgDepth"))
                  cell.txtValue.isUserInteractionEnabled = true
            }
            cell.txtValue.placeholder = "0"
            cell.txtValue.keyboardType = .numberPad
            cell.txtValue.rightVC()
            cell.txtValue.inputView = nil
            cell.txtValue.delegate = self
            return cell
        }
            
        else  if indexPath.section == 11 {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as! DrillCell
            cell.txtNumber.text = "Number"
            cell.txtType.text = "Type"
            cell.txtType.leftview(image: #imageLiteral(resourceName: "imgCalculator"))
            cell.txtNumber.leftview(image: #imageLiteral(resourceName: "imgType"))
            
            
            cell.txtType.addshodowToTextField()
            cell.txtType.addshodowToTextField()
            
            
            
            
            cell.txtNumberValue.text = JSON(arraySampleData[indexPath.row])["number"].stringValue
            cell.txtTypeValue.text = JSON(arraySampleData[indexPath.row])["type"].stringValue
            cell.txtNumberValue.tag = 20 + 2 * indexPath.row
            cell.txtTypeValue.tag =  21 + 2 * indexPath.row
            
            cell.txtNumberValue.accessibilityHint = "number\(indexPath.row)"
            cell.txtTypeValue.accessibilityHint =  "type\(indexPath.row)"
            
            cell.txtNumberValue.placeholder = "number"
            cell.txtTypeValue.placeholder =  "type"
            cell.txtNumberValue.rightVC()
            cell.txtTypeValue.rightVC()
            cell.txtNumberValue.delegate = self
            cell.txtTypeValue.delegate = self
            cell.btnMinus.tag = indexPath.row
            cell.btnMinus.addTarget(self, action: #selector(self.minusSampleTapped(sender:)), for: .touchUpInside)
            
            return cell
        }
            
            
        else {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! DrillCell
            cell.txtView.text = JSON(self.mainDict)["notes"].stringValue.base64Decoded()
            
            cell.txtView.addshodowToTextView()
            
            
            if  cell.txtView.text == "" {
                cell.txtView.text = "Enter your notes"
                cell.txtView.textColor = UIColor.lightGray
            }
            else{
                cell.txtView.textColor = UIColor.black
            }
            cell.btnSave.addTarget(self, action: #selector(self.btnSaveTapped(sender:)), for: .touchUpInside)
            cell.txtView.delegate = self
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 11{
            return 120 
        }
        else if indexPath.section == 13{
            return 220
        }
        return 44
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! DrillCell
        if section == 11 {
            cell.btnAdd.isHidden = false
            cell.lblHeader.isHidden = false
            cell.lblHeader.text =  "SAMPLES DATA"
            cell.btnAdd.addTarget(self, action: #selector(self.plusSampleTapped(sender:)), for: .touchUpInside)
        }
        else{
            cell.btnAdd.isHidden = true
            cell.lblHeader.isHidden = true
        }
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 11 {
            return 55
        }
        return 20
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section > 3 &&  indexPath.section < 8{
            
            if JSON(self.mainDict)[arrayKeys[indexPath.section]].stringValue == "1"{
                self.mainDict.updateValue("0", forKey: arrayKeys[indexPath.section])
                
            }
            else{
                self.mainDict.updateValue("1", forKey: arrayKeys[indexPath.section])
            }
            
            self.tableView.reloadData()
        }
    }
    //MARK:- PickerView Data Source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 2{
            return drillDropDownList[0]["subcategory"].arrayValue.count
        }
        else{
            return drillDropDownList[1]["subcategory"].arrayValue.count
            
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 2{
            return drillDropDownList[0]["subcategory"][row]["name"].stringValue
        }
        else{
            return drillDropDownList[1]["subcategory"][row]["name"].stringValue
            
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let cell = self.tableView.cellForRow(at: IndexPath.init(row: 0, section: pickerView.tag)) as! DrillCell
        
        if pickerView.tag == 2{
            cell.txtOptionValue.text = drillDropDownList[0]["subcategory"][row]["name"].stringValue
        }
        else{
            cell.txtOptionValue.text = drillDropDownList[1]["subcategory"][row]["name"].stringValue
            
        }
    }
    
    
    //MARK:- DrillApiCall
    func drillApiCall(isOffline : Bool , edit : Bool){
        
        let parameter  : Parameters  =  ["action": "drill_category", "userid": standard.value(forKey: "id")!, "projectid":  projectId,"date":strDate
            , "start_time": JSON(self.mainDict)["start_time"].stringValue
            , "stop_time": JSON(self.mainDict)["stop_time"].stringValue
            , "bitsize": JSON(self.mainDict)["bitsize"].stringValue
            , "method": JSON(self.mainDict)["method"].stringValue
            , "new": JSON(self.mainDict)["new"].stringValue
            , "offset": JSON(self.mainDict)["offset"].stringValue
            , "ream": JSON(self.mainDict)["ream"].stringValue
            
            , "cave": JSON(self.mainDict)["cave"].stringValue
            , "depth_from": JSON(self.mainDict)["depth_from"].stringValue
            
            , "depth_to": JSON(self.mainDict)["depth_to"].stringValue
            , "hole_number": JSON(self.mainDict)["hole_number"].stringValue
            , "total_footage": JSON(self.mainDict)["total_footage"].stringValue,
              "notes": JSON(self.mainDict)["notes"].stringValue.base64Decoded() ?? "",
              "sample_data": arraySampleData
        ]
        
        
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  json["response"]["msg"].stringValue )
                return
            }
            
            DispatchQueue.main.async {
                 ProjectsOffline.sharedInstance.getOfflineDates(isOffline: isOffline, strDate: self.strDate, projectID: projectId)
                    if ProjectsOffline.sharedInstance.getOfflineTables(tables: "Drill", strDate: self.strDate, projectID: projectId) == true {
                        self.updateCoredata(isOffline: false, edit: false, isShowAlert: true)
                    }
                    else{
                        self.saveCoredata(isOffline: isOffline, edit: edit, msg: json["response"]["msg"].stringValue, isShow: true)
                        
                    }
                
            }
          
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  error.localizedDescription)
            }
            
            print(error.localizedDescription)
        }
        
    }
    //MARK:- fetchDetails function
    func  fetchDetails(){
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid": projectId,"table":"drill","date": strDate]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            self.mainDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
            let array =  json["response"]["data"]["sample_data"].arrayValue
            for i in 0..<array.count{
                
                let dict =  array[i].dictionaryObject
                self.arraySampleData.append(dict ?? [String: Any]())
            }
            print(self.arraySampleData)
            
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "Drill", strDate: self.strDate, projectID: projectId) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false)
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false)
                
            }
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
 }
