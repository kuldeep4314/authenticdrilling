//
//  ProjectInfoViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-25.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit

class ProjectInfoViewController: ViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    let arrNames = ["Travel","Drill", "Hole Completion" ,"Site Activities" ,"Add Rentals" ,"Add Consumables","Miscellaneous","Billing & Rates","Inspection"]
     var projectInformation :ProjectData?
    let arrImagesNames = [#imageLiteral(resourceName: "imgTravel"),#imageLiteral(resourceName: "imgDrill"),  #imageLiteral(resourceName: "imgHoleCompletion") ,#imageLiteral(resourceName: "imgSiteActivity") ,#imageLiteral(resourceName: "imgAddRentals") ,#imageLiteral(resourceName: "imgAddConsumables"),#imageLiteral(resourceName: "imgMiscellaneous"),#imageLiteral(resourceName: "imgBillingRates"),#imageLiteral(resourceName: "imgInspection")]
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblProjectName: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
self.lblProjectName.text = projectInformation?.projectName
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- CollectionView Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrNames.count
    }
  
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
       
        cell.lblName.text = arrNames[indexPath.row]
        cell.imgView.image = arrImagesNames[indexPath.row]
        cell.bgView.addshodowToView()
        return cell
        
    }
    
    //MARK:- CollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        print(indexPath.row)
        if indexPath.row == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TravelViewController") as! TravelViewController
            vc.projectInfo = projectInformation
            vc.strDate = Comman.getCurrentDate()
            self.navigationController?.pushViewController(vc, animated: true)
        }
      else  if indexPath.row == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DrillViewController") as! DrillViewController
            vc.projectInfo = projectInformation
            vc.strDate = Comman.getCurrentDate()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 2{
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HoleCompletionViewController") as! HoleCompletionViewController
            vc.projectInfo = projectInformation
         vc.strDate = Comman.getCurrentDate()
        self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 3{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SiteActivitiesViewController") as! SiteActivitiesViewController
            vc.projectInfo = projectInformation
            vc.strDate = Comman.getCurrentDate()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 4{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddRentalViewController") as! AddRentalViewController
             vc.projectInfo = projectInformation
            vc.strDate = Comman.getCurrentDate()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 5{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddConsumablesViewController") as! AddConsumablesViewController
            vc.projectInformation = projectInformation
            vc.dateString = Comman.getCurrentDate()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 6{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MiscellaneousInfoViewController") as! MiscellaneousInfoViewController
            vc.projectInfo = projectInformation
            vc.strDate = Comman.getCurrentDate()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 7{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BillingAndRatesViewController") as! BillingAndRatesViewController
               vc.linkStr = "https://www.google.co.in"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 8{
            let story = UIStoryboard.init(name: "Inspection", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "InspectionViewController") as! InspectionViewController
        
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        
        return CGSize(width: collectionView.frame.size.width/2 - 0.5, height: collectionView.frame.size.width/2 - 0.5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }

}
