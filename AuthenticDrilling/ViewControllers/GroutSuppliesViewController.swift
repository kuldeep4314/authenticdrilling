//
//  GroutSuppliesViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-28.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit
import CoreData
class GroutSuppliesViewController: ViewController,UITableViewDelegate,UITableViewDataSource ,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
    var currentIndex = 0
    var strDate = String()
    var activityList = [JSON]()
    var subCatgoriesList =  [[String: Any]]()
    var projectInfo :ProjectData?
    var subCatgoriesIDs = [String]()
    var mainDict =  [String: Any]()
    var dictQtyUnit =  [[String: Any]]()
    let arrayKeys = ["sand_qty","sand_unit","allpurpose_qty","allpurpose_unit","mesh_qty" ,"mesh_unit","other_qty","other_unit"]
    
    var sessions: [NSManagedObject] = []
    var dropDownList = [JSON]()
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
        tableView.tableFooterView = UIView()
        
        if  let decoded  = standard.value(forKey: "catData") as? [String : Any]{
            print(decoded)
            activityList  = JSON(decoded)["Rentals sand"].arrayValue
            dropDownList  = JSON(decoded)["Grout Supply"].arrayValue
            print(dropDownList)
            
        }
        let  temp = self.activityList[currentIndex]["subcategory"].arrayValue
        
        self.subCatgoriesList.removeAll()
        for i in 0..<temp.count{
            
            self.subCatgoriesList.append(temp[i].dictionaryObject!)
        }
        print(self.subCatgoriesList)
        
        self.mainDict.updateValue(self.activityList[currentIndex]["name"].stringValue, forKey: "category_name")
        
        self.dictQtyUnit.removeAll()
        for i in 0..<self.subCatgoriesList.count{
            
            print(self.subCatgoriesList)
            let dict = ["id": JSON(subCatgoriesList[i])["subcategory_id"].stringValue,
                        "qty" : "",
                        "unit" : ""
            ]
            self.subCatgoriesIDs.append("")
            self.dictQtyUnit.append(dict)
            
        }
        self.mainDict.updateValue( self.dictQtyUnit, forKey: "subcat_name")
        self.mainDict.updateValue(self.subCatgoriesList, forKey: "sub_category")
        
        if Network.isConnectedToNetwork() == true {
            self.getOfflineData()
        }
        else{
            self.getCoreData()
        }
    }
    func setUpView(){
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == -111 {
            
            
            let  temp = self.activityList[currentIndex]["subcategory"].arrayValue
            
            self.subCatgoriesList.removeAll()
            for i in 0..<temp.count{
                
                self.subCatgoriesList.append(temp[i].dictionaryObject!)
            }
            
            
            
            self.mainDict.updateValue(self.subCatgoriesList, forKey: "sub_category")
            print(self.subCatgoriesList)
            
            self.dictQtyUnit.removeAll()
            self.subCatgoriesIDs.removeAll()
            for i in 0..<self.subCatgoriesList.count{
                
                print(self.subCatgoriesList)
                let dict = ["id": JSON(subCatgoriesList[i])["subcategory_id"].stringValue,
                            "qty" : "",
                            "unit" : ""
                ]
                self.subCatgoriesIDs.append("")
                self.dictQtyUnit.append(dict)
                
            }
            self.mainDict.updateValue( self.dictQtyUnit, forKey: "subcat_name")
            
        }
        else{
            let value =  JSON( self.dictQtyUnit[textField.tag - 1])["id"].stringValue
            let  arrayTemp  =    self.dictQtyUnit.filter{return $0["id"] as? String == value}
            var dict = arrayTemp[0]
            
            if textField.accessibilityHint == "qty"{
                dict.updateValue(textField.text ?? "", forKey: "qty")
            }
            else{
                dict.updateValue(textField.text ?? "", forKey: "unit")
            }
            self.dictQtyUnit[textField.tag - 1] = dict
            self.mainDict.updateValue( self.dictQtyUnit, forKey: "subcat_name")
            print(dict)
        }
        
        
        self.tableView.reloadData()
        
    }
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
        if sender.view?.tag != 0{
            
            if subCatgoriesIDs.contains(JSON(subCatgoriesList[sender.view!.tag - 1])["subcategory_id"].stringValue){
                
                self.subCatgoriesIDs[sender.view!.tag - 1] = ""
            }
                
            else{
                self.subCatgoriesIDs[sender.view!.tag - 1] = JSON(subCatgoriesList[sender.view!.tag - 1])["subcategory_id"].stringValue
                
            }
            print(self.subCatgoriesIDs)
            
            let strIDS = self.subCatgoriesIDs.joined(separator: ",")
            self.mainDict.updateValue(strIDS, forKey: "subcat")
            
        }
        
        self.tableView.reloadData()
        
    }
    
    func getOfflineData(){
        
        let offlineDetails = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "GroutSupply")
        print(offlineDetails)
        if offlineDetails.count > 0 {
            let parameters : Parameters = ["action": "ac_grout_supplies" ,"data" :  offlineDetails ]
            
            self.syncOfflineMultipleApi(parameter: parameters)
            
        }
        else{
            self.fetchDetails()
        }
        
    }
    
    
    
    //MARK: - SyncOfflineMultipleApi
    func syncOfflineMultipleApi(parameter :Parameters){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(consumableUrlOffline, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            ProjectsOffline.sharedInstance.updateCoredataTables(tables: "GroutSupply")
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            self.getCoreData()
            print(error.localizedDescription)
        }
    }
    
    
    
    @objc func btnSaveTapped(sender : UIButton){
        
        if Network.isConnectedToNetwork() == true {
            
            self.addGroutSupplyApiCall(isOffline: false, edit: false)
        }
        else  if Network.isConnectedToNetwork() == false {
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "GroutSupply", strDate: self.strDate, projectID: projectId) == true
            {
                self.updateCoredata(isOffline: true, edit: true, isShowAlert: true)
            }
                
            else{
                ProjectsOffline.sharedInstance.getOfflineDates(isOffline: true, strDate: self.strDate, projectID: projectId)
                self.saveCoredata(isOffline: true, edit: false, msg: "Add consumption Grout Supplies list added successfully", isShow: true)
            }
        }
        
        
    }
    
    //MARK:- Update Core Data
    func updateCoredata(isOffline : Bool , edit : Bool , isShowAlert : Bool){
        
        let managedContext = CoreDataStack.managedObjectContext
        let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "GroutSupply")
        updateFetch.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , strDate)
        do {
            let results = try managedContext.fetch(updateFetch) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                
                // In my case, I only updated the first item in results
                let session = results![0]
                
                let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
                session.setValue(data1, forKey: "dataDetails")
                
                session.setValue(strDate, forKey: "date")
                session.setValue(projectId, forKey: "projectid")
                session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                session.setValue(isOffline, forKey: "isOffline")
                session.setValue(edit, forKey: "edit")
                
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try managedContext.save()
            if isShowAlert == true {
                self.navigationController?.popViewController(animated: true)
                appDelegates.window?.rootViewController?.showAlert(messageStr:  "Add consumption Grout Supplies list updated successfully")
            }
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
        
    }
    
    
    //MARK:- Save Core Data Function
    func saveCoredata(isOffline : Bool,edit: Bool , msg : String , isShow : Bool){
        print(isOffline)
        print(edit)
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "GroutSupply",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        
        
        print(self.mainDict)
        
        let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict )
        
        
        session.setValue(data1, forKey: "dataDetails")
        
        session.setValue(strDate, forKey: "date")
        session.setValue(projectId, forKey: "projectid")
        session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
        session.setValue(isOffline, forKey: "isOffline")
        session.setValue(edit, forKey: "edit")
        
        
        CoreDataStack.saveContext()
        
        
        if isShow == true{
            self.navigationController?.popViewController(animated: true)
            appDelegates.window?.rootViewController?.showAlert(messageStr: msg)
        }
        
    }
    //MARK:- Get Core Data Function
    func getCoreData() {
        
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "GroutSupply")
        
        fetchRequest.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , strDate)
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            for session in sessions{
                var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                
                dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                
                dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                
                let  temp =   JSON(dictionary)["sub_category"].arrayValue
                self.subCatgoriesList.removeAll()
                for i in 0..<temp.count{
                    
                    self.subCatgoriesList.append(temp[i].dictionaryObject!)
                }
                print(self.subCatgoriesList)
                
                
                
                
                self.subCatgoriesIDs =  JSON(dictionary)["subcat"].stringValue.components(separatedBy: ",")
                
                self.dictQtyUnit =  JSON(dictionary)["subcat_name"].arrayObject as? [[String : Any]]  ?? [[String : Any]]()
                
                self.mainDict = dictionary
                
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    
    
    
    //MARK:- TableView Data Source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.subCatgoriesList.count + 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.subCatgoriesList.count + 1 == section  {
            return 0
        }
        else if  section == 0{
            return 1
        }
        else{
            
            if subCatgoriesIDs.contains(JSON(subCatgoriesList[section - 1 ])["subcategory_id"].stringValue) {
                
                return 1
            }
            else{
                return 0
            }
        }
        
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SupplyCell
            cell.txtName.leftview(image: #imageLiteral(resourceName: "imgSand"))
            cell.txtName.tag = -111
            cell.txtName.delegate = self
            cell.txtName.rightview(Img: #imageLiteral(resourceName: "imgDropDown"))
            let picker = UIPickerView()
            picker.dataSource = self
            picker.delegate = self
            cell.txtName.isUserInteractionEnabled = true
            cell
                .txtName.placeholder = "Select Categories"
            cell.txtName.text = JSON(self.mainDict)["category_name"].stringValue
            picker.tag = 1000
            cell.txtName.inputView = picker
            return cell
            
        }
        else{
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! SupplyCell
            cell.txtQtyTitle.leftview()
            cell.txtQty.rightVC()
            cell.txtQty.tag = indexPath.section
            cell.txtUnit.tag = indexPath.section
            cell.txtUnit.rightVC()
            cell.txtQty.accessibilityHint = "qty"
            cell.txtUnit.accessibilityHint = "unit"
            cell.txtUnit.rightview(Img: #imageLiteral(resourceName: "imgDropDown"))
            
            let valueQty = dictQtyUnit.filter({$0["id"] as! String == subCatgoriesIDs[indexPath.section - 1]}).map({$0["qty"]})
            let valueUnit = dictQtyUnit.filter({$0["id"] as! String == subCatgoriesIDs[indexPath.section - 1]}).map({$0["unit"]})
            
            if valueQty.count > 0{
                cell.txtQty.text = valueQty[0] as? String
            }
            if valueUnit.count > 0{
                cell.txtUnit.text = valueUnit[0] as? String
            }
            
            cell.txtUnit.delegate = self
            cell.txtQty.delegate  = self
            
            
            let picker = UIPickerView()
            picker.dataSource = self
            picker.delegate = self
            picker.tag = indexPath.section
            cell.txtUnit.inputView = picker
            
            
            cell.txtUnit.leftview()
            cell.txtQty.leftview()
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
        
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        if self.subCatgoriesList.count + 1 == section{
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! SupplyCell
            cell.btnSave.addTarget(self, action: #selector(self.btnSaveTapped(sender:)), for: .touchUpInside)
            return cell.contentView
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SupplyCell
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
            
            cell.bgView.addGestureRecognizer(tap)
            cell.bgView.tag = section
            cell.bgView.isUserInteractionEnabled = true
            
            cell.bgView.addGestureRecognizer(tap)
            if subCatgoriesIDs.contains(JSON(subCatgoriesList[section - 1])["subcategory_id"].stringValue){
                cell.txtName.rightview(Img: #imageLiteral(resourceName: "imgCheck"))
            }
            else{
                cell.txtName.rightview(Img: #imageLiteral(resourceName: "imgUnselect"))
            }
            cell.txtName.isUserInteractionEnabled = false
            
            
            cell.txtName.text = JSON(self.subCatgoriesList[section - 1])["name"].stringValue
            return cell.contentView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if self.subCatgoriesList.count + 1 == section{
            return 120
        }
        else if section == 0{
            return 0
        }
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK:- PickerView Data Source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        
        if pickerView.tag == 1000{
            return self.activityList.count
        }
        else{
            
            return dropDownList[0]["subcategory"].arrayValue.count
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        
        if pickerView.tag == 1000{
            return self.activityList[row]["name"].stringValue
        }
        else{
            
            return dropDownList[0]["subcategory"][row]["name"].stringValue
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
        if pickerView.tag == 1000{
            currentIndex = row
            let cell = tableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! SupplyCell
            cell.txtName.text = self.activityList[row]["name"].stringValue
            self.mainDict.updateValue(self.activityList[row]["name"].stringValue, forKey: "category_name")
            self.mainDict.updateValue(self.activityList[row]["main_category"].stringValue, forKey: "category")
            print(self.mainDict)
            
        }
        else{
            let cell = tableView.cellForRow(at: IndexPath.init(row: 0, section: pickerView.tag)) as! SupplyCell
            
            cell.txtUnit.text = dropDownList[0]["subcategory"][row]["name"].stringValue
        }
        
    }
    
    //MARK:- AddGroutSupplyApiCall
    func addGroutSupplyApiCall(isOffline : Bool , edit : Bool ){
        let parameter  : Parameters  =  ["action": "ac_grout_supplies", "userid": standard.value(forKey: "id")!, "projectid":  projectId ,"date":strDate, "category": JSON(self.mainDict)["category"].stringValue, "category_name": JSON(self.mainDict)["category_name"].stringValue, "subcat": JSON(self.mainDict)["subcat"].stringValue, "subcat_name": dictQtyUnit , "sub_category": JSON(self.mainDict)["sub_category"].arrayObject ?? [String: Any]()]
        
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(consumableUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  json["response"]["msg"].stringValue )
                return
            }
            
            
            DispatchQueue.main.async {
                
                
                
                ProjectsOffline.sharedInstance.getOfflineDates(isOffline: isOffline, strDate: self.strDate, projectID: projectId)
                
                
                if ProjectsOffline.sharedInstance.getOfflineTables(tables: "GroutSupply", strDate: self.strDate, projectID: projectId) == true {
                    self.updateCoredata(isOffline: false, edit: false, isShowAlert: true)
                }
                else{
                    self.saveCoredata(isOffline: isOffline, edit: edit, msg: json["response"]["msg"].stringValue, isShow: true)
                }
            }
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  error.localizedDescription)
            }
            
            print(error.localizedDescription)
        }
        
    }
    //MARK:- fetchDetails function
    func  fetchDetails(){
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid": projectId,"table":"ac_grout_supplies","date": strDate]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            self.mainDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
            
            let  temp =   json["response"]["data"]["sub_category"].arrayValue
            self.subCatgoriesList.removeAll()
            for i in 0..<temp.count{
                
                self.subCatgoriesList.append(temp[i].dictionaryObject!)
            }
            print(self.subCatgoriesList)
            
            self.subCatgoriesIDs = json["response"]["data"]["subcat"].stringValue.components(separatedBy: ",")
            
            self.dictQtyUnit = json["response"]["data"]["subcat_name"].arrayObject as? [[String : Any]]  ?? [[String : Any]]()
            
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "GroutSupply", strDate: self.strDate, projectID: projectId) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false)
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false)
                
            }
            self.getCoreData()
            
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    
}


