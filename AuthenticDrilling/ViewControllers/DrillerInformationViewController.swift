//
//  DrillerInformationViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-24.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit

class DrillerInformationViewController: ViewController  ,UITextFieldDelegate ,UIScrollViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
let imgArr = [#imageLiteral(resourceName: "imgDriller"),#imageLiteral(resourceName: "imgRig"),#imageLiteral(resourceName: "imgTruck"),#imageLiteral(resourceName: "imgTrailer"),#imageLiteral(resourceName: "imgTruck"),#imageLiteral(resourceName: "imgTrailer"),#imageLiteral(resourceName: "imgDrillerName"),#imageLiteral(resourceName: "imgDrillerName")]
    let helpersArray = ["Helper 1","Helper 2"]
   
    var isEdit = Bool()
    
    var arrayHelper = [String]()
    
     var arraySupport = [String]()
     var arrayRig = [String]()
     var arrayTrailer = [String]()
    
    @IBOutlet weak var imgTrailer: UIImageView!
    @IBOutlet weak var imgSupport: UIImageView!
    @IBOutlet weak var txtSupportLeft: AlignedPlaceholderTextField!
  
    @IBOutlet weak var top1: NSLayoutConstraint!
    @IBOutlet weak var txtTrailerLeft: AlignedPlaceholderTextField!
    
    @IBOutlet weak var trailerHeight2: NSLayoutConstraint!
    @IBOutlet weak var supportTruckHeight: NSLayoutConstraint!
    
    @IBOutlet weak var txtOtherHelper2: UITextField!
    @IBOutlet weak var txtOtherHelper1: UITextField!
    @IBOutlet weak var heightHelper2: NSLayoutConstraint!
    @IBOutlet weak var heightHelper1: NSLayoutConstraint!
    
    @IBOutlet weak var trailerTop: NSLayoutConstraint!
    @IBOutlet weak var topHelper1: NSLayoutConstraint!
  
    var mainDict =  [String: Any]()
     var keys = ["crew_driller_name","rig","support_truk1","trailer1","support_truck2","trailer2","crew_helpers","crew_helpers2","perdiems","injuries"]
    
     var keysCheck = ["crew_driller_name","rig","support_truk1","crew_helpers"]

    @IBOutlet weak var topHelper2: NSLayoutConstraint!
    var CurrentVCValue = Int()
    @IBOutlet weak var txtDrillerName: UITextField!
    
    @IBOutlet weak var txtRig: UITextField!
    
    @IBOutlet weak var txtSupportTruck1: UITextField!
    
    @IBOutlet weak var txtHelper1: UITextField!
     @IBOutlet weak var txtHelper2: UITextField!
    
    @IBOutlet weak var txtTrailer1: UITextField!
    @IBOutlet weak var txtSupportTruck2: UITextField!
    @IBOutlet weak var txtTrailer2: UITextField!
    @IBOutlet weak var txtPerDiem: AlignedPlaceholderTextField!
    @IBOutlet weak var txtInjuries: AlignedPlaceholderTextField!
    
    @IBOutlet weak var drillScrollView: UIScrollView!
    
     override func viewDidLoad() {
        super.viewDidLoad()
        self.topHelper1.constant = 0
        self.topHelper2.constant = 0
        self.heightHelper1.constant = 0
        self.txtOtherHelper1.isHidden = true
        self.heightHelper2.constant = 0
        self.txtOtherHelper2.isHidden = true
        self.imgSupport.isHidden = true
          self.imgTrailer.isHidden = true
        self.supportTruckHeight.constant = 0
         self.top1.constant = 0
        txtSupportLeft.isHidden = true
        txtSupportTruck2.isUserInteractionEnabled = false
        txtTrailer2.isUserInteractionEnabled = false
        self.trailerHeight2.constant = 0
        self.trailerTop.constant = 0
        txtTrailerLeft.isHidden = true
       
        self.setUpView()
         arrayRig =  standard.value(forKey: "rig") as? [String] ?? [String]()
        
          arraySupport =  standard.value(forKey: "support") as? [String] ?? [String]()
        
          arrayTrailer =  standard.value(forKey: "trailer") as? [String] ?? [String]()
        
        arrayHelper =  standard.value(forKey: "helpers") as? [String] ?? [String]()
        arrayHelper.append("Others")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnSupportTapped(_ sender: UIButton) {
        if sender.imageView?.image != #imageLiteral(resourceName: "ImgAddNumber") {
            self.supportTruckHeight.constant = 0
            self.top1.constant = 0
              txtSupportLeft.isHidden = true
            self.imgSupport.isHidden = true
            txtSupportTruck2.isUserInteractionEnabled = false
            sender.setImage(#imageLiteral(resourceName: "ImgAddNumber"), for: .normal)
        }
        else{
            self.supportTruckHeight.constant = 44
            self.top1.constant = 10
              txtSupportLeft.isHidden = false
            txtSupportTruck2.isUserInteractionEnabled = true
            self.imgSupport.isHidden = false
              sender.setImage(#imageLiteral(resourceName: "imgMinus"), for: .normal)
        }
    }
    
    @IBAction func btnTrailer2Tapped(_ sender: UIButton) {
        if sender.imageView?.image != #imageLiteral(resourceName: "ImgAddNumber") {
            self.trailerHeight2.constant = 0
            self.trailerTop.constant = 0
            txtTrailerLeft.isHidden = true
              txtTrailer2.isUserInteractionEnabled = false
              self.imgTrailer.isHidden = true
          sender.setImage(#imageLiteral(resourceName: "ImgAddNumber"), for: .normal)

        }
        else{
            
              txtTrailer2.isUserInteractionEnabled = true
            self.trailerHeight2.constant = 44
            self.trailerTop.constant = 10
            txtTrailerLeft.isHidden = false
              self.imgTrailer.isHidden = false
          sender.setImage(#imageLiteral(resourceName: "imgMinus"), for: .normal)
        }
    }
    
    
    
    
    func setUpView(){
        
    
        txtDrillerName.requiredText()
      
        txtOtherHelper1.rightVC()
        txtOtherHelper2.rightVC()
        
        for i in 101..<109{
            
            let txtTitle = self.view.viewWithTag(i) as! UITextField
            txtTitle.addshodowToTextField()
            txtTitle.leftview(image: imgArr[i-101])
        }
        
          for i in 1..<11{
            
             let txt = self.view.viewWithTag(i) as! UITextField
            txt.delegate = self
           
            if i == 2 || i == 3 || i == 4 || i == 5 || i == 6 || i == 7 || i == 8{
                let picker = UIPickerView()
                picker.dataSource = self
                picker.delegate = self
                picker.tag = 5000 + i
                txt.inputView = picker
                if i == 5 || i == 6{
                     txt.rightWithWidth(width: 20)
                }
                else{
                    txt.rightview(Img: #imageLiteral(resourceName: "imgDropDown"))
                    }
                
               
            }
            if i == 9 || i == 10{
                txt.addshodowToTextField()
                  print(self.mainDict)
                
                
                
                if JSON(self.mainDict)[keys[i - 1]].stringValue == "1" {
               
                    txt.rightview(Img: #imageLiteral(resourceName: "imgCheck"))
                 
                }
                else{
                       txt.rightview(Img: #imageLiteral(resourceName: "imgUnselect"))
                }
            }
            
            else{
                txt.text = JSON(self.mainDict)["\(self.keys[i - 1])"].stringValue
            }
            
        }

    }
    
    
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func btnCheckBoxTapped(_ sender: UIButton) {
     
        if JSON(self.mainDict)[keys[sender.tag - 111]].stringValue == "1" {
            self.mainDict.updateValue("0", forKey: keys[sender.tag - 111])
             let txt = self.view.viewWithTag(sender.tag - 110) as! UITextField
               txt.rightview(Img: #imageLiteral(resourceName: "imgUnselect"))
            
        }
        else{
            self.mainDict.updateValue("1", forKey: keys[sender.tag - 111])
            let txt = self.view.viewWithTag(sender.tag - 110) as! UITextField
            txt.rightview(Img: #imageLiteral(resourceName: "imgCheck"))
        }
        print(self.mainDict)
        
    }
   
    
    @IBAction func btnSaveAction(_ sender: Any) {
        
        
        
         self.popViewController()
      
    }
    @objc  func btnSaveTapped(sender : UIButton){
        self.popViewController()
    }
    //MARK: - ScrollView Delegates
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        
    }
   
    

    //MARK:- UITEXTFILED DELEGATES
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
      
        
        if txtOtherHelper1 == textField {
            self.mainDict.updateValue(textField.text ?? "", forKey: "crew_helpers")
        }
        else if txtOtherHelper2 == textField {
             self.mainDict.updateValue(textField.text ?? "", forKey: "crew_helpers2")
        }
        else{
            print(textField.tag)
              self.mainDict.updateValue(textField.text ?? "", forKey: keys[textField.tag - 1])
        }
    
        print(mainDict)
       
    }
    func popViewController(){
        
        var count = 0
        for  i in 0..<keysCheck.count {
            
            if  JSON(self.mainDict)[keysCheck[i]].stringValue == "" {
                count = count + 1
        }
        }
            if count == 0 {
                
                DispatchQueue.main.async {
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                    for aViewController in viewControllers {
                        if aViewController is CreateProjectViewController {
                            let vc  = aViewController as! CreateProjectViewController
                            vc.setCrewInformation(dict: self.mainDict)
                            self.navigationController!.popToViewController(aViewController, animated: false)
                              appDelegates.window?.rootViewController?.showAlert(messageStr: "data is saved")
                            
                        }
                    }
                    
                }
            }
            
            else{
                 self.showAlert(messageStr: "Missing Required Fields!")
                
            }
        }
    
    //MARK:- PickerView Data Source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       if pickerView.tag == 5007 ||  pickerView.tag == 5008{
            return arrayHelper.count
        }
       else if pickerView.tag == 5002 {
          return arrayRig.count
        }
        
       else if pickerView.tag == 5003  || pickerView.tag == 5005{
        return arraySupport.count
        }
       else if pickerView.tag == 5004  || pickerView.tag == 5006{
        return arrayTrailer.count
        }
        return 0
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 5007 ||  pickerView.tag == 5008{
            return arrayHelper[row]
        }
        else if pickerView.tag == 5002 {
            return arrayRig[row]
        }
        else if pickerView.tag == 5003  || pickerView.tag == 5005{
            return arraySupport[row]
        }
        else if pickerView.tag == 5004  || pickerView.tag == 5006{
            return arrayTrailer[row]
        }
            return ""
       
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
          if pickerView.tag == 5007 {
            if arrayHelper[row] == "Others"{
                  self.topHelper1.constant = 10
                self.heightHelper1.constant = 44
                self.txtOtherHelper1.isHidden = false
               
            }else{
                 self.topHelper1.constant = 0
                self.heightHelper1.constant = 0
                self.txtOtherHelper1.isHidden = true
            }
            txtHelper1.text = arrayHelper[row]
        }
          else  if pickerView.tag == 5008{
            if arrayHelper[row] == "Others"{
                  self.topHelper2.constant = 10
                self.heightHelper2.constant = 44
                self.txtOtherHelper2.isHidden = false
                
            }else{
                  self.topHelper2.constant = 0
                self.heightHelper2.constant = 0
                self.txtOtherHelper2.isHidden = true
            }
                txtHelper2.text = arrayHelper[row]
        }
       else if pickerView.tag == 5002 {
            
            txtRig.text = arrayRig[row]
        }
          else if pickerView.tag == 5003  {
            return  txtSupportTruck1.text = arraySupport[row]
        }
          else if pickerView.tag == 5005  {
            return  txtSupportTruck2.text = arraySupport[row]
        }
          else if pickerView.tag == 5004  {
            return  txtTrailer1.text = arrayTrailer[row]
        }
          else if pickerView.tag == 5006  {
            return  txtTrailer2.text = arrayTrailer[row]
        }
    }

}
