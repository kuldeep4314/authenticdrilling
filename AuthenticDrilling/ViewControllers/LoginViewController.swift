//
//  LoginViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-24.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit

class LoginViewController: ViewController {

    @IBOutlet weak var txtPassword: AlignedPlaceholderTextField!
    @IBOutlet weak var txtEmail: AlignedPlaceholderTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtEmail.borderColor()
        txtPassword.borderColor()
        self.txtEmail.leftviewWithImage(image: #imageLiteral(resourceName: "imgUser"))
        self.txtPassword.leftviewWithImage(image: #imageLiteral(resourceName: "imgPassword"))
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func loginBtnTapped(_ sender: Any) {
        if txtEmail.validate() && txtPassword.validate(){
             self.loginApiCall()
        }
      
    }
    
    @IBAction func forgotPassword(_ sender: Any) {
    }
    //MARK:- Login Api Call
    func loginApiCall(){
       
        let parameter  : Parameters  = ["action": "login", "email": txtEmail.text!, "password" : txtPassword.text!, "gcm_id" : standard.value(forKey: "gcm") ?? "","platform" : "0", "device_id":deviceUUID]
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(loginURl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
           
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
           
              let user = DAUser.init(json: json)
            guard user.response?.result == "201" else{
            appDelegates.window?.rootViewController?.showAlert(messageStr:  user.response?.msg ?? "" )
                return
            }
            
            if (user.response?.data?.loginStatus ?? "") == "0"{
                standard.set(user.response?.data?.email, forKey: "email")
                standard.set(user.response?.data?.internalIdentifier, forKey: "id")
                standard.set(user.response?.data?.name, forKey: "username")
                  standard.set(user.response?.data?.dictionaryRepresentation(), forKey: "userData")
                
                DispatchQueue.main.async {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else{
                 appDelegates.window?.rootViewController?.showAlert(messageStr:  "Please logout from other device first.")
            }
            
        }) { (error) in
         appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  error.localizedDescription)
            }
           print(error.localizedDescription)
        }
        
    }
    

}
