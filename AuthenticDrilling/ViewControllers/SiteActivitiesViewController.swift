//
//  SiteActivitiesViewController.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-25.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//


import UIKit
import CoreData

class SiteActivitiesViewController: ViewController,UITableViewDataSource,UITableViewDelegate ,UITextViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate{
    
    var isEdit = Bool()
    let arrayNames = ["Select Site Activity","Start Time","Stop Time"]
    var activityList = [JSON]()
    var subCatgoriesList = [[String: Any]]()
    var subCatgoriesIDs = [String]()
    var mainDict = [String: Any]()
    var currentIndex = Int()
    let arrayImagesLeft = [#imageLiteral(resourceName: "imgSelectSite"),#imageLiteral(resourceName: "imgTimer"),#imageLiteral(resourceName: "imgTimer")]
    let arrayImagesRight = [#imageLiteral(resourceName: "imgDropDown"),#imageLiteral(resourceName: "imgWatch"),#imageLiteral(resourceName: "imgWatch")]
    var strDate = String()
    var isShowNotes = Bool()
    var sessions: [NSManagedObject] = []
    var currentTextField = UITextField()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblProjectName: UILabel!
    var projectInfo :ProjectData?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        
        if  let decoded  = standard.value(forKey: "catData") as? [String : Any]{
            print(decoded)
            activityList  = JSON(decoded)["Site Activities list"].arrayValue
        }
        
        if Network.isConnectedToNetwork() == true {
            self.getOfflineData()
        }
        else{
            self.getCoreData()
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc  func btnSaveTapped(sender:UIButton){
        
        if Network.isConnectedToNetwork() == true {
            
            self.siteApiCall(isOffline: false, edit: false)
        }
        else  if Network.isConnectedToNetwork() == false {
            
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "SiteActivity", strDate: self.strDate, projectID: projectId) == true
            {
                self.updateCoredata(isOffline: true, edit: true, isShowAlert: true)
            }
                
            else{
                ProjectsOffline.sharedInstance.getOfflineDates(isOffline: true, strDate: self.strDate, projectID: projectId)
                self.saveCoredata(isOffline: true, edit: false, msg: "site activity list added successfully", isShow: true)
            }
        }
    }
    
    //MARK:- TextView Delegates
    func textViewDidChange(_ textView: UITextView) {
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray
        {
            textView.text = ""
            textView.textColor = UIColor.black
            
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
        if textView.text.isEmpty {
            textView.text = "Enter your notes"
            textView.textColor = UIColor.lightGray
            mainDict.updateValue(
                "", forKey:"notes")
        }
        else{
            mainDict.updateValue( textView.text ?? "", forKey:"notes")
        }
    }
    
    @objc  func dateChange(sender: UIDatePicker){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        print(sender.tag)
        if  sender.tag == 1 {
            mainDict.updateValue(formatter.string(from: sender.date), forKey: "start_time")
            currentTextField.text = formatter.string(from: sender.date)
        }
        else{
            mainDict.updateValue(formatter.string(from: sender.date), forKey: "stop_time")
            currentTextField.text = formatter.string(from: sender.date)
        }
    }
    
    func addToolBar(sender : UITextField){
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        sender.inputAccessoryView = toolBar
        
        
    }
    @objc   func doneClick()  {
        if self.isShowNotes == false{
            
            let  temp = self.activityList[currentIndex]["subcategory"].arrayValue
            
            self.subCatgoriesList.removeAll()
            for i in 0..<temp.count{
                
                self.subCatgoriesList.append(temp[i].dictionaryObject!)
            }
            print(self.subCatgoriesList)
            
            
            mainDict.updateValue( self.subCatgoriesList, forKey: "sub_category")
            mainDict.updateValue("", forKey:"notes")
        }
        
        self.tableView.reloadData()
        
    }
    
    
    @objc   func cancelClick()  {
        self.tableView.reloadData()
    }
    
    func getOfflineData(){
        
        let offlineDetails = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "SiteActivity")
        print(offlineDetails)
        if offlineDetails.count > 0 {
            let parameters : Parameters = ["action": "siteactivity_category" ,"data" :  offlineDetails ]
            
            self.syncOfflineMultipleApi(parameter: parameters)
            
        }
        else{
            self.fetchDetails()
        }
        
    }
    
    
    
    //MARK: - SyncOfflineMultipleApi
    func syncOfflineMultipleApi(parameter :Parameters){
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(projectUrlOffline, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            ProjectsOffline.sharedInstance.updateCoredataTables(tables: "SiteActivity")
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            self.getCoreData()
            print(error.localizedDescription)
        }
    }
    
    
    
    //MARK:- Update Core Data
    func updateCoredata(isOffline : Bool , edit : Bool , isShowAlert : Bool){
        
        let managedContext = CoreDataStack.managedObjectContext
        let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "SiteActivity")
        updateFetch.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , strDate)
        do {
            let results = try managedContext.fetch(updateFetch) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                
                // In my case, I only updated the first item in results
                let session = results![0]
                
                let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
                session.setValue(data1, forKey: "dataDetails")
                
                session.setValue(strDate, forKey: "date")
                session.setValue(projectId, forKey: "projectid")
                session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                session.setValue(isOffline, forKey: "isOffline")
                session.setValue(edit, forKey: "edit")
                
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try managedContext.save()
            
            
            if isShowAlert == true {
                self.navigationController?.popViewController(animated: true)
                appDelegates.window?.rootViewController?.showAlert(messageStr:  "site activity list updated successfully")
            }
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
        
    }
    
    
    //MARK:- Save Core Data Function
    func saveCoredata(isOffline : Bool,edit: Bool , msg : String , isShow : Bool){
        print(isOffline)
        print(edit)
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "SiteActivity",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        let data1: Data = NSKeyedArchiver.archivedData(withRootObject: self.mainDict)
        session.setValue(data1, forKey: "dataDetails")
        
        session.setValue(strDate, forKey: "date")
        session.setValue(projectId, forKey: "projectid")
        session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
        session.setValue(isOffline, forKey: "isOffline")
        session.setValue(edit, forKey: "edit")
        
        
        CoreDataStack.saveContext()
        
        
        if isShow == true{
            self.navigationController?.popViewController(animated: true)
            appDelegates.window?.rootViewController?.showAlert(messageStr: msg)
        }
        
    }
    //MARK:- Get Core Data Function
    func getCoreData() {
        
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "SiteActivity")
        
        fetchRequest.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  projectId , strDate)
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            for session in sessions{
                var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                
                dictionary.updateValue(session.value(forKey: "edit") ?? "", forKey: "edit")
                dictionary.updateValue(session.value(forKey: "isOffline") ?? "", forKey: "isOffline")
                dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                
                dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                
                self.mainDict = dictionary
                
                self.subCatgoriesIDs = JSON(self.mainDict)["subcat"].stringValue.components(separatedBy: ",")
                
                
                let temp = JSON(self.mainDict)["sub_category"].arrayValue
                
                
                self.subCatgoriesList.removeAll()
                for i in 0..<temp.count{
                    
                    self.subCatgoriesList.append(temp[i].dictionaryObject!)
                }
                print(self.subCatgoriesList)
                
                DispatchQueue.main.async {
                    if JSON(self.mainDict)["notes"].stringValue.base64Decoded() != ""{
                        self.isShowNotes = true
                    }
                    else{
                        self.isShowNotes = false
                    }
                    self.tableView.reloadData()
                }
                
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    
    
    
    
    
    
    //MARK:- TableView Data Source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayNames.count + 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if  self.isShowNotes == true{
                return  1
            }
            else{
                return  subCatgoriesList.count
            }
            
        }
        
        return 0
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if  self.isShowNotes == true {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! RentalCell
            cell.txtView.text = JSON(self.mainDict)["notes"].stringValue.base64Decoded()
            
            
            if  cell.txtView.text == "" {
                cell.txtView.text = "Enter your notes"
                cell.txtView.textColor = UIColor.lightGray
            }
            else{
                cell.txtView.textColor = UIColor.black
            }
            cell.txtView.delegate = self
            return cell
        }
        else{
            
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as! RentalCell
            cell.lblName.text = JSON(subCatgoriesList[indexPath.row])["name"].stringValue
            if subCatgoriesIDs.contains(JSON(subCatgoriesList[indexPath.row])["subcategory_id"].stringValue){
                cell.btnRight.setImage(#imageLiteral(resourceName: "imgCheck"), for: .normal)
            }
            else{
                cell.btnRight.setImage(#imageLiteral(resourceName: "imgUnselect"), for: .normal)
            }
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if  self.isShowNotes == true {
            return 120
        }
        return 60
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == arrayNames.count  {
            let cellSave =   tableView.dequeueReusableCell(withIdentifier: "cell2") as! RentalCell
            
            
            cellSave.saveBtn.addTarget(self, action: #selector(self.btnSaveTapped(sender:)), for: .touchUpInside)
            return cellSave
        }
            
        else {  let cell =  tableView.dequeueReusableCell(withIdentifier: "cell") as! RentalCell
            
            
            if section == 0 {
                
                let picker = UIPickerView()
                picker.dataSource = self
                picker.delegate = self
                cell.txtTitle.inputView = picker
                self.addToolBar(sender:   cell.txtTitle)
                cell.txtTitle.tag = section
                cell.txtTitle.delegate = self
                cell.txtTitle.text =  JSON(self.mainDict)["site_activity"].stringValue
                
                
                if JSON(self.mainDict)["site_activity"].stringValue == ""{
                    cell.txtTitle.text = arrayNames[section]
                }
                
                
                cell.txtTitle.rightview(Img: arrayImagesRight[section])
                cell.txtTitle.isUserInteractionEnabled = true
                cell.txtName.isHidden = true
            }
            else {
                if section == 1 {
                    cell.txtName.text = JSON(self.mainDict)["start_time"].stringValue
                }
                else{
                    cell.txtName.text = JSON(self.mainDict)["stop_time"].stringValue
                }
                cell.txtTitle.text = arrayNames[section]
                cell.txtTitle.isUserInteractionEnabled = false
                cell.txtName.isHidden = false
                self.addToolBar(sender:   cell.txtName)
                cell.txtName.delegate = self
                cell.txtName.tag = section
                let picker = UIDatePicker()
                picker.addTarget(self, action: #selector(self.dateChange(sender:)), for: .valueChanged)
                picker.datePickerMode = .time
                 picker.locale = Locale(identifier: "en_GB")
                picker.tag = section
                cell.txtName.inputView = picker
                
            }
            
            cell.txtTitle.leftview(image: arrayImagesLeft[section])
            cell.txtName.rightview(Img: arrayImagesRight[section])
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == arrayNames.count{
            return 120
        }
        return 70
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if subCatgoriesIDs.contains(JSON(subCatgoriesList[indexPath.row])["subcategory_id"].stringValue){
            let value = JSON(subCatgoriesList[indexPath.row])["subcategory_id"].stringValue
            
            self.subCatgoriesIDs = self.subCatgoriesIDs.filter{return $0 != value}
        }
            
        else{
            self.subCatgoriesIDs.append(JSON(subCatgoriesList[indexPath.row])["subcategory_id"].stringValue)
        }
        print(self.subCatgoriesIDs)
        self.subCatgoriesIDs = self.subCatgoriesIDs.filter{return $0 != ""}
        let strIDS = self.subCatgoriesIDs.joined(separator: ",")
        self.mainDict.updateValue(strIDS, forKey: "subcat")
        print(self.mainDict)
        self.tableView.reloadData()
    }
    func betweenTimeFunction(textField : UITextField ,start :String ,stop :String){
        
        let arrayStartTime = start.components(separatedBy: ":")
        let arrayStopTime = stop.components(separatedBy: ":")
        if textField.tag == 1 {
            self.mainDict.updateValue( ""  , forKey: "start_time")
            
        }
        else if textField.tag == 2{
            mainDict.updateValue("", forKey:"stop_time")
        }
        if textField.tag == 1 {
            
            
            if JSON(arrayStartTime[0]).intValue > JSON(arrayStopTime[0]).intValue {
                if JSON(arrayStartTime[0]).intValue >= 12 && JSON(arrayStopTime[0]).intValue < 12{
                    appDelegates.window?.rootViewController?.view.makeToast("Please select stop time between 12-24.", duration: 2, position: .center)
                }
                else{
                    appDelegates.window?.rootViewController?.view.makeToast("Start time should be less than Stop time.", duration: 2, position: .center)
                }
                
            }
            else{
                appDelegates.window?.rootViewController?.view.makeToast("Start time should be less than Stop time.", duration: 2, position: .center)
            }
            
        }
        else{
            if JSON(arrayStartTime[0]).intValue > JSON(arrayStopTime[0]).intValue {
                if JSON(arrayStartTime[0]).intValue >= 12 && JSON(arrayStopTime[0]).intValue < 12{
                    appDelegates.window?.rootViewController?.view.makeToast("Please select stop time between 12-24.", duration: 2, position: .center)
                }
                else{
                    appDelegates.window?.rootViewController?.view.makeToast("Stop time should be greater than Start time.", duration: 2, position: .center)
                }
            }
            else{
                appDelegates.window?.rootViewController?.view.makeToast("Stop time should be greater than Start time.", duration: 2, position: .center)
            }
            
            
        }
        
    }
    //MARK:- TextFields Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.tag == 1 {
            
            mainDict.updateValue(textField.text ?? "", forKey:"start_time")
        }
        else if textField.tag == 2{
            mainDict.updateValue(textField.text ?? "", forKey:"stop_time")
        }
      
        if textField.tag == 1 || textField.tag == 2 {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            
            let dateformatter = DateFormatter()
            dateformatter.dateFormat = "yyyy-MM-dd HH:mm"
            
            if   JSON(self.mainDict)["start_time"].stringValue != "" &&  JSON(self.mainDict)["stop_time"].stringValue != "" {
                let dateCurrent = formatter.string(from: Date())
                
                let firstDate = dateformatter.date(from: "\(dateCurrent) \(JSON(self.mainDict)["start_time"].stringValue)")
                let secondDate = dateformatter.date(from: "\(dateCurrent) \(JSON(self.mainDict)["stop_time"].stringValue )")
                
                
                let check = firstDate?.timeIntervalSince(secondDate!)
                
                if  Int(check!) < 0 {
                   
                    if textField.tag == 1 {
                         self.mainDict.updateValue(textField.text ?? ""  , forKey: "start_time")
                        
                    }
                    else if textField.tag == 2{
                        mainDict.updateValue(textField.text ?? "", forKey:"stop_time")
                    }
                    
                    
                }
                else{
                   
                    self.betweenTimeFunction(textField: textField, start: JSON(self.mainDict)["start_time"].stringValue, stop: JSON(self.mainDict)["stop_time"].stringValue)
                  
                }
                
            }
            
            
        }
 
        else{
            mainDict.updateValue(activityList[currentIndex]["category_id"].stringValue, forKey: "category")
            mainDict.updateValue(activityList[currentIndex]["name"].stringValue, forKey: "site_activity")
            
        }
        print(self.mainDict)
    }
    
    
    
    
    
    //MARK:- PickerView Data Source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return activityList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        currentTextField.text = activityList[row]["name"].stringValue
        currentIndex = row
        self.subCatgoriesIDs.removeAll()
        if activityList[row]["name"].stringValue.contains("Other") == true{
            self.isShowNotes = true
        }
        else{
            self.isShowNotes = false
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return activityList[row]["name"].stringValue
    }
    
    func siteApiCall(isOffline : Bool , edit : Bool ){
        let parameter  : Parameters  =  ["action": "siteactivity_category", "userid": standard.value(forKey: "id")!, "projectid":  projectId,"date":strDate
            , "category": JSON(self.mainDict)["category"].stringValue
            , "sub_category": JSON(self.mainDict)["sub_category"].arrayObject ?? [String: Any]()
            , "start_time": JSON(self.mainDict)["start_time"].stringValue
            , "stop_time": JSON(self.mainDict)["stop_time"].stringValue
            , "notes": JSON(self.mainDict)["notes"].stringValue
            , "site_activity": JSON(self.mainDict)["site_activity"].stringValue
            , "subcat": JSON(self.mainDict)["subcat"].stringValue
        ]
        
        print(parameter)
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            
            guard json["response"]["result"].stringValue == "201" else{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  json["response"]["msg"].stringValue )
                return
            }
            
            
            DispatchQueue.main.async {
                ProjectsOffline.sharedInstance.getOfflineDates(isOffline: isOffline, strDate: self.strDate, projectID: projectId)
                
                
                if ProjectsOffline.sharedInstance.getOfflineTables(tables: "SiteActivity", strDate: self.strDate, projectID: projectId) == true {
                    self.updateCoredata(isOffline: false, edit: false, isShowAlert: true)
                }
                else{
                    self.saveCoredata(isOffline: isOffline, edit: edit, msg: json["response"]["msg"].stringValue, isShow: true)
                }
            }
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr:  error.localizedDescription)
            }
            
            print(error.localizedDescription)
        }
        
    }
    //MARK:- fetchDetails function
    func  fetchDetails(){
        let parameter  : Parameters  = ["action": "fetch_details", "userid": standard.value(forKey: "id")!,"projectid": projectId,"table":"site_activity","date": strDate]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].stringValue == "201" else{
                return
            }
            
            self.mainDict = json["response"]["data"].dictionaryObject ?? [String: Any]()
            if ProjectsOffline.sharedInstance.getOfflineTables(tables: "SiteActivity", strDate: self.strDate, projectID: projectId) == true {
                self.updateCoredata(isOffline: false, edit: false, isShowAlert: false)
            }
            else{
                self.saveCoredata(isOffline: false, edit: false, msg: "", isShow: false)
                
            }
            self.getCoreData()
            
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    
}


