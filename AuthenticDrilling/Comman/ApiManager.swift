//
//  ApiManager.swift
//  PhotoSpot
//
//  Created by Avatar Singh on 2017-07-21.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ApiManager: NSObject {

    

    
    
    class func requestPOSTURL(_ strURL : String, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        
        Alamofire.request(strURL, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: headers).responseJSON { (responseObject) -> Void in

            
            if responseObject.result.isSuccess {
                
              
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
                
                
                
            }
        }
    }
    
    
    
    class func requestPOSTURL24(_ strURL : String, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        
        Alamofire.request(strURL, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: headers).responseString { (string) in
            print(string)
        }

    }
    
    
    
    class func requestMultiPartURL(_ strURL : String, params : [String : AnyObject]?, headers : [String : String]? , imagesArray : [UIImage], success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if imagesArray.count != 0 {
                
                for i in 0..<imagesArray.count{
                    
                    multipartFormData.append(UIImageJPEGRepresentation(imagesArray[i], 0.3)!, withName: "image\(i)", fileName: "swift_file\(i).jpeg", mimeType: "image/jpg")
                }
            }
            
            for (key, value ) in params! {
                multipartFormData.append((value).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: strURL)
        { (result) in
            switch result{
            case .success(let upload, _,_ ):
                
                upload.uploadProgress(closure: { (progress) in
                    UILabel().text = "\((progress.fractionCompleted * 100)) %"
                    print (progress.fractionCompleted * 100)
                })
                
                
                upload.responseJSON { response in
                    
                    guard ((response.result.value) != nil) else{
                        failure(response.result.error!.localizedDescription as! Error)
                        return
                    }
                    
                    let resJson = JSON(response.result.value!)
                    success(resJson)
                    
                }
                
            case .failure(let encodingError):
                failure(encodingError.localizedDescription as! Error)
                
            }
        }
    }

    
    
    
}
