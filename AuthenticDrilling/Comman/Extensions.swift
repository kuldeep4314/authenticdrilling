//
//  Extensions.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-23.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class Extensions: NSObject {

}



extension String {
        
        func chopPrefix(_ count: Int = 1) -> String {
            if count >= 0 && count <= self.count {
                let indexStartOfText = self.index(self.startIndex, offsetBy: count)
                return String(self[indexStartOfText...])
            }
            return ""
        }
        
        func chopSuffix(_ count: Int = 1) -> String {
            if count >= 0 && count <= self.count {
                let indexEndOfText = self.index(self.endIndex, offsetBy: -count)
                return String(self[..<indexEndOfText])
            }
            return ""
        }
    
    
    
    func dropLast(_ n: Int = 1) -> String {
        return String(dropLast(n))
    }
    var dropLast: String {
        return dropLast()
    }
    
    func dropFirst(_ n: Int = 1) -> String {
        return String(dropFirst(n))
    }
    var dropFirst: String {
        return dropFirst()
    }
    func base64Encoded() -> String? {
        if let data = self.data(using: .utf8) {
            return data.base64EncodedString()
        }
        return ""
    }
    
    func base64Decoded() -> String? {
        var base64Encoded = self
        
        if base64Encoded.characters.last == "\n"{
            base64Encoded.characters.removeLast()
        }
        
        
        if Data(base64Encoded: base64Encoded) == nil{
            
            return self
            
        } else{
            
            let decodedData = Data(base64Encoded: base64Encoded)!
            
            if String(data: decodedData, encoding: .utf8) == nil{
                return self
            }
            
            
            let decodedString = String(data: decodedData, encoding: .utf8)!
            
            return decodedString
        }
    }
    
}
extension UITextField{
    func leftview(){
        let vc = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 44))
        
        self.leftView = vc
        self.leftViewMode = .always
    }
    func rightVC(){
        let vc = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 44))
        
        self.rightView = vc
        self.rightViewMode = .always
    }
    func rightWithWidth(width :CGFloat){
        let vc = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 44))
        
        self.rightView = vc
        self.rightViewMode = .always
    }
    func leftviewWithImage(image :UIImage){
        let vc = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 34))
        let btnLeft = UIButton(frame: CGRect(x: 8, y: 2, width: 30, height: 30))
        btnLeft.setImage(image, for: .normal)
        btnLeft.isUserInteractionEnabled = false
        vc.addSubview(btnLeft)
        self.leftView = vc
        self.leftViewMode = .always
    }
    
    func leftview(image :UIImage){
        let vc = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 44))
        let btnLeft = UIButton(frame: CGRect(x: 8, y: 15, width: 14, height: 14))
        btnLeft.setImage(image, for: .normal)
        btnLeft.isUserInteractionEnabled = false
        vc.addSubview(btnLeft)
        self.leftView = vc
        self.leftViewMode = .always
    }
    
    func rightview(Img:UIImage ){
        let vc = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 44))
        let btnLeft = UIButton(frame: CGRect(x: 10, y: 15, width: 14, height: 14))
        btnLeft.setImage(Img, for: .normal)
        btnLeft.isUserInteractionEnabled = false
      
        vc.addSubview(btnLeft)
        self.rightView = vc
        self.rightViewMode = .always
    }
    func requiredText(){
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: 7, height: 20))
        lbl.text = "*"
        lbl.textAlignment = .center
        lbl.textColor = UIColor.red
        self.rightView = lbl
        self.rightViewMode = .always
    }

    
    func borderColor(){
        self.layer.borderColor = UIColor(red: 225/255, green: 162/255, blue: 136/255, alpha: 1.0).cgColor
        
        self.layer.borderWidth = 1.0;
    }
    func addshodowToTextField(){
      
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 2
        self.layer.masksToBounds = false
        
        
    }
   
    
}
extension UIButton {
    func addshodowToButton(){
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 2
        self.layer.masksToBounds = false
        
        
    }
}


extension UITextView {
    
    func addshodowToTextView(){
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 2
        self.layer.masksToBounds = false
        
        
    }
    
}


extension UIView{
    func addshodowToView(){
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 5
        self.layer.masksToBounds = false
        
    }
    func startIndicator(){
        
        let vc = NVActivityIndicatorView.init(frame: CGRect.init(x: self.frame.size.width / 2 - 22, y: self.frame.size.height / 2 - 22, width: 44, height: 44), type: .ballSpinFadeLoader, color: UIColor.lightGray, padding: 0)
        vc.tag = 1000
        self.isUserInteractionEnabled = false
        vc.startAnimating()
        self.addSubview(vc)
        
    }
    
    func stopIndicator(){
        let vc =  self.viewWithTag(1000) as? NVActivityIndicatorView
        self.isUserInteractionEnabled = true
        vc?.stopAnimating()
        vc?.removeFromSuperview()
        
    }
}


extension UIViewController{
    
    func showAlert(messageStr :String){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: nil, message: messageStr as String, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
