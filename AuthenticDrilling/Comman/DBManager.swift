//
//  DBManager.swift
//  Practice Dairy
//
//  Created by user on 26/10/16.
//  Copyright © 2016 Rohit Garg. All rights reserved.
//

import UIKit

struct CatInfo {
    var ID = String()
    var Des = Data()
    
}

class DBManager: NSObject {
    let ID = "ID"
    let Des = "Des"
    
    static let shared : DBManager = DBManager()
    
    let databaseFileName = "database.sqlite"
    
    var pathToDatabase: String!
    
    var database: FMDatabase!
    
    override init(){
        super.init()
        let documentsDirectory = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString) as String
        pathToDatabase = documentsDirectory.appending("/\(databaseFileName)")
        print(pathToDatabase)
    }
    
    func createDatabase() -> Bool {
        var created = false
        
        
        if !FileManager.default.fileExists(atPath: pathToDatabase) {
            database = FMDatabase(path: pathToDatabase!)
            
            if database != nil {
                // Open the database.
                if database.open() {
                    let createTableQuery = "create table categorites (\(ID) integer primary key autoincrement not null, \(Des) blob)"
                    
                    do {
                        try database.executeUpdate(createTableQuery, values: nil)
                        print("created table.")
                        created = true
                    }
                    catch {
                        print("Could not create table.")
                    }
                    
                    database.close()
                }
                else {
                    print("Could not open the database.")
                }
            }
        }
        
        return created
    }
    
    func openDatabase() -> Bool {
        if database == nil {
            if FileManager.default.fileExists(atPath: pathToDatabase) {
                database = FMDatabase(path: pathToDatabase)
            }
        }
        
        if database != nil {
            if database.open() {
                return true
            }
        }
        
        return false
    }
    
    func insertData(des :Data)-> Bool {
        var success = true
        if openDatabase() {
            var query = ""
            query += "insert into categorites (\(Des)) values ( '\(des)');"
            if !database.executeStatements(query) {
                success = false
                print("Failed to insert initial data into the database.")
                print(database.lastError(), database.lastErrorMessage())
            }
        }
        else{
             success = false
        }
        database.close()
        
        return success
        
    }
   
    func deleteData() -> Bool {
        var deleted = false
        
        if openDatabase() {
            let query = "delete from categorites where \(ID) !=?"
            
            do {
                try database.executeUpdate(query, values: [0])
                deleted = true
            }
            catch {
                print(error.localizedDescription)
            }
            
            database.close()
        }
        
        return deleted
    }

    func loadData() -> CatInfo? {
        var cat: CatInfo?
        
        if openDatabase() {
            let query = "select * from categorites where ID != ?"
            
            do {
                
                let results = try database.executeQuery(query, values: [""])
                
                
                while results.next() {
                    let t = CatInfo(ID: results.string(forColumn: ID),
                                         Des: results.data(forColumn: Des)
                        
                    )
                    
                    
                   cat = t
                }
            }
            catch {
                print(error.localizedDescription)
            }
            
            database.close()
        }
        return cat
    }
  
  
 
    
   
}
