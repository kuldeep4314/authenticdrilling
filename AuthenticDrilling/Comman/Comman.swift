//
//  Comman.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-23.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit

class Comman: NSObject {

class func sortDateWise( array : [ProjectData]) ->  [ProjectData]{
        
        // sorting array
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateCurrent = formatter.string(from: Date())
        let arraySorted =  array.sorted() {return formatter.date(from: $0.addOn ?? dateCurrent) ?? Date() > formatter.date(from: $1.addOn ?? dateCurrent) ?? Date()}
        
        return arraySorted
    }
    
    class func getCurrentDate() ->  String{
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let dateCurrent = formatter.string(from: Date())
       
        return dateCurrent
    }
    
    class func changedDateFormatter(date : String)->  String{
    
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
         let date = formatter.date(from: date)
        
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "MM/dd/yyyy"
        
        
        let strDate = formatter1.string(from: date ?? Date())
        
         print(strDate)
        
    
        return strDate
       
      
    }
    class func changedDateFormatter1(date : String)->  String{
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: date)
        
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "MM/dd/yyyy"
        
        
        let strDate = formatter1.string(from: date ?? Date())
        
        print(strDate)
        
        
        return strDate
        
        
    }
    
    class func getCurrentDateWithTime() ->  String{
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateCurrent = formatter.string(from: Date())
        
        return dateCurrent
    }
    
  
    class func timeInterval(date : String , startTime :String , stopTime: String) -> String{
        
        // sorting array
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        let dateA = date + " " + startTime
        let dateB = date + " " + stopTime
        let date1 = formatter.date(from: dateA)
        let date2 = formatter.date(from: dateB)

        let components = Set<Calendar.Component>([ .minute, .hour])
        let differenceOfDate = Calendar.current.dateComponents(components, from: date1 ?? Date(), to: date2 ?? Date())
        
        return "\(differenceOfDate.hour ?? 0):\(differenceOfDate.minute ?? 0) hours"
    }
    
    
    class func writeTofile(){
        let file = "file.txt" //this is the file. we will write to and read from it
        
        let text = "some text" //just a text
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let fileURL = dir.appendingPathComponent(file)
            print(fileURL)
            
            //writing
            do {
                try text.write(to: fileURL, atomically: false, encoding: .utf8)
            }
            catch {print("error handling here")}
            
            //reading
            do {
                let text2 = try String(contentsOf: fileURL, encoding: .utf8)
                print(text2)
            }
            catch {/* error handling here */}
        }
        
    }
    
    
 class   func betweenTimeFunction(textField : UITextField ,textStart :UITextField ,textStop :UITextField){
        
        let arrayStartTime = textStart.text?.components(separatedBy: ":")
        let arrayStopTime = textStop.text?.components(separatedBy: ":")
        if textField.tag == 1 {
            
            
            if JSON(arrayStartTime![0]).intValue > JSON(arrayStopTime![0]).intValue {
                if JSON(arrayStartTime![0]).intValue >= 12 && JSON(arrayStopTime![0]).intValue < 12{
                    appDelegates.window?.rootViewController?.view.makeToast("Please select stop time between 12-24.", duration: 2, position: .center)
                }
                else{
                    appDelegates.window?.rootViewController?.view.makeToast("Start time should be less than Stop time.", duration: 2, position: .center)
                }
                
            }
            else{
                appDelegates.window?.rootViewController?.view.makeToast("Start time should be less than Stop time.", duration: 2, position: .center)
            }
            
        }
        else{
            if JSON(arrayStartTime![0]).intValue > JSON(arrayStopTime![0]).intValue {
                if JSON(arrayStartTime![0]).intValue >= 12 && JSON(arrayStopTime![0]).intValue < 12{
                    appDelegates.window?.rootViewController?.view.makeToast("Please select stop time between 12-24.", duration: 2, position: .center)
                }
                else{
                    appDelegates.window?.rootViewController?.view.makeToast("Stop time should be greater than Start time.", duration: 2, position: .center)
                }
            }
            else{
                appDelegates.window?.rootViewController?.view.makeToast("Stop time should be greater than Start time.", duration: 2, position: .center)
            }
            
            
        }
        
    }
}



