//
//  GeoLogicalInfo.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-28.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit
protocol screenDelegates {
    func setScreenInformation(screenDict: [String: Any])
}
class Screen: UIView ,UITextFieldDelegate{
    @IBOutlet weak var txtFrom: UITextField!
      var delegates : screenDelegates?
    @IBOutlet var view: UIView!
    @IBOutlet weak var txtTo: UITextField!
      var screenDict = [String : Any]()
    @IBOutlet weak var txtWell: UITextField!
     var textFieldKeys = ["hole_diameter","pvc_screen","well_od","from_ft","to_ft"]
    @IBOutlet weak var txtPvc: UITextField!
    @IBOutlet weak var txtHoleDiameter: UITextField!
    
    
    
    @IBOutlet weak var txtHoleDiameterTitle: UITextField!
    @IBOutlet weak var txtPvcTitle: UITextField!
    
    @IBOutlet weak var txtWellTitle: UITextField!
    @IBOutlet weak var txtFromTitle: UITextField!
    @IBOutlet weak var txtToTitle: UITextField!
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
        self.setup()
    }
    override init(frame: CGRect)   {
        super.init(frame: frame)
        self.setup()
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func setup() {
        
        view = Bundle.main.loadNibNamed("Screen", owner: self, options: nil)?[0] as? UIView
        view.frame = self.bounds
        
        self.setUpView()
        
        
        self.addSubview(view)
    }
    
    func setUpView(){
        self.txtFromTitle.leftview(image: #imageLiteral(resourceName: "imgDepth"))
        self.txtToTitle.leftview(image: #imageLiteral(resourceName: "imgDepth"))
        self.txtHoleDiameterTitle.leftview(image:#imageLiteral(resourceName: "imgHoleDiameters"))
        self.txtPvcTitle.leftview(image: #imageLiteral(resourceName: "imgPvc"))
        self.txtWellTitle.leftview(image: #imageLiteral(resourceName: "imgWell"))
        self.txtFromTitle.addshodowToTextField()
        self.txtToTitle.addshodowToTextField()
        self.txtHoleDiameterTitle.addshodowToTextField()
        self.txtPvcTitle.addshodowToTextField()
        self.txtWellTitle.addshodowToTextField()
        
        self.txtFrom.delegate = self
        self.txtTo.delegate = self
        self.txtHoleDiameter.delegate = self
        self.txtPvc.delegate = self
        self.txtWell.delegate = self
        
        
        self.txtFrom.rightVC()
        self.txtTo.rightVC()
        self.txtHoleDiameter.rightVC()
        self.txtPvc.rightVC()
        self.txtWell.rightVC()
    }
    func setTextFieldValues (){
        
        
        self.txtHoleDiameter.text = JSON(self.screenDict)["\(textFieldKeys[0])"].stringValue
        self.txtPvc.text = JSON(self.screenDict)["\(textFieldKeys[1])"].stringValue
        self.txtWell.text = JSON(self.screenDict)["\(textFieldKeys[2])"].stringValue
        self.txtFrom.text = JSON(self.screenDict)["\(textFieldKeys[3])"].stringValue
        self.txtTo.text = JSON(self.screenDict)["\(textFieldKeys[4])"].stringValue
        
        }
    func textFieldDidEndEditing(_ textField: UITextField) {
        screenDict.updateValue( textField.text ?? "", forKey: textFieldKeys[textField.tag - 1])
       delegates?.setScreenInformation(screenDict: screenDict)
    }
}
