//
//  GeoLogicalInfo.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-28.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit

protocol geoLogicalDelegates {
    func setGeoInformation(geoDict: [String: Any])
}
class GeoLogicalInfo: UIView ,UITextFieldDelegate{
    @IBOutlet weak var txtDepthFrom: UITextField!
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var txtDepthTo: UITextField!
    var delegates : geoLogicalDelegates?
    @IBOutlet weak var txtType: UITextField!
    var geologicalDict = [String : Any]()
    @IBOutlet weak var txtColors: UITextField!
    @IBOutlet weak var txtGrainSize: UITextField!
    
    
    @IBOutlet weak var txtDepthFromTitle: UITextField!
    @IBOutlet weak var txtDepthToTitle: UITextField!
    @IBOutlet weak var txtTypeTitle: UITextField!
    @IBOutlet weak var txtGrainTitle: UITextField!
    @IBOutlet weak var txtColorTitle: UITextField!
    var textFieldKeys = ["depth_from","depth_to","type","grain_size","colour"]
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
        self.setup()
    }
    override init(frame: CGRect)   {
        super.init(frame: frame)
        self.setup()
        txtDepthFrom.delegate = self
        txtDepthTo.delegate = self
        txtType.delegate = self
        txtColors.delegate = self
        txtGrainSize.delegate = self
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func setup() {
        
        view = Bundle.main.loadNibNamed("GeoLogicalInfo", owner: self, options: nil)?[0] as? UIView
        view.frame = self.bounds
        
        self.setUpView()
        
        
        self.addSubview(view)
    }
    
    func setUpView(){
        self.txtDepthFromTitle.leftview(image: #imageLiteral(resourceName: "imgDepth"))
        self.txtDepthToTitle.leftview(image: #imageLiteral(resourceName: "imgDepth"))
        self.txtGrainTitle.leftview(image: #imageLiteral(resourceName: "imgGrainSize"))
        self.txtColorTitle.leftview(image: #imageLiteral(resourceName: "imgColors"))
        self.txtTypeTitle.leftview(image: #imageLiteral(resourceName: "imgType"))
        
        self.txtDepthFromTitle.addshodowToTextField()
        self.txtDepthToTitle.addshodowToTextField()
        self.txtGrainTitle.addshodowToTextField()
        self.txtColorTitle.addshodowToTextField()
        self.txtTypeTitle.addshodowToTextField()
        
        self.txtDepthFrom.rightVC()
        self.txtDepthTo.rightVC()
        self.txtGrainSize.rightVC()
        self.txtColors.rightVC()
        self.txtType.rightVC()
        
        
    }
    
    func setTextFieldValues (){
        
        self.txtDepthFrom.text = JSON(self.geologicalDict)["\(textFieldKeys[0])"].stringValue
        self.txtDepthTo.text = JSON(self.geologicalDict)["\(textFieldKeys[1])"].stringValue
        self.txtType.text = JSON(self.geologicalDict)["\(textFieldKeys[2])"].stringValue
        self.txtGrainSize.text = JSON(self.geologicalDict)["\(textFieldKeys[3])"].stringValue
        self.txtColors.text = JSON(self.geologicalDict)["\(textFieldKeys[4])"].stringValue
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
          print(geologicalDict)
        geologicalDict.updateValue( textField.text ?? "", forKey: textFieldKeys[textField.tag - 1])
        print(geologicalDict)
        
        
        if textField.tag == 1 || textField.tag == 2 {
            
            if JSON(self.geologicalDict)["depth_from"].stringValue != "" &&
                
                JSON(self.geologicalDict)["depth_to"].stringValue != ""{
                
                let value = JSON(self.geologicalDict)["depth_to"].intValue - JSON(self.geologicalDict)["depth_from"].intValue
                
                print(value)
                
                if value > 0 {
                  
                    
                }
                else{
                   geologicalDict.updateValue("", forKey: textFieldKeys[textField.tag - 1])
                   self.setTextFieldValues()
                    
                    if textField.tag == 1 {
                        appDelegates.window?.rootViewController?.view.makeToast("Depth from should be less than depth to.", duration: 2, position: .center)
                    }
                    else{
                        appDelegates.window?.rootViewController?.view.makeToast("Depth to should be greater than depth from.", duration: 2, position: .center)
                    }
                    
                    
                }
            }
            
        }
        
         self.delegates?.setGeoInformation(geoDict: geologicalDict)
        
    }
    
}
