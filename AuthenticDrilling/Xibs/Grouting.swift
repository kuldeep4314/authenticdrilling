//
//  GeoLogicalInfo.swift
//  AuthenticDrilling
//
//  Created by Avatar Singh on 2017-11-28.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit
protocol groutingDelegates {
    func groutingInformation(groutingDict: [String: Any])
}

class Grouting: UIView ,UIPickerViewDelegate,UIPickerViewDataSource ,UITextFieldDelegate{
    @IBOutlet weak var txtMaterial: UITextField!
   
     let arrayPicker1 = ["Gravity","Tremie"]
    var delegates : groutingDelegates?
    var groutingDict = [String : Any]()
    var textFieldKeys = ["material","bag","lbs","from_ft","to_ft","gravity"]
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var txtTo: UITextField!
    
    @IBOutlet weak var txtFrom: UITextField!
    @IBOutlet weak var txtNumBags: UITextField!
    
    @IBOutlet weak var txtNumLBS: UITextField!
    @IBOutlet weak var txtGravity: UITextField!
    
    @IBOutlet weak var txtMaterialTitle: UITextField!
    @IBOutlet weak var txtToTitle: UITextField!
    
    @IBOutlet weak var txtFromTitle: UITextField!
    @IBOutlet weak var txtNumBagsTitle: UITextField!
    
    @IBOutlet weak var txtNumLBSTitle: UITextField!
    @IBOutlet weak var txtGravityTitle: UITextField!
    
      var dropDownList = [JSON]()
    let picker = UIPickerView()
    let picker1 = UIPickerView()
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
        self.setup()
    }
    override init(frame: CGRect)   {
        super.init(frame: frame)
        if  let decoded  = standard.value(forKey: "catData") as? [String : Any]{
            print(decoded)
            dropDownList  = JSON(decoded)["Filter grouting material"].arrayValue
        }
        self.setup()
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func setup() {
        
        view = Bundle.main.loadNibNamed("Grouting", owner: self, options: nil)?[0] as? UIView
        view.frame = self.bounds
        picker.dataSource = self
        picker.delegate = self
        
        txtMaterial.inputView = picker
        picker1.dataSource = self
        picker1.delegate = self
        
        txtGravity.inputView = picker1
        self.setUpView()
        
        
        self.addSubview(view)
    }
    
    func setUpView(){
        self.txtFromTitle.leftview(image: #imageLiteral(resourceName: "imgDepth"))
        self.txtToTitle.leftview(image: #imageLiteral(resourceName: "imgDepth"))
        self.txtGravityTitle.leftview(image : #imageLiteral(resourceName: "imgGravity"))
        self.txtNumLBSTitle.leftview(image:  #imageLiteral(resourceName: "imgLbs"))
        self.txtNumBagsTitle.leftview(image: #imageLiteral(resourceName: "imgBags"))
        self.txtMaterialTitle.leftview(image: #imageLiteral(resourceName: "imgHoleDiameters"))
        self.txtMaterial.rightview(Img: #imageLiteral(resourceName: "imgDropDown"))
        self.txtMaterialTitle.addshodowToTextField()
        self.txtMaterial.addshodowToTextField()
        self.txtFromTitle.addshodowToTextField()
        self.txtToTitle.addshodowToTextField()
        self.txtGravityTitle.addshodowToTextField()
        self.txtNumLBSTitle.addshodowToTextField()
        self.txtNumBagsTitle.addshodowToTextField()
        
        
        self.txtMaterial.delegate = self
        self.txtFrom.delegate = self
        self.txtTo.delegate = self
        self.txtGravity.delegate = self
        self.txtNumLBS.delegate = self
        self.txtNumBags.delegate = self
      
        self.txtFrom.rightVC()
        self.txtTo.rightVC()
        self.txtGravity.rightview(Img: #imageLiteral(resourceName: "imgDropDown"))
        self.txtNumLBS.rightVC()
        self.txtNumBags.rightVC()
    }
    //MARK:- PickerView Data Source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == picker1{
            
            return arrayPicker1.count
        }
        else{
              return dropDownList.count
        }
      
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       
        
        if pickerView == picker1{
            
            return arrayPicker1[row]
        }
        else{
            return dropDownList[row]["name"].stringValue
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == picker1{
            
           self.txtGravity.text = arrayPicker1[row]
        }
        else{
            self.txtMaterial.text = dropDownList[row]["name"].stringValue
        }
        
        
       
    }
    func setTextFieldValues (){
        
        
        self.txtMaterial.text = JSON(self.groutingDict)["\(textFieldKeys[0])"].stringValue
        self.txtNumBags.text = JSON(self.groutingDict)["\(textFieldKeys[1])"].stringValue
        self.txtNumLBS.text = JSON(self.groutingDict)["\(textFieldKeys[2])"].stringValue
        self.txtFrom.text = JSON(self.groutingDict)["\(textFieldKeys[3])"].stringValue
        self.txtTo.text = JSON(self.groutingDict)["\(textFieldKeys[4])"].stringValue
        
        self.txtGravity.text = JSON(self.groutingDict)["\(textFieldKeys[5])"].stringValue
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        groutingDict.updateValue( textField.text ?? "", forKey: textFieldKeys[textField.tag - 1])
        delegates?.groutingInformation(groutingDict: groutingDict)
    }
    
}
