//
//  UpdateProjectsIDS.swift
//  AuthenticDrilling
//
//  Created by goyal on 29/03/18.
//  Copyright © 2018 Avatar Singh. All rights reserved.
//

import UIKit
import CoreData


class UpdateProjectsIDS: NSObject {
  static let sharedInstance = UpdateProjectsIDS()
    
    
    func updateProjectsIdsWithTables(arrayIds : [JSON] , tables: String, pids : String){
        
        for i in 0..<arrayIds.count {
            
            
            let managedContext = CoreDataStack.managedObjectContext
            let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: tables)
            updateFetch.predicate =  NSPredicate(format: "\(pids) = %@", arrayIds[i]["oldid"].stringValue)
            do {
                let results = try managedContext.fetch(updateFetch) as? [NSManagedObject]
                if results?.count != 0 { // Atleast one was returned
                    
                    // In my case, I only updated the first item in results
                    let session = results![0]
                    
                    session.setValue(arrayIds[i]["serverid"].stringValue, forKey: pids)
                    
                }
            } catch {
                print("Fetch Failed: \(error)")
            }
            
            do {
                try managedContext.save()
                
            }
            catch {
                print("Saving Core Data Failed: \(error)")
            }
            
            
            
        }
        
    }
    
    
    func updateProjectsIds(arrayIds : [JSON]){
        let offlineTravels = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Travel")
        
        
        let offlineRentals = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Rental")
        
        let offlineDrill = ProjectsOffline.sharedInstance.getOfflineDrillTablesData(tables: "Drill")
        
        
        let damagedTools = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "DamagedTools")
        
        
        let miscellaneousSupply = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "MiscellaneousSupply")
        
        
        let surfaceCompletion = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "SurfaceCompletion")
        
        
        let sampleSupply = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "SampleSupply")
        
        
        let miscellaneous = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Miscellaneous")
        let installSupply = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "InstallSupply")
        
        let groutSupply = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "GroutSupply")
        
        
        let siteActivity = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "SiteActivity")
        
        let grout = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Grout")
        let abondon = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Abandon")
        
        let inclinometer = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Inclinometer")
        let holeCompletion = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "HoleCompletion")
        
        let installation = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Installation")
        
        
        if installation.count > 0 {
            
            self.updateProjectsIdsWithTables(arrayIds: arrayIds, tables: "Installation", pids: "projectid")
        }
        
        
        
        if holeCompletion.count > 0 {
             self.updateProjectsIdsWithTables(arrayIds: arrayIds, tables: "HoleCompletion", pids: "projectid")
            }
        
        
        if inclinometer.count > 0 {
             self.updateProjectsIdsWithTables(arrayIds: arrayIds, tables: "Inclinometer", pids: "projectid")
            
        }
        
        if abondon.count > 0 {
          
            self.updateProjectsIdsWithTables(arrayIds: arrayIds, tables: "Abandon", pids: "projectid")
        }
        
        if grout.count > 0 {
            self.updateProjectsIdsWithTables(arrayIds: arrayIds, tables: "Grout", pids: "projectid")
        }
        
        if offlineTravels.count > 0 {
          
             self.updateProjectsIdsWithTables(arrayIds: arrayIds, tables: "Travel", pids: "projectid")
        }
        
        if offlineRentals.count > 0 {
           
             self.updateProjectsIdsWithTables(arrayIds: arrayIds, tables: "Rental", pids: "projectid")
        }
        
        if offlineDrill.count > 0 {
         
            self.updateProjectsIdsWithTables(arrayIds: arrayIds, tables: "Drill", pids: "projectid")
            
        }
        
        if damagedTools.count > 0 {
    
             self.updateProjectsIdsWithTables(arrayIds: arrayIds, tables: "DamagedTools", pids: "projectid")
        }
        
        if miscellaneousSupply.count > 0 {
        
            self.updateProjectsIdsWithTables(arrayIds: arrayIds, tables: "MiscellaneousSupply", pids: "projectid")
            
        }
        
        
        if sampleSupply.count > 0 {
    
            self.updateProjectsIdsWithTables(arrayIds: arrayIds, tables: "SampleSupply", pids: "projectid")
            
            
        }
        
        if surfaceCompletion.count > 0 {
            
             self.updateProjectsIdsWithTables(arrayIds: arrayIds, tables: "SurfaceCompletion", pids: "projectid")
            
        }
        
        
        if miscellaneous.count > 0 {

            
            self.updateProjectsIdsWithTables(arrayIds: arrayIds, tables: "Miscellaneous", pids: "projectid")
            
        }
        
        if installSupply.count > 0 {
          
            self.updateProjectsIdsWithTables(arrayIds: arrayIds, tables: "InstallSupply", pids: "projectid")
            
        }
        
        if groutSupply.count > 0 {
 
            self.updateProjectsIdsWithTables(arrayIds: arrayIds, tables: "GroutSupply", pids: "projectid")
        }
        
        
        if siteActivity.count > 0 {
             self.updateProjectsIdsWithTables(arrayIds: arrayIds, tables: "SiteActivity", pids: "projectid")
        }
        
        
        SyncData.sharedInstance.self.dataToSyncWithoutProjects()
    }
    
    
}
