//
//  SyncData.swift
//  AuthenticDrilling
//
//  Created by goyal on 12/02/18.
//  Copyright © 2018 Avatar Singh. All rights reserved.
//

import UIKit
import  CoreData

class SyncData: NSObject {
  var sessions: [NSManagedObject] = []
    var arrOfflineProjects = [[String : Any?]]()
    //MARK:- Get Offline Projects
    
      static let sharedInstance = SyncData()
    
    func syncData(){
        self.getOfflineProjects()
    }
   
    func getOfflineProjects() {
        
        self.arrOfflineProjects.removeAll()
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Projects")
           fetchRequest.predicate = NSPredicate(format: "isOffline = %@", "1")
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            for session in sessions{
                
                var dict = ["trailer2" :  session.value(forKey: "trailer2"),
                            "client_job_number" : session.value(forKey: "client_job_number"),
                            "support_truck2" : session.value(forKey: "support_truck2"),
                            "city_name" : session.value(forKey: "city_name"),
                            "crew_helpers" : session.value(forKey: "crew_helpers"),
                            "rig" : session.value(forKey: "rig"),
                            "crew_helpers2" : session.value(forKey: "crew_helpers2"),
                            "ADI_job_number" : session.value(forKey: "ADI_job_number".lowercased()),
                            "site_address" : session.value(forKey: "site_address"),
                            "trailer1" : session.value(forKey: "trailer1"),
                            "project_name" : session.value(forKey: "project_name"),
                            "support_truk1" : session.value(forKey: "support_truk1"),
                        
                            "client_name" : session.value(forKey: "client_name"),
                            "perdiems" : session.value(forKey: "perdiems"),
                            "userid" : session.value(forKey: "userid"),
                            "injuries" : session.value(forKey: "injuries"),
                            "crew_driller_name" : session.value(forKey: "crew_driller_name"),
                            "start_date" : session.value(forKey: "start_date"),
                     "client_rep" : session.value(forKey: "client_rep"),
                       "isLocalAddEdit": session.value(forKey: "isLocalAddEdit")]
                
               
                if session.value(forKey: "isLocalAddEdit") as? Bool == true {
                    dict.updateValue(session.value(forKey: "pid"), forKey: "localid")
                    dict.updateValue("create", forKey: "perform")
                }
                else{
                if  session.value(forKey: "edit") as? Bool == true {
                  dict.updateValue(session.value(forKey: "pid"), forKey: "projectid")
                 dict.updateValue("edit", forKey: "perform")
                    }
                else{
                    dict.updateValue(session.value(forKey: "pid"), forKey: "localid")
                    dict.updateValue("create", forKey: "perform")
                   }
                }
                self.arrOfflineProjects.append(dict)
           }
            
            if self.arrOfflineProjects.count > 0 {
                
                appDelegates.showAlertWithAction(isProjectsEditAdd: true)
             }
            else{
                self.checkOfflineData()
            }
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    
    func checkOfflineData(){
        
         let offlineTravels = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Travel")
         let offlineRentals = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Rental")
        
           let offlineDrill = ProjectsOffline.sharedInstance.getOfflineDrillTablesData(tables: "Drill")
        
         let damagedTools = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "DamagedTools")
        
        
        let miscellaneousSupply = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "MiscellaneousSupply")
        
         let sampleSupply = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "SampleSupply")
        
         let surfaceCompletion = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "SurfaceCompletion")
        
        let installSupply = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "InstallSupply")
        
         let groutSupply = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "GroutSupply")
        
        
         let siteActivity = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "SiteActivity")
        
        let grout = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Grout")
        
        let abondon = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Abandon")
        
        
         let inclinometer = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Inclinometer")
        
        let holeCompletion = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "HoleCompletion")
        
        
         let installation = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Installation")
        
        if offlineTravels.count > 0 || offlineRentals.count > 0 || offlineDrill.count > 0 || damagedTools.count > 0 || miscellaneousSupply.count > 0 || sampleSupply.count > 0 || surfaceCompletion.count > 0 || installSupply.count > 0 || groutSupply.count > 0 || siteActivity.count > 0 || grout.count > 0 || abondon.count > 0 || inclinometer.count > 0 || holeCompletion.count > 0 || installation.count > 0{
           appDelegates.showAlertWithAction(isProjectsEditAdd: false)
        }
        }
    
    func dataToSync(){
        
        let param : Parameters = ["action" : "create_project","data" :  self.arrOfflineProjects]
        
        self.createProjectApi(parameter: param)
    }

    func dataToSyncWithoutProjects(){
        let offlineTravels = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Travel")
        

        let offlineRentals = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Rental")
        
        let offlineDrill = ProjectsOffline.sharedInstance.getOfflineDrillTablesData(tables: "Drill")
        
        
        let damagedTools = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "DamagedTools")
        
        
        let miscellaneousSupply = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "MiscellaneousSupply")
        
       
        let surfaceCompletion = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "SurfaceCompletion")
        
        
        let sampleSupply = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "SampleSupply")
        
        
        let miscellaneous = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Miscellaneous")
         let installSupply = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "InstallSupply")
        
         let groutSupply = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "GroutSupply")
        
        
        let siteActivity = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "SiteActivity")
        
          let grout = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Grout")
         let abondon = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Abandon")
        
          let inclinometer = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Inclinometer")
        let holeCompletion = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "HoleCompletion")
        
        let installation = ProjectsOffline.sharedInstance.getOfflineTablesData(tables: "Installation")
        
        
        if installation.count > 0 {
            
             let parameters : Parameters = ["action": "hc_installation" ,"data" :  installation ]
             ProjectsOffline.sharedInstance.syncOfflineApi(parameter: parameters, tables: "Installation", urlStr: holeCompletionOfflineUrl)
        }
        
        
        
        if holeCompletion.count > 0 {
            let parameters : Parameters = ["action": "hole_completion_leave_openOrbackFill" ,"data" :  holeCompletion ]
             ProjectsOffline.sharedInstance.syncOfflineApi(parameter: parameters, tables: "HoleCompletion", urlStr: holeCompletionOfflineUrl)
            
        }
        
        
        if inclinometer.count > 0 {
              let parameters : Parameters = ["action": "ac_install_inclinometer" ,"data" :  inclinometer ]
            
             ProjectsOffline.sharedInstance.syncOfflineApi(parameter: parameters, tables: "Inclinometer", urlStr: consumableUrlOffline)
        }
        
        if abondon.count > 0 {
            
            let parameters : Parameters = ["action": "hc_abandon" ,"data" :  abondon ]
            
            ProjectsOffline.sharedInstance.syncOfflineApi(parameter: parameters, tables: "Abandon", urlStr: holeCompletionOfflineUrl)
            
        }
        
        if grout.count > 0 {
             let parameters : Parameters = ["action": "hc_grout" ,"data" :  grout ]
            
            ProjectsOffline.sharedInstance.syncOfflineApi(parameter: parameters, tables: "Grout", urlStr: holeCompletionOfflineUrl)
        }
        
        if offlineTravels.count > 0 {
            let parameters : Parameters = ["action": "travel_category" ,"data" :  offlineTravels ]
            
            ProjectsOffline.sharedInstance.syncOfflineApi(parameter: parameters, tables: "Travel", urlStr: projectUrlOffline)
        }
        
        if offlineRentals.count > 0 {
             let parameters : Parameters = ["action": "rental_category" ,"data" :  offlineRentals ]
            ProjectsOffline.sharedInstance.syncOfflineApi(parameter: parameters, tables: "Rental", urlStr: projectUrlOffline)
        }
        
        if offlineDrill.count > 0 {
            
              let parameters : Parameters = ["action": "drill_category" ,"data" :  offlineDrill ]
            ProjectsOffline.sharedInstance.syncOfflineApi(parameter: parameters, tables: "Drill", urlStr: projectUrlOffline)
        }
        
        if damagedTools.count > 0 {
              let parameters : Parameters = ["action": "ac_damaged_tool" ,"data" :  damagedTools ]
            ProjectsOffline.sharedInstance.syncOfflineApi(parameter: parameters, tables: "DamagedTools", urlStr: consumableUrlOffline)
        }
        
        if miscellaneousSupply.count > 0 {
            
            
             let parameters : Parameters = ["action": "ac_miscellaneous" ,"data" :  miscellaneousSupply ]
            
            ProjectsOffline.sharedInstance.syncOfflineApi(parameter: parameters, tables: "MiscellaneousSupply", urlStr: consumableUrlOffline)
            
        }
        
        
        if sampleSupply.count > 0 {
            
            let parameters : Parameters = ["action": "ac_sampling_supplies" ,"data" :  sampleSupply ]
            ProjectsOffline.sharedInstance.syncOfflineApi(parameter: parameters, tables: "SampleSupply", urlStr: consumableUrlOffline)
            
            }
        
        if surfaceCompletion.count > 0 {
            let parameters : Parameters = ["action": "ac_surface_completion" ,"data" :  surfaceCompletion ]
             ProjectsOffline.sharedInstance.syncOfflineApi(parameter: parameters, tables: "SurfaceCompletion", urlStr: consumableUrlOffline)
        }
        
        
        if miscellaneous.count > 0 {
            
                   let parameters : Parameters = ["action": "miscellaneous_category" ,"data" :  miscellaneous ]
            
            ProjectsOffline.sharedInstance.syncOfflineApi(parameter: parameters, tables: "Miscellaneous", urlStr: projectUrlOffline)
            
        }
        
        if installSupply.count > 0 {
             let parameters : Parameters = ["action": "ac_install_supplies" ,"data" :  installSupply ]
            
            
             ProjectsOffline.sharedInstance.syncOfflineApi(parameter: parameters, tables: "InstallSupply", urlStr: consumableUrlOffline)
            
        }
        
        if groutSupply.count > 0 {
             let parameters : Parameters = ["action": "ac_grout_supplies" ,"data" :  groutSupply ]
           ProjectsOffline.sharedInstance.syncOfflineApi(parameter: parameters, tables: "GroutSupply", urlStr: consumableUrlOffline)
            
        }
        
        
        if siteActivity.count > 0 {
             let parameters : Parameters = ["action": "siteactivity_category" ,"data" :  siteActivity ]
             ProjectsOffline.sharedInstance.syncOfflineApi(parameter: parameters, tables: "SiteActivity", urlStr: projectUrlOffline)
        }
        
    }
    
    
    
    func createProjectApi(parameter :Parameters){
        print(parameter)
         appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(projectUrlOffline, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
             appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
            
            self.loadProducts(arrayIds: json["response"]["ids"].arrayValue)
            
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()

            print(error.localizedDescription)
        }
        
        
    }
    
    
    
    
    
    
    func deleteAllRecords() {
        let managedContext = CoreDataStack.managedObjectContext
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Projects")
        
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
            print ("There was an error")
        }
    }
    //MARK:- Load Projects function
    func  loadProducts(arrayIds :[JSON]){
        
        let parameter  : Parameters  = ["action": "fetch_project", "userid": standard.value(forKey: "id")!]
        print(parameter)
        
        appDelegates.window?.rootViewController?.view.startIndicator()
        
        ApiManager.requestPOSTURL(projectUrl, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            
            self.deleteAllRecords()
            let projects = ProjectDA.init(json: json)
            
            guard projects.response?.result == "201" else{
                return
            }
            
            let array = projects.response?.data ??  [ProjectData]()
            
            
            for i in 0..<array.count {
                
                self.saveCoredata(saveObject: array[i], isOffline: false, edit: false)
            }
            
            
           UpdateProjectsIDS.sharedInstance.updateProjectsIds(arrayIds: arrayIds)
            
            
           let arrayIdsValue = arrayIds.filter({$0["oldid"].stringValue == projectId}).map({$0["serverid"].stringValue})
            if arrayIdsValue.count > 0 {
                projectId = arrayIdsValue[0]
            }
            
          
           
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            
            print(error.localizedDescription)
        }
    }
    
    
    
    //MARK:- Save Core Data Function
    func saveCoredata(saveObject : ProjectData, isOffline : Bool,edit: Bool){
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "Projects",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        session.setValue(saveObject.trailer2, forKey: "trailer2")
        session.setValue(saveObject.clientJobNumber, forKey: "client_job_number")
        session.setValue(saveObject.supportTruck2, forKey: "support_truck2")
        session.setValue(saveObject.pid, forKey: "pid")
        session.setValue(saveObject.cityName, forKey: "city_name")
        session.setValue(saveObject.crewHelpers, forKey: "crew_helpers")
        session.setValue(saveObject.rig, forKey: "rig")
        session.setValue(saveObject.crewHelpers2, forKey: "crew_helpers2")
        session.setValue(saveObject.aDIJobNumber, forKey: "ADI_job_number".lowercased())
        session.setValue(saveObject.siteAddress, forKey: "site_address")
        session.setValue(saveObject.trailer1, forKey: "trailer1")
        session.setValue(saveObject.projectName, forKey: "project_name")
        session.setValue(saveObject.supportTruk1, forKey: "support_truk1")
        session.setValue(saveObject.addOn, forKey: "add_on")
        session.setValue(saveObject.modifyOn, forKey: "modify_on")
        session.setValue(saveObject.clientName, forKey: "client_name")
        session.setValue(saveObject.perdiems, forKey: "perdiems")
        session.setValue(saveObject.userid, forKey: "userid")
        session.setValue(saveObject.injuries, forKey: "injuries")
        session.setValue(saveObject.crewDrillerName, forKey: "crew_driller_name")
        session.setValue(saveObject.startDate, forKey: "start_date")
        session.setValue(saveObject.clientRep, forKey: "client_rep")
        session.setValue(isOffline, forKey: "isOffline")
        session.setValue(edit, forKey: "edit")
        CoreDataStack.saveContext()
    }
    
}
