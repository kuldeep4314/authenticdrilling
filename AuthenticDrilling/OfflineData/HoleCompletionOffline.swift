//
//  HoleCompletionOffline.swift
//  AuthenticDrilling
//
//  Created by goyal on 20/02/18.
//  Copyright © 2018 Avatar Singh. All rights reserved.
//

import UIKit
import CoreData
class HoleCompletionOffline: NSObject {
var sessions: [NSManagedObject] = []
    
    static let sharedInstace = HoleCompletionOffline()
    
    
    func getOfflineHoleNumber(prjectId : String , date : String) -> [String] {
        
       var holeArray = [String]()
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "HoleCompletion")
      
         fetchRequest.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@)",  prjectId , date)
        
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            
            for session in sessions{
                
                let hole = session.value(forKey: "holenumber") as! String
            
                 holeArray.append(hole)
            }
            
            return holeArray
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
              return holeArray
        }
    }
    
    
    func getOfflineHoleDictionary(arrayHole : [String],prjectId : String , date : String) -> [String : Any] {
        
        var mainDict  = [String : Any]()
        mainDict.updateValue(arrayHole, forKey: "holenumber")
        
        var subCatDict = [String : Any]()
        
        
        for i in 0..<arrayHole.count {
        
            var optionDict = [String : Any]()
            var installWell = [String : Any]()
            let groutDict  = getGroutDictWithHole(holeNumber: arrayHole[i], prjectId: prjectId, date: date, tables: "Grout")
            let abandonDict  = getGroutDictWithHole(holeNumber: arrayHole[i], prjectId: prjectId, date: date, tables: "Abandon")
            
            let installsDict  = getGroutDictWithHole(holeNumber: arrayHole[i], prjectId: prjectId, date: date, tables: "Installation")
            
            let inclinometerDict  = getGroutDictWithHole(holeNumber: arrayHole[i], prjectId: prjectId, date: date, tables: "Inclinometer")
            
            let holeCompletionDict  = getGroutDictWithHole(holeNumber: arrayHole[i], prjectId: prjectId, date: date, tables: "HoleCompletion")
            
            
              optionDict.updateValue(groutDict, forKey: "grout")
              optionDict.updateValue(abandonDict, forKey: "abandon")
              optionDict.updateValue(JSON(holeCompletionDict)["leavehole"].stringValue, forKey: "Leave Hole")
              optionDict.updateValue(JSON(holeCompletionDict)["backfill"].stringValue, forKey: "Backfill")
            
              installWell  = installsDict
              installWell.updateValue(inclinometerDict, forKey: "inclinometer")
             optionDict.updateValue(installWell, forKey: "installation well")
            
            subCatDict.updateValue(optionDict, forKey: arrayHole[i])
    
        }
          mainDict.updateValue(subCatDict, forKey: "main category")
        return mainDict
    }
    
    //MARK:- Get Core Data Function
    func getGroutDictWithHole(holeNumber : String,prjectId : String , date : String,tables : String)-> [String : Any] {
        
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName:tables)
        
        fetchRequest.predicate =  NSPredicate(format: "(projectid = %@) AND (date = %@) AND (holenumber = %@)",  prjectId , date,holeNumber)
        //3
        do {
            sessions = try managedContext.fetch(fetchRequest)
            var dict = [String : Any]()
            for session in sessions{
              dict = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
               
                
            }
              return dict

        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
            return [String : Any]()
        }
    }
    
    
    
   

    
    
}
