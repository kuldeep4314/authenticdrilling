//
//  ProjectsOffline.swift
//  AuthenticDrilling
//
//  Created by goyal on 08/02/18.
//  Copyright © 2018 Avatar Singh. All rights reserved.
//

import UIKit
import CoreData
class ProjectsOffline: NSObject {
    
    static let sharedInstance = ProjectsOffline()
var sessions: [NSManagedObject] = []
    func saveCoredata(isOffline : Bool , strDate : String ,  projectID : String){
        let managedContext = CoreDataStack.managedObjectContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "WorkingDates",
                                                in: managedContext)!
        let session = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        session.setValue(strDate, forKey: "date")
        session.setValue(projectID, forKey: "projectid")
        session.setValue("\(standard.value(forKey: "id")!)", forKey: "userid")
        
        session.setValue(isOffline, forKey: "isOffline")
        CoreDataStack.saveContext()
    }
    
    //MARK:- Get Offline Projects
    func getOfflineDates(isOffline : Bool , strDate : String ,  projectID : String){
      let managedContext =
            CoreDataStack.managedObjectContext
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "WorkingDates")
     fetchRequest.predicate =    NSPredicate(format: "(projectid = %@) AND (date = %@)", projectID , strDate)
        //3
        do {
           let  sessios = try managedContext.fetch(fetchRequest)
            
            if  sessios.count == 0 {
               self.saveCoredata(isOffline: isOffline, strDate: strDate, projectID: projectID)
            }
      } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
   
    //MARK:- Get Offline Projects
    func getOfflineTables(tables : String , strDate : String ,  projectID : String) -> Bool{
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: tables)
        fetchRequest.predicate =    NSPredicate(format: "(projectid = %@) AND (date = %@)", projectID , strDate)
        //3
        do {
            let  sessios = try managedContext.fetch(fetchRequest)
            
            if  sessios.count == 0 {
            return false
            }
    else{
     return true
    }
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
             return false
        }
    }

    
    //MARK:- Get Offline Projects
    func getOfflineTablesWithHolenumber(tables : String , strDate : String ,  projectID : String, holenumber : String) -> Bool{
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: tables)
        fetchRequest.predicate =    NSPredicate(format: "(projectid = %@) AND (date = %@) AND (holenumber = %@)", projectID , strDate,holenumber)
        //3
        do {
            let  sessios = try managedContext.fetch(fetchRequest)
            
            if  sessios.count == 0 {
                return false
            }
            else{
                return true
            }
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
            return false
        }
    }
      //MARK:- GetOfflineTablesData Functions
    func getOfflineTablesData(tables : String) -> [[String : Any]] {
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: tables)
        fetchRequest.predicate =    NSPredicate(format: "isOffline = %@", "1")
        //3
        do {
            let  sessios = try managedContext.fetch(fetchRequest)
            
            var arrOfDictionary = [[String : Any]]()
            
            for session in sessios{
                var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                
                dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
            
                 arrOfDictionary.append(dictionary)
            }
            return arrOfDictionary
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
            return [[String : Any]]()
        }
    }
    
    
    //MARK:- GetOfflineDrillTablesData
    func getOfflineDrillTablesData(tables : String) -> [[String : Any]] {
        let managedContext =
            CoreDataStack.managedObjectContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: tables)
        fetchRequest.predicate =    NSPredicate(format: "isOffline = %@", "1")
        //3
        do {
            let  sessios = try managedContext.fetch(fetchRequest)
            
            var arrOfDictionary = [[String : Any]]()
            for session in sessios{
                var dictionary = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "dataDetails") as! Data) as? [String : Any] ?? [String : Any]()
                dictionary.updateValue(session.value(forKey: "date") ?? "", forKey: "date")
                dictionary.updateValue(session.value(forKey: "projectid") ?? "", forKey: "projectid")
                
                dictionary.updateValue("\(standard.value(forKey: "id")!)", forKey: "userid")
                
                let arraySampleData = NSKeyedUnarchiver.unarchiveObject(with:  session.value(forKey: "sample") as! Data) as? [[String : Any]] ?? [[String : Any]]()
                
                 dictionary.updateValue(arraySampleData, forKey: "sample_data")
                
                arrOfDictionary.append(dictionary)
            }
            return arrOfDictionary
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
            return [[String : Any]]()
        }
    }
    
    
    //MARK:- Update Core Data
    func updateCoredataTables(tables: String){
        
        let managedContext = CoreDataStack.managedObjectContext
        let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: tables)
       
        do {
            let results = try managedContext.fetch(updateFetch) as? [NSManagedObject]
            
            if results != nil{
              for i in 0..<results!.count { // Atleast one was returned
                let session = results![i]
                session.setValue(false, forKey: "isOffline")
                session.setValue(false, forKey: "edit")
                
            }
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try managedContext.save()
            
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
        
    }
    
    //MARK: - SyncOfflineMultipleApi
    func syncOfflineApi(parameter :Parameters , tables : String ,urlStr  : String){
        print(parameter)
        print(JSON(parameter))
        appDelegates.window?.rootViewController?.view.startIndicator()
        ApiManager.requestPOSTURL(urlStr, params: parameter as [String : AnyObject], headers: nil, success: { (json) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            print(json)
            guard json["response"]["result"].string == "201" else{
                
                return
            }
        
            ProjectsOffline.sharedInstance.updateCoredataTables(tables: tables)
            
           
        }) { (error) in
            appDelegates.window?.rootViewController?.view.stopIndicator()
            if error.localizedDescription != "The Internet connection appears to be offline."{
                appDelegates.window?.rootViewController?.showAlert(messageStr: error.localizedDescription)
            }
            print(error.localizedDescription)
        }
    }
    
    
    
    
}
